<?php
session_start();
if(isset($_SESSION['susu'])){
	require_once("util/utilerias.php");
}else{
    header("Location: index.php");
}
    $obj = new Utilerias;
?>       

<h6 style="color:#0054a4;text-shadow: 5px 5px 5px #aaa; padding:20px 5px;"> Alta de Certificado(s) </h6>

<div class="row">
  <div class="col-9">
  	<?php
	  $obj->cmbCampus();
	?>	      
    <div class="row">
      <div class="col-9">
      	<br />
            <input type="date" id="finicio" name="finicio" step="1" min="2018-01-01" max="2030-12-31" placeholder="Fecha Inicial">
            <input type="date" id="ffin" name="ffin" step="1" min="2018-01-01" max="2030-12-31" placeholder="Fecha Final">
            <button class="btn btn-primary" align="right" onclick="javascript:cargaContenido2('GenXML');"> Generar XML </button>        
      </div>
    </div>
  </div>
  <div class="col-3" style=" text-align: -webkit-center; ">
    <button class='btn btn-lg ' style="background-color: #0054a4;border-color: #0054a4;border-width: 2px;color: white;" onclick="javascript:cargaContenido2('dataTableQ');"> 
        <div style='text-align:center;'><i class="fa fa-search fa-4"></i></div>
    </button>
    <br />
    <button type="button" class="btn btn-primary" style="margin-top: 5px;" data-toggle="modal" data-target=".bd-example-modal-lg">Valida información</button>        
  </div>
</div>
<hr>















<?php
    /*
    $obj = new Utilerias;
    //$campos = "Campo1,Campo2,,Campo3
    $cabeceros = "#,V.P,V.C,Campus,F.Control,Carrera,No.Control,No.Alumno,A.Paterno,A.Materno,T";
    $campos = "RW,vacio,vacio,ACADEMIC_SESSION,folioControl,nombreCarrera,numeroControl,nombre3,primerApellido4,segundoApellido5,tipoCertificacion";		

    $query = str_replace("VARIABLE=CAMPUS", "ORIENTE", CERTIFICADOSALTAS);
    //$MCA = str_replace("VARIABLE=PEOPLE_CODE_ID", "P000026007", $MCA);
    $query = str_replace("VARIABLE=TIPO_CERTIFICADO", "T", $query);
    $query = str_replace("VARIABLE=ANHO", "2018", $query);
    $query = str_replace("VARIABLE=FINICIAL", "2018-02-01", $query);
    $query = str_replace("VARIABLE=FINAL", "2018-12-31", $query);

    //Cabeceros, Campos, Query		
    $obj->dTable($cabeceros,$campos,$query);
    */    
?>  




<div id="fsModal"
     class="modal animated bounceIn"
     tabindex="-1"
     role ="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true" style="width:75%;">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-secondary"
                data-dismiss="modal">
            close
        </button>
        <input type="submit" value="Validado plantel" />
    </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg"
     tabindex="-1"
     role ="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true"
      >
      
    <div class="modal-content">
          <div class="modal-header">
            <h6>Alumno: Nombre del Alumno</h6>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
	      </div>
          <hr />  
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#GENERAL">GENERAL</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#IPES">IPES</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#RESPONSABLE">RESPONSABLE</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#RVOE">RVOE</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#CARRERA">CARRERA</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#ALUMNO">ALUMNO</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#EXPEDICION">EXPEDICIÓN</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#ASIGNATURAS">ASIGNATURAS</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#DOCUMENTOS">DOCUMENTOS</a></li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <div id="GENERAL" class="container tab-pane active"><br>
                <p class="navbar-text">Tip. Certificado</p>
                <input id="Tip. Certificado" type="text" autocomplete="false" disabled/>
                
                <p class="navbar-text">Fol. Control</p>                            
                <input id="Fol. Control" type="text" autocomplete="false" disabled />                                                 
            </div>
            <div id="IPES" class="container tab-pane fade"><br>
                    <p class='navbar-text'>Nom. Institucíon</p>
                    <input id='Nom. Institucíon' type='text' autocomplete='false' disabled />
                    
                    <p class='navbar-text'>Campus</p>
                    <input id='Campus' type='text' autocomplete='false' disabled />
                    
                    <p class='navbar-text'>Ent. Federativa</p>
                    <input id='Ent. Federativa' type='text' autocomplete='false' disabled />
            </div>
            <div id="RESPONSABLE" class="container tab-pane fade"><br>
                <p class='navbar-text'>Nombre</p>
                <input id='Nombre' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>A. Paterno</p>
                <input id='A. Paterno' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>A. Materno</p>
                <input id='A. Materno' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Cargo</p>
                <input id='Cargo' type='text' autocomplete='false' disabled />
            </div>
            <div id="RVOE" class="container tab-pane fade"><br>
                <p class='navbar-text'>Número</p>
                <input id='Número' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Fec. Expedición </p>
                <input id='Fec. Expedición ' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Clve. Plan</p>
                <input id='Clve. Plan' type='text' autocomplete='false' disabled />
            </div>
            <div id="CARRERA" class="container tab-pane fade"><br>
                <p class='navbar-text'>Nom. Carrera</p>
                <input id='Nom. Carrera' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Tip. Periodo</p>
                <input id='Tip. Periodo' type='text' autocomplete='false' disabled />
            </div>
            <div id="ALUMNO" class="container tab-pane fade"><br>
                <p class='navbar-text'>ID</p>
                <input id='IdAlumno' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>CURP</p>
                <input id='CURP' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Nombre</p>
                <input id='Nombre' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>A. Paterno</p>
                <input id='A. Paterno' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>A. Materno</p>
                <input id='A. Materno' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Genero</p>
                <input id='Id. Genero' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Fec. Nacimiento</p>
                <input id='Fec. Nacimiento' type='text' autocomplete='false' disabled />
            </div>
            <div id="EXPEDICION" class="container tab-pane fade"><br>
                <p class='navbar-text'>Tip. Certificación</p>
                <input id='Tip. Certificación' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Fecha</p>
                <input id='Fecha' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Lugar Espedición</p>
                <input id='Lugar Espedición' type='text' autocomplete='false' disabled />
            </div>
            <div id="ASIGNATURAS" class="container tab-pane fade"><br>
                    <div class="tabla">
                        <table id="grid" class="table table-hover dt-responsive nowrap ">
                            <thead>
                                <tr>
                                    <th>Clave</th>
                                    <th>Nombre</th>
                                    <th>Ciclo</th>
                                    <th>Calificación</th>
                                    <th>Observaciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr></tr>

                            </tbody>
                        </table>
                    </div>
            </div>                          
            <div id="DOCUMENTOS" class="container tab-pane fade"><br>
                <table id="grid" class="table table-hover dt-responsive nowrap ">
                    <thead>
                        <tr>
                            <th>Acta de Nacimiento</th>
                            <th>Historial Academico</th>
                            <th>Certificado Antecedente</th>
                            <th>Oficio de Validacion Antecedente Academico</th>
                            <th>CURP 200%</th>
                            <th>Equivalencia o Revalidación Estudios (Opcional)</th>
                            <th>Carta Compromiso</th>
                            <th>Otros</th>
                            <th>Otros</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                        </tr>
                        <tr></tr>
                    </tbody>
                </table>
            </div>                                                                                                                   
          </div>
          <div class="modal-footer">                    
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-success">Validado</button>
          </div>
    </div>
  </div>
</div>


<style>

body .modal-content{
  width: 130%!important;
  height: 550px;;
  margin-top: 5%!important;
  margin-left: -15%!important;
}

</style>
