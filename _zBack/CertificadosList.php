<?php
	require_once("util/utilerias.php");
    $obj = new Utilerias;
	$obj->CnnBD();
	$CAMPUS = "XOLA";
	$ANHO = "2018";
	$FINICIO = "2018-01-01";
	$FFINAL = "2018-11-30";
	
    $query = str_replace("VARIABLE=CAMPUS", $CAMPUS, CERTIFICADOSALTAS);
    $query = str_replace("VARIABLE=TIPO_CERTIFICADO", "T", $query);
    $query = str_replace("VARIABLE=ANHO", $ANHO, $query);
    $query = str_replace("VARIABLE=FINICIAL", $FINICIO, $query);
    $query = str_replace("VARIABLE=FFINAL", $FFINAL, $query);
	$query = str_replace("VARIABLE=NUMCONTROL", "", $query);
	//echo "Query <br>".$query;	
?>
<!--	<link rel='stylesheet' id='compiled.css-css'  href='./css/compiled-4.5.15.min.css' type='text/css' media='all' />-->
<!--    <script type='text/javascript' src='./js/compiled.0.min.js?ver=4.5.15'></script>-->
<!--	<script type='text/javascript' src='./js/tablax2.js'></script>-->
<!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
	<script>
    // Datatable vertical dynamic height
    $(document).ready(function () {
		$('#dtDynamicVerticalScrollExample').DataTable({
			"scrollY": "45vh",
			"scrollCollapse": true,
			"paging": false
		});
		$('.dataTables_length').addClass('bs-select');

		$(".btn1").click(function() {
			//valores obtendra el dato del td por posciones [0]
			var vNumControl = $(this).parents("tr").find("td")[2].innerHTML;
			$("#ViewInfoAlumno").text("");
			
			var campus = "XOLA";
			var anho = "2018";
			var finicio = "2018-01-01";
			var ffinal = "2018-11-30";
			var vNumControl = vNumControl;
			var tipoCert = "T";

            $("#infoModalBody").empty();
			$("#ViewInfoAlumno").load('infoAlumno.php?campus='+campus
			+'&anho='+anho
			+'&finicio='+finicio
			+'&ffinal='+ffinal
			+'&vNumControl='+vNumControl
			+'&tipoCert='+tipoCert);
			//console.log(valores);
			//alert(valores);
		});
		$(".btn2").click(function() {
			//valores obtendra el dato del td por posciones [0]
			var valores = $(this).parents("tr").find("td")[3].innerHTML;
			console.log(valores);
			alert(valores);
		});		
    });
    </script>
    
    
    <div class="row">
        <div class="col-9">
	        <?php echo $obj->cmbCampus(); ?>	      
            <div class="row">
                <div class="col-9">
                    <br />
                    <input type="date" id="finicio" name="finicio" step="1" min="2001-01-01" max="2030-12-31" placeholder="Fecha Inicial">
                    <input type="date" id="ffin" name="ffin" step="1" min="2001-01-01" max="2030-12-31" placeholder="Fecha Final">
                </div>
            </div>
		</div>
    </div>
    <hr>
    <?php
		$rQuery = $obj->xQuery($query);
	?>
    <section id="datatable-vertical-dynamic-height">
      <section>
        <table id="dtDynamicVerticalScrollExample" class="table table-striped table-bordered table-sm" cellspacing="0"
          width="100%">
          <thead>
            <tr>
              <th class="col-xs-2">#</th>
              <th class="col-xs-2"> &nbsp; &nbsp;</th>
              <th class="col-xs-2">Id Alumno</th>
              <th class="col-xs-2">Nombre</th>
              <th class="col-xs-2">A.Paterno</th>
              <th class="col-xs-2">A.Materno</th>
              <th class="col-xs-2">Validar Info</th>
              <th class="col-xs-2"></th>
              <th class="col-xs-2">Estatus</th>            	
            </tr>
          </thead>
          <tbody>
			<?php		 
                  while ($data = sqlsrv_fetch_array($rQuery)) {    
                      echo '
                          <tr>
                            <td >'.$data["RW"].'</td>
							<td >
								<div class="form-check form-check-inline">
								  <input type="checkbox" class="form-check-input" id="materialInline'.$data["RW"].'" style="visibility:hidden">
								  <label class="form-check-label" for="materialInline'.$data["RW"].'"></label>
								</div>
							</td>
                            <td >P'.$data["numeroControl"].'</td>
                            <td >'.utf8_encode($data["nombre3"]).'</td>
                            <td >'.utf8_encode($data["primerApellido4"]).'</td>
                            <td >'.utf8_encode($data["segundoApellido5"]).'</td>
                            <td ><a href="#" id="'.$data["numeroControl"].'" title="'.$data["numeroControl"].'">Info Alumno</a></td>
							<td >
								<!--<button class="btn btn-outline-info btn2"><i class="fa fa-id-card-o"></i></button>-->
								<button class="btn btn-outline-info btn1" value='.$data["numeroControl"].'><i class="fa fa-id-card-o"></i></button>
							</td>
                            <td class="table-light">Prospecto</td>
                          </tr>
                      ';    
                  }
            ?>
          </tbody>
          <tfoot>
            <tr>
              <th>#</th>
              <th></th>
              <th>Id Alumno</th>
              <th>Nombre</th>
              <th>A.Paterno</th>
              <th>A.Materno</th>
              <th>Validar Info</th>
              <th>Subir doc.</th>
              <th>Estatus</th> 
            </tr>
          </tfoot>
        </table>
     </section>
    </section>

<div id='ViewInfoAlumno'>
</div>


