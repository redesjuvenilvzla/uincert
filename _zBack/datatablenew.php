<?php
	require_once("util/utilerias.php");
    $obj = new Utilerias;
	$obj->CnnBD();	
	$CAMPUS = "XOLA";
	$ANHO = "2018";
	$FINICIO = "2018-01-01";
	$FFINAL = "2018-11-30";
	
    $query = str_replace("VARIABLE=CAMPUS", $CAMPUS, CERTIFICADOSALTAS);
    $query = str_replace("VARIABLE=TIPO_CERTIFICADO", "T", $query);
    $query = str_replace("VARIABLE=ANHO", $ANHO, $query);
    $query = str_replace("VARIABLE=FINICIAL", $FINICIO, $query);
    $query = str_replace("VARIABLE=FFINAL", $FFINAL, $query);
	$query = str_replace("VARIABLE=NUMCONTROL", "", $query);
	//echo "Query <br>".$query;	
	$rQuery = $obj->xQuery($query);
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="./js/jquery-1.3.2.min.js" type="text/javascript"></script>	


<input id="fc" name="FolioControl" type="text" value="">
  <table class="table table-hover">
    <thead>
      <tr>
        <th class="col-xs-2">#</th>
        <th class="col-xs-2"><input type="checkbox" name="chkAll" value="All"></th>
        <th class="col-xs-2">Id Alumno</th>
        <th class="col-xs-2">Nombre</th>
        <th class="col-xs-2">A.Paterno</th>
        <th class="col-xs-2">A.Materno</th>
        <th class="col-xs-2">Validar Info</th>
        <th class="col-xs-2">Estatus</th>
      </tr>
    </thead>
<div class="container" style="width: 100%; height: 350px; overflow-y: scroll;">0

    <tbody class="my-tbody">
    <?php		 
      while ($data = sqlsrv_fetch_array($rQuery)) {    
		  echo '
			  <tr>
				<td >'.$data["RW"].'</td>
				<td ><input type="checkbox" name="chkAll" value="' .$data["numeroControl"]. '"></td>
				<td >'.$data["numeroControl"].'</td>
				<td >'.utf8_encode($data["nombre3"]).'</td>
				<td >'.utf8_encode($data["primerApellido4"]).'</td>
				<td >'.utf8_encode($data["segundoApellido5"]).'</td>
				<td ><a href="#" id="'.$data["numeroControl"].'" title="'.$data["numeroControl"].'">Info Alumno</a></td>
				<td class="table-light">Prospecto</td>
			  </tr>
		  ';    
	  }
	?>	
    </tbody>    
  </table>
</div>  

<div id='ViewInfoAlumno'>
</div>

<script>
$(document).ready(function(){
	$("a").click(function(){
		val = isNaN($(this).attr('title'));
		if (isNaN($(this).attr('title')) == false){
			if(document.getElementById("fc").value == $(this).attr('title')){
				//$("#ViewInfoAlumno").css("display","none");
				//$("#ViewInfoAlumno").text("");
			}else{
				//$("#ViewInfoAlumno").css("display","block");
				$("#ViewInfoAlumno").text("");
				$("#ViewInfoAlumno").load('infoAlumno.php');
			}		
			document.getElementById("fc").value = $(this).attr('title');				
		}
	});
});
function cancelar() {
	$("#ViewInfoAlumno").text("");
}

</script>
</head>
