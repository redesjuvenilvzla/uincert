<?php
    session_start();
    $SecureSection = false;
    require_once("util/utilerias.php");
    $obj = new Utilerias;
    $obj->CnnBD();
?>

<script>    
	$("#mainTable").dataTable( {
		destroy: true,
		retrieve:true,
		paging: false
	});
    $(document).on('click', '#searchCertificadosPlantel', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        formdata = new FormData(); 
        //campus = $("#cmbCampus option:selected").text();
        //rol = $("#txtRol").val();
        campus = $("#cmbCampus").val();


        xtipCert = $("#cmbTipCertificado option:selected").text();        
        if (xtipCert == "Ambos"){
            tipCert = "'T','P'";
        }else{
            if (xtipCert == "Total"){
                tipCert = "'T'";    
            }else{
                tipCert = "'P'";
            }
        }
        //alert(tipCert);
        finicial = $('#finicio').val();        
        ffinal = $('#ffin').val();        
        anho = parseInt(finicial.substr(0,4));
        //alert(campus);

        //alert(campus); //VERIFICAR POR QUE LA CONSULTA SE HACE DOBLE VEZ

        $('#searchCertificadosPlantel').unbind('click.new');
        formdata.append("campus", campus);
        formdata.append("tipCert", tipCert);
        formdata.append("anho", anho);
        formdata.append("finicial", finicial);
        formdata.append("ffinal", ffinal);
        jQuery.ajax({
            url: 'CertificadosPlantelList.php',
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            success: function (result) {
                $("#ListAlumnos" ).html(result);
            }
        });
        $("div").find("#ViewInfoAlumno").html("");
        //$("#ListAlumnos").load('CertificadosPlantelList.php?campus='+campus+'&finicial='+finicial+'&ffinal='+ffinal);
    });    
</script>
<div id="" class="">
<h2 class="text-primary"> PLANTEL </h2>
<hr>
<div class="row">
  <div class="col"><h6>Campus</h6></div>
  <div class="col"><h6>Tipo Certificado</h6></div>
  <div class="col"><h6>Fecha Inicial</h6></div>
  <div class="col"><h6>Fecha Final</h6></div>
  <div class="col"></div>
</div>
</div>
<div class="row">
  <div class="col"><?php echo $obj->cmbCampus(); ?></div>
  <div class="col"><?php echo $obj->cmbTipCert(); ?></div>
  <div class="col"><?php echo $obj->dpFecIni(); ?></div>
  <div class="col"><?php echo $obj->dpFecFin(); ?></div>
  <div class="col" align="left"><?php echo $obj->btnBuscar('CertificadosPlantel'); ?></div>
</div>        
<hr/>    
<div id='ListAlumnos'>
    <br /><br /><br /><br /><br />
</div>
<br /><br />
<div>
<div id="pills-tabContent">
<div class="col-md-12 left">
<div class="col-sm-12">
<div id='ViewInfoAlumno'>
    <br /><br /><br /><br /><br />
</div>
</div>
</div>
</div>
</div>
<br /><br />