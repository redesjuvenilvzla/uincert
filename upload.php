<?php
    $thisType       = $_POST['thisType'];
    $pathFile       = $_POST['pathFile'];
    $TipoDoc        = $_POST['tipoDoc'];
    $FolioControl   = $_POST['idAlumno'];
    $ThisCarrer     = $_POST['CveCarrera'];
    $FileNumber     = $_POST['FileNumber'];
    
    $SecureSection = true;
    require_once("util/utilerias.php");
    $obj = new Utilerias;
    $obj->CnnBD();
    
    if(!file_exists($pathFile)){mkdir($pathFile, 0777, true);}
    
    $TotalFiles = 0;
    foreach($_FILES as $key){
        if($key['error'] == UPLOAD_ERR_OK){
            $TotalFiles = $TotalFiles + 1;
            $name       = $key['name'];
            $temp       = $key['tmp_name'];
            $type       = $key['type'];
            $size       = ($key['size'] / 1000);
            $FileSize   = formatSizeUnits($key['size']);

            if($type === "application/pdf"){
                if($size > 2000){
                    echo "<script>";
                    echo "  alert('Unicamente esta permitida la carga de archivos con un peso no mayor a los 2 Mb, el archivo seleccionado pesa: ".$FileSize."');";
                    echo "</script>";
                    die();
                }else{
                    $DocName = $obj->sanear_Acents($name);
                    if($TipoDoc === "OtherFiles"){
                        $path_file = $pathFile."/".$DocName;
                    }else{
                        $path_file = $pathFile."/".$DocName;
                    }

                    $path_file = str_replace(" ", "_", $path_file);

                    if(is_uploaded_file($temp)){ 
                        move_uploaded_file($temp, $path_file);
                        if($TipoDoc === "OtherFiles"){
                            echo "<tr id='newfile_".$TotalFiles."'><td id='FileType' style='text-align:center;'>Archivo Cargado</td><td style='text-align:center;'><div id='ButtonsFile".$name."' style='text-align:center;'><a onclick=window.open('".$path_file."');><span id='ViewFile".$name."' class='btn btn-info' style='padding:5px;'><i class='fa fa-eye'></i> Ver Archivo</span></a><a id='".$TotalFiles."' file='".$path_file."' tip='newfile' onclick='deleteThisFile(this, 0);'><span id='deleteFile' class='btn btn-danger' style='padding:5px;'><i class='fa fa-times'></i>Eliminar</span></a></div></td></tr>";
                        }else{
                            $QueryFilter = "WHERE FolioControl = '".$FolioControl."' AND CveCarrera = '".$ThisCarrer."' AND TipoDocumento = '".$TipoDoc."'";
                            $ThisFile = $obj->getDbRowName("NombreDocumento", "Info_Documentos", $QueryFilter, 1);
                            if($ThisFile === "error"){
                                if($ThisCarrer === "NA"){
                                    $TipoMod = "CERT";
                                }else{
                                    $TipoMod = "TIT";
                                }
                                $query = "INSERT INTO Info_Documentos (TipoMod, FolioControl, CveCarrera, TipoDocumento, NombreDocumento, FechaCarga) VALUES ('".$TipoMod."', '".$FolioControl."','".$ThisCarrer."','".$TipoDoc."','".$DocName."','".date("Y-m-d")."')"; 
                            }else{
                                $query = "UPDATE Info_Documentos SET NombreDocumento='".$DocName."', FechaCarga='".date("Y-m-d")."' ".$QueryFilter;
                            }
                            if(isset($query)){$query = $obj->xQuery($query);}
                            echo "1 | ";
                            echo "<a onclick=window.open('".$path_file."');><span id='ViewFile".$name."' class='btn btn-info' style='padding:5px;'><i class='fa fa-eye'></i> Ver Archivo</span></a><a id='".$FileNumber."' file='".$path_file."' tip='reqfile' tipoDoc='".$TipoDoc."' fileFolio='".$FolioControl."' fileCarrer='".$ThisCarrer."' onclick='deleteThisFile(this, ".$FileNumber.")'><span id='delete".$FileNumber."' class='btn btn-danger UploadNewFile' style='padding:5px;'><i class='fa fa-times'></i> Eliminar</span></a>";
                        }
                    }else{ 
                        echo "0"; 
                    }
                }
            }else{
                echo "<script>";
                echo "  alert('Unicamente esta permitida la carga de archivos del tipo PDF, favor de intentarlo con un archivo correcto');";
                echo "</script>";
                die();
            }
        }else{
            echo "<script>";
            echo "  alert('Ha ocurrido un error inesperado al intentar cargar el archivo deseado, favor de intentarlo con un archivo correcto');";
            echo "</script>";
        }
    }
    
    function formatSizeUnits($bytes){
        if($bytes >= 1073741824){
            $bytes = number_format($bytes / 1073741824, 2).' GB';
        }elseif($bytes >= 1048576){
            $bytes = number_format($bytes / 1048576, 2).' MB';
        }elseif($bytes >= 1024){
            $bytes = number_format($bytes / 1024, 2).' KB';
        }elseif($bytes > 1){
            $bytes = $bytes.' bytes';
        }elseif($bytes == 1){
            $bytes = $bytes.' byte';
        }else{
            $bytes = '0 bytes';
        }
        return $bytes;
    }
?>