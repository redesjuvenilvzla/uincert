﻿<?php
    session_start(); 
    $SecureSection = false;
	require_once("util/utilerias.php");
    $obj = new Utilerias;
	$obj->CnnBD();
?>
<!--	<link rel='stylesheet' id='compiled.css-css'  href='./css/compiled-4.5.15.min.css' type='text/css' media='all' />-->
<!--    <script type='text/javascript' src='./js/compiled.0.min.js?ver=4.5.15'></script>-->
<!--	<script type='text/javascript' src='./js/tablax2.js'></script>-->
<!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
	<script>
    $(document).on('click', '#searchc', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        formdata = new FormData(); 
        //campus = $("#cmbCampus option:selected").text();
        campus = $("#cmbCampus").val();
        xtipCert = $("#cmbTipCertificado option:selected").text();
        if (xtipCert == "Ambos"){
            tipCert = "'T','P'";
        }else{
            if (xtipCert == "Total"){
                tipCert = "'T'";    
            }else{
                tipCert = "'P'";
            }
        }        
        finicial = $('#finicio').val();        
        ffinal = $('#ffin').val();        
        anho = parseInt(finicial.substr(0,4));
                
        $('#search').unbind('click.new');
        formdata.append("campus", campus);
        formdata.append("tipCert", tipCert);
        formdata.append("anho", anho);
        formdata.append("finicial", finicial);
        formdata.append("ffinal", ffinal);
        jQuery.ajax({
            url: 'CertificadosCoordinacionList.php',
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            success: function (result) {
                $("#ListAlumnos").html(result);
            }
        });
        $("div").find("#ViewInfoAlumno").html("");
    });    
</script>
    <h2 class="text-primary"> COORDINACIÓN </h2>
    <hr>
    <div class="row">
      <div class="col"><h6>Campus</h6></div>
      <div class="col"><h6>Tipo Certificado</h6></div>
      <div class="col"><h6>Fecha Inicial</h6></div>
      <div class="col"><h6>Fecha Final</h6></div>
      <div class="col"></div>
    </div>
    <div class="row">
      <div class="col"><?php echo $obj->cmbCampus(); ?></div>
      <div class="col"><?php echo $obj->cmbTipCert(); ?></div>
      <div class="col"><?php echo $obj->dpFecIni(); ?></div>
      <div class="col"><?php echo $obj->dpFecFin(); ?></div>
      <div class="col" align="left"><?php echo $obj->btnBuscarc(); ?></div>
    </div>        
    <hr/>     
    <div id='ListAlumnos'></div>
    <br /><br />
    <div>
<div id="pills-tabContent">
<div class="col-md-12 left">
<div class="col-sm-12">
<div id='ViewInfoAlumno'>
    <br /><br /><br /><br /><br />
</div>
</div>
</div>
</div>
</div>
