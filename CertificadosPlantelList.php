<?php
    header('Content-Type: text/html; charset=UTF-8');
    //error_reporting(0);
    
    $SecureSection = false;
    require_once("util/utilerias.php");
    $obj = new Utilerias;
    $obj->CnnBD();
    
    $CAMPUS = $_POST['campus'];
    $TIPCERT = $_POST['tipCert'];
    $ANHO = $_POST['anho'];
    $FINICIO = $_POST['finicial'];
    $FFINAL = $_POST['ffinal'];

    $AHIO_ALUMNO = '';
       
    $VALPLANTEL = "LEFT JOIN REG_Validacion RVP ON RVP.VP_ID_UNICO_CERT = T1.PEOPLE_CODE_ID+T1.folioControl+T1.campus";

    //$VALPLANTEL = "INNER JOIN REG_Validacion RVP ON RVP.VP_ID_UNICO_CERT = T1.PEOPLE_CODE_ID+T1.folioControl+T1.Plantel  where vp_estatus IN ('Gen.XML','Validado por plantel') ";
        
    $query = str_replace("VARIABLE=CAMPUS", $CAMPUS, CERTIFICADOSHEADER);
    $query = str_replace("VARIABLE=TIPO_CERTIFICADO", "AND (RC.TIPO_CERTIFICADO IN (".$TIPCERT."))", $query);
    $query = str_replace("VARIABLE=ANHO", $ANHO, $query);
    $query = str_replace("VARIABLE=FINICIAL", $FINICIO, $query);
    $query = str_replace("VARIABLE=FFINAL", $FFINAL, $query);
    $query = str_replace("VARIABLE=NUMCONTROL", "", $query);
    $query = str_replace("VARIABLE=VALPLANTEL", $VALPLANTEL, $query);
//    echo "Query <br>".$query;

?>
<!--    <link rel='stylesheet' id='compiled.css-css'  href='./css/compiled-4.5.15.min.css' type='text/css' media='all' />-->
<!--    <script type='text/javascript' src='./js/compiled.0.min.js?ver=4.5.15'></script>-->
<!--    <script type='text/javascript' src='./js/tablax2.js'></script>-->
<!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
<!--<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>-->
    <script>
      // Datatable vertical dynamic height
      $(document).ready(function () {
  		$('#dtDynamicVerticalScrollExample').DataTable({
  			"scrollY": "45vh",
        "bDestroy": true, 
  			"scrollCollapse": true,
  			"paging": false
  		});
  		$('.dataTables_length').addClass('bs-select');
      $("#example").dataTable().fnDestroy();
      //$.fn.dataTable.ext.errMode = 'none';
  		var table = $('#example').dataTable( {
        "bDestroy": true, 
  			paging: false,
  			searching: false
  		} );
  		$(".btn1").click(function() {
  			//valores obtendra el dato del td por posciones [0]
  			var vNumControl = $(this).parents("tr").find("td")[1].innerHTML;
  			$("#ViewInfoAlumno").text("");			
  			//var campus = $("#cmbCampus option:selected").text();		
        var campus = $("#cmbCampus").val();	
  			var finicio = $('#finicio').val();
  			var ffinal = $('#ffin').val();
        var vNumControl = vNumControl;
        var anho = $(this).attr('year');
              var curiculum = $('#txt'+vNumControl).val();
              // alert(curiculum);
              xtipCert = $("#cmbTipCertificado option:selected").text();       
              if (xtipCert == "Ambos"){
                  tipCert = "'T','P'";
              }else{
                  if (xtipCert == "Total"){
                      tipCert = "'T'";    
                  }else{
                      tipCert = "'P'";
                  }
              }  

        var res = campus.replace(" ", "%20");

       // var ahioVar = "<?php echo $AHIO_ALUMNO; ?>";
        //alert(ahioVar);

  		// 	$("#ViewInfoAlumno").load('infoAlumno.php?campus='+(res)
    		// 	+'&anho='+anho
    		// 	+'&finicio='+finicio
    		// 	+'&ffinal='+ffinal
    		// 	+'&vNumControl='+vNumControl
        //   +'&curiculum='+curiculum
    		// 	+'&tipCert='+tipCert
        // );
        //
        // $("html, body").animate({scrollTop: $(document).height()}, 1000);

          $('#infoModalTitle').html('Detalle del estudiante');
          $("#infoModalBody").empty();
          $("#infoModalBody").load('infoAlumno.php?campus='+(res)
              +'&anho='+anho
              +'&finicio='+finicio
              +'&ffinal='+ffinal
              +'&vNumControl='+vNumControl
              +'&curiculum='+curiculum
              +'&tipCert='+tipCert
          );
          $('#infoModal').modal();
        });


    		$(".btn2").click(function(){
    			var valores = $(this).parents("tr").find("td")[1].innerHTML;
    		});		
      });

      $(document).ready(function(){
          $(".DisplayType").click(function(){
            var thisValue = $(this).val();
            if($(this).prop('checked')){
              $(".Alumno_"+thisValue).show();
            }else{
              $(".Alumno_"+thisValue).hide();
            }
          });
      })
    </script>
    

    <?php
		$rQuery = $obj->xQuery($query);
	?>
    <section id="datatable-vertical-dynamic-height">
      <section>
        <div class="text-center" style="position:relative; width:100%;">

              <label class="container-checkbox mr-2">Prospecto
                  <input type="checkbox" class="DisplayType" value="Prospecto" checked>
                  <span class="checkmark" ></span>
              </label>
            <label class="container-checkbox mr-2">Rechazado
                  <input type="checkbox" class="DisplayType" value="Rechazado" checked>
                  <span class="checkmark" ></span>
              </label>
            <label class="container-checkbox ">Validado
                  <input type="checkbox" class="DisplayType" value="Validado" checked>
                  <span class="checkmark" ></span>
              </label>
<!--              <input type="checkbox" class="DisplayType checkbox-style-1" value="Prospecto" style="pointer-events:auto; margin-top:5px;" checked> <span style='margin-left:15px'>Prospecto |</span>-->
<!--              <input type="checkbox" class="DisplayType  checkbox-style-1" value="Rechazado" style="pointer-events:auto; margin-top:5px;" checked> <span style='margin-left:15px'>Rechazado |</span>-->
<!--              <input type="checkbox" class="DisplayType checkbox-style-1" value="Validado" style="pointer-events:auto; margin-top:5px;" checked> <span style='margin-left:15px'>Validado</span>-->


        </div>

          <div class="accordion mb-2 " id="accordionExample">
              <div class="card border-0">
                  <div class="card-header p-1 px-2 border-0 bg-white rounded-top text-white bb-style-1" id="headingThree">
                      <h6 class=" float-left  text-primary">
                          Notas:

                      </h6>
                      <a class="btn btn-link float-right border-0 p-0 border-0" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseOne">
                          <i class="fa fa-angle-down text-primary"></i>
                      </a>
                  </div>
                  <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionExample" style="">
                      <div class="card-body p-0 pt-2">
                          <ol>
                              <li>
                                  <h6>Haga click sobre el icono: <i class="text-info fa fa-id-card-o"></i> para ver o editar la información de cada alumno</h6>
                              </li>
                              <li>
                                  <h6>Haga click sobre el icono: <i class="text-warning fa fa-user-times"></i> para ver la Razón del por que se rechazó cada alumno</h6>
                              </li>
                          </ol>
                      </div>
                  </div>
              </div>
          </div>

<!--        <b><h4>Notas:</h4></b>-->
<!--        <ol>-->
<!--          <h5>-->
<!--          <li>Haga click sobre el icono: <i class="text-info fa fa-id-card-o"></i> para ver o editar la informaci&oacute;n de cada alumno</li>-->
<!--        </h5>-->
<!--        <h5>-->
<!--          <li>Haga click sobre el icono: <i class="text-warning fa fa-user-times"></i> para ver la Raz&oacute;n del por que se rechaz&oacute; cada alumno</li>-->
<!--        </h5>-->
<!--        </ol>-->
        <div id="pills-tabContent" class="tab-content">
             <div class="col-md-12 left">
            <div class="container col-lg-12">
        <table id="dtDynamicVerticalScrollExample" class="table table-rounded table-striped table-sm" cellspacing="0" width="100%" style="text-align:center; "
        >
          <thead>
            <tr class="bg-primary rounded-top text-white" style='text-align:center; background-color:#67a1fc;'>
<!--              <br><br>-->
              
<!--              <div id="dtDynamicVerticalScrollExample_filter" class="dataTables_filter">-->
             
                
              
              
              <th class="col-xs-2">#</th>
              <th class="col-xs-2">Id Alumno</th>
              <th class="col-xs-2">Nombre</th>
              <th class="col-xs-2">A.Paterno</th>
              <th class="col-xs-2">A.Materno</th>
              <th class="col-xs-2">Tip.Certificado</th>
              <th class="col-xs-2">Validar Info</th>
              <th class="col-xs-2">Estatus</th>
              <th class="col-xs-2">Raz&oacute;n</th>
            </tr>
          </thead>
          <tbody>

			<?php		 
                  while ($data = sqlsrv_fetch_array($rQuery)){
                      $StudentValidate = $obj->getDbRowName("vp_estatus", "REG_Validacion", "WHERE vp_id_unico_cert = '".($data["ID_UNICO_CERT"])."'", 1);
                      //echo utf8_encode($data["ID_UNICO_CERT"]);
                      if($StudentValidate === "error"){
                        $thisStatus   = "Prospecto";
                        $ClassStatus  = "Prospecto";
                        $vlicono      = "text-warning fa fa-exclamation";
                      }else{
                        if(($StudentValidate === "Validado") OR ($StudentValidate === "Gen.XML") OR ($StudentValidate === "XML autenticado") OR ($StudentValidate === "XML timbrado") OR ($StudentValidate === "Validado por CSE")){
                          $thisStatus   = $StudentValidate;
                          $ClassStatus  = "Validado";
                          $vlicono      = "text-success fa fa-check";
                        }else{
                          $thisStatus   = $StudentValidate;
                          $ClassStatus  = "Rechazado";
                          $vlicono      = "text-warning fa fa-exclamation";
                        }
                      }


                      $DeniedReason = $obj->getDbRowName("vp_razon_rechazo", "Reg_Validacion", "WHERE vp_id_unico_cert = '".$data["ID_UNICO_CERT"]."'", 0);

                      if($DeniedReason <> ""){
                        $DeniedButton = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-fancybox='VerRazonRechazo' data-src='#VerRazonRechazo".$data["numeroControl"]."' href='javascript:;'><i class='text-warning fa fa-user-times'></i></a>";
                      }else{
                        $DeniedButton = "N/A";
                      }

                      echo '
                          <tr class="Alumno_'.$ClassStatus.'">
                            <td >'.$data["RW"].'</td>
                            <td >P'.$data["numeroControl"].'</td>
                            <td >'.utf8_encode($data["nombre3"]).'</td>
                            <td >'.utf8_encode($data["primerApellido4"]).'</td>
                            <td >'.utf8_encode($data["segundoApellido5"]).'</td>
                            <td >'.utf8_encode($data["tipoCertificacion"]).'</td>
                            <td >
                              <button class="btn btn-outline-primary btn1" title='.$data["curriculum"].' value='.$data["numeroControl"].' year='.$data["ANHO"].'>
                                <i class="fa fa-id-card-o"></i>
                              </button>
                              <i class="'.$vlicono.'" id="img'.$data["numeroControl"].'"></i>
                              <input type="text" readonly id="txtP'.$data["numeroControl"].'" value='.$data["curriculum"].' style="width: 15px; visibility: hidden;">            
                            </td>
                            <td>
                                <input type="text" readonly class="form-control-plaintext" id="est'.$data["numeroControl"].'" value="'.$thisStatus.'" style="text-align:center;">
                            </td>
                            <td id="raz_P'.$data["numeroControl"].'" style="text-align:center;">'.$DeniedButton.'</td>
                          </tr>
                      ';
                      if(strlen($DeniedReason) <> 0){
                        echo '<span id="VerRazonRechazo'.$data["numeroControl"].'" style="width:600px;" class="fancy_info">
                            <b>Raz&oacute;n de Rechazo del Alumno:</b><br />'.utf8_encode($data["nombre3"]).' '.utf8_encode($data["primerApellido4"]).' '.utf8_encode($data["segundoApellido5"]).'
                            <hr style="color: #0056b2;" />
                            <p>'.$DeniedReason.'</p>
                        </span>';
                      }
                  }
            ?>
          </tbody>
        
         
          <tfoot>
            <tr class="bg-primary rounded-top text-white">
              <th>#</th>
              <th>Id Alumno</th>
              <th>Nombre</th>
              <th>A.Paterno</th>
              <th>A.Materno</th>
              <th>Tip.Certificado</th>
              <th>Validar Info</th>
              <th>Estatus</th> 
              <th>Raz&oacute;n</th>
            </tr>
          </tfoot>
        </div>
        </div>        
        </div>

        </table>
     </section>
    </section>


