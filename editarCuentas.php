
<?php
    session_start();

    switch ($_SESSION["SectionType"]) {
        case 'Servicios_Profesionales':
            $thisPath = "../";
        break;
        default:
            $thisPath = "";
        break;
    }

    if(isset($_SESSION['susu'])){
        $SecureSection = false;
        require_once("util/utilerias.php");  

        $obj = new Utilerias;
        $obj->CnnBD();
        $query = "SELECT * FROM reg_usu WHERE usu_nombre = '".$_SESSION['nom']."'";  
        $rQuery = $obj->xQuery($query);
        while ($data = sqlsrv_fetch_array($rQuery)) {

        }
    }else{
        header("Location: ".$thisPath."index2.php");
    }
?>

<script>
    document.getElementById('cmbCampus').value='';
    $('#DatosUsuario').css('display', 'none');
    function editarAdmin()
    {
        formdata    = new FormData(); 
        usuario     = $("#txtUsuario").val();   
        contra      = $("#txtContra").val();
        correo      = $("#txtCorreo").val();
        campus      = $("#cmbCampus").val();
        rol         = $("#txtRol").val();

        if ($("#auth_ss").is(':checked')) {auth_ss = 1;}else{auth_ss = 0;}
        if ($("#auth_pp").is(':checked')) {auth_pp = 1;}else{auth_pp = 0;}
        if ($("#auth_ti").is(':checked')) {auth_ti = 1;}else{auth_ti = 0;}
        if ($("#auth_co").is(':checked')) {auth_co = 1;}else{auth_co = 0;}

        formdata.append("usuario", usuario);
        formdata.append("contra", contra);
        formdata.append("correo", correo);
        formdata.append("cmbCampus", campus);
        formdata.append("rol", rol);

        formdata.append("auth_ss", auth_ss);
        formdata.append("auth_pp", auth_pp);
        formdata.append("auth_ti", auth_ti);
        formdata.append("auth_co", auth_co);

        jQuery.ajax({
            url: '<?php echo $thisPath;?>editarUsuarioAdministrador.php',
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            success: function (result) {
                alert("Usuario editado correctamente");
                $("#txtRol").val('');
                $("#txtUsuario").val('');
                $("#txtContra").val('');
                $("#txtCorreo").val('');
                $("#cmbCampus").val('');
                
                $("#auth_ss").val('');
                $("#auth_pp").val('');
                $("#auth_ti").val('');
                $("#auth_co").val('');

                $("#auth_ss").prop("checked", false);
                $("#auth_pp").prop("checked", false);
                $("#auth_ti").prop("checked", false);
                $("#auth_co").prop("checked", false);

                $('#txtContra').prop('disabled', true);
                $('#txtCorreo').prop('disabled', true);
                $('#txtRol').prop('disabled', true);
                $('#buttonedit').prop('disabled', true);
            }
        });
    } 

    function consulta()
    {
        formdata = new FormData(); 
        usuario = $("#txtUsuario").val();   
        formdata.append("usuario", usuario);
            jQuery.ajax({
            url: '<?php echo $thisPath;?>consultaUsuario.php',
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            success: function (result) {
                var fields = result.split('&&');
                var correo = fields[0];
                var contrasena = fields[1];
                var rol = fields[2];
                var existe = fields[3];
                var campus = fields[4];
                var auth_ss = fields[5];
                var auth_pp = fields[6];
                var auth_ti = fields[7];
                var auth_co = fields[8];

                if(auth_ss == 0 || auth_ss === "NULL"){
                    auth_ss = 0;
                    $("#auth_ss").prop("checked", false);
                }else{
                    $("#auth_ss").prop("checked", true);
                }
                if(auth_pp == 0 || auth_pp === "NULL"){
                    auth_pp = 0;
                    $("#auth_pp").prop("checked", false);
                }else{
                    $("#auth_pp").prop("checked", true);
                }
                if(auth_ti == 0 || auth_ti === "NULL"){
                    auth_ti = 0;
                    $("#auth_ti").prop("checked", false);
                }else{
                    $("#auth_ti").prop("checked", true);
                }
                if(auth_co == 0 || auth_co === "NULL"){
                    auth_co = 0;
                    $("#auth_co").prop("checked", false);
                }else{
                    $("#auth_co").prop("checked", true);
                }

                if (existe == 0)
                {
                    alert("El usuario capturado no existe ");
                }
                else
                {
                    $('#DatosUsuario').fadeIn(900);
                    $('#txtContra').prop('disabled', false);
                    $('#txtCorreo').prop('disabled', false);
                    $('#txtRol').prop('disabled', false);
                    $('#buttonedit').prop('disabled', false);
                    if(rol == 1){
                        $('#cmbCampus').prop('disabled', true);
                        $('#PermisosAdicionales').hide();
                    }else{
                        document.getElementById('cmbCampus').value=campus;
                        $('#PermisosAdicionales').show();
                    }
                    $("#txtRol").val(rol).change();
                    document.getElementById('txtCorreo').value=correo;
                    document.getElementById('txtContra').value=contrasena;

                    document.getElementById('auth_ss').value=auth_ss;
                    document.getElementById('auth_pp').value=auth_pp;
                    document.getElementById('auth_ti').value=auth_ti;
                    document.getElementById('auth_co').value=auth_co;
                }
            }
        });
    }

    $(document).ready(function(){
        var thisRol = $("#txtRol").val();
        $("#txtRol").change(function(){
            if($("#txtRol").val() == 1 || $("#txtRol").val() == 3 || $("#txtRol").val() == 4 || $("#txtRol").val() == 5){
                $("#cmbCampus").val('');
                $("#cmbCampus").prop('disabled', true);
            }else{
                $("#cmbCampus").prop('disabled', false);
            }
            switch ($("#txtRol").val()) {
                case '0':
                    $("#cmbCampus").prop('disabled', true);
                    $("#txtContra").prop('disabled', true);
                    $("#txtCorreo").prop('disabled', true);
                    $('#PermisosAdicionales').fadeOut('fast');
                    break;
                case '1':
                    $("#cmbCampus").prop('disabled', true);
                    $("#txtContra").prop('disabled', false);
                    $("#txtCorreo").prop('disabled', false);
                    $('#PermisosAdicionales').fadeOut('fast');
                    break;
                default:
                    $("#cmbCampus").prop('disabled', false);
                    $("#txtContra").prop('disabled', false);
                    $("#txtCorreo").prop('disabled', false);
                    $('#PermisosAdicionales').fadeIn('fast');
                    break;
            }
        });
    })
</script>

<h2 class="text-primary">EDITAR USUARIO</h2>
<div class="text-center" style="background-color:#f1f2f3; width: 70%; padding:20px; margin-left: auto; margin-right: auto;">
    <div class='row'>
        <div class='col-12' style='text-align:left;'><h4>Buscar Usuario:</h4></div>
    </div>
    <hr>
    <div class='row'>
        <div class="input-group mb-3">
            <input data-toggle="tooltip" title="" data-placement="top" class="form-control" placeholder="Ingresar usuario" type="text" id='txtUsuario' name='txtUsuario'>
            <div class="input-group-append">
                <button class="btn btn-outline-primary" type="button" id="registerButton" onclick='consulta()'><i class='fa fa-search' aria-hidden='true'> </i> Buscar usuario</button>
            </div>
        </div>
    </div>
    <br />
    <div id='DatosUsuario'>
        <div class='row'>
            <div class='col-12' style='text-align:left;'><h4>Campos Editables</h4></div>
        </div>
        <hr>
        <div class='row'>
            <div class='col-sm-3' style='text-align:left;'><h6>Rol</h6></div>
            <div class='col-sm-6'>
                <select class='form-control' id='txtRol' name='txtRol' >
                    <option value="0">Inactivo</option>
                    <option value="1">Administrador</option>
                    <option value="2">Usuario certificado plantel</option>
                    <option value="3">Usuario certificado CSE</option>
                    <option value="4">Usuario títulos alta</option>
                    <option value="5">Usuario títulos administrador</option> 
                </select>
            </div> 
        </div>
        <br />
        <div class='row'>
            <div class='col-3' style='text-align:left;'><h6>Campus</h6></div>
            <div class='col-8'><?php echo $obj->cmbCampus(); ?></div>
        </div>
        <br />
        <div class='row'>
            <div class='col-3' style='text-align:left;'><h6>Contraseña</h6></div>
            <div class='col-8'>
                <input class="form-control valid" placeholder="Contraseña" type="password" id="txtContra" name="txtContra" disabled="true">
            </div>
        </div>
        <br />
        <div class='row'>
            <div class='col-3' style='text-align:left;'><h6>E-mail</h6></div>
            <div class='col-8'>
                <input class="form-control valid" data-toggle="tooltip" title="" data-placement="top" placeholder="Correo electronico" type="email" data-val="true" id="txtCorreo" name="txtCorreo" disabled="true">
            </div>
        </div>
        <br /><br />
        <div id='PermisosAdicionales' class='row'>
            <div class='col-12' style='text-align:left;'><h4>Permisos Adicionales</h4></div>
            <div class='row' style='width:100%; justify-content:center; margin-top:20px; margin-bottom:20px;'>
                <div class='col-2'>
                    <h6 class='card-title'>Servicio Social</h6>
                    <input type='checkbox' class='form-check-input' id='auth_ss' name='auth_ss' value='' style='width:20px; height:20px; margin:auto;'>
                </div>
                <div class='col-4'>
                    <h6 class='card-title'>&nbsp;&nbsp;&nbsp;&nbsp;Pr&aacute;cticas Profesionales</h6>
                    <input type='checkbox' class='form-check-input' id='auth_pp' name='auth_pp' value='' style='width:20px; height:20px; margin:auto;'>
                </div>
                <div class='col-2'>
                    <h6 class='card-title'>&nbsp;&nbsp;&nbsp;&nbsp;Titulaci&oacute;n</h6>
                    <input type='checkbox' class='form-check-input' id='auth_ti' name='auth_ti' value='' style='width:20px; height:20px; margin:auto;'>
                </div>
                <div class='col-2'>
                    <h6 class='card-title'>&nbsp;&nbsp;&nbsp;&nbsp;Convenios</h6>
                    <input type='checkbox' class='form-check-input' id='auth_co' name='auth_co' value='' style='width:20px; height:20px; margin:auto;'>
                </div>
            </div>
        </div>
        <hr>
        <br />
        <div class='row'>
            <div class='col-12' style='text-align:center;'>
                <button id="buttonedit" type="button" class="btn btn-success bg-success btn-sm"  onclick='editarAdmin()' style='' disabled><i class='fa fa-save' aria-hidden='true'></i> Guardar Cambios</button>
            </div>
        </div>
    </div>
</div>