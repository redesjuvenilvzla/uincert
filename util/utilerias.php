<?php
    setlocale(LC_TIME, 'spanish');
    setlocale(LC_MONETARY, 'es_mx');
	date_default_timezone_set('America/Mexico_City');
	
    define('__ROOT__', dirname(dirname(__FILE__)));

	$DetectServer   = ".com";
	$CurrentServer  = $_SERVER['SERVER_NAME'];
	$findServ       = strpos($CurrentServer, $DetectServer);

	if (session_status() == PHP_SESSION_NONE){
		session_start();
	}

	require_once(__ROOT__."/util/security.php");
	require_once(__ROOT__."/config/db_config.php");
	require_once(__ROOT__."/config/xData.php");

	class Utilerias
	{
		public function CnnBD()
		{
			$connectionInfo = array("Database"=>DATABASE, "UID"=>USERNAME, "PWD"=>PASSWORD);
			$this->cnn = sqlsrv_connect(SERVER, $connectionInfo);
			if ($this->cnn)
			{
				$this->r = "";
			} else {
				$this->r = "Fallo en la Conexion!";
				die(print_r(sqlsrv_errors()));
			}	  
			return $this->r;

			sqlsrv_close($this->cnn);
		}

		public function xQuery($sql)
		{
			$this->sql = $sql;
			$this->rQuery = sqlsrv_query($this->cnn, $this->sql);
			return $this->rQuery;

			sqlsrv_close($this->cnn);
		}

		public function xQueryI($sql)
		{
			self::CnnBD();
			$this->sql = $sql;
			$this->rQuery = sqlsrv_query($this->cnn, $this->sql);
			return $this->rQuery;

			sqlsrv_close($this->cnn);
		}
		
		public function xCQuery()
		{	
			$this->cQuery = sqlsrv_has_rows( $this->rQuery );
			if ($this->cQuery === false)
				$this->r = "0";
			else
				$this->r = "1";
	 		return $this->r;

			sqlsrv_close($this->cnn);
		}

		public function cmbCampus2()
		{
			self::CnnBD();
			self::xQueryI("SELECT DISTINCT DESCRIPCION_CERT FROM ENTIDAD_CERTIFICADO");
			$this->result  = '<select class="form-control" id="cmbCampus" name="cmbCampus">';
			$this->result .= '<option value=""></option>';
			while ($this->data = sqlsrv_fetch_array($this->rQuery)) {
	            
	            	$this->result .= '<option value="'.$this->data["DESCRIPCION_CERT"].'">'.$this->data["DESCRIPCION_CERT"].'</option>';

			}
			$this->result .= '</select>';
			return $this->result;
		}

		public function cmbCampus(){
			self::CnnBD();
			switch($_SESSION["rol"]){
				case 1:
				case 3:
				case 4:
				case 5:
					self::xQueryI("SELECT DISTINCT ACADEMIC_SESSION,DESCRIPCION_CERT FROM ENTIDAD_CERTIFICADO");
				break;
				default:
					self::xQueryI("SELECT DISTINCT ACADEMIC_SESSION,DESCRIPCION_CERT FROM ENTIDAD_CERTIFICADO WHERE ACADEMIC_SESSION = '".$_SESSION["campus"]."'");
				break;
			}
			$this->result  = '<select class="form-control" id="cmbCampus" name="cmbCampus">';
			while ($this->data = sqlsrv_fetch_array($this->rQuery)) {
	       		if($_SESSION["campus"] === $this->data["ACADEMIC_SESSION"]){$this->SelectedCampus = "selected";}else{$this->SelectedCampus = "";}
				if(utf8_encode($this->data["DESCRIPCION_CERT"]) == "CÉNTRO"){
	            	$this->result .= '<option value='.'CENTRO'.'>'.'CENTRO'.'</option>';
	            }else{
	            	$this->result .= '<option value="'.$this->data["ACADEMIC_SESSION"].'" '.$this->SelectedCampus.'>'.utf8_encode($this->data["DESCRIPCION_CERT"]).'</option>';
	            }
			}
			$this->result .= '</select>';
			return $this->result;
		}

		public function cmbTipCert()
		{
			$this->result  = '<select class="form-control" id="cmbTipCertificado">';
			$this->result .= '<option>Ambos</option>';
			$this->result .= '<option>Total</option>';
			$this->result .= '<option>Parcial</option>';
			$this->result .= '</select>';
			return $this->result;
		} 
		public function dpFecIni()
		{	
			$this->IniYear	= date("Y");
			$this->IniDate	= $this->IniYear.'-'.date("m").'-01';
			$this->result  	= '<input type="date" class="form-control" id="finicio" name="finicio" step="1" min="1980-01-01" max="2030-12-31" value="'.$this->IniDate.'" placeholder="Fecha Inicial">';
//			$this->result  	= '<input type="date" class="form-control" id="finicio" name="finicio" step="1" min="1980-01-01" max="2030-12-31" value="2017-08-01" placeholder="Fecha Inicial">';
			return $this->result;
		} 
		public function dpFecFin()
		{
			/**/$this->result  = '<input type="date" class="form-control" id="ffin" name="ffin" step="1" min="1980-01-01" max="2030-12-31" value="'.date("Y-m-d").'" placeholder="Fecha Final">';
//			$this->result  = '<input type="date" class="form-control" id="ffin" name="ffin" step="1" min="1980-01-01" max="2030-12-31" value="2017-09-01" placeholder="Fecha Final">';
			return $this->result;
		} 
		public function btnBuscar($section)
		{
			$this->result  = '<button id="search' . $section . '" name="search" class="btn btn-primary bg-primary btn-block"><i class="fa fa-search fa-1x mr-3" ></i>Buscar</button>';
			return $this->result;
		}
	   	public function btnBuscarc()
		{
			$this->result  = '<button id="searchc" name="search" class="btn btn-primary btn-block bg-primary"><i class="fa fa-search fa-1x mr-3" ></i>Buscar</button>';
			return $this->result;
		} 
		public function btnGenera()
		{
			$this->result  = '<button class="btn btn-success  btn-block btnGenXML"><i class="fa-file-o fa-1x mr-3"></i>Genera XML</button>';
			return $this->result;
		} 
		public function btnCancela()
		{
			$this->result  = '<button class="btn btn-primary  btn-block btnCancela"><i class="fa-file-o fa-1x mr-3"></i>Cancelar</button>';
			return $this->result;
		}    
		public function xTabla($campos)
		{
			$this->r="";
			$this->campos = $campos;		
			$this->xcampos = explode(",",$campos);
			$this->r .= "<table>";
			while ($this->data = sqlsrv_fetch_array($this->rQuery)) {
				$this->r .= ' <tbody> <tr> ';
				foreach ($this->xcampos as $this->xcampo) {
		            $this->r .= " <td> " . $this->data[$this->xcampo] . " </td> ";
				}
				$this->r .= ' </tr></tbody> ';
			}
			$this->r .= "</table>";
			return $this->r;
		}
		
		public function tabla($sql, $campos)
		{
			$this->r="";
			$this->sql = $sql;
			$this->campos = $campos;		
			$this->xcampos = explode(",",$campos);
			//$this->r = print_r($this->xcampos);
			$this->res = sqlsrv_query($this->cnn, $this->sql);
			$this->r .= "<table>";
			while ($this->data = sqlsrv_fetch_array($this->res)) {
				$this->r .= ' <tbody> <tr> ';
				foreach ($this->xcampos as $this->xcampo) {
		            $this->r .= " <td> " . $this->data[$this->xcampo] . " </td> ";
				}
				$this->r .= ' </tr></tbody> ';
			}
			$this->r .= "</table>";
			return $this->r;

			sqlsrv_close($this->cnn);
		}
	    
	    public function menu($SectionType)
	    {
	        require_once(__ROOT__."/menu.php");    
	    }
		
		public function dTable($Cabeceros, $Campos, $query)	
		{
			$this->xcabeceros = explode(",",$Cabeceros);
			$this->xcampos = explode(",",$Campos);
			$this->query = $query;
			echo '
			<div class="fw-container">
				<div class="fw-nav">
					<div class="mobile-show"></div>
				</div>
				<div class="fw-body">
					<div class="content">
						<table id="example" name="example" class="display" >
							<thead>
								<tr>
			';
									foreach ($this->xcabeceros as $this->xcabecero) {
										echo " <td> " . $this->xcabecero . " </td> ";
									}
			echo '									
								</tr>
							</thead>
							<tbody style="font-size:12px;">
			';						
		
									self::xQueryI($this->query);
									while ($this->data = sqlsrv_fetch_array($this->rQuery)) {
										$this->r .= '<tr> ';
										foreach ($this->xcampos as $this->xcampo) {
											if ($this->xcampo != "vacio")
											{
												if (trim($this->xcampo) === "tipoCertificacion"){
													$this->r.= " <td> ".substr($this->data[$this->xcampo],0,1)." </td> ";
												}else{
													$this->r.= " <td> ".$this->data[$this->xcampo]." </td> ";
												}
											}else{
												$this->r .= " <td>  </td> ";
											}
										}
										$this->r .= '</tr>';
									}

									echo $this->r;
			echo '								
							</tbody>
							<tfoot>
							<tr>
			';
									foreach ($this->xcabeceros as $this->xcabecero) {
										echo " <td> " . $this->xcabecero . " </td> ";
									}
			echo '				
							</tr>
							</tfoot>
						</table>
				</div>
			</div>
			';
		}
		// Nuevas Funciones - Otakon Van Rock
		// ****************************************************************************************
		// ****************************************************************************************
			public function getDbRowName($thisRow, $thisTable, $thisFilter, $displayError){
				$this->sql 	= "SELECT ".$thisRow." AS RowName FROM ".$thisTable." ".$thisFilter;
				$this->res 	= sqlsrv_query($this->cnn, $this->sql);
				$this->data = sqlsrv_fetch_array($this->res);
				
		        $this->GetRowName = $this->data["RowName"];
		        if($displayError == 1){
		        	if(strlen($this->GetRowName) == 0){$this->GetRowName = "error";}
		        }
				$this->result = $this->GetRowName;
				return $this->result;

				sqlsrv_close($this->cnn);
			}
			
			public function getThisRcd($thisRow, $thisTable, $thisFilter){
				$sql 		= "SELECT ".$thisRow." AS RowName FROM ".$thisTable." ".$thisFilter;
				$res		= sqlsrv_query($this->cnn, $sql);
				$thisData 	= sqlsrv_fetch_array($res);
		        $GetRowName = utf8_encode($thisData["RowName"]);
				return $GetRowName;
				sqlsrv_close($this->cnn);
			}

			public function InCampus(){
				$this->result  = '<input type="text" id="Campus" name="Campus" class="form-control" placeholder="Ingrese el ID del Campus">';
				return $this->result;
			} 

			public function InCarrer(){
				$this->result  = '<input type="text" id="Carrer" name="Carrer" value="611310" class="form-control" placeholder="Ingrese el ID de la Carrera">';
				return $this->result;
			}

			public function FormatDateSQL($thisDate){
				$this->GetDateYear   	= substr($thisDate, 0, 4);
				$this->GetDateMonth  	= substr($thisDate, 5, 2);
				$this->GetDateDay    	= substr($thisDate, 8, 2);
				$this->ThisFormatDate 	= $this->GetDateYear."-".$this->GetDateMonth."-".$this->GetDateDay;
				return $this->ThisFormatDate;
			}

			public function cat_Estados($divName, $thisValue){
				$this->result = "";
				$this->result .= "<select class='form-control' id='".$divName."' name='".$divName."'>";
	        	$this->result .= "	<option value=''>SELECCIONAR</option>";
	            for($this->j=1; $this->j<=32; $this->j++){
	                if($thisValue == $this->j){$this->thisSelection = "selected";}else{$this->thisSelection = "";}
	                switch($this->j){
	                	case 1: $this->StateName = "AGUASCALIENTES"; break;
	                	case 2: $this->StateName = "BAJA CALIFORNIA"; break;
	                	case 3: $this->StateName = "BAJA CALIFORNIA SUR"; break;
	                	case 4: $this->StateName = "CAMPECHE"; break;
	                	case 5: $this->StateName = "COAHUILA DE ZARAGOZA"; break;
	                	case 6: $this->StateName = "COLIMA"; break;
	                	case 7: $this->StateName = "CHIAPAS"; break;
	                	case 8: $this->StateName = "CHIHUAHUA"; break;
	                	case 9: $this->StateName = "CIUDAD DE MÉXICO"; break;
	                	case 10: $this->StateName = "DURANGO"; break;
	                	case 11: $this->StateName = "GUANAJUATO"; break;
	                	case 12: $this->StateName = "GUERRERO"; break;
	                	case 13: $this->StateName = "HIDALGO"; break;
	                	case 14: $this->StateName = "JALISCO"; break;
	                	case 15: $this->StateName = "MÉXICO"; break;
	                	case 16: $this->StateName = "MICHOACÁN DE OCAMPO"; break;
	                	case 17: $this->StateName = "MORELOS"; break;
	                	case 18: $this->StateName = "NAYARIT"; break;
	                	case 19: $this->StateName = "NUEVO LEÓN"; break;
	                	case 20: $this->StateName = "OAXACA"; break;
	                	case 21: $this->StateName = "PUEBLA"; break;
	                	case 22: $this->StateName = "QUERÉTARO"; break;
	                	case 23: $this->StateName = "QUINTANA ROO"; break;
	                	case 24: $this->StateName = "SAN LUIS POTOSÍ"; break;
	                	case 25: $this->StateName = "SINALOA"; break;
	                	case 26: $this->StateName = "SONORA"; break;
	                	case 27: $this->StateName = "TABASCO"; break;
	                	case 28: $this->StateName = "TAMAULIPAS"; break;
	                	case 29: $this->StateName = "TLAXCALA"; break;
	                	case 30: $this->StateName = "VERACRUZ DE IGNACIO DE LA LLAVE"; break;
	                	case 31: $this->StateName = "YUCATÁN"; break;
	                	case 32: $this->StateName = "ZACATECAS"; break;
	                }
	            	if($this->j < 10){$this->j = "0".$this->j;}
	                $this->result .= "<option value='".$this->j."' ".$this->thisSelection.">".$this->StateName."</option>";
	            }
				$this->result .= "</select>";
				return $this->result;
			}

			public function cat_EntFed($divName, $thisValue){
				$this->result = "";
				$this->result .= "<select class='form-control' id='".$divName."' name='".$divName."'>";
	        	$this->result .= "	<option value=''>SELECCIONAR</option>";
	            for($this->j=1; $this->j<=3; $this->j++){
	                switch($this->j){
						case 1:
							$this->StateId = "15";
							$this->StateName = "MÉXICO";
							break;
						case 2:
							$this->StateId = "9";
							$this->StateName = "CIUDAD DE MÉXICO";
							break;
						case 3:
							$this->StateId = "11";
							$this->StateName = "GUANAJUATO";
							break;
	                }
	                if($thisValue == $this->StateId){$this->thisSelection = "selected";}else{$this->thisSelection = "";}
	                $this->result .= "<option value='".$this->StateId."' ".$this->thisSelection.">".$this->StateName."</option>";
	            }
				$this->result .= "</select>";
				return $this->result;
			}

			public function cat_TipoEstudios($divName, $thisValue){
				$this->result = "";
				$this->result .= "<select class='form-control' id='".$divName."' name='".$divName."'>";
	        	$this->result .= "	<option value=''>SELECCIONAR</option>";
	            for($this->j=1; $this->j<=6; $this->j++){
	                if($thisValue == $this->j){$this->thisSelection = "selected";}else{$this->thisSelection = "";}
	                switch($this->j){
	                	case 1: $this->MatterName = "MAESTRÍA"; break;
	                	case 2: $this->MatterName = "LICENCIATURA"; break;
	                	case 3: $this->MatterName = "TÉCNICO SUPERIOR UNIVERSITARIO"; break;
	                	case 4: $this->MatterName = "BACHILLERATO"; break;
	                	case 5: $this->MatterName = "EQUIVALENTE A BACHILLERATO"; break;
	                	case 6: $this->MatterName = "SECUNDARIA"; break;
	                }
	                $this->result .= "<option value='".$this->j."' ".$this->thisSelection.">".$this->MatterName."</option>";
	            }
				$this->result .= "</select>";
				return $this->result;
			}

			public function cat_ModTitulacion($divName, $thisValue){
				$this->result = "";
				$this->result .= "<select class='form-control' id='".$divName."' name='".$divName."'>";
	        	$this->result .= "	<option value=''>SELECCIONAR</option>";
	            for($this->j=1; $this->j<=6; $this->j++){
	                if($thisValue == $this->j){$this->thisSelection = "selected";}else{$this->thisSelection = "";}
	                switch($this->j){
	                	case 1: $this->TitulationName = "POR TESIS"; break;
	                	case 2: $this->TitulationName = "POR PROMEDIO"; break;
	                	case 3: $this->TitulationName = "POR ESTUDIOS DE POSGRADOS"; break;
	                	case 4: $this->TitulationName = "POR EXPERIENCIA LABORAL"; break;
	                	case 5: $this->TitulationName = "POR CENEVAL"; break;
	                	case 6: $this->TitulationName = "OTRO"; break;
	                }
	                $this->result .= "<option value='".$this->j."' ".$this->thisSelection.">".$this->TitulationName."</option>";
	            }
				$this->result .= "</select>";
				return $this->result;
			}

			public function cat_FundLegal($divName, $thisValue){
				$this->result = "";
				$this->result .= "<select class='form-control' id='".$divName."' name='".$divName."'>";
	        	$this->result .= "	<option value=''>SELECCIONAR</option>";
	            for($this->j=1; $this->j<=5; $this->j++){
	                if($thisValue == $this->j){$this->thisSelection = "selected";}else{$this->thisSelection = "";}
	                switch($this->j){
	                	case 1: $this->LegalName = "ART. 52 LRART. 5 CONST"; break;
	                	case 2: $this->LegalName = "ART. 55 LRART. 5 CONST"; break;
	                	case 3: $this->LegalName = "ART. 91 RLRART. 5 CONST"; break;
	                	case 4: $this->LegalName = "ART. 10 REGLAMENTO PARA LA PRESTACIÓN DEL SERVICIO SOCIAL DE LOS ESTUDIANTES DE LAS INSTITUCIONES DE EDUCACIÓN SUPERIOR EN LA REPÚBLICA MEXICANA"; break;
	                	//case 4: $this->LegalName = "ART. 10 REGLAMENTO PARA LA PRESTACIÓN DEL SERVICIO SOCIAL..."; break;
	                	case 5: $this->LegalName = "NO APLICA"; break;
	                }
	                $this->result .= "<option value='".$this->j."' ".$this->thisSelection.">".$this->LegalName."</option>";
	            }
				$this->result .= "</select>";
				return $this->result;
			}

			public function cat_ServSocial($divName, $thisValue){
				$this->result = "";
				$this->result .= "<select class='form-control' id='".$divName."' name='".$divName."'>";
	        	$this->result .= "	<option value=''>SELECCIONAR</option>";
	            for($this->j=0; $this->j<=1; $this->j++){
	                if($thisValue == $this->j){$this->thisSelection = "selected";}else{$this->thisSelection = "";}
	                switch($this->j){
	                	case 0: $this->ServName = "NO"; break;
	                	case 1: $this->ServName = "SI"; break;
	                }
	                $this->result .= "<option value='".$this->j."' ".$this->thisSelection.">".$this->ServName."</option>";
	            }
				$this->result .= "</select>";
				return $this->result;
			}

			public function sanear_Acents($string){
		        $this->string = trim($string);
		        $this->string = str_replace(
		            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
		            array('&amp;aacute;', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
		            $this->string
		        );
		        $this->string = str_replace(
		            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
		            array('&amp;eacute;', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
		            $this->string
		        );
		        $this->string = str_replace(
		            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
		            array('&amp;iacute;', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
		            $this->string
		        );
		        $this->string = str_replace(
		            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
		            array('&amp;oacute;', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
		            $this->string
		        );
		        $this->string = str_replace(
		            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
		            array('&amp;uacute;', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
		            $this->string
		        );
		        $this->string = str_replace(
		            array('ñ', 'Ñ', 'ç', 'Ç', ' '),
		            array('n', 'N', 'c', 'C', '_'),
		            $this->string
		        );
		        $this->string = str_replace(
		            array('1900-01-01'),
		            array('&amp;nbps;',),
		            $this->string
		        );
		        return $this->string;
		    }

			public function getCatalogueData($thisCatId){
				switch($thisCatId){
					case "EntFed":
						$this->totalCells	= 3;
						$this->thisTable 	= "cat_Entidad_Federativa";
						$this->thisFilter	= "";
						$this->cellName[1]	= "ID_ENTIDAD_FEDERATIVA";
						$this->cellName[2]	= "C_NOM_ENT";
						$this->cellName[3]	= "C_ENTIDAD_ABR";
						$this->rowName[1]	= "ID_ENTIDAD_FEDERATIVA";
						$this->rowName[2]	= "C_NOM_ENT";
						$this->rowName[3]	= "C_ENTIDAD_ABR";
					break;
					case "Obs":
						$this->totalCells	= 6;
						$this->thisTable 	= "OBSERVACION_CERTIFICADO";
						$this->thisFilter	= "";
						$this->cellName[1]	= "ID";
						$this->cellName[2]	= "CODE_VALUE_KEY";
						$this->cellName[3]	= "STATUS";
						$this->cellName[4]	= "ID OBSERVACIÓN";
						$this->cellName[5]	= "DESCRIPCIÓN";
						$this->cellName[6]	= "DESCRIPCIÓN CORTA";
						$this->rowName[1]	= "ID";
						$this->rowName[2]	= "CODE_VALUE_KEY";
						$this->rowName[3]	= "STATUS";
						$this->rowName[4]	= "ID_OBSERVACION";
						$this->rowName[5]	= "DESCRIPCION";
						$this->rowName[6]	= "DESCRIPCION_CORTA";
					break;
					case "nivel_de_estudio":
						$this->totalCells	= 3;
						$this->thisTable 	= "cat_Nivel_de_estudios";
						$this->thisFilter	= "";
						$this->cellName[1]	= "id";
						$this->cellName[2]	= "idNivelEstudios";
						$this->cellName[3]	= "descripcion";
						$this->rowName[1]	= "id";
						$this->rowName[2]	= "idNivelEstudios";
						$this->rowName[3]	= "descripcion";
					break;
					case "FirAut": 
						$this->totalCells	= 2;
						$this->thisTable 	= "cat_Cargo_Firmante_Autorizado";
						$this->thisFilter	= "";
						$this->cellName[1]	= "ID_CARGO";
						$this->cellName[2]	= "CARGO_FIRMANTE";
						$this->rowName[1]	= "ID_CARGO";
						$this->rowName[2]	= "CARGO_FIRMANTE";
					break;
					case "tipoperiodo": 
						$this->totalCells	= 2;
						$this->thisTable 	= "cat_Tipo_de_periodo";
						$this->thisFilter	= "";
						$this->cellName[1]	= "idTipoPeriodo";
						$this->cellName[2]	= "TIPO_PERIODO";
						$this->rowName[1]	= "idTipoPeriodo";
						$this->rowName[2]	= "TIPO_PERIODO";
					break;
					case "genero": 
						$this->totalCells	= 2;
						$this->thisTable 	= "cat_Genero";
						$this->thisFilter	= "";
						$this->cellName[1]	= "idGenero";
						$this->cellName[2]	= "Genero";
						$this->rowName[1]	= "idGenero";
						$this->rowName[2]	= "Genero";
					break;
					case "tipo_certificacion": 
						$this->totalCells	= 2;
						$this->thisTable 	= "cat_Tipo_certificacion";
						$this->thisFilter	= "";
						$this->cellName[1]	= "idTipoCertificacion";
						$this->cellName[2]	= "TIPOCERT";
						$this->rowName[1]	= "idTipoCertificacion";
						$this->rowName[2]	= "TIPOCERT";
					break;
					case "campus": 
						$this->totalCells	= 10;
						$this->thisTable 	= "ENTIDAD_CERTIFICADO";
						$this->thisFilter	= "ORDER BY DESCRIPCION_CERT ASC";
						$this->cellName[1]	= "CLAVE_CAMPUS";
						$this->cellName[2]	= "CLAVE_CAMPUS";
						$this->cellName[3]	= "ACADEMIC_SESSION";
						$this->cellName[4]	= "ORG_CODE_ID";
						$this->cellName[5]	= "ID_ENTIDAD";
						$this->cellName[6]	= "ENTIDAD";
						$this->cellName[7]	= "ID_INSTITUCION";
						$this->cellName[8]	= "CAMPUS";
						$this->cellName[9]	= "ENTIDAD_TITULO";
						$this->cellName[10]	= "DESCRIPCION_CERT";
						$this->rowName[1]	= "CLAVE_CAMPUS";
						$this->rowName[2]	= "CLAVE_CAMPUS";
						$this->rowName[3]	= "ACADEMIC_SESSION";
						$this->rowName[4]	= "ORG_CODE_ID";
						$this->rowName[5]	= "ID_ENTIDAD";
						$this->rowName[6]	= "ENTIDAD";
						$this->rowName[7]	= "ID_INSTITUCION";
						$this->rowName[8]	= "CAMPUS";
						$this->rowName[9]	= "ENTIDAD_TITULO";
						$this->rowName[10]	= "DESCRIPCION_CERT";
					break;
					case "EstAnt": 
						$this->totalCells	= 3;
						$this->thisTable 	= "cat_Estudio_Antecedente";
						$this->thisFilter	= "";
						$this->cellName[1]	= "ID_TIPO_ESTUDIO_ANTECEDENTE";
						$this->cellName[2]	= "TIPO_ESTUDIO_ANTECEDENTE";
						$this->cellName[3]	= "TIPO_EDUCATIVO_DEL_ANTECEDENTE";
						$this->rowName[1]	= "ID_TIPO_ESTUDIO_ANTECEDENTE";
						$this->rowName[2]	= "TIPO_ESTUDIO_ANTECEDENTE";
						$this->rowName[3]	= "TIPO_EDUCATIVO_DEL_ANTECEDENTE";
					break;
					case "FunLeg": 
						$this->totalCells	= 2;
						$this->thisTable 	= "cat_Fundamento_Legal_Servicio_Social";
						$this->thisFilter	= "";
						$this->cellName[1]	= "ID Fundamento Legal Servicio Social";
						$this->cellName[2]	= "Fundamento Legal Servicio Social";
						$this->rowName[1]	= "ID_FUNDAMENTO_LEGAL_SERVICIO_SOCIAL";
						$this->rowName[2]	= "FUNDAMENTO_LEGAL_SERVICIO_SOCIAL";
					break;
					case "ModTit": 
						$this->totalCells	= 3;
						$this->thisTable 	= "cat_Modalidad_Titulacion";
						$this->thisFilter	= "";
						$this->cellName[1]	= "ID";
						$this->cellName[2]	= "Modalidad Titulaci&oacute;n";
						$this->cellName[3]	= "Tipo de Modalidad";
						$this->rowName[1]	= "ID_MODALIDAD_TITULACION";
						$this->rowName[2]	= "MODALIDAD_TITULACION";
						$this->rowName[3]	= "TIPO_DE_MODALIDAD";
					break;
					case "AutRec": 
						$this->totalCells	= 3;
						$this->thisTable 	= "cat_Autorizacion_Reconocimiento";
						$this->thisFilter	= "";
						$this->cellName[1]	= "id";
						$this->cellName[2]	= "ID_AUTORIZACION_RECONOCIMIENTO";
						$this->cellName[3]	= "AUTORIZACIÓN_RECONOCIMIENTO";
						$this->rowName[1]	= "id";
						$this->rowName[2]	= "ID_AUTORIZACION_RECONOCIMIENTO";
						$this->rowName[3]	= "AUTORIZACION_RECONOCIMIENTO";
					break;
					case "RVOE": 
						$this->totalCells	= 7;
						$this->thisTable 	= "Rvoe";
						$this->thisFilter	= "";
						$this->cellName[1]	= "ID";
						$this->cellName[2]	= "Org Code ID";
						$this->cellName[3]	= "ID Req";
						$this->cellName[4]	= "Status";
						$this->cellName[5]	= "Num";
						$this->cellName[6]	= "Mod";
						$this->cellName[7]	= "Lev";
						$this->rowName[1]	= "RvoeId";
						$this->rowName[2]	= "OrgCodeId";
						$this->rowName[3]	= "DegreeRequirementId";
						$this->rowName[4]	= "RvoeStatusId";
						$this->rowName[5]	= "AgreementNumber";
						$this->rowName[6]	= "Modality";
						$this->rowName[7]	= "Level";
					break;
					case "Carrera": 
						$this->totalCells	= 8;
						$this->thisTable 	= "CARRERA_CERTIFICADO";
						$this->thisFilter	= "";
						$this->cellName[1]	= "Carrerra";
						$this->cellName[2]	= "Instituci&oacute;n";
						$this->cellName[3]	= "Nivel";
						$this->cellName[4]	= "Clave Carrera";
						$this->cellName[5]	= "Descripci&oacute;n";
						$this->cellName[6]	= "Curriculum";
						$this->cellName[7]	= "Id Ca";
						$this->cellName[8]	= "Titulos";
						$this->rowName[1]	= "ID_CARRERA";
						$this->rowName[2]	= "ID_INSTITUCION";
						$this->rowName[3]	= "ID_NIVEL";
						$this->rowName[4]	= "CLAVE_CARRERA";
						$this->rowName[5]	= "DESCRIPCION_CARRERA";
						$this->rowName[6]	= "CURRICULUM";
						$this->rowName[7]	= "ID_CA";
						$this->rowName[8]	= "DESCRIPCION_TITULOS";
					break;
					case "Asignaturas": 
						$this->totalCells	= 8;
						$this->thisTable 	= "PROGRAMA_CERTIFICADO";
						$this->thisFilter	= "";
						$this->cellName[1]	= "ID_ASIGNATURA";
						$this->cellName[2]	= "ID_CARRERA";
						$this->cellName[3]	= "NO_RVOE";
						$this->cellName[4]	= "DESCRIPCION";
						$this->cellName[5]	= "CLAVE_ASIGNATURA";
						$this->cellName[6]	= "CLAVE_CARRERA";
						$this->cellName[7]	= "ID";
						$this->cellName[8]	= "PROGRAMA";
				        $this->rowName[1]	= "ID_ASIGNATURA";
						$this->rowName[2]	= "ID_CARRERA";
						$this->rowName[3]	= "NO_RVOE";
						$this->rowName[4]	= "DESCRIPCION";
						$this->rowName[5]	= "CLAVE_ASIGNATURA";
						$this->rowName[6]	= "CLAVE_CARRERA";
						$this->rowName[7]	= "ID";
						$this->rowName[8]	= "PROGRAMA";
					break;
				}
				self::CnnBD();
				self::xQueryI("SELECT * FROM ".$this->thisTable." ".$this->thisFilter);
				echo "		<div class='container col-lg-12'>";
				echo "			<table id='".$thisCatId."' class='table table-rounded table-striped' width='100%' cellspacing='0'>";
				echo "		   		<thead>";
				echo "		  			<tr class='bg-primary rounded-top text-white' style='text-align:center; background-color:#0054a4 !important;'>";
	            for($this->i=1; $this->i <= $this->totalCells; $this->i++){
					echo "		  			<th style='text-align:center;'>".$this->cellName[$this->i]."</th>";
	            }
				echo "		    		</tr>";
				echo "				</thead>";
				echo "		    	<tbody>";
				while($this->data = sqlsrv_fetch_array($this->rQuery)){
					echo "				<tr style='height:60px; vertical-align:middle; font-size:12px;'>";
		            for($this->i=1; $this->i <= $this->totalCells; $this->i++){
						echo "				<td style='vertical-align:middle; text-align:center;'>".utf8_encode($this->data[$this->rowName[$this->i]])."</td>";
		            }
					echo "				</tr>";
				}
				echo "		  		</tbody>";
				echo "			</table>";
				echo "		</div>";
			}

			public function cmbCampusId(){
				if($_SESSION["campus"] === "SUR1"){
					$this->CampusID = "SUR";
				}else{
					$this->CampusID = $_SESSION["campus"];
				}
				self::CnnBD();
				self::xQueryI("SELECT ENTIDAD_CERTIFICADO.CLAVE_CAMPUS,ENTIDAD_CERTIFICADO.CAMPUS FROM ENTIDAD_CERTIFICADO WHERE CAMPUS LIKE '%".$this->CampusID."%'");
				if($this->data = sqlsrv_fetch_array($this->rQuery)){
					$this->result = $this->data["CLAVE_CAMPUS"];
				}
				return $this->result;
			}

			public function cmbCampusTitulos(){
				self::CnnBD();
				switch($_SESSION["rol"]){
					case 1:
					case 3:
					case 4:
					case 5:
						self::xQueryI("SELECT ENTIDAD_CERTIFICADO.CLAVE_CAMPUS,ENTIDAD_CERTIFICADO.CAMPUS FROM ENTIDAD_CERTIFICADO");
					break;
					default:
						self::xQueryI("SELECT ENTIDAD_CERTIFICADO.CLAVE_CAMPUS,ENTIDAD_CERTIFICADO.CAMPUS FROM ENTIDAD_CERTIFICADO WHERE ACADEMIC_SESSION = '".$_SESSION["campus"]."'");
					break;
				}
				switch($_SESSION["campus"]){
					case 'SUR1': $this->FindStr = "SUR "; break;
					default: $this->FindStr = $_SESSION["campus"]; break;
				}
				while($this->data = sqlsrv_fetch_array($this->rQuery)){
					$this->FindType = strpos(strtolower(utf8_encode($this->data["CAMPUS"])), strtolower($this->FindStr));
					if($this->FindType === false){$this->SelectedCampus = "";}else{$this->SelectedCampus = "selected";}
					$this->PlantelName = str_replace("UNIVERSIDAD INSURGENTES PLANTEL", "", $this->data["CAMPUS"]);
					$this->PlantelName = str_replace("UNIVERSIDAD INSURGENTES, PLANTEL", "", $this->PlantelName);
					$this->result .= '<option value="'.$this->data["CLAVE_CAMPUS"].'" '.$this->SelectedCampus.'>'.utf8_encode($this->PlantelName).'</option>';
				}
				return $this->result;
			}

			public function cmbCarreraTitulos($id){
				self::CnnBD();
				self::xQueryI("SELECT DISTINCT PROGRAMA_CERTIFICADO.CLAVE_CAMPUS,PROGRAMA_CERTIFICADO.CLAVE_CARRERA , CARRERA_CERTIFICADO.DESCRIPCION_TITULOS FROM PROGRAMA_CERTIFICADO INNER JOIN CARRERA_CERTIFICADO ON PROGRAMA_CERTIFICADO.CLAVE_CARRERA=CARRERA_CERTIFICADO.CLAVE_CARRERA WHERE PROGRAMA_CERTIFICADO.CLAVE_CAMPUS='".$id."'");
				while ($this->data = sqlsrv_fetch_array($this->rQuery)){
					$this->result .= '<option value="'.$this->data["CLAVE_CARRERA"].'">'.utf8_encode($this->data["DESCRIPCION_TITULOS"]).'</option>';
				}
				return $this->result;
			}

			public function Notificaciones($thisTable, $thisFilter){
				self::CnnBD();
				self::xQueryI("SELECT * FROM ".$thisTable." ".$thisFilter);
				$TotalRcds = 0;
				while ($this->data = sqlsrv_fetch_array($this->rQuery)) {
					$TotalRcds = $TotalRcds + 1;
                    if($TotalRcds % 2 == 0){
                        $CelBgColor = "#f1f2f3;";
                    }else{
						$CelBgColor = "#ffffff;";
					}
					switch ($this->data["SERVICIO_PROFESIONAL_ID"]){
						case 1:
							$ThisServiceName 	= "Servicio Social"; 
							$thisSection 		= "servicio-social";
							break;
						case 2:
							$ThisServiceName 	= "Pr&aacute;cticas Profesionales";
							$thisSection 		= "practicas-profesionales";
							break;
						case 3:
							$ThisServiceName 	= "Titulaci&oacute;n";
							$thisSection 		= "titulacion";
							break;
					}
					$this->FechaRegistro 	= self::formatDate($this->data["FECHA_HISTORICO"]->format('Y-m-d'));
					$this->StatusName		= self::getThisRcd("DESCRIPCION_ESTATUS", "ACC_CAT_ESTATUS_SPR_SP", "WHERE ESTATUS_ID = ".$this->data["ESTATUS_ID"]);
					$this->StatusName		= str_replace("[SERVICIO]", $ThisServiceName, $this->StatusName);
					$this->MatricSession	= self::getThisRcd("MATRIC_SESSION", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->data["ACC_STDDEGREQ_SS_ID"]);
					$this->SocialService	= self::getThisRcd("DESCRIPCION_SERVICIO_PROFESIONAL", "ACC_CAT_SERVICIO_PROFESIONAL_SS", "WHERE ID = ".$this->data["SERVICIO_PROFESIONAL_ID"]);
					if($this->data["ESTATUS_ID"] == 0){
						$DisplayRcd = false;
					}else{
						if(strtoupper($_SESSION["campus"]) === "NULL"){
							$DisplayRcd = true;
						}else{
							if($_SESSION["campus"] === $this->MatricSession){
								$DisplayRcd = true;
							}else{
								$DisplayRcd = false;
							}
						}
					}
					switch ($this->StatusName) {
						case "Carta de aceptación guardada";
							$SubSection = "registros-tramite";
							break;
						default:
							$SubSection = "solicitudes-validacion";
							break;
					}
					if($DisplayRcd){
						echo "	<a href='?ty=".$thisSection."&SecId=".$SubSection."'>";
						echo "		<div style='background-color:".$CelBgColor."; padding:10px;'>";
						echo "			<b>".$this->SocialService.":</b> <small><span style='float:right;'><i class='fa fa-calendar'></i> ".$this->FechaRegistro."</span></small><br /><i class='fa fa-user'></i> ".$this->StatusName."";
						echo "		</div>";
						echo "	</a>";
					}
				}
			}

			function monthName($thisMonth, $thisType){
				setlocale(LC_TIME, 'spanish');
				$nombreMes  = strftime("%B",mktime(0, 0, 0, $thisMonth, 1, 2000));
				$nombreMes  = ucwords($nombreMes);
				switch ($thisType) {
					case 0:
						$nombreMes = substr($nombreMes, 0, 3);
						break;
					default:
						$nombreMes = strtoupper($nombreMes);
						break;
				}
				return $nombreMes;
			}

			function formatDate($thisDate){
				setlocale(LC_TIME, 'spanish');
				$thisYear			= substr($thisDate,0, 4);
				$thisMonth 			= self::monthName(substr($thisDate,5, 2), 0);
				$thisDay			= substr($thisDate,8, 2);
				$FrtDate			= $thisDay."/".$thisMonth."/".$thisYear;
				return $FrtDate;
			}
		// ****************************************************************************************
		// ****************************************************************************************
	}
?>
