<?php
    if($SecureSection){
        securityCore();
    }

    function securityCore(){
	    $filterQR = array("*","^","'","+",'"',"//","()","'or'",'"x"','"y"',"='","dump","alert","script","javascript","session","true","false","like","char","select","insert","update","union","delete","from","table","where","truncate","drop","group_concat","create","find","md5","benchmark","sleep","now","phpsessid","die(pi()*");
	    //$filterQR = array("< ",">","[","]","*","^","'","alert","script","javascript","like","char","select","insert","update","delete","from","table","where","undo","truncate","drop","sql","create","find");
	    foreach($_REQUEST as $nombreVariable => $valorVariable){
	        $thisQR                     = $valorVariable;
	        $thisQR                     = addslashes($thisQR);
	        $thisQR                     = strtolower($thisQR);
	        $_REQUEST[$valorVariable]   = addslashes($valorVariable);

	        $_REQUEST[$valorVariable]   = str_replace($filterQR,"",$valorVariable);
	        $_REQUEST[$valorVariable]   = @mysqli_real_escape_string($valorVariable);
	        $_REQUEST[$valorVariable]   = htmlspecialchars($valorVariable, ENT_QUOTES);
	        //if(strlen($thisQR) == 0 && isset($thisQR)){
	        //    inyectionReport('Empty Var => ', $nombreVariable);
	        //}else{
	            if(findThreat($thisQR, $filterQR, 1)){
	                inyectionReport($nombreVariable, $valorVariable);
	            }else{
	                if(in_array($thisQR, $filterQR)){inyectionReport($nombreVariable, $valorVariable);};
	            }
	        //}
	    } 
    }

    function findThreat($haystack, $needles=array(), $offset=0){
        $chr = array();
        foreach($needles as $needle){
            $res = strpos($haystack, $needle, $offset);
            if ($res !== false) $chr[$needle] = $res;
        }
        if(empty($chr)) return false;
        return min($chr);
    }

    function inyectionReport($thisVar, $thisThreat){
        @session_start();
        if(!isset($_SESSION["attacker"])){$_SESSION["attacker"] = 0;}else{$_SESSION["attacker"] = $_SESSION["attacker"] + 1;}
        $pathFile       = __ROOT__."/reportes";
        if(!file_exists($pathFile)){mkdir($pathFile, 0777, true);}
        $fileReport     = $pathFile."/data.log";
        $reportThreat   = "DATE = ".systemDate()." | IP = ".getHackerIP()." | VAR = [".$thisVar."] | THREAT = [".$thisThreat."]";
        genReport($fileReport,$reportThreat);
        session_destroy();
        header("Location: http://".$_SERVER['SERVER_NAME']);
    }

    function genReport($thisFile, $thisContent){
        $fileReport = fopen($thisFile, "a");
        fwrite($fileReport, $thisContent.PHP_EOL);
        fclose($fileReport);
    }

    function systemDate(){
        date_default_timezone_set('America/Mexico_City');
        return date("Y-m-d H:i:s");
    }

    function getHackerIP(){
        return "";
    }
?>