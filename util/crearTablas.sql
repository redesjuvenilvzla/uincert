DROP TABLE [dbo].[Info_Validacion]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Info_Validacion](
        [id] [int] IDENTITY(1,1) NOT NULL,
        [folioControl] [varchar](100) NULL,
        [cveCarrera] [varchar](100) NULL,
        [fechaInicio] [date] NULL,
        [fechaTerminacion] [date] NULL,
        [idAutorizacionReconocimiento] [tinyint] NULL,
        [autorizacionReconocimiento] [varchar](100) NULL,
        [correoElectronico] [varchar](100) NULL,
        [fechaExpedicion] [date] NULL,
        [idModalidadTitulacion] [tinyint] NULL,
        [modalidadTitulacion] [varchar](100) NULL,
        [fechaExamenProfesional] [date] NULL,
        [fechaExencionExamenProfesional] [date] NULL,
        [cumplioServicioSocial] [varchar](100) NULL,
        [idFundamentoLegalServicioSocial] [tinyint] NULL,
        [fundamentoLegalServicioSocial] [varchar](200) NULL,
        [idEntidadFederativaExpedicion] [tinyint] NULL,
        [entidadFederativaExpedicion] [varchar](100) NULL,
        [institucionProcedencia] [varchar](200) NULL,
        [idTipoEstudioAntecedente] [tinyint] NULL,
        [tipoEstudioAntecedente] [varchar](100) NULL,
        [idEntidadFederativaAntecedente] [tinyint] NULL,
        [entidadFederativaAntecedente] [varchar](100) NULL,
        [fechaInicio2] [date] NULL,
        [fechaTerminacion2] [date] NULL,
        [noCedula] [varchar](100) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Info_Validacion] ADD PRIMARY KEY CLUSTERED
(
        [id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


DROP TABLE [dbo].[REG_Validacion]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REG_Validacion](
        [id_vplantel] [int] IDENTITY(1,1) NOT NULL,
        [vp_id_unico_cert] [varchar](100) NULL,
        [vp_estatus] [varchar](100) NULL,
        [vp_fec_mov] [date] NULL,
        [vp_type] [char](50) NULL
) ON [PRIMARY]
GO


DROP TABLE [dbo].[reg_usu]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reg_usu](
        [id] [int] IDENTITY(1,1) NOT NULL,
        [usu_nombre] [varchar](60) NULL,
        [usu_contrasena] [varchar](30) NULL,
        [usu_correo] [varchar](60) NULL,
        [usu_pregunta] [varchar](60) NULL,
        [usu_respuesta] [varchar](60) NULL,
        [id_rol] [tinyint] NULL,
        [usu_fecmov] [date] NULL
) ON [PRIMARY]
GO


ALTER TABLE [dbo].[reg_usu] ADD PRIMARY KEY CLUSTERED
(
        [id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


INSERT INTO reg_usu
VALUES ('adminUIN1.', 'p455w0rd$', 'admincorreo','pregunta','respuesta',1,'');


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cat_Autorizacion_Reconocimiento](
        [id] [int] IDENTITY(1,1) NOT NULL,
        [name] [varchar](100) NULL
) ON [PRIMARY]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cat_Estados](
        [id] [int] IDENTITY(1,1) NOT NULL,
        [name] [varchar](100) NULL,
        [description] [char](50) NULL
) ON [PRIMARY]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cat_Estudio_Antecedente](
        [id] [int] IDENTITY(1,1) NOT NULL,
        [name] [varchar](100) NULL,
        [description] [varchar](100) NULL
) ON [PRIMARY]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cat_Firmantes_Autorizados](
        [id] [int] IDENTITY(1,1) NOT NULL,
        [name] [varchar](100) NULL
) ON [PRIMARY]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cat_Fundamento_Legal](
        [id] [int] IDENTITY(1,1) NOT NULL,
        [name] [varchar](200) NULL
) ON [PRIMARY]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cat_Modalidad_Titulacion](
        [id] [int] IDENTITY(1,1) NOT NULL,
        [name] [varchar](100) NULL,
        [description] [varchar](100) NULL
) ON [PRIMARY]
GO


ALTER TABLE dbo.REG_Validacion ADD vp_razon_rechazo text NULL;
ALTER TABLE dbo.REG_usu ADD usu_campus [varchar](50) NULL;