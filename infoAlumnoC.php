﻿
<!--<script src="https://code.jquery.com/jquery-1.8.3.min.js"></script>-->
<script>
	$(document).ready(function(){
		$('#ValidaAlumno').click(function(e){
			e.preventDefault();
			var data = $(this).serializeArray();
			data.push({name:'SecType', value:'CER'});
			data.push({name:'StudentId', value:$('#IdCertificado').val()});
			data.push({name:'Status', value:'Validado por CSE'});
			$.ajax({
				url: 'valplantel.php',
				type: 'post',
				dataType: 'json',
				data: data
			})
			.always(function(){
				var thisId  = '#img'+$('#IdAlumno').val();
				var name    = '#est'+$('#IdAlumno').val();
           		var chkStud = '#CheckAlumno_P'+$('#IdAlumno').val();

	  			var thisCampus 		= $('#cmbCampus').val();
	  			var thisAnho 		= $('#anho').val();
	  			var thisFInicio 	= $('#finicio').val();
	  			var thisFFinal 		= $('#ffin').val();
	  			var thisTipCert		= $('#cmbTipCertificado').val();

				if(thisTipCert == "Ambos"){
					thisTipCert = "'T','P'";
				}else{
					if(thisTipCert == "Total"){
						thisTipCert = "'T'";    
					}else{
						thisTipCert = "'P'";
					}
				}

				$(name).val('Validado por CSE');
       			$("#ValButtons").fadeOut(800);
				$(thisId).removeClass('text-warning fa fa-exclamation').addClass('text-success fa fa-check');
                var chkStud = '#CheckAlumno_P'+$('#IdAlumno').val();
                // $('html, body').animate({scrollTop: 150}, 1500, function(){
					$.post( 
						"CertificadosCoordinacionList.php",{
							campus: thisCampus,
							tipCert: thisTipCert,
							anho: '2014',
							finicial: thisFInicio,
							ffinal: thisFFinal
						},
						function(data){
							$('#ListAlumnos').html(data);
						}
					);
                $('#infoModal').modal('hide');
                // });
			});
		})
	})

	$(document).ready(function(){
		$('#RechazaAlumno').click(function(e){
			e.preventDefault();
			var data = $(this).serializeArray();
			data.push({name:'SecType', value:'CER'});
			data.push({name:'StudentId', value:$('#IdCertificado').val()});
			data.push({name:'Status', value:'Rechazado por CSE'});
            data.push({name:'RazonRechazo', value:$('#razon_rechazo').val()});

            $(".tab-pane").removeClass("active");
            $("#RAZON").removeClass("fade");
            $("#RAZON").addClass("active");
            
            if($('#razon_rechazo').val().length == 0){
                alert("Es Necesario que introduzca la razon del rechazo del alumno");
            }else{
                var opcion = confirm("Esta completamente seguro de querer Rechazar a este Alumno?");
                if(opcion == true){
					$.ajax({
						url: 'valplantel.php',
						type: 'post',
						dataType: 'json',
						data: data
					})
					.always(function(){
						var thisId  = '#img'+$('#IdAlumno').val();
						var name    = '#est'+$('#IdAlumno').val();
                        var raz     = '#raz_P'+$('#IdAlumno').val();
                        var chkStud = '#CheckAlumno_P'+$('#IdAlumno').val();
						$(name).val('Rechazado por CSE');
                        $(raz).html("<i class='text-warning fa fa-user-times'></i>");
						$(thisId).removeClass('text-success fa fa-check').addClass('text-warning fa fa-exclamation');
        				// $("html, body").animate({scrollTop: 150}, 1500, function(){
        					$(chkStud).html('');
        				// });
                        $('#infoModal').modal('hide');
					});
				}
			}
		})
	})
</script>

<?php
$SecureSection = false;
require_once("util/utilerias.php");
require_once("./config/xData.php");

$obj = new Utilerias;
$obj->CnnBD();

$CAMPUS 		= $_GET['campus'];
$ANHO 			= $_GET['anho'];
$FINICIO 		= $_GET['finicio'];
$FFINAL 		= $_GET['ffinal'];
$vNumControl	= $_GET['vNumControl'];
$TIPCERT 		= $_GET['tipCert'];
$VALPLANTEL 	= " INNER JOIN REG_Validacion RVP ON RVP.VP_ID_UNICO_CERT = TT.PEOPLE_CODE_ID+TT.folioControl+TT.campus ";
$CURRICULUM 	= $_GET['curiculum'];

	//AND RC.PEOPLE_CODE_ID = 'P000008520'
$NUMCONTROL = "AND RC.PEOPLE_CODE_ID = '".$vNumControl."'";
$query = str_replace("VARIABLE=CAMPUS", $CAMPUS, CERTIFICADOSALTAS);
$query = str_replace("VARIABLE=TIPO_CERTIFICADO", "AND (RC.TIPO_CERTIFICADO IN (".$TIPCERT."))", $query);
$query = str_replace("VARIABLE=ANHO", $ANHO, $query);
$query = str_replace("VARIABLE=FINICIAL", $FINICIO, $query);
$query = str_replace("VARIABLE=FFINAL", $FFINAL, $query);
$query = str_replace("VARIABLE=NUMCONTROL", $NUMCONTROL, $query);
$query = str_replace("VARIABLE=VALPLANTEL", $VALPLANTEL, $query);
$query = str_replace("VARIABLE=CURRICULUM", $CURRICULUM, $query);
$query = str_replace("VARIABLE=TOP", " TOP 1 ", $query);

$rQuery = $obj->xQuery($query);

$ValidateGranted = true;
while ($data = sqlsrv_fetch_array($rQuery)) {
	$nombreCampus = utf8_encode($data["campus"]);
	echo "
	<h5>".utf8_encode($data["numeroControl"])."  ".utf8_encode($data["nombre3"])." ".utf8_encode($data["primerApellido4"])." ".utf8_encode($data["segundoApellido5"])." <span class='label label-default'></span></h5>
	<ul class='nav nav-tabs' role='tablist'>
		<li class='nav-item'><a class='nav-link active' data-toggle='tab' href='#GENERAL'>General</a></li>
		<li class='nav-item'><a class='nav-link' data-toggle='tab' href='#IPES'>Ipes</a></li>
		<li class='nav-item'><a class='nav-link' data-toggle='tab' href='#RESPONSABLE'>Responsable</a></li>
		<li class='nav-item'><a class='nav-link' data-toggle='tab' href='#RVOE'>Rvoe</a></li>
		<li class='nav-item'><a class='nav-link' data-toggle='tab' href='#CARRERA'>Carrera</a></li>
		<li class='nav-item'><a class='nav-link' data-toggle='tab' href='#ALUMNO'>Alumno</a></li>
		<li class='nav-item'><a class='nav-link' data-toggle='tab' href='#EXPEDICION'>Expedici&oacute;n</a></li>
		<li class='nav-item'><a class='nav-link' data-toggle='tab' href='#ASIGNATURAS'>Asignaturas</a></li>
		<li class='nav-item'><a class='nav-link' data-toggle='tab' href='#DOCUMENTOS'>Documentos</a></li>
		<li class='nav-item'><a class='nav-link' data-toggle='tab' href='#RAZON'>Raz&oacute;n Rechazo</a></li>
	</ul>
	";

	echo "	  
	<div class='tab-content'>
	<div id='GENERAL' class='container tab-pane active'><br>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Tip. Certificado</p></div>
	<div class='col-sm-6'>
	<input id='Tip. Certificado' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["tipoCertificado"])."' disabled/>		
	</div>
	</div>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Fol. Control</p></div>
	<div class='col-sm-6'>
	<input id='Fol. Control' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["folioControl"])."' disabled />		
	</div>
	</div>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Id Unico Certificado</p></div>
	<div class='col-sm-6'>
	<input id='IdCertificado' name='IdCertificado' type='text' size='29' autocomplete='false' class='form-control' value='".utf8_encode($data["ID_UNICO_CERT"])."' disabled/>		
	</div>
	</div>                
	</div>
	<div id='IPES' class='container tab-pane fade'><br>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Nom. Institucíon</p></div>
	<div class='col-sm-6'>
	<input id='Nom. Institucíon' type='text' size='60' autocomplete='false' class='form-control' value='UNIVERSIDAD INSURGENTES' disabled />		
	</div>
	</div>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Campus</p></div>
	<div class='col-sm-6'>
	<input id='Campus' type='text' size='60' autocomplete='false' class='form-control' value='".$nombreCampus."' disabled />		
	</div>
	</div>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Ent. Federativa</p></div>
	<div class='col-sm-6'>
	<input id='Ent. Federativa' type='text' size='60' autocomplete='false' class='form-control' value='".utf8_encode($data["entidadFederativa"])."' disabled />		
	</div>
	</div>				
	</div>
	<div id='RESPONSABLE' class='container tab-pane fade'><br>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Nombre</p></div>
	<div class='col-sm-6'>
	<input id='Nombre' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["nombre"])."' disabled />		
	</div>
	</div>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>A. Paterno</p></div>
	<div class='col-sm-6'>
	<input id='A. Paterno' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["primerApellido"])."' disabled />		
	</div>
	</div>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>A. Materno</p></div>
	<div class='col-sm-6'>
	<input id='A. Materno' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["segundoApellido"])."' disabled />		
	</div>
	</div>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Cargo</p></div>
	<div class='col-sm-6'>
	<input id='Cargo' type='text' size='60' autocomplete='false' class='form-control' value='".utf8_encode($data["cargo"])."' disabled />		
	</div>
	</div>			
	</div>
	<div id='RVOE' class='container tab-pane fade'><br>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Número RVOE</p></div>
	<div class='col-sm-6'>
	<input id='Número' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["numero"])."' disabled />
	</div>
	</div>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Fec. Expedición </p></div>
	<div class='col-sm-6'>
	<input id='Fec. Expedición ' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["fechaExpedicion"])."'  disabled />
	</div>
	</div>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Clve. Plan</p></div>
	<div class='col-sm-6'>
	<input id='Clve. Plan' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["clavePlan"])."' disabled />
	</div>
	</div>
	</div>
	<div id='CARRERA' class='container tab-pane fade'><br>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Nom. Carrera</p></div>
	<div class='col-sm-6'>
	<input id='Nom. Carrera' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["nombreCarrera"])."' disabled />
	</div>
	</div>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Tip. Periodo</p></div>
	<div class='col-sm-6'>
	<input id='Tip. Periodo' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["tipoPeriodo"])."' disabled />
	</div>
	</div>
	</div>
	<div id='ALUMNO' class='container tab-pane fade'><br>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>ID</p></div>
	<div class='col-sm-6'>
	<input id='IdAlumno' name='IdAlumno' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["numeroControl"])."' disabled />
	</div>
	</div>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>CURP</p></div>
	<div class='col-sm-6'>
	<input id='CURP' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["curp2"])."' disabled />
	</div>
	</div>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Nombre</p></div>
	<div class='col-sm-6'>
	<input id='Nombre' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["nombre3"])."' disabled />
	</div>
	</div>				
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>A. Paterno</p></div>
	<div class='col-sm-6'>
	<input id='A. Paterno' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["primerApellido4"])."' disabled />
	</div>
	</div>				
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>A. Materno</p></div>
	<div class='col-sm-6'>
	<input id='A. Materno' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["segundoApellido5"])."' disabled />
	</div>
	</div>				
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Género</p></div>
	<div class='col-sm-6'>
	<input id='Id. Genero' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["idGenero"])."' disabled />
	</div>
	</div>				
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Fec. Nacimiento</p></div>
	<div class='col-sm-6'>
	<input id='Fec. Nacimiento' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["fechaNacimiento"])."' disabled />
	</div>
	</div>				
	</div>
	<div id='EXPEDICION' class='container tab-pane fade'><br>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Tip. Certificación</p></div>
	<div class='col-sm-6'>
	<input id='Tip. Certificación' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["tipoCertificacion"])."' disabled />
	</div>
	</div>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Fecha</p></div>
	<div class='col-sm-6'>
	<input id='Fecha' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["fecha"])."' disabled />
	</div>
	</div>
	<div class='row'>				
	<div class='col-sm-3'><p class='navbar-text'>Lugar Expedición</p></div>
	<div class='col-sm-6'>
	<input id='Lugar Espedición' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["lugarExpedicion"])."' disabled />
	</div>
	</div>				
	</div>
	<div id='ASIGNATURAS' class='container tab-pane fade'><br>

		        <div class='row'>               
                    <div class='col-sm-3'><p class='navbar-text'>Total de créditos</p></div>
                    <div class='col-sm-6'>
                        <input id='TOTAL_CREDITOS' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["TOTAL_CREDITOS"])."' disabled />
                    </div>
                </div>

                <div class='row'>               
                    <div class='col-sm-3'><p class='navbar-text'>Créditos obtenidos</p></div>
                    <div class='col-sm-6'>
                        <input id='CREDITOS_OBTENIDOS' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["CREDITOS_OBTENIDOS"])."' disabled />
                    </div>
                </div>

	<div class='tabla table-scroll'>
	<table id='grid' class='table table-rounded table-striped table-sm dataTable'>
	<thead>
	<tr class='bg-primary rounded-top text-white' >
	<th>Clave</th>
	<th>Nombre</th>
	<th>Ciclo</th>
	<th>Calificación</th>
	<th>Observaciones</th>
	<th>id Tipo Asignatura</th>
	<th>Tipo Asignatura</th>
	<th>Créditos</th>
	</tr>
	</thead>
	<tbody>
	";	
	if($data["tipoCertificacion"] === 'TOTAL'){$TC = 'T';} else { $TC = 'P';}
	$queryA = str_replace("VARIABLE=PEOPLE_CODE_ID", $data["PEOPLE_CODE_ID"], CERTIFICADOSALTASMATERIAS);
	$queryA = str_replace("VARIABLE=CAMPUS", $data["ACADEMIC_SESSION"], $queryA);
	$queryA = str_replace("VARIABLE=TIPOCERTIFICADO", $TC, $queryA);
	$queryA = str_replace("VARIABLE=ANHO", substr($data["fecha"], 0, 4), $queryA);
	$queryA = str_replace("VARIABLE=CURRICULUM", $CURRICULUM, $queryA);
	//$queryA = str_replace("VARIABLE=ID_UNICO_CERT", $data["ID_UNICO_CERT"], $queryA);
	$rQueryA = $obj->xQuery($queryA);
	$TRegA = $obj->xCQuery();
	
	//echo $queryA;

	while ($datac = sqlsrv_fetch_array($rQueryA)) {
		echo"<tr>";
		echo"    <td>".utf8_encode($datac["claveAsignatura"])."</td>";
		echo"    <td>".utf8_encode($datac["nombre6"])."</td>";
		echo"    <td>".utf8_encode($datac["ciclo"])."</td>";
		echo"    <td>".utf8_encode($datac["calificacion"])."</td>";
		echo"    <td>".utf8_encode($datac["observaciones"])."</td>";
		echo"    <td>".utf8_encode($datac["ID_TIPO_ASIGNATURA"])."</td>";
		echo"    <td>".utf8_encode($datac["TIPO_ASIGNATURA"])."</td>";
		echo"    <td>".utf8_encode($datac["CREDITOS"])."</td>";
		echo"</tr>";
	}

    $DeniedReason 	= $obj->getDbRowName("vp_razon_rechazo", "Reg_Validacion", "WHERE vp_id_unico_cert = '".$data["ID_UNICO_CERT"]."'", 0);
	$ValidateStatus = $obj->getDbRowName("vp_estatus", "Reg_Validacion", "WHERE vp_id_unico_cert = '".$data["ID_UNICO_CERT"]."'", 0);

	echo"
	<tr></tr>
	</tbody>
	</table>
	</div>
	</div>                          
	<div id='DOCUMENTOS' class='container tab-pane fade'><br>
  		<h4>Documentos Requeridos</h4>
	    <table id='dtDynamicVerticalScrollExample' class='table table-rounded table-striped table-sm dataTable' cellspacing='0' width='100%' style='text-align:center; background-color:#fff;'>
	      <thead>
	            <tr style='text-align:center;/* background-color:#67a1fc;*/' class='bg-primary rounded-top text-white' >
	                <th>Tipo de Documento</th>
	                <th>Acci&oacute;n</th>
	            </tr>
	        </thead>
	        <tbody>";
            $LoadedFiles = count(glob('./XDoc/'.trim($vNumControl).'/{*.pdf}',GLOB_BRACE));
            echo "<input type='hidden' id='LoadedFiles' value='".$LoadedFiles."'>";
            for($i=1; $i<=7; $i++){
                $CountthisFile[1]   = 1;
                $GrantedFile[1]     = false;
                $thisFileName[1]    = "Acta de Nacimiento";
                $thisDivName[1]     = "Acta";

                $CountthisFile[2]   = 1;
                $GrantedFile[2]     = false;
                $thisFileName[2]    = "Historial Académico";
                $thisDivName[2]     = "HAcademico";

                $CountthisFile[3]   = 1;
                $GrantedFile[3]     = false;
                $thisFileName[3]    = "Certificado Antecedente";
                $thisDivName[3]     = "CertificadoA";

                $CountthisFile[4]   = 1;
                $GrantedFile[4]     = false;
                $thisFileName[4]    = "Oficio de Validación Antecedente Académico";
                $thisDivName[4]     = "OfiValidacion";

                $CountthisFile[5]   = 1;
                $GrantedFile[5]     = false;
                $thisFileName[5]    = "CURP 200%";
                $thisDivName[5]     = "CURP";

                $CountthisFile[6]   = 0;
                $GrantedFile[6]     = true;
                $thisFileName[6]    = "Equivalencia o Revalidación Estudios (Opcional)";
                $thisDivName[6]     = "Equivalencia";

                $CountthisFile[7]   = 0;
                $GrantedFile[7]     = true;
                $thisFileName[7]    = "Carta Compromiso (Opcional)";
                $thisDivName[7]     = "CartaCompromiso";

                echo "<script>";
                echo "    $('#upfile".$i."').click(function(){";
                echo "        $('#file".$i."').trigger('click');";
                echo "    });";
                echo "</script>";

                $DisplayButtons[$i] = "none";

                if($CountthisFile[$i] == 1){
                    $thisPath = "./XDoc/".trim($vNumControl);
                }else{
                    $thisPath = "./XDoc/".trim($vNumControl)."/opcionales/";
                }

				if(!file_exists($thisPath)){mkdir($thisPath, 0777, true);}
				if(file_exists($thisPath)){
					$directorio = opendir($thisPath);
					$ThisFile = $obj->getDbRowName("NombreDocumento", "Info_Documentos", "WHERE FolioControl = '".$data["ID_UNICO_CERT"]."' AND TipoDocumento = '".$thisDivName[$i]."'", 1);
					if($ThisFile === "error"){
						$ThisFile = $thisDivName[$i]."_".$vNumControl.".pdf";
						while($archivo = readdir($directorio)){                   
							if(!is_dir($archivo)){
								$pos = strpos($archivo, '_');
								$fcad = substr($archivo, 0, $pos);
								if($fcad === $thisDivName[$i]){
									$DisplayButtons[$i] = "inline";
									$GrantedFile[$i]    = true;
								}
							}
						}
					}else{
						$DisplayButtons[$i] = "inline";
						$GrantedFile[$i]    = true;
					}
				}

				if($GrantedFile[$i] == false){$ValidateGranted = false;}
				if($DisplayButtons[$i] === "inline"){$DisplayUpload[$i] = "none";}else{$DisplayUpload[$i] = "inline";}

                if($ValidateGranted){
                    echo "<script>";
                    echo "  $('#ValidaAlumno').attr('disabled', false);";
                    echo "</script>";
                }else{
                    echo "<script>";
                    echo "  $('#ValidaAlumno').attr('disabled', true);";
                    echo "</script>"; 
                }

                echo "<tr>";
                echo "  <td id='FileType' style='text-align:left;'><b>".$thisFileName[$i]."</b></td>";
                echo "  <td style='text-align:center;'>";
                echo "      <div id='Buttonsfile".$i."' style='display:".$DisplayButtons[$i].";'>";
                echo "          <a onclick=window.open('".$thisPath."/".$ThisFile."');><span id='viewfile".$i."' class='btn btn-info' style='padding:5px;'><i class='fa fa-eye'></i> Ver Archivo</span></a>";
                echo "      </div>";
                echo "  </td>";
                echo "</tr>";
            }
	        echo "
	        </tbody>
	    </table>
        <h4 style='margin-top:20px;'>Otros Documentos</h4>
        <table id='dtDynamicVerticalScrollExample' class='other_files table table-rounded table-striped table-sm dataTable' cellspacing='0' width='100%' style='text-align:center; background-color:#fff;'>
            <thead>
                <tr style='text-align:center; /*background-color:#67a1fc;*/' class='bg-primary rounded-top text-white' >
                    <th>Tipo de Documento</th>
                    <th>Acci&oacute;n</th>
                </tr>
            </thead>
            <tbody>";
                $OthersFilesPath = "./XDoc/".trim($vNumControl)."/otros/";
                if(!file_exists($OthersFilesPath)){mkdir($OthersFilesPath, 0777, true);}
                if(file_exists($OthersFilesPath)){
                    $TotalFiles = 0;
                    $directorio = opendir($OthersFilesPath);
                    while($archivo = readdir($directorio)){
                        if(!is_dir($archivo)){
                            $TotalFiles = $TotalFiles + 1;
                            echo "<tr id='file_".$TotalFiles."'><td id='FileType' style='text-align:center;'>Archivo Cargado</td><td style='text-align:center;'><div id='ButtonsFile".$archivo."' style='text-align:center;'><a onclick=window.open('".$OthersFilesPath."/".$archivo."');><span id='ViewFile".$archivo."' class='btn btn-info' style='padding:5px;'><i class='fa fa-eye'></i> Ver Archivo</span></a></div></td></tr>";
                        }
                    }
                }
            echo "
            </tbody>
        </table>
	</div>
    <div id='RAZON' class='container tab-pane fade'><br>
        <div class='row'>               
            <div class='col-sm-3'><span style='color:#ff0000; float:left;'><h2>*</h2></span><p class='navbar-text'>Raz&oacute;n de Rechazo</p></div>
            <div class='col-sm-6'>
                <textarea name='razon_rechazo' id='razon_rechazo' rows='5' cols='45' class='form-control'>".$DeniedReason."</textarea>
            </div>
        </div>
    </div>
    <b>Notas:</b>
    <ol>
      <li>Para <span class='text-success'><b>VALIDAR</b></span> Un Alumno es necesario llenar todos los campos requeridos y cargar todos los documentos necesarios</li>
      <li>Para <span class='text-danger'><b>RECHAZAR</b></span> Un Alumno es necesario ingresar la raz&oacute;n por la cual ha sido rechazado</li>
	</ol>
	<div id='ValButtons' style='width:100%; text-align:center;'>";
	switch($_SESSION["rol"]){
		case 2:
			switch($ValidateStatus){
				case '':
				case 'Prospecto':
					echo "<button type='button' id='ValidaAlumno' class='btn btn-success ValidaAlumno' status='validate' style='padding:5px;'>";
					echo "    <i class='fa fa-check'></i>";
					echo "    <span>Validar</span>";
					echo "</button>";
					echo "<button type='button' id='RechazaAlumno' class='btn btn-danger ValidaAlumno' status='rejected' style='padding:5px;'>";
					echo "    <i class='fa fa-check'></i>";
					echo "    <span>Rechazar</span>";
					echo "</button>";
				break;
				case 'Gen.XML':
				case 'XML timbrado':
				case 'XML autenticado':
				break;
				case 'Validado':
				case 'Validado por CSE':
					echo "
					<div id='ValButtons' style='width:100%; text-align:center;'>
						<button type='button' id='ValidaAlumno' class='btn btn-success ValidaAlumno' status='validate' style='padding:5px;'>
							<i class='fa fa-check'></i>
							<span>Validar</span>
						</button>";
					echo "</div> ";
				break;
				case 'Rechazado':
				case 'Rechazado por CSE':
					echo "<button type='button' id='ValidaAlumno' class='btn btn-success ValidaAlumno' status='validate' style='padding:5px;'>";
					echo "    <i class='fa fa-check'></i>";
					echo "    <span>Validar</span>";
					echo "</button>";
				break;
			}
		break;
		default:
			switch($ValidateStatus){
				case '':
				case 'Prospecto':
					echo "<button type='button' id='ValidaAlumno' class='btn btn-success ValidaAlumno' status='validate' style='padding:5px;'>";
					echo "    <i class='fa fa-check'></i>";
					echo "    <span>Validar</span>";
					echo "</button>";
					echo "<button type='button' id='RechazaAlumno' class='btn btn-danger ValidaAlumno' status='rejected' style='padding:5px;'>";
					echo "    <i class='fa fa-check'></i>";
					echo "    <span>Rechazar</span>";
					echo "</button>";
				break;
				case 'Gen.XML':
				case 'Validado por CSE':
				case 'XML timbrado':
				case 'XML autenticado':
					echo "<button type='button' id='RechazaAlumno' class='btn btn-danger ValidaAlumno' status='rejected' style='padding:5px;'>";
					echo "    <i class='fa fa-check'></i>";
					echo "    <span>Rechazar</span>";
					echo "</button>";
				break;
				case 'Validado':
					echo "<button type='button' id='ValidaAlumno' class='btn btn-success ValidaAlumno' status='validate' style='padding:5px;'>";
					echo "    <i class='fa fa-check'></i>";
					echo "    <span>Validar</span>";
					echo "</button>";
					echo "<button type='button' id='RechazaAlumno' class='btn btn-danger ValidaAlumno' status='rejected' style='padding:5px;'>";
					echo "    <i class='fa fa-check'></i>";
					echo "    <span>Rechazar</span>";
					echo "</button>";
				break;
				case 'Rechazado':
				case 'Rechazado por CSE':
					echo "<button type='button' id='ValidaAlumno' class='btn btn-success ValidaAlumno' status='validate' style='padding:5px;'>";
					echo "    <i class='fa fa-check'></i>";
					echo "    <span>Validar</span>";
					echo "</button>";
				break;
			}
		break;
	}
	echo "</div> ";
}
?>