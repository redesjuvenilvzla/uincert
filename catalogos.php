<?php
    $SecureSection = true;
    require_once("util/utilerias.php");
    $obj = new Utilerias;
    $obj->CnnBD();

    $SecType = $_GET["SecType"];
    switch($SecType){
        case 'entidades_federativas':
            $thisTitle  = "Entidades Federativas";
            $catId      = "EntFed";
        break;
        case 'observaciones':
            $thisTitle  = "Observaciones";
            $catId      = "Obs";
        break;
        case 'nivel_estudio':
            $thisTitle  = "Nivel de estudio";
            $catId      = "nivel_de_estudio";
        break;
        case 'cargos_autorizados':
            $thisTitle  = "Entidades Federativas";
            $catId      = "FirAut";
        break;
        case 'tipo_periodo':
            $thisTitle  = "Tipo de periodo";
            $catId      = "tipoperiodo";
        break;
        case 'genero':
            $thisTitle  = "G&eacute;nero";
            $catId      = "genero";
        break;
        case 'tipo_certificacion':
            $thisTitle  = "Tipo de certificaci&oacute;n";
            $catId      = "tipo_certificacion";
        break;
        case 'campus':
            $thisTitle  = "Campus";
            $catId      = "campus";
        break;
        case 'estudio_antecedente':
            $thisTitle  = "Estudio Antecedente";
            $catId      = "EstAnt";
        break;
        case 'fundamento_social':
            $thisTitle  = "Fundamento Legal";
            $catId      = "FunLeg";
        break;
        case 'mod_titulacion':
            $thisTitle  = "Modalidad Titulaci&oacute;n";
            $catId      = "ModTit";
        break;
        case 'auth_recon':
            $thisTitle  = "Autorizaci&oacute;n o Reconocimiento";
            $catId      = "AutRec";
        break;
        case 'rvoe':
            $thisTitle  = "RVOE";
            $catId      = "RVOE";
        break;
        case 'carreras':
            $thisTitle  = "Carreras";
            $catId      = "Carrera";
        break;
        case 'asignaturas':
            $thisTitle  = "Asignaturas";
            $catId      = "Asignaturas";
        break;

    }
    echo "<div class='tab-content' id='pills-tabContent'>";
    echo "<div class='section' id='pills-calificaciones' role='tabpanel' aria-labelledby='pills-calificaciones-tab'>";
    echo "<div class='row'>";
    echo "<div class='col-md-12 left'>";
    echo "<h2 class='text-primary'>Cat&aacute;logos - ".$thisTitle."</h2><br /><br />";
    echo "</div>";
    echo "<div class='container col-lg-12' style='height: 400px !important; overflow-y: scroll'>";
    $obj->getCatalogueData($catId);
    echo "</div>";
    echo "</div>";
    echo "</div>";
    echo "</div>";
?>
