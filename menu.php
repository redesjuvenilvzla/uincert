<?php
if(isset($_SESSION['susu'])){
    $SecureSection = true;
    require_once("util/utilerias.php");

    switch ($_SESSION["SectionType"]) {
        case 'Servicios_Profesionales':
            $thisPath = "../";
        break;
        default:
            $thisPath = "";
        break;
    }

    $obj = new Utilerias;
    $obj->CnnBD();
?>

<!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top bg-primary" style="position:fixed; width:100%; height:90px; z-index:10;">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand pull-left" href="<?php echo $thisPath; ?>" style=""><img src='<?php echo $thisPath; ?>img/logo_uin.png' style='width: 200px'></a>
                <nav class="navbar navbar-expand-lg navbar-dark pull-left">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item ">
                            <a class="nav-link cbfuente" href="<?php echo $thisPath; ?>">
                                Inicio            
                            </a>
                        </li>
                        <?php 
                        switch ($_SESSION["SectionType"]) {
                            case 'Cert-Tit':
                                if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="2") OR ($_SESSION['rol']=="3")){?>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Certificados                        
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="2")){?>
                                        <a class="dropdown-item" href="javascript:cargaContenido('CertificadosPlantel', '');">
                                            Plantel
                                        </a>
                                        <?php } ?>
                                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="3")){?>
                                        <a class="dropdown-item" href="javascript:cargaContenido('CertificadosCoordinacion', '');">
                                            Coordinación
                                        </a>  
                                        <?php } ?>                      
                                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="3")){?>
                                        <a class="dropdown-item" href="javascript:cargaContenido('GeneraXML', 'CERT_TIM');">
                                            XML Timbrado
                                        </a>
                                        <?php } ?>  
                                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="3")){?>
                                        <a class="dropdown-item" href="javascript:cargaContenido('GeneraXML', 'CERT_SEP');" target="_top">
                                            XML SEP
                                        </a>
                                        <?php } ?>  
                                    </div>
                                </li>
                                <?php } ?>
                                <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="4") OR ($_SESSION['rol']=="5")){?>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Títulos
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <?php if (($_SESSION['rol']=="4")){?>
                                        <a class="dropdown-item" href="javascript:cargaContenido('TitulosPlantel', '');">
                                            Validación
                                        </a>
                                        <?php } else if ( ($_SESSION['rol']=="5") OR ($_SESSION['rol']=="1") ) { ?>
                                        <a class="dropdown-item" href="javascript:cargaContenido('TitulosPlantel', '');">
                                            Validación y XML
                                        </a>
                                        <?php } ?>
                                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="5")){?>
                                        <a class="dropdown-item" href="javascript:cargaContenido('GeneraXML', 'TIT_TIM');">
                                            XML Timbrado
                                        </a>
                                        <?php } ?>
                                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="5")){?>
                                        <a class="dropdown-item" href="javascript:cargaContenido('GeneraXML', 'TIT_SEP');" target="_top">
                                            XML SEP
                                        </a>
                                        <?php } ?>
                                    </div>
                                </li>
                                <?php } ?>
                                <?php if (($_SESSION['rol']!="0")){?>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Catálogos
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'entidades_federativas');"> 
                                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                                            Entidades federativas 
                                        </a>
                                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'observaciones');"> 
                                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                                            Observaciones 
                                        </a>
                                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'nivel_estudio');"> 
                                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                                            Nivel de estudio 
                                        </a>
                                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'tipo_periodo');"> 
                                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                                            Tipo de periodo 
                                        </a>
                                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'genero');"> 
                                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                                            Genero 
                                        </a>
                                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'tipo_certificacion');"> 
                                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                                            Tipo de certificacion 
                                        </a>
                                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'campus');"> 
                                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                                            Campus 
                                        </a>
                                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'asignaturas');"> 
                                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                                            Asignaturas 
                                        </a>                                                                                                                                                                        
                                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'carreras');"> 
                                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                                            Carreras 
                                        </a>
                                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'rvoe');"> 
                                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                                            RVOE 
                                        </a>
                                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'fundamento_social');"> 
                                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                                            Fundamento legal serv social 
                                        </a>
                                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'mod_titulacion');"> 
                                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                                            Modalidad titulacion 
                                        </a>
                                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'auth_recon');"> 
                                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                                            Autorizacion o reconocimiento 
                                        </a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="servicios-profesionales" id="" role="button" data-toggle="" aria-haspopup="" aria-expanded="">
                                        |&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Servicios Profesionales
                                    </a>
                                </li> <?php }
                                break;
                            
                            case 'Servicios_Profesionales':
                                if($_SESSION["Auth_SS"] == 1){
                                    echo "
                                        <li class='nav-item dropdown'>
                                            <a class='nav-link' href='?ty=servicio-social&SecId=solicitudes-validacion' id='' role='button'>Servicio Social</a>
                                        </li>
                                    ";
                                }
                                if($_SESSION["Auth_PP"] == 1){
                                    echo "
                                        <li class='nav-item dropdown'>
                                            <a class='nav-link' href='?ty=practicas-profesionales&SecId=solicitudes-validacion' id='' role='button'>Pr&aacute;cticas Profesionales</a>
                                        </li>
                                    ";
                                }
                                if($_SESSION["Auth_TI"] == 1){
                                    echo "
                                        <li class='nav-item dropdown'>
                                            <a class='nav-link' href='?ty=titulacion&SecId=solicitudes-validacion' id='' role='button'>Titulaci&oacute;n</a>
                                        </li>
                                    ";
                                }
                                if($_SESSION["Auth_CO"] == 1){
                                    echo "
                                        <li class='nav-item dropdown'>
                                            <a class='nav-link' href='?ty=convenios' id='' role='button'>Convenios</a>
                                        </li>
                                    ";
                                }
                                break;
                        }?>
                    </ul>
                </nav>
            </div>

            <nav class="navbar navbar-expand-lg navbar-dark">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class='nav-link dropdown-toggle' href='#' style='margin-top:2px; font-size:15px;' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                <i class='fa fa-bell'></i>
                                <span class='label'></span>
                            </a>
                            <div class='dropdown-menu media-list dropdown-menu-right' x-placement='bottom-end' style='position:absolute; background-color:#007bff; width:300px; font-size:14px; will-change: transform; top: 0px; left: 0px; transform: translate3d(-230px, 60px, 0px);'>
                                <span href='#' class='list-group-item active' style='text-align:center'>NUEVAS SOLICITUDES</span>
					            <div style='font-size:12px; width:100%; max-height:450px; overflow:auto;'>
                                    <?php
                                    echo $obj->Notificaciones("ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP", "WHERE REG_TIPO = 0 AND FECHA_HISTORICO LIKE '%".date('Y-d-m')."%'");
                                    ?>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-top: -1%;">
                                <img src='<?php echo $thisPath; ?>img/img-user.png' style='height: 32px'>
                                <span class="c"> Bienvenido
                                <?php if (($_SESSION['rol']!="0")){?>
                                    <?php echo " " .$_SESSION['nom']." "?>
                                <?php } ?> !
                            </span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <?php if (($_SESSION['rol']=="1")){?>
                                    <a class="dropdown-item" href="javascript:cargaContenido('<?php echo $thisPath;?>editarCuentas', '');">
                                        Editar usuario
                                    </a>
                                <?php } ?>
                                <!-- <a class="dropdown-item" href="/Manage/ChangePassword">Cambiar Contraseña</a> -->
                                <?php if (($_SESSION['rol']=="1")){?>
                                <a class="dropdown-item" href="javascript:cargaContenido('<?php echo $thisPath;?>gcuentas', '');">
                                    Alta usuario 
                                </a>
                                <?php } ?>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?php echo $thisPath;?>logout.php">
                                    Cerrar Sesión
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </nav>
    <div class='row' style='padding-top:80px;'>

<?php
    }else{
        header("Location: index.php");
    }
?>



