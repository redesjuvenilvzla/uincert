<?php
/******************************************/
/*****************QUERY'S******************/
/******************************************/

/*			CERTIFICADOSALTAS'S			*/

$QHederCertificados = "
SELECT T1.PEOPLE_CODE_ID+T1.folioControl+T1.Plantel AS ID_UNICO_CERT ,* FROM (SELECT  ROW_NUMBER() OVER(ORDER BY rc.ACADEMIC_SESSION,cc.FORMAL_TITLE,pe.LAST_NAME,pe.Last_Name_Prefix,pe.FIRST_NAME,pe.MIDDLE_NAME DESC) AS RW
     ,PE.PEOPLE_ID AS numeroControl
     , pe.FIRST_NAME+' '+pe.MIDDLE_NAME as nombre3
     , pe.LAST_NAME as primerApellido4
     , pe.Last_Name_Prefix as segundoApellido5
     , (case rc.TIPO_CERTIFICADO when 'T' THEN 'TOTAL'
            when 'P' THEN 'PARCIAL'
            else '' end ) as tipoCertificacion
     , rc.ACADEMIC_SESSION as Plantel 
     , cc.FORMAL_TITLE as Carrera
     , (SELECT        TOP (1) CODIGO_PLANTEL + '' + RIGHT(RC.ANHO, 2) + RC.TIPO_CERTIFICADO + ABREVIATURA_PROGRAMA + '-' + RC.NUM_CERTIFICADO AS Expr1
               FROM            CLAVES_CERTIFICADOS AS cf
               WHERE        (ORG_CODE_ID = ORG.ORG_CODE_ID) 
             AND (CURRICULUM = RC.CURRICULUM) 
             AND (ABREVIATURA_PROGRAMA IS NOT NULL) 
             AND (ANO_PLAN = RC.ANO_PLAN)) AS folioControl 
     , rc.ANHO
     , (case rc.TIPO_CERTIFICADO when 'T' THEN 'Total'
            when 'P' THEN 'Parcial'
            else '' end ) as Tipo_certificado
     , pe.PEOPLE_ID as Matricula
     , pe.LAST_NAME+' '+ pe.Last_Name_Prefix+' '+ pe.FIRST_NAME+' '+ pe.MIDDLE_NAME as Nombre
     ,convert(nvarchar(10),convert(date,rc.CEATE_DATE)) as Fecha 
     ,cc.code_value_key as curriculum
     ,rc.TIPO_CERTIFICADO as tp
     ,pe.PEOPLE_CODE_ID as PEOPLE_CODE_ID
     ,rc.PROGRAM
     ,rc.STATUS_XML
     FROM            REG_CERTIFICADO AS rc INNER JOIN
            PEOPLE AS pe ON rc.PEOPLE_CODE_ID = pe.PEOPLE_CODE_ID INNER JOIN
            CODE_CURRICULUM AS cc ON rc.CURRICULUM = cc.CODE_VALUE_KEY INNER JOIN
            CODE_ACASESSION AS CA ON rc.ACADEMIC_SESSION = CA.CODE_VALUE_KEY INNER JOIN
            ORGANIZATION AS ORG ON CA.CODE_XVAL = ORG.ORG_IDENTIFIER 
     WHERE RC.ACADEMIC_SESSION IN ('VARIABLE=CAMPUS')
       VARIABLE=TIPO_CERTIFICADO
       and convert(date,(rc.CEATE_DATE))>= convert (date,('VARIABLE=FINICIAL')) 
       and convert(date,(rc.CEATE_DATE))<= convert (date,('VARIABLE=FFINAL')) 
       ) AS T1
       VARIABLE=VALPLANTEL
       ORDER BY T1.Plantel,T1.Carrera,T1.Nombre DESC
";

$QCertificados = "
SELECT VARIABLE=TOP TT.PEOPLE_CODE_ID+TT.folioControl+TT.ACADEMIC_SESSION AS
ID_UNICO_CERT,* FROM (
SELECT DISTINCT  ROW_NUMBER() over (order by RC.PEOPLE_CODE_ID) AS RW
                        , RC.PEOPLE_CODE_ID
                        ,RC.ACADEMIC_SESSION
                        ,'1.0' AS version
                        , '5' AS tipoCertificado
                        , (SELECT  TOP(1) CF.CODIGO_PLANTEL + '' + RIGHT(RC.ANHO, 2) + RC.TIPO_CERTIFICADO + CF.ABREVIATURA_PROGRAMA + '-' + RC.NUM_CERTIFICADO AS Expr1
                                FROM            CLAVES_CERTIFICADOS AS cf
                                WHERE        (CF.ORG_CODE_ID = ORG.ORG_CODE_ID)
                                                                                        AND (CF.CURRICULUM = RC.CURRICULUM)
                                                                                        AND (CF.ABREVIATURA_PROGRAMA IS NOT NULL)
                                                                                        AND (CF.ANO_PLAN = RC.ANO_PLAN)
                                                                                        AND (CF.DEGREE = RC.DEGREE)
                                                                                        ) AS folioControl
                        ,nd.noCertificadoResponsable
                        , EC.ID_INSTITUCION AS idNombreInstitucion
                        , 'UNIVERSIDAD INSURGENTES' AS nombreInstitucion
                                                , EC.CLAVE_CAMPUS AS idCampus
                        , EC.CAMPUS AS campus
                        , EC.ID_ENTIDAD AS idEtidadFederativa
                        , EC.ENTIDAD AS entidadFederativa
                        , ND.CURP_RESP AS curp
                        , ND.NOMBRE_RESP as nombre
                        , ND.PRIMERAPELLIDO_RESP as primerApellido
                        , ND.SEGUNDOAPELLIDO_RESP as segundoApellido
                        , ND.ID_CARGO as idCargo
                        , ND.CARGO as cargo
                        , PCE.NO_RVOE as numero
                        , CONVERT(NVARCHAR(10),CONVERT(DATE,PCE.FECHA_RVOE))+'T00:00:00'   
as fechaExpedicion
                        , CC.ID_CARRERA as idCarrera
                        , CC.CLAVE_CARRERA as claveCarrera
                        , CC.DESCRIPCION_CARRERA as nombreCarrera
                        ,(SELECT TOP(1)PP.ID_TIPOPERIODO FROM PERIODO_CERTIFICADO PP WHERE PP.ACADEMIC_TERM=RC.MATRIC_TERM AND PP.PROGRAM=RC.PROGRAM) AS idTipoPeriodo
                        ,(SELECT  TOP(1)PP.TIPO_PERIODO FROM PERIODO_CERTIFICADO PP WHERE PP.ACADEMIC_TERM=RC.MATRIC_TERM AND PP.PROGRAM=RC.PROGRAM) AS tipoPeriodo
                        , PCE.CLAVE_PLAN as clavePlan
                        , PE.PEOPLE_ID as numeroControl
                        ,isnull((SELECT        TOP (1) US.CURP
                                FROM            USERDEFINEDIND AS us
                                WHERE        (US.PEOPLE_CODE_ID =  
RC.PEOPLE_CODE_ID) AND (US.CURP<>'') ),'') AS curp2
                        , PE.FIRST_NAME+' '+PE.MIDDLE_NAME as nombre3
                        , IIF (PE.LAST_NAME='.',' ',PE.LAST_NAME) primerApellido4
                        , IIF (PE.Last_Name_Prefix='.',' ',PE.Last_Name_Prefix) segundoApellido5

                        ,(SELECT TOP(1)  CASE DG.GENDER WHEN 'F'  THEN '250'
                                                                                        WHEN'M' THEN '251' ELSE '' END AS FF FROM DEMOGRAPHICS DG WHERE DG.PEOPLE_CODE_ID = PE.PEOPLE_CODE_ID AND DG.GENDER IN ('F','M') ORDER BY CREATE_DATE DESC )AS idGenero
                        , CONVERT(NVARCHAR(10),CONVERT(DATE,PE.BIRTH_DATE))+'T00:00:00' AS fechaNacimiento
                        ,(CASE RC.TIPO_CERTIFICADO WHEN 'T' THEN '79'
                        WHEN 'P' THEN '80'
                        END
                        ) as idTipoCertificado
                        ,(CASE RC.TIPO_CERTIFICADO WHEN 'T' THEN 'TOTAL'
                        WHEN 'P' THEN 'PARCIAL'
                        END
                        ) tipoCertificacion
                        , CONVERT(NVARCHAR(10),CONVERT(DATE,RC.CEATE_DATE))+'T00:00:00' as fecha
                        ,'09' idLugarExpedicion
                        ,'CIUDAD DE MEXICO'  lugarExpedicion
                        ,(select top(1) st.COURSE_MIN from STDDEGREQ st where st.PEOPLE_CODE_ID=rc.PEOPLE_CODE_ID and st.CURRICULUM=rc.CURRICULUM and st.MATRIC_TERM=RC.MATRIC_TERM and st.MATRIC_YEAR=RC.MATRIC_YEAR and st.PROGRAM=RC.PROGRAM) as total
                        ,(select top(1) st.COURSES_TAKEN from STDDEGREQ st where st.PEOPLE_CODE_ID=rc.PEOPLE_CODE_ID and st.CURRICULUM=rc.CURRICULUM and st.MATRIC_TERM=RC.MATRIC_TERM and st.MATRIC_YEAR=RC.MATRIC_YEAR and st.PROGRAM=RC.PROGRAM) as asignadas
                                                ,CONVERT(DECIMAL(18,1), ROUND((SELECT   
sum(iif(isnumeric(ttr.FINAL_GRADE)=1,convert(numeric(10,2),ttr.FINAL_GRADE),0))/ count(ste.event_id) as promedio
FROM            STDDEGREQ AS st INNER JOIN
                          STDDEGREQEVENT AS ste ON st.PEOPLE_CODE_ID = ste.PEOPLE_CODE_ID AND st.MATRIC_YEAR = ste.MATRIC_YEAR AND st.MATRIC_TERM = ste.MATRIC_TERM AND
                          st.RECORD_TYPE = ste.RECORD_TYPE AND st.PROGRAM = ste.PROGRAM AND st.DEGREE = ste.DEGREE AND st.CURRICULUM = ste.CURRICULUM INNER JOIN
                          TRANSCRIPTDETAIL AS ttr ON ste.PEOPLE_CODE_ID = ttr.PEOPLE_CODE_ID AND ste.TAKEN_SESSION = ttr.ACADEMIC_SESSION AND ste.TAKEN_TERM = ttr.ACADEMIC_TERM AND
                          ste.TAKEN_YEAR = ttr.ACADEMIC_YEAR AND ste.TAKEN_SECTION = ttr.SECTION AND ste.TAKEN_EVENT_ID = ttr.EVENT_ID AND ste.TAKEN_SUB_TYPE = ttr.EVENT_SUB_TYPE
                                                 where ste.STATUS='C'
                                                 and ste.PEOPLE_CODE_ID=RC.PEOPLE_CODE_ID
                                                 and ste.MATRIC_YEAR=RC.MATRIC_YEAR
                                                 and ste.MATRIC_TERM=RC.MATRIC_TERM
                                                 and ste.CURRICULUM=RC.CURRICULUM
                                                 and ste.PROGRAM=RC.PROGRAM
                                                 ),1,1))promedio
FROM            REG_CERTIFICADO AS RC INNER JOIN
                          CODE_ACASESSION AS CA ON RC.ACADEMIC_SESSION = CA.CODE_VALUE_KEY INNER LOOP JOIN
                          ORGANIZATION AS ORG ON CA.CODE_XVAL = ORG.ORG_IDENTIFIER
                                                 LEFT JOIN NODO_RESPONSABLE ND ON
                        RC.ACADEMIC_SESSION= ND.ACADEMIC_SESSION
                        LEFT JOIN ENTIDAD_CERTIFICADO AS EC ON
                        RC.ACADEMIC_SESSION = EC.ACADEMIC_SESSION
                        LEFT JOIN      CARRERA_CERTIFICADO AS CC ON
                                EC.ID_INSTITUCION = CC.ID_INSTITUCION
                                AND RC.CURRICULUM = CC.CURRICULUM
                        INNER JOIN PEOPLE AS PE ON
                        PE.PEOPLE_CODE_ID=RC.PEOPLE_CODE_ID
                     LEFT JOIN (
                         SELECT DISTINCT PR.ID_INSTITUCION
                         ,PR.CLAVE_CAMPUS,PR.ACADEMIC_SESSION,PR.ID_CARRERA
                         ,PR.CURRICULUM,PR.CLAVE_CARRERA,PR.CLAVE_PLAN, PR.NO_RVOE
                         ,PR.FECHA_RVOE,PR.CURP_RESPONSABLE
                        FROM PROGRAMA_CERTIFICADO PR
                         ) AS PCE ON
                         EC.ID_INSTITUCION = PCE.ID_INSTITUCION
                                AND EC.CLAVE_CAMPUS = PCE.CLAVE_CAMPUS
                                AND RC.CURRICULUM = PCE.CURRICULUM
                                AND RC.ACADEMIC_SESSION = PCE.ACADEMIC_SESSION
                                AND PCE.CLAVE_PLAN=RC.ANO_PLAN

WHERE
(RC.STATUS='A')
AND (ND.TIPO_REG='CERT') VARIABLE=NUMCONTROL
AND (RC.ACADEMIC_SESSION IN ('VARIABLE=CAMPUS'))  
VARIABLE=TIPO_CERTIFICADO
AND (RC.ANHO='VARIABLE=ANHO')
) AS TT
VARIABLE=VALPLANTEL
WHERE Fecha BETWEEN 'VARIABLE=FINICIAL' AND 'VARIABLE=FFINAL'
ORDER BY RW ASC
OPTION (FORCE ORDER)
";


$QCertificadosMaterias = 
"

select   ROW_NUMBER() over (order by   
ALUMNO.PRINT_ORDER,ALUMNO.ENROLLED_SEQ) AS RW
                        ,PCE.ID_ASIGNATURA as idAsignatura
                        ,PCE.CLAVE_ASIGNATURA as claveAsignatura
                        ,PCE.DESCRIPCION as nombre6
                        ,iif(ALUMNO.CICLO_L='1900-EQ','0000-0',
IIF(ALUMNO.OBSERVACIONES='*', '',ALUMNO.CICLO_L))as ciclo
                        ,ALUMNO.CAL__P as calificacion
                                ,ISNULL(OC.ID_OBSERVACION,'100') AS idObservaciones
                                ,ISNULL(OC.DESCRIPCION,'NORMAL') AS observaciones

                                ,ALUMNO.PEOPLE_CODE_ID+
                                         (SELECT  TOP(1) CF.CODIGO_PLANTEL + '' + RIGHT(RC.ANHO, 2) + RC.TIPO_CERTIFICADO + CF.ABREVIATURA_PROGRAMA + '-' + RC.NUM_CERTIFICADO AS Expr1
                                FROM            CLAVES_CERTIFICADOS AS cf
                                WHERE        (CF.ORG_CODE_ID = ORG_CODE_ID)
                                                                                        AND (CF.CURRICULUM = RC.CURRICULUM)
                                                                                        AND (CF.ABREVIATURA_PROGRAMA IS NOT NULL)
                                                                                        AND (CF.ANO_PLAN = RC.ANO_PLAN)
                                                                                        AND (CF.DEGREE = RC.DEGREE)
                                                                                        ) +PCE.ACADEMIC_SESSION AS ID_UNICO_CERT

FROM (
SELECT * FROM (
SELECT
STD.PEOPLE_CODE_ID,STD.CURRICULUM, STD.MATRIC_YEAR, STD.MATRIC_TERM ,STD.PROGRAM, IIF(STD.MATRIC_YEAR<=1999 AND (STD.MATRIC_TERM='CUATRI1' OR STD.MATRIC_TERM='SEMES1'), (SELECT (Case
    when CHARINDEX (RVOE.AgreementNumber, RVOE.AgreementNumber)=0 then RVOE.AgreementNumber
    Else  substring (RVOE.AgreementNumber, CHARINDEX ('|', RVOE.AgreementNumber)+1,len (RVOE.AgreementNumber))
   END)as reg_rvoe
FROM RVOE, DEGREQ DER WHERE ( RVOE.OrgCodeId ='O000000009' )AND RVOE.DegreeRequirementId = DER.DegreeRequirementId AND STDE.MATRIC_YEAR = DER.MATRIC_YEAR AND STDE.MATRIC_TERM = DER.MATRIC_TERM
AND STDE.PROGRAM         = DER.PROGRAM
AND STDE.DEGREE                 = DER.DEGREE
AND STDE.CURRICULUM  = DER.CURRICULUM
)
  ,STD.MATRIC_YEAR )ANHOPLAN
, STD.PRINT_ORDER
, STDE.ENROLLED_SEQ
, STDE.EVENT_ID
, EV.PUBLICATION_NAME_1 ASIGNATURA
, EV.SPEEDE_CODE CLAVE
, (SELECT TOP(1)  RIGHT(TD2.ACADEMIC_YEAR,2)+'-'+CAC.CODE_XVAL
        FROM TRANSCRIPTDETAIL TD2
        , CODE_ACATERM CAC
        WHERE
        (
                (
                        TD2.PEOPLE_CODE_ID = TD.PEOPLE_CODE_ID
                        AND TD2.EVENT_ID = TD.EVENT_ID
                        AND TD2.EVENT_SUB_TYPE = TD.EVENT_SUB_TYPE
                )
                OR
                (
                        TD2.PEOPLE_CODE_ID = STDE.PEOPLE_CODE_ID
                        AND        TD2.EVENT_ID = STDE.EVENT_ID
                        AND TD2.EVENT_TYPE = STDE.EVENT_SUB_TYPE
                        )
        )
        AND (TD2.HONORS not in('EXT')  OR TD2.HONORS IS NULL)
        AND TD2.SECTION  NOT IN ('EXT')
        AND CAC.CODE_VALUE_KEY = TD2.ACADEMIC_TERM
        ORDER BY TD2.ACADEMIC_YEAR desc, CAC.SORT_ORDER desc
        ) CICLO
        ,STDE.EVENT_ID LO1
,(SELECT TOP(1) TD2.FINAL_GRADE
        FROM TRANSCRIPTDETAIL TD2
        , CODE_ACATERM CAC
        WHERE
        (
                (
                        TD2.PEOPLE_CODE_ID = TD.PEOPLE_CODE_ID
                        AND TD2.EVENT_ID = TD.EVENT_ID
                        AND TD2.EVENT_SUB_TYPE = TD.EVENT_SUB_TYPE
                )
                OR
                (
                                TD2.PEOPLE_CODE_ID = STDE.PEOPLE_CODE_ID
                        AND        TD2.EVENT_ID = STDE.EVENT_ID
                        AND TD2.EVENT_TYPE = STDE.EVENT_SUB_TYPE)
        )
        AND (TD2.HONORS not in('EXT')  OR TD2.HONORS IS NULL)
        AND TD2.SECTION  NOT IN ('EXT')
        AND CAC.CODE_VALUE_KEY = TD2.ACADEMIC_TERM
        ORDER BY TD2.ACADEMIC_YEAR DESC, CAC.SORT_ORDER desc
        ) ORDINARIO
,IIF(TD.FINAL_GRADE IS NULL,(SELECT TOP(1) TR.FINAL_GRADE FROM TRANSCRIPTDETAIL TR WHERE  TR.EVENT_ID=STDE.EVENT_ID AND TR.PEOPLE_CODE_ID=STDE.PEOPLE_CODE_ID AND TR.COURSE_PRINT_TRAN='Y'),TD.FINAL_GRADE)AS CAL__P ,IIF(TD.HONORS='*,**',''
,(IIF(TD.ACADEMIC_YEAR IS NULL,(SELECT TOP(1) TR.ACADEMIC_YEAR+'-'+CAC.CODE_XVAL FROM TRANSCRIPTDETAIL TR, CODE_ACATERM CAC WHERE  TR.EVENT_ID=STDE.EVENT_ID
                        AND TR.PEOPLE_CODE_ID=STDE.PEOPLE_CODE_ID AND CAC.CODE_VALUE_KEY =
TR.ACADEMIC_TERM)  ,(TD.ACADEMIC_YEAR)+'-'+(SELECT CC.CODE_XVAL FROM CODE_ACATERM CC WHERE CC.CODE_VALUE_KEY=TD.ACADEMIC_TERM)))
  )AS CICLO_L
,(TD.HONORS
)OBSERVACIONES
FROM                                        STDDEGREQEVENT AS STDE
LEFT OUTER JOIN
                          TRANSCRIPTDETAIL AS TD ON
                                                STDE.PEOPLE_CODE_ID = TD.PEOPLE_CODE_ID
                                                AND STDE.TAKEN_YEAR = TD.ACADEMIC_YEAR
                                                AND STDE.TAKEN_TERM = TD.ACADEMIC_TERM
                                                AND STDE.TAKEN_SESSION = TD.ACADEMIC_SESSION
                                                AND STDE.TAKEN_EVENT_ID = TD.EVENT_ID
                                                AND STDE.TAKEN_SUB_TYPE = TD.EVENT_SUB_TYPE
                                                AND STDE.TAKEN_SECTION = TD.SECTION                                                
                                                AND TD.COURSE_PRINT_TRAN = 'Y'
INNER JOIN
                          STDDEGREQDISC AS STD ON
                                                 STDE.PEOPLE_CODE_ID = STD.PEOPLE_CODE_ID
                                                 AND STDE.MATRIC_YEAR = STD.MATRIC_YEAR
                                                 AND STDE.MATRIC_TERM = STD.MATRIC_TERM
                                                 AND STDE.PROGRAM = STD.PROGRAM
                                                 AND STDE.RECORD_TYPE=STD.RECORD_TYPE
                                                 AND STDE.DEGREE = STD.DEGREE
                                                 AND STDE.CURRICULUM = STD.CURRICULUM
                                                 AND STDE.DISCIPLINE = STD.DISCIPLINE INNER JOIN
                          EVENT AS EV ON
                                                 STDE.EVENT_ID = EV.EVENT_ID
                                                 AND STDE.EVENT_SUB_TYPE = EV.EVENT_TYPE

WHERE        (STD.RECORD_TYPE = 'A')
AND ( STDE.PEOPLE_CODE_ID IN ('VARIABLE=PEOPLE_CODE_ID') ) AND ( STDE.CURRICULUM in('VARIABLE=CURRICULUM'))
) AS T1 WHERE T1.CAL__P IS NOT NULL and IIF(T1.CAL__P='AC',10,T1.CAL__P)>=6
) AS ALUMNO
INNER JOIN REG_CERTIFICADO RC ON
                        RC.PEOPLE_CODE_ID=ALUMNO.PEOPLE_CODE_ID
                        AND RC.CURRICULUM=ALUMNO.CURRICULUM
                        AND RC.MATRIC_YEAR=ALUMNO.MATRIC_YEAR
                        AND RC.MATRIC_TERM=ALUMNO.MATRIC_TERM LEFT JOIN ENTIDAD_CERTIFICADO AS EC ON
                        RC.ACADEMIC_SESSION = EC.ACADEMIC_SESSION
LEFT JOIN      PROGRAMA_CERTIFICADO AS PCE ON
                                EC.ID_INSTITUCION = PCE.ID_INSTITUCION
                                AND EC.CLAVE_CAMPUS = PCE.CLAVE_CAMPUS
                                AND RC.CURRICULUM = PCE.CURRICULUM
                                AND RC.ACADEMIC_SESSION = PCE.ACADEMIC_SESSION
                                and PCE.CLAVE_ASIGNATURA=ALUMNO.CLAVE
                                AND PCE.CLAVE_PLAN=RC.ANO_PLAN
								AND PCE.PROGRAMA=ALUMNO.PROGRAM
LEFT JOIN OBSERVACION_CERTIFICADO AS OC ON
                                OC.CODE_VALUE_KEY=ALUMNO.OBSERVACIONES
WHERE EC.ACADEMIC_SESSION ='VARIABLE=CAMPUS'
AND RC.TIPO_CERTIFICADO ='VARIABLE=TIPOCERTIFICADO'
AND RC.ANHO ='VARIABLE=ANHO'
ORDER BY RW
OPTION (FORCE ORDER)

";

$QTitulacion = "

SELECT TOP 100
ROW_NUMBER() over(order by std.PEOPLE_CODE_ID desc) as row
,'1.0'as version 
,std.PEOPLE_ID as folioControl
,nd.CURP_RESP 
,nd.ID_CARGO
,nd.CARGO
,nd.abrTitulo
,nd.NOMBRE_RESP
,nd.PRIMERAPELLIDO_RESP
,nd.SEGUNDOAPELLIDO_RESP
,'' as sello
,'' certificadoResponsable
,nd.noCertificadoResponsable
,nd1.CURP_RESP as CURP_RESP2
,nd1.ID_CARGO as ID_CARGO2
,nd1.CARGO as CARGO2
,nd1.abrTitulo as abrTitulo2
,nd1.NOMBRE_RESP as NOMBRE_RESP2
,nd1.PRIMERAPELLIDO_RESP as PRIMERAPELLIDO_RESP2
,nd1.SEGUNDOAPELLIDO_RESP as SEGUNDOAPELLIDO_RESP2
,'' as sello2
,'' certificadoResponsable2
,nd1.noCertificadoResponsable as noCertificadoResponsable2
,ED.CLAVE_CAMPUS cvelInstitucion
,ED.CAMPUS nombreInstitucion
,cc.CLAVE_CARRERA cveCarrera
,cc.DESCRIPCION_TITULOS nombreCarrera
,'' fechaInicio
,'' fechaTerminacion
,'' idAutorizacionReconocimiento
,'' autorizacionReconocimiento
,( select distinct PCE.NO_RVOE  
		from PROGRAMA_CERTIFICADO PCE 
		WHERE PCE.CLAVE_CAMPUS=ED.CLAVE_CAMPUS 
		AND PCE.CURRICULUM=STD.CURRICULUM 
		AND PCE.ACADEMIC_SESSION=ED.ACADEMIC_SESSION
		AND PCE.CLAVE_PLAN IN ( IIF(STD.MATRIC_YEAR<=1999 AND (STD.MATRIC_TERM='CUATRI1' OR STD.MATRIC_TERM='SEMES1'),
						(SELECT 
						(Case  
						   when CHARINDEX (RVOE.AgreementNumber, RVOE.AgreementNumber)=0 then RVOE.AgreementNumber
						   Else  substring (RVOE.AgreementNumber, CHARINDEX ('|', RVOE.AgreementNumber)+1,len (RVOE.AgreementNumber)) 
						  END)as reg_rvoe
						FROM RVOE, DEGREQ DER WHERE ( RVOE.OrgCodeId ='O000000009' )AND RVOE.DegreeRequirementId = DER.DegreeRequirementId AND STD.MATRIC_YEAR = DER.MATRIC_YEAR
						AND STD.MATRIC_TERM = DER.MATRIC_TERM
						AND STD.PROGRAM	 = DER.PROGRAM
						AND STD.DEGREE		 = DER.DEGREE
						AND STD.CURRICULUM  = DER.CURRICULUM
						) 
						 ,STD.MATRIC_YEAR )))	numeroRvoe
						 ,isnull((SELECT        TOP (1) US.CURP
                               FROM            USERDEFINEDIND AS us
                               WHERE        (US.PEOPLE_CODE_ID = PE.PEOPLE_CODE_ID) AND (US.CURP<>'') ),'') AS curp
			, PE.FIRST_NAME+' '+PE.MIDDLE_NAME as nombre
			, IIF (PE.LAST_NAME='.',' ',PE.LAST_NAME) primerApellido
			, IIF (PE.Last_Name_Prefix='.',' ',PE.Last_Name_Prefix) segundoApellido
			,'' correoElectronico
			,''fechaExpedicion
			,''idModalidadTitulacion
			,''modalidadTitulacion
			,''fechaExamenProfesional
			,''fechaExencionExamenProfesional
			,''cumplioServicioSocial
			,''idFundamentoLegalServicioSocial
			,''fundamentoLegalServicioSocial
			,''idEntidadFederativa
			,''entidadFederativa
			,''institucionProcedencia
			,''idTipoEstudioAntecedente
			,''tipoEstudioAntecedente
			,''idEntidadFederativa2
			,''entidadFederativa2
			,''fechaInicio2
			,''fechaTerminacion2
			,''noCedula 
			        
FROM            STDDEGREQ AS std INNER JOIN
                         PEOPLE AS PE ON PE.PEOPLE_CODE_ID = std.PEOPLE_CODE_ID INNER JOIN
                         CARRERA_CERTIFICADO AS CC ON std.CURRICULUM = CC.CURRICULUM LEFT OUTER JOIN
                         NODO_RESPONSABLE AS nd1 ON nd1.ACADEMIC_SESSION = std.MatricSession AND nd1.ID_CARGO = '6' AND nd1.tipo_reg = 'TITULO' LEFT OUTER JOIN
                         NODO_RESPONSABLE AS nd ON std.MatricSession = nd.ACADEMIC_SESSION AND nd.ID_CARGO = '3' AND nd.tipo_reg = 'TITULO' LEFT OUTER JOIN
                         ENTIDAD_CERTIFICADO AS EC ON std.MatricSession = EC.ACADEMIC_SESSION LEFT OUTER JOIN
                         ENTIDAD_CERTIFICADO AS ED ON ED.ACADEMIC_SESSION = std.MatricSession
						 WHERE 
                         pe.PEOPLE_CODE_ID LIKE '%VARIABLE=PEOPLE_CODE_ID%'
						 AND cc.CLAVE_CARRERA LIKE '%VARIABLE=CARRERA%'
						 AND ED.CLAVE_CAMPUS LIKE '%VARIABLE=CAMPUS%'
";


$QTitulacionTest = "

SELECT TOP 1
ROW_NUMBER() over(order by std.PEOPLE_CODE_ID desc) as row
,'1.0'as version 
--,CC.CLAVE_CARRERA
--,ED.CLAVE_CAMPUS
,std.PEOPLE_ID as folioControl
,nd.CURP_RESP 
,nd.ID_CARGO
,nd.CARGO
,nd.abrTitulo
,nd.NOMBRE_RESP
,nd.PRIMERAPELLIDO_RESP
,nd.SEGUNDOAPELLIDO_RESP
,'' as sello
,'' certificadoResponsable
,nd.noCertificadoResponsable
,nd1.CURP_RESP as CURP_RESP2
,nd1.ID_CARGO as ID_CARGO2
,nd1.CARGO as CARGO2
,nd1.abrTitulo as abrTitulo2
,nd1.NOMBRE_RESP as NOMBRE_RESP2
,nd1.PRIMERAPELLIDO_RESP as PRIMERAPELLIDO_RESP2
,nd1.SEGUNDOAPELLIDO_RESP as SEGUNDOAPELLIDO_RESP2
,'' as sello2
,'' certificadoResponsable2
,nd1.noCertificadoResponsable as noCertificadoResponsable2
,ED.CLAVE_CAMPUS cvelInstitucion
,ED.CAMPUS nombreInstitucion
,cc.CLAVE_CARRERA cveCarrera
,cc.DESCRIPCION_TITULOS nombreCarrera
,'' fechaInicio
,'' fechaTerminacion
,'' idAutorizacionReconocimiento
,'' autorizacionReconocimiento
,( select distinct PCE.NO_RVOE  
        from PROGRAMA_CERTIFICADO PCE 
        WHERE PCE.CLAVE_CAMPUS=ED.CLAVE_CAMPUS 
        AND PCE.CURRICULUM=STD.CURRICULUM 
        AND PCE.ACADEMIC_SESSION=ED.ACADEMIC_SESSION
        AND PCE.CLAVE_PLAN IN ( IIF(STD.MATRIC_YEAR<=1999 AND (STD.MATRIC_TERM='CUATRI1' OR STD.MATRIC_TERM='SEMES1'),
                        (SELECT 
                        (Case  
                           when CHARINDEX (RVOE.AgreementNumber, RVOE.AgreementNumber)=0 then RVOE.AgreementNumber
                           Else  substring (RVOE.AgreementNumber, CHARINDEX ('|', RVOE.AgreementNumber)+1,len (RVOE.AgreementNumber)) 
                          END)as reg_rvoe
                        FROM RVOE, DEGREQ DER WHERE ( RVOE.OrgCodeId ='O000000009' )AND RVOE.DegreeRequirementId = DER.DegreeRequirementId AND STD.MATRIC_YEAR = DER.MATRIC_YEAR
                        AND STD.MATRIC_TERM = DER.MATRIC_TERM
                        AND STD.PROGRAM  = DER.PROGRAM
                        AND STD.DEGREE       = DER.DEGREE
                        AND STD.CURRICULUM  = DER.CURRICULUM
                        ) 
                         ,STD.MATRIC_YEAR )))   numeroRvoe
                         ,isnull((SELECT        TOP (1) US.CURP
                               FROM            USERDEFINEDIND AS us
                               WHERE        (US.PEOPLE_CODE_ID = PE.PEOPLE_CODE_ID) AND (US.CURP<>'') ),'') AS curp
            , PE.FIRST_NAME+' '+PE.MIDDLE_NAME as nombre
            , IIF (PE.LAST_NAME='.',' ',PE.LAST_NAME) primerApellido
            , IIF (PE.Last_Name_Prefix='.',' ',PE.Last_Name_Prefix) segundoApellido
            ,'' correoElectronico
            ,''fechaExpedicion
            ,''idModalidadTitulacion
            ,''modalidadTitulacion
            ,''fechaExamenProfesional
            ,''fechaExencionExamenProfesional
            ,''cumplioServicioSocial
            ,''idFundamentoLegalServicioSocial
            ,''fundamentoLegalServicioSocial
            ,''idEntidadFederativa
            ,''entidadFederativa
            ,''institucionProcedencia
            ,''idTipoEstudioAntecedente
            ,''tipoEstudioAntecedente
            ,''idEntidadFederativa2
            ,''entidadFederativa2
            ,''fechaInicio2
            ,''fechaTerminacion2
            ,''noCedula 
                    
FROM            STDDEGREQ AS std INNER JOIN
                         PEOPLE AS PE ON PE.PEOPLE_CODE_ID = std.PEOPLE_CODE_ID INNER JOIN
                         CARRERA_CERTIFICADO AS CC ON std.CURRICULUM = CC.CURRICULUM LEFT OUTER JOIN
                         NODO_RESPONSABLE AS nd1 ON nd1.ACADEMIC_SESSION = std.MatricSession AND nd1.ID_CARGO = '6' AND nd1.tipo_reg = 'TITULO' LEFT OUTER JOIN
                         NODO_RESPONSABLE AS nd ON std.MatricSession = nd.ACADEMIC_SESSION AND nd.ID_CARGO = '3' AND nd.tipo_reg = 'TITULO' LEFT OUTER JOIN
                         ENTIDAD_CERTIFICADO AS EC ON std.MatricSession = EC.ACADEMIC_SESSION LEFT OUTER JOIN
                         ENTIDAD_CERTIFICADO AS ED ON ED.ACADEMIC_SESSION = std.MatricSession
                         WHERE 
                        pe.PEOPLE_CODE_ID ='P008148038'
                         AND cc.CLAVE_CARRERA='621301'
                         AND ED.CLAVE_CAMPUS='090316'
";

?>