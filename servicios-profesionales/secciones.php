<?php
    require_once("util/mod-servprof.php");
    
    function HomeMenu(){
        echo "
            <div class='row' style='width:100%; justify-content:center; margin-top:0px; margin-bottom:20px;'>
                <h3>Seleccione la opci&oacute;n de su Preferencia</h3>
            </div>
                <div class='row' style='width:100%; text-align:center; margin-top:0px; margin-bottom:20px;'>
                    <div class='row' style='width:1250px; margin:auto;'>";
                    if($_SESSION["Auth_SS"] == 1){
                        echo "
                            <div class='col-md-3 col-sm-6'>
                                <div class='card' style='width: 18rem;'>
                                    <img class='card-img-top' src='img/servicio-social.jpg' alt='Servicio Social'>
                                    <div class='card-body'>
                                        <h5 class='card-title'>Servicio Social</h5>
                                        <p class='card-text'>Ingrese a la Secci&oacute;n Principal de Servicio Social donde podr&aacute; gestionar toda la informaci&oacute;n obtenida por parte de los Alumnos que han iniciado su Tr&aacute;mite.</p>
                                        <form id='SeleccionaModulo' action='index.php' method='post'>
                                            <input type='hidden' id='ty' name='ty' value='servicio-social'>
                                            <input type='hidden' id='ty' name='SecId' value='solicitudes-validacion'>
                                            <button type='submit' class='btn btn-warning btn-sm'>Ingresar</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        ";
                    }
                    if($_SESSION["Auth_PP"] == 1){
                        echo "
                            <div class='col-md-3 col-sm-6'>
                                <div class='card' style='width: 18rem;'>
                                    <img class='card-img-top' src='img/practicas-profesionales.jpg' alt='Pr&aacute;cticas Profesionales'>
                                    <div class='card-body'>
                                        <h5 class='card-title'>Pr&aacute;cticas Profesionales</h5>
                                        <p class='card-text'>Ingrese a la Secci&oacute;n Principal de Servicio Social donde podr&aacute; gestionar toda la informaci&oacute;n obtenida por parte de los Alumnos que han iniciado su Tr&aacute;mite.</p>
                                        <form id='SeleccionaModulo' action='index.php' method='post'>
                                            <input type='hidden' id='ty' name='ty' value='practicas-profesionales'>
                                            <input type='hidden' id='ty' name='SecId' value='solicitudes-validacion'>
                                            <button type='submit' class='btn btn-info btn-sm'>Ingresar</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        ";
                    }
                    if($_SESSION["Auth_TI"] == 1){
                        echo "
                            <div class='col-md-3 col-sm-6'>
                                <div class='card' style='width: 18rem;'>
                                    <img class='card-img-top' src='img/titulacion.jpg' alt='Titulaci&oacute;n'>
                                    <div class='card-body'>
                                        <h5 class='card-title'>Titulaci&oacute;n</h5>
                                        <p class='card-text'>Ingrese a la Secci&oacute;n Principal de Servicio Social donde podr&aacute; gestionar toda la informaci&oacute;n obtenida por parte de los Alumnos que han iniciado su Tr&aacute;mite.</p>
                                        <form id='SeleccionaModulo' action='index.php' method='post'>
                                            <input type='hidden' id='ty' name='ty' value='titulacion'>
                                            <input type='hidden' id='ty' name='SecId' value='solicitudes-validacion'>
                                            <button type='submit' class='btn btn-primary btn-sm'>Ingresar</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        ";
                    }
                    if($_SESSION["Auth_CO"] == 1){
                        echo "
                            <div class='col-md-3 col-sm-6'>
                                <div class='card' style='width: 18rem;'>
                                    <img class='card-img-top' src='img/convenios.jpg' alt='Convenios'>
                                    <div class='card-body'>
                                        <h5 class='card-title'>Convenios</h5>
                                        <p class='card-text'>Ingrese a la Secci&oacute;n Principal de Servicio Social donde podr&aacute; gestionar toda la informaci&oacute;n obtenida por parte de los Alumnos que han iniciado su Tr&aacute;mite.</p>
                                        <form id='SeleccionaModulo' action='index.php' method='post'>
                                            <input type='hidden' id='ty' name='ty' value='convenios'>
                                            <button type='submit' class='btn btn-success btn-sm'>Ingresar</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        ";
                    }
                    echo "
                </div>
            </div>
        ";
    }

    function ServProf($thisType){
        require_once($thisType.".php");
        switch ($thisType) {
            case "servicio-social": $GLOBALS['ThisServiceName'] = "Servicio Social"; break;
            case "practicas-profesionales": $GLOBALS['ThisServiceName'] = "Pr&aacute;cticas Profesionales"; break;
            case "titulacion": $GLOBALS['ThisServiceName'] = "Titulaci&oacute;n"; break;
            case "convenios": $GLOBALS['ThisServiceName'] = "Convenios"; break;
        }
        echo "
            <div class='col-md-3'>
                <div class='sectionMenu' style='min-height:450px; background-color:#f1f2f3;'>
                    <div class='info' style='padding:10px; margin-left:0px;'>
                        <div class='list-group' style='font-size:14px;'>
                        <span href='#' class='list-group-item active' style='text-align:center; background-color:#0054a4; text-transform:uppercase;'>MEN&Uacute; ".$GLOBALS['ThisServiceName']."</span>
                            <img class='card-img-top' src='img/".$thisType.".jpg' alt='".$GLOBALS['ThisServiceName']."'>";
                            MenuInterno($thisType);
                    echo "</div>
                    </div>
                </div>
            </div>
            <div class='col-md-9'>
                <div class='' style='width:100%; height:auto; background-color:#fff;'>";
                    if(isset($_REQUEST["SecId"])){
                        MuestraContenido();
                    }else{
                        ContenidoPrincipal();
                    }
                echo "</div>
            </div>
        ";
    }
?>