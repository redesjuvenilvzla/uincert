<?php
    $GLOBALS['thisSPId'] = 3;
    function MenuInterno(){
        echo "
            <a href='?ty=".$_REQUEST["ty"]."&SecId=solicitudes-validacion' class='list-group-item'><i class='fa fa-warning'></i> Procesos Iniciados</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=servicios-vigentes' class='list-group-item'><i class='fa fa-file'></i> Autorizaci&oacute;n de Documentos</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=titulos-electronicos' class='list-group-item'><i class='fa fa-file-pdf-o'></i> T&iacute;tulos Electr&oacute;nicos</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=titulos-fisicos' class='list-group-item'><i class='fa fa-file-text'></i> T&iacute;tulos F&iacute;sicos</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=servicios-concluidos' class='list-group-item'><i class='fa fa-check'></i> Servicios Concluidos</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=servicios-cancelados' class='list-group-item'><i class='fa fa-times'></i> Procesos Cancelados</a>
        ";
    }
    
    function MuestraContenido(){
        $obj = new ServProf;
        switch ($_REQUEST["SecId"]) {
            case 'solicitudes-validacion':
            case 'registros-tramite':
            case 'servicios-vigentes':
            case 'titulos-electronicos':
            case 'titulos-fisicos':
            case 'servicios-concluidos':
            case 'servicios-cancelados':
                Validaciones($_REQUEST["SecId"]);
                break;
            case 'info-alumno':
                InfoAlumno($_POST["thisId"]);
                break;
            case 'actualiza-data':
            case 'nuevo-convenio':
                $obj->UpdateInfo($_POST["SecId"], $_POST["thisId"]);
                break;
            case 'historico':
                InfoAlumno($_REQUEST["regId"]);
                break;
            case 'generar-titulo':
            case 'notificar-titulo':
                $obj->GeneraDocumentos($_REQUEST["SecId"], $_REQUEST["StudentId"]);
                break;
            case 'validar-documentos':
                ValidaDocumentos($_REQUEST["StudentId"]);
                break;
            default:
                break;
        }
    }
    
    function ContenidoPrincipal(){
        Validaciones("solicitudes-validacion");
    }
    
    function Validaciones($thisType){
        $obj = new ServProf;
        switch ($thisType) {
            case 'solicitudes-validacion':
                $DisplayButtons     = 1;
                $DisplayData        = 0;
                $thisTitle          = "Listado de Alumnos que comienzan su Proceso de Titulaci&oacute;n";

                $thisTable_1        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(7) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
            case 'servicios-vigentes':
                $DisplayButtons     = 1;
                $DisplayData        = 0;
                $thisTitle          = "Listado de Registros Pendientes de Autorizar sus Documentos";

                $thisTable_1        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(10,19,26,28) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
            case 'titulos-electronicos':
                $DisplayButtons     = 1;
                $DisplayData        = 0;
                $thisTitle          = "Listado de Alumnos en Espera de entrega de su T&iacute;tulo Electr&oacute;nico";

                $thisTable_1        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(49) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
            case 'titulos-fisicos':
                $DisplayButtons     = 1;
                $DisplayData        = 0;
                $thisTitle          = "Listado de Alumnos en Espera de entrega de su T&iacute;tulo F&iacute;sico";

                $thisTable_1        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(50) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
            case 'servicios-concluidos':
                $DisplayButtons     = 1;
                $DisplayData        = 0;
                $thisTitle          = "Listado de Alumnos con Procesos de Titulaci&oacute;n Concluidos";

                $thisTable_1        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(51) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
            case 'servicios-cancelados':
                $DisplayButtons     = 1;
                $DisplayData        = 0;
                $thisTitle          = "Listado de Alumnos con Procesos de Titulaci&oacute;n Canceladas";

                $thisTable_1        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(21) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
        }
        echo "
            <div class='info' style='padding:10px; margin-left:0px; margin-top:0px; text-align:left;'>
                <h3 class='page-header'>".$thisTitle."</h3>
                <div id='content' class='content'>
                    <p>Seleccione desde el listado de Alumnos que han realizado una solicitud de Revisi&oacute;n de Informaci&oacute;n el registro de su interes:
                </div>
            </div>
            <table id='TablaSolicitudesPendientes' class='table table-rounded table-striped table-sm' cellspacing='0' width='100%' style='text-align:center;'>
                <thead>";
                switch ($thisType) {
                    case 'registros-tramite':
                        echo " 
                            <tr class='bg-primary rounded-top text-white' style='text-align:center;'>
                                <th class='col-xs-2'>#</th>
                                <th class='col-xs-2'>Tipo</th>
                                <th class='col-xs-2'>Id Alumno</th>
                                <th class='col-xs-2'>Nombre</th>
                                <th class='col-xs-2'>A.Paterno</th>
                                <th class='col-xs-2'>A.Materno</th>
                                <th class='col-xs-2'>Titulaci&oacute;n</th>
                                <th class='col-xs-2'>Estatus</th>
                                <th class='col-xs-2'>Pagado</th>
                                <th class='col-xs-2'>Fecha</th>
                                <th class='col-xs-2'>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>";
                            echo $obj->SolicitudesValidacion($thisTable_1, $ThisSqlFilter_1, $DisplayButtons);
                        echo "</tbody>";
                        break;
                    case 'solicitudes-validacion':
                    case 'servicios-vigentes':
                    case 'titulos-electronicos':
                    case 'titulos-fisicos':
                    case 'servicios-concluidos':
                    case 'servicios-cancelados':
                        echo "
                            <tr class='bg-primary rounded-top text-white' style='text-align:center;'>
                                <th class='col-xs-2'>Alumno</th>
                                <th class='col-xs-2'>Carrera</th>
                                <th class='col-xs-2'>Convenio</th>
                                <th class='col-xs-2'>Estatus</th>
                                <th class='col-xs-2'>Pagado</th>
                                <th class='col-xs-2'>Titulaci&oacute;n</th>";
                                if($thisType === "titulos-electronicos"){
                                    echo "<th class='col-xs-2'>Estatus</th>";
                                }
                                echo "<th class='col-xs-2'>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            ".$obj->RegistrosServiciosProfesionales($thisTable_1, $ThisSqlFilter_1, $DisplayButtons, $DisplayData)."
                        </tbody>
                        ";
                        break;
                }
            echo "</table>";
    }
?>