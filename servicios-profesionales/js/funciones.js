function cargaContenido(pag, thisType) {
    $("#container").html('');
    $("#container").load(pag + '.php?SecType=' + thisType);
}

$(document).ready(function() {
    $("#TablaSolicitudesPendientes").dataTable().fnDestroy();
    $.fn.dataTable.ext.errMode = 'none';
    $('#TablaSolicitudesPendientes').DataTable({
        "scrollY": "80vh",
        "scrollCollapse": true,
        "paging": true
    });
    $('#TablaDocumentos').DataTable({
        "scrollY": "45vh",
        "scrollCollapse": true,
        "paging": false
    });
    $('.dataTables_length').addClass('bs-select');
    $('#example').dataTable({
        paging: false,
        searching: false
    });
});

$(document).ready(function() {
    $('.ValidateRequest').click(function(e) {
        e.preventDefault();
        var result = confirm("Esta Seguro que desea Continuar?");
        if (result == true) {
            $(this).attr("disabled", true);
            var data = $(this).serializeArray();
            var ThisSection = $(this).attr("Section");
            var StudentId = $(this).attr("StudentId");
            var StdRegId = $(this).attr("StdRegId");
            var PeopleCodeId = $(this).attr("PeopleCodeId");
            var NewStatus = $(this).attr("NewStatus");
            var RegId = $(this).attr("RegId");
            var RegName = $(this).attr("RegName");
            var SPId = $(this).attr("SPId");
            var AuthDocs = $("#TotalAuthDocs").val();
            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            var DefaultNum = 1;

            switch (ThisSection) {
                case "valida-archivo":
                case "rechaza-archivo":
                    var thisComments = $('#AddComments_' + RegId).val();
                    var DocumentName = $('#DocumentName_' + RegId).val();
                    break;
                default:
                    var thisComments = $('#AddComments').val();
                    var DocumentName = $('#DocumentName').val();
                    break;
            }

            data.push({ name: 'ThisSection', value: ThisSection });
            data.push({ name: 'StudentId', value: StudentId });
            data.push({ name: 'StdRegId', value: StdRegId });
            data.push({ name: 'PeopleCodeId', value: PeopleCodeId });
            data.push({ name: 'NewStatus', value: NewStatus });
            data.push({ name: 'thisComments', value: DocumentName + '<br />' + thisComments });
            data.push({ name: 'RegId', value: RegId });
            data.push({ name: 'RegName', value: RegName });
            data.push({ name: 'SPId', value: SPId });

            switch (ThisSection) {
                case 'valida-carta-aceptacion':
                    if(NewStatus == 26){
                        data.push({ name: 'Start_Date', value: "" });
                        data.push({ name: 'End_Date', value: "" });
                    }else{
                        data.push({ name: 'Start_Date', value: start_date });
                        data.push({ name: 'End_Date', value: end_date });
                    }
                    break;
                default:
                    data.push({ name: 'Start_Date', value: start_date });
                    data.push({ name: 'End_Date', value: end_date });
                    break;
            }
            
            $.ajax({
                url: 'getdata.php',
                type: 'POST',
                data: data
            }).done(function(msg) {
                switch (msg.split(" | ")[1]) {
                    case '1':
                        var ProcessComplete = true;
                        var NewMsg = msg.split(" | ")[2];
                        break;
                    case '3':
                        var ProcessComplete = false;
                        break;
                    default:
                        var ProcessComplete = true;
                        var NewMsg = "No fu&eacute; Posible realizar esta acci&oacute;n, int&eacute;ntelo de nuevo por favor";
                        break;
                }
                if (ProcessComplete) {
                    switch (ThisSection) {
                        case 'valida-carta-aceptacion':
                            if(NewStatus == 26){
                                if(start_date != ""){
                                    alert("El Documento ha sido Rechazado. No aplicará el rango de fechas ingresado");
                                }
                                $("#RegDate").fadeOut(800, function() {
                                    $("html, body").animate({ scrollTop: 500 }, 1500)
                                    $("#ThisRequest").fadeOut(800, function() {
                                        $('#PrintMsg').html(NewMsg);
                                        $("#MsgResult").fadeIn(800);
                                    });
                                });
                            }else{
                                $("html, body").animate({ scrollTop: 500 }, 1500)
                                $("#ThisRequest").fadeOut(800, function() {
                                    $('#PrintMsg').html(NewMsg);
                                    $("#MsgResult").fadeIn(800);
                                });
                            }
                            break;
                        default:
                            switch (NewStatus) {
                                case '11':
                                    $("html, body").animate({ scrollTop: 500 }, 1500)
                                    $("#BtnRevision").fadeOut(800, function() {
                                        $("#NewComment").fadeIn(800);
                                        $("#BtnRejected").fadeIn(800);
                                        $("#BtnSuccess").fadeIn(800);
                                    });
                                    break;
                                case '25':
                                    var CurrentAuthDocs = parseInt(AuthDocs) + parseInt(DefaultNum);
                                    $("#TotalAuthDocs").attr("value", (CurrentAuthDocs));
                                    $("#success_" + RegId).attr("disabled", true);
                                    $("#pre_decline_" + RegId).attr("disabled", false);
                                    $("#DocStatus_" + RegId).html("Documento Aceptado");
                                    $("#DocStatus_" + RegId).css("background-color", "#13b002");
                                    break;
                                case '26':
                                    $.fancybox.close();
                                    var CurrentAuthDocs = AuthDocs - DefaultNum;
                                    $("#TotalAuthDocs").attr("value", (CurrentAuthDocs));
                                    $("#pre_decline_" + RegId).attr("disabled", true);
                                    $("#success_" + RegId).attr("disabled", true);
                                    $("#DocStatus_" + RegId).html("Documento Rechazado");
                                    $("#DocStatus_" + RegId).css("color", "#FFFFFF");
                                    $("#DocStatus_" + RegId).css("background-color", "#800000");
                                    break;
                                case '49':
                                    $("html, body").animate({ scrollTop: 500 }, 1500)
                                    $("#TablaDocumentos").fadeOut(800, function() {
                                        $('#PrintMsg').html(NewMsg);
                                        $("#MsgResult").fadeIn(800);
                                    });
                                    break;
                                default:
                                    $("html, body").animate({ scrollTop: 500 }, 1500)
                                    $("#ThisRequest").fadeOut(800, function() {
                                        $('#PrintMsg').html(NewMsg);
                                        $("#MsgResult").fadeIn(800);
                                    });
                                    break;
                            }
                            switch (NewStatus) {
                                case '25':
                                case '26':
                                    if (CurrentAuthDocs >= 12) {
                                        $('#ValidateAllFiles').attr('disabled', false);
                                    } else {
                                        $('#ValidateAllFiles').attr('disabled', true);
                                    }
                                    break;
                            }
                            break;

                    }
                } else {
                    $("html, body").animate({ scrollTop: 500 }, 1500)
                    $("#ThisRequest").fadeOut(800, function() {
                        $(location).delay(500).attr('href', 'index.php?ty=practicas-profesionales&SecId=carta-final-sp&StudentId=' + StudentId);
                    });
                }
            });
        }
    })
})

$(document).ready(function() {
    $('#start_date').css('background-color', '#ffffff');
    $('#end_date').css('background-color', '#f1f2f3');
    $('#start_date').dateTimePicker({
    });
})

$(document).ready(function() {
    $('#container').mouseover(function() {
        var thisDate = $('#start_date').val();
        var thisTime = $('#start_date').attr("ConteoTiempo");
        var MounthCount = parseInt(thisTime)+1;
        var NewDate = new Date(thisDate);
        
        NewDate.setMonth(NewDate.getMonth()+MounthCount);
        var NewYear = NewDate.getFullYear();
        var NewMonth = NewDate.getMonth();
        var NewDay = NewDate.getDate()+1;
        
        if(NewMonth < 10){NewMonth = '0'+NewMonth;}
        if(NewDay < 10){NewDay = '0'+NewDay;}

        FormatDate = NewYear+'-'+NewMonth+'-'+NewDay;
        if(FormatDate === "NaN-NaN-NaN"){
            FormatDate = "Ajuste Automático";
        }
        $('#end_date').attr('value', FormatDate);
    })
})

$(document).ready(function() {
    $('#GlobalForm').submit(function(e) {
        e.preventDefault();
        var result = confirm("Esta Seguro que desea Continuar?");
        if (result == true) {
            $(this).attr("disabled", true);
            var data = $(this).serializeArray();
            var ThisSection = $(this).attr("type");
            data.push({ name: 'ThisSection', value: ThisSection });
            
            $.ajax({
                url: 'getdata.php',
                type: 'POST',
                data: data
            }).done(function(msg) {
                if (msg.split(" | ")[1] == 1) {
                    var NewMsg = msg.split(" | ")[2];
                } else {
                    var NewMsg = "No fu&eacute; Posible realizar esta acci&oacute;n, int&eacute;ntelo de nuevo por favor";
                }
                $("html, body").animate({ scrollTop: 500 }, 1500)
                $("#ThisRequest").fadeOut(800, function() {
                    $('#PrintMsg').html(NewMsg);
                    $("#MsgResult").fadeIn(800);
                });
            });
        }
    })
})

$(document).ready(function() {
    $('.ValidaRegistro').click(function(e) {
        e.preventDefault();
        var result = confirm("Esta Seguro que desea Continuar?");
        if (result == true) {
            $(this).attr("disabled", true);
            var data = $(this).serializeArray();
            var RegId = $(this).attr("RegId");
            var ThisSection = $(this).attr("Section");

            data.push({ name: 'RegId', value: RegId });
            data.push({ name: 'ThisSection', value: ThisSection });

            $.ajax({
                url: 'getdata.php',
                type: 'POST',
                data: data
            }).done(function(msg) {
                if (msg.split(" | ")[1] == 1) {
                    $("#Registro_" + RegId).fadeOut();
                } else {
                    var NewMsg = msg.split(" | ")[2];
                    alert(NewMsg);
                }
            });
        }
    })
})


$(document).ready(function() {
    $('.CancelService').click(function(e) {
        e.preventDefault();
        alert('Prueba');
    })
})