<?php
    header('Content-Type: text/html; charset=UTF-8');
    $SecureSection = true;
    define('__ROOT__', dirname(dirname(__FILE__)));
    require_once("util/mod-servprof.php");
    
    $obj = new ServProf;
    if(isset($_SESSION['susu'])){
        switch ($_POST["ThisSection"]) {
            case 'actualiza-data':
            case 'nuevo-convenio':
                $ThisTable  = "ACC_CAMBIO_DATOS_SPR_SP";
                $ThisFilter = "WHERE ID = ".$_POST["RegId"];
                $ThisStatus = "VALIDACI&Oacute;N REALIZADA CON &Eacute;XITO";
                $obj->globalStatusUpdate($ThisTable, $_POST["StudentId"], $_POST["PeopleCodeId"], $_SESSION["PerId"], $_POST["RegId"], $_POST["RegName"], $_POST["NewStatus"], $_POST["thisComments"], $_POST["SPId"], $ThisFilter);
                break;
            case 'solicitud-ss-iniciada':
            case 'solicitud-sp-iniciada':
            case 'carta-presentacion-ss':
            case 'carta-presentacion-sp':
            case 'carta-liberacion-ss':
            case 'carta-liberacion-sp':
            case 'carta-final-sp':
            case 'generar-titulo':
            case 'notificar-titulo':
                $ThisTable  = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisFilter = "WHERE Acc_Progress_Status_Id = ".$_POST["StudentId"];
                switch ($_POST["ThisSection"]) {
                    case 'solicitud-ss-iniciada':
                    case 'solicitud-sp-iniciada':
                        $ThisStatus = "CONSTANCIA DE CREDITOS GENERADA";
                        break;
                    case 'notificar-titulo':
                        $ThisStatus = "Notificaci&oacute;n Enviada";
                        break;
                    default:
                        $ThisStatus = "CARTA DE PRESENTACI&Oacute;N GENERADA";
                        break;
                }
                $obj->globalStatusUpdate($ThisTable, $_POST["StudentId"], $_POST["PeopleCodeId"], $_SESSION["PerId"], $_POST["RegId"], $_POST["RegName"], $_POST["NewStatus"], $_POST["thisComments"], $_POST["SPId"], $ThisFilter);
                break;
            case 'agregar-convenio':
                $ThisTable  = "ACC_CAT_CONVENIO_SP";
                $ThisRows   = "NOMBRE_EMPRESA, REPONSABLE, CARGO_RESPONSABLE, DIRECCION, COLONIA, MUNICIPIO, ID_ENTIDAD_FEDERATIVA, CODIGO_POSTAL, SECTOR, TELEFONO, EXTENCION, HORARIO_SERVICIO, CAT_TIPO_PRESTACION, ESTATUS_ID";
                $ThisValues = "'".utf8_decode($_POST["NOMBRE_EMPRESA"])."', '".utf8_decode($_POST["REPONSABLE"])."', '".utf8_decode($_POST["CARGO_RESPONSABLE"])."', '".utf8_decode($_POST["DIRECCION"])."', '".utf8_decode($_POST["COLONIA"])."', '".utf8_decode($_POST["MUNICIPIO"])."', '".utf8_decode($_POST["ENTIDAD_FEDERATIVA"])."', '".$_POST["CODIGO_POSTAL"]."', '".$_POST["SECTOR"]."', '".$_POST["TELEFONO"]."', '".$_POST["EXTENCION"]."', '".$_POST["HORARIO_SERVICIO"]."', ".$_POST["CAT_TIPO_PRESTACION"].", 0";
                $ThisStatus = "NUEVO CONVENIO GUARDADO";
                $obj->globalDataInsert($ThisTable, $ThisRows, $ThisValues);
                break;
            case 'editar-convenio':
                $ThisTable  = "ACC_CAT_CONVENIO_SP";
                $ThisValues = "NOMBRE_EMPRESA='".utf8_decode($_POST["NOMBRE_EMPRESA"])."', REPONSABLE='".utf8_decode($_POST["REPONSABLE"])."', CARGO_RESPONSABLE='".utf8_decode($_POST["CARGO_RESPONSABLE"])."', RESPONSABLE_LEGAL='".utf8_decode($_POST["RESPONSABLE_LEGAL"])."', RESPONSABLE_SERVICIO_SOCIAL='".utf8_decode($_POST["RESPONSABLE_SERVICIO_SOCIAL"])."', DIRECCION='".utf8_decode($_POST["DIRECCION"])."', COLONIA='".utf8_decode($_POST["COLONIA"])."', MUNICIPIO='".utf8_decode($_POST["MUNICIPIO"])."', ID_ENTIDAD_FEDERATIVA='".utf8_decode($_POST["ENTIDAD_FEDERATIVA"])."', CODIGO_POSTAL='".$_POST["CODIGO_POSTAL"]."', SECTOR='".$_POST["SECTOR"]."', TELEFONO='".$_POST["TELEFONO"]."', EXTENCION='".$_POST["EXTENCION"]."', HORARIO_SERVICIO='".$_POST["HORARIO_SERVICIO"]."', CAT_TIPO_PRESTACION='".$_POST["CAT_TIPO_PRESTACION"]."'";
                $ThisFilter = "WHERE ID = ".$_POST["CONVENIO_ID"];
                $ThisStatus = "CONVENIO ACTUALIZADO";
                $obj->globalDataUpdate($ThisTable, $ThisValues, $ThisFilter);
                break;
            case 'autoriza-convenio':
                $ThisTable  = "ACC_CAT_CONVENIO_SP";
                $ThisValues = "ESTATUS_ID = 1";
                $ThisFilter = "WHERE ID = ".$_POST["RegId"];
                $ThisStatus = "";
                $obj->globalDataUpdate($ThisTable, $ThisValues, $ThisFilter);
                break;
            case 'desactiva-convenio':
                $ThisTable  = "ACC_CAT_CONVENIO_SP";
                $ThisValues = "ESTATUS_ID = 0";
                $ThisFilter = "WHERE ID = ".$_POST["RegId"];
                $ThisStatus = "";
                $obj->globalDataUpdate($ThisTable, $ThisValues, $ThisFilter);
                break;
            case 'cancela-servicio':
                $ThisStatus = "";
                $obj->cancelService($_POST["StudentId"]);
                break;
            case 'valida-archivo';
            case 'rechaza-archivo';
            case 'valida-carta-aceptacion';
                switch ($_POST["RegName"]) {
                    case 'Generales':
                        $ThisStatus     = "";
                        $CurrentTable   = "ACC_Documentos_Generales";
                        $CurrentFilter  = "WHERE PEOPLE_CODE_ID = '".$_POST["PeopleCodeId"]."' AND ID_DOCUMENTO = ".$_POST["RegId"];
                        break;
                    case 'Academicos':
                        $ThisStatus     = "";
                        $CurrentTable   = "ACC_ALUMNO_DOCUMENTOS_SP";
                        $CurrentFilter  = "WHERE Acc_Progress_Status_Id = ".$_POST["StudentId"]." AND ID_DOCUMENTO = ".$_POST["RegId"];
                        break;
                }
                switch ($_POST["ThisSection"]) {
                    case 'valida-carta-aceptacion':
                        $ThisStatus     = "Validación Realizada";
                        $CurrentTable   = "ACC_ALUMNO_DOCUMENTOS_SP";
                        $CurrentFilter  = "WHERE Acc_Progress_Status_Id = ".$_POST["StudentId"]." AND ID_DOCUMENTO = ".$_POST["RegId"];
                        break;
                }
                $ThisTable  = $CurrentTable;
                $ThisValues = "ESTATUS_ID = ".$_POST["NewStatus"];
                $ThisFilter = $CurrentFilter;
                $obj->globalFileUpdate($ThisTable, $_POST["StudentId"], $_POST["StdRegId"], $_SESSION["PerId"], $_POST["RegId"], $_POST["RegName"], $_POST["NewStatus"], $_POST["thisComments"], $_POST["SPId"], $_POST["Start_Date"], $_POST["End_Date"], $ThisValues, $ThisFilter);
                break;
            case 'autorizacion-archivos';
                $ThisTable  = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisValues = "ESTATUS_ID = ".$_POST["NewStatus"];
                $ThisFilter = "WHERE Acc_Progress_Status_Id = ".$_POST["StudentId"];
                $ThisStatus = "";
                $obj->globalFileUpdate($ThisTable, $_POST["StudentId"], $_POST["StdRegId"], $_SESSION["PerId"], $_POST["RegId"], $_POST["RegName"], $_POST["NewStatus"], $_POST["thisComments"], $_POST["SPId"], $ThisValues, $ThisFilter);
                break;
        }
        switch ($_POST["ThisSection"]) {
            case 'solicitud-ss-iniciada':
            case 'solicitud-sp-iniciada':
            case 'carta-presentacion-ss':
            case 'carta-presentacion-sp':
            case 'carta-liberacion-ss':
            case 'carta-liberacion-sp':
            case 'carta-final-sp':
			case 'generar-titulo':
                require_once("generaPDF.php");
                break;
        }
        echo $ThisStatus;
    }
?>