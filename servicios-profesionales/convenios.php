<?php
    $GLOBALS['thisSPId'] = 4;
    function MenuInterno(){
        echo "
            <a href='?ty=".$_REQUEST["ty"]."&SecId=alta-convenio' class='list-group-item'><i class='fa fa-gears'></i> Alta de Nuevo Convenio</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=convenios-autorizados' class='list-group-item'><i class='fa fa-check'></i> Convenios Autorizados</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=convenios-proceso' class='list-group-item'><i class='fa fa-warning'></i> Convenios Desactivados</a>
        ";
    }
    
    function MuestraContenido(){
        $obj = new ServProf;
        switch ($_REQUEST["SecId"]) {
            case 'alta-convenio':
                HeaderConvenios($_REQUEST["SecId"]);
                $obj->FormularioConvenio("WHERE ID = 0");
                break;
            case 'editar-convenio':
                HeaderConvenios($_REQUEST["SecId"]);
                $obj->FormularioConvenio("WHERE ID = ".$_REQUEST["thisid"]);
                break;
            case 'convenios-autorizados':
            case 'convenios-proceso':
                MuestraConvenios($_REQUEST["SecId"]);
                break;
        }
    }
    
    function ContenidoPrincipal(){
        MuestraConvenios('convenios-autorizados');
    }

    function HeaderConvenios($thisType){
        $obj = new ServProf;
        $RcdDate = $obj->formatDate(date("Y-m-d"));
        switch ($thisType) {
            case 'alta-convenio':
                $thisTitle  = "ALTA DE NUEVO CONVENIO";
                $TxtMsg     = "Ingrese los datos necesarios para dar de alta un nuevo Convenio";
                break;
            case 'editar-convenio':
                $thisTitle  = "EDITAR CONVENIO";
                $TxtMsg     = "Ingrese los datos necesarios corregir la informaci&oacute;n de este Convenio";
                break;
            case 'convenios-autorizados':
                $thisTitle  = "CONVENIOS AUTORIZADOS";
                $TxtMsg     = "Listado de Convenios Autorizados y listos para ser integrados en las solicitudes de Servicio Social";
                break;
            case 'convenios-proceso':
                $thisTitle  = "CONVENIOS EN PROCESO";
                $TxtMsg     = "Listado de Convenios en Proceso de Autorizaci&oacute;n";
                break;
        }
        echo "
            <div class='info' style='padding:0px; text-align:left;'>
                <div id='invoice'>
                    <div class='invoice overflow-auto'>
                        <div style='min-width: 600px'>
                            <div class='row contacts'>
                                <div class='col invoice-to'>
                                    <div class='text-gray-light'>ADMINISTRADOR DE CONVENIOS:</div>
                                    <h2 class='to'>".$thisTitle."</h2>
                                    <div class='address'>Universidad Insurgentes | Unidad de Servicios Escolares</div>
                                    <div class='email'><a href='mailto:correo@uinsurgentes.com.mx'>correo@uinsurgentes.com.mx</a></div>
                                </div>
                                <div class='col invoice-details'>
                                    <h6 class='to'>XXX-XX-X</h6>
                                    <h1 class='invoice-id'></h1>
                                    <div class='date'>Fecha Actual: ".$RcdDate."</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id='content' class='content'>
                    <p>".$TxtMsg."</p>
                </div>
            </div>
        ";
    }
    
    function MuestraConvenios($thisType){
        $obj = new ServProf;
        HeaderConvenios($thisType);
        switch ($thisType) {
            case 'alta-convenio':
                $thisFilter = "";
                break;
            case 'convenios-autorizados':
                $thisFilter = "WHERE ESTATUS_ID = 1";
                break;
            case 'convenios-proceso':
                $thisFilter = "WHERE ESTATUS_ID = 0";
                break;
        }
        echo "
            <div class='info' style='padding:0px; text-align:left;'>
                <br />
                <table id='TablaDocumentos' class='table table-rounded table-striped table-sm' cellspacing='0'>
                    <thead>
                        <tr class='bg-primary rounded-top text-white'>
                            <th class='col-xs-2'>#</th>
                            <th class='col-xs-2'>Nombre de la Empresa</th>
                            <th class='col-xs-2'>Direcci&oacute;n</th>
                            <th class='col-xs-2'>Tel&eacute;fono</th>
                            <th class='col-xs-2'>Sector</th>
                            <th class='col-xs-2'>Código Postal</th>
                            <th class='col-xs-2' style='text-align:center;'>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        ".$obj->ListadoConvenios($thisType, $thisFilter)."
                    </tbody>
                </table>
            </div>
        ";
    }
?>