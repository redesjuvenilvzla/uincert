<?php
    setlocale(LC_TIME, 'spanish');
    setlocale(LC_MONETARY, 'es_mx');
	date_default_timezone_set('America/Mexico_City');
	
	if (session_status() == PHP_SESSION_NONE){
		session_start();
	}

	require_once(__ROOT__."/util/security.php");
	require_once(__ROOT__."/config/db_config.php");

	class ServProf{
		public function SP_Conn(){
			$connectionInfo = array("Database"=>DATABASE, "UID"=>USERNAME, "PWD"=>PASSWORD);
			$this->cnn = sqlsrv_connect(SERVER, $connectionInfo);
			if ($this->cnn)
			{
				$this->r = "";
			} else {
				$this->r = "Fallo en la Conexion!";
				die(print_r(sqlsrv_errors()));
			}	  
			return $this->r;

			sqlsrv_close($this->cnn);
		}

		public function xQueryI($sql){
			self::SP_Conn();
			$this->sql = $sql;
			$this->rQuery = sqlsrv_query($this->cnn, $this->sql);
			return $this->rQuery;

			sqlsrv_close($this->cnn);
		}
		
		// Funciones - Servicios Profesionales
		// ****************************************************************************************
			public function SolicitudesValidacion($thisTable, $thisFilter, $DisplayButtons){
				self::SP_Conn();
				self::xQueryI("SELECT * FROM ".$thisTable." ".$thisFilter);
				$this->result  = "";
				$DisplayRcds = true;
				while ($this->data = sqlsrv_fetch_array($this->rQuery)) {
					$this->ThisRegId			= self::getThisRcd("ACC_STDDEGREQ_SS_ID", "ACC_Progress_Status_SP", "WHERE Acc_Progress_Status_Id = ".$this->data["Acc_Progress_Status_Id"]);
					$this->PeopleCodeId			= self::getThisRcd("PEOPLE_CODE_ID", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
					$this->StudentName			= self::getThisRcd("PRIMER_NOMBRE", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
					$this->StudentMiddleName	= self::getThisRcd("APELLIDO_PATERNO", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
					$this->StudentLastName		= self::getThisRcd("APELLIDO_MATERNO", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
					$this->MatricSession		= self::getThisRcd("MATRIC_SESSION", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
					$this->StatusName			= self::getThisRcd("DESCRIPCION_ESTATUS", "ACC_CAT_ESTATUS_SPR_SP", "WHERE ESTATUS_ID = ".$this->data["ESTATUS_ID"]);
					$this->StatusName			= str_replace("[SERVICIO]", $GLOBALS['ThisServiceName'], $this->StatusName);

					if(strtoupper($_SESSION["campus"]) === "NULL"){
						$MyCampus = true;
					}else{
						if($_SESSION["campus"] === $this->MatricSession){
							$MyCampus = true;
						}else{
							$MyCampus = false;
						}
					}

					if($MyCampus){
						switch ($thisTable) {
							case 'ACC_CAMBIO_DATOS_SPR_SP':
								$StatusPagado = "";
								$this->TipoLiberacion = "N/A";
								switch ($this->data["TIPO_ALTA"]) {
									case 0:
										$this->SectionId 	= "actualiza-data";
										$this->TipoAlta 	= "Actualizaci&oacute;n de datos";
									break;
									case 1:
										$this->SectionId 	= "nuevo-convenio";
										$this->TipoAlta 	= "Nuevo Convenio";
									break;
								}
								switch ($this->data["ESTATUS_ID"]) {
									case 11:
										$SectionButton = "<button type='submit' class='btn btn-warning btn-sm'>Revisar</button>";
										break;
									default:
										$SectionButton = "<button type='submit' class='btn btn-primary btn-sm'>Comenzar</button>";
										break;
								}
								$this->RcdDate = self::formatDate($this->data["FECHAREGISTRO"]->format('Y-m-d'));
								break;
							default:
								$this->TipoLiberacion = self::getDbRowName("DESCRIPCION_TIPO_LIBERACION", "ACC_CAT_TIPO_LIBERACION_SS", "WHERE ID = ".$this->data["TIPO_LIBERACION_ID"],1);
								switch ($this->data["PAGADO"]) {
									case 0:
										$StatusPagado = "<span style='color:#8c0003'><i class='fa fa-times fa-fw fa-lg m-r-3'></i></span> No";
										break;
									case 1:
										$StatusPagado = "<span style='color:#00ae00'><i class='fa fa-check fa-fw fa-lg m-r-3'></i></span> Si";
										break;
								}
								switch ($this->data["ESTATUS_ID"]) {
									case 7:
									case 16:
										switch ($GLOBALS["thisSPId"]) {
											case 1: $this->SectionId = "solicitud-ss-iniciada"; break;
											case 2: $this->SectionId = "solicitud-sp-iniciada"; break;
											case 3: $this->SectionId = "solicitud-tit-iniciada"; break;
										}
										$this->TipoAlta 	= "Solicitud Iniciada";
										$this->GetUserDoc	= self::getDbRowName("NOMBRE_DOCUMENTO", "ACC_Documentos_Generales", "WHERE ID_DOCUMENTO = 2 AND PEOPLE_CODE_ID = '".$this->PeopleCodeId."'", 1);
										if($this->GetUserDoc <> "error"){
											if($this->data["PAGADO"] == 1){
												if($_REQUEST["SecId"] === "solicitudes-validacion"){ 
													$SectionButton	= "<button type='submit' class='btn btn-success btn-sm'><i class='fa fa-file-pdf-o fa-fw fa-lg m-r-3'></i> Generar</button>";
												}else{ 
													$DisplayRcds 	= false;
													$SectionButton	= "N/A";
												}
											}else{
												if(isset($_REQUEST["SecId"])){
													if($_REQUEST["SecId"] === "solicitudes-validacion"){ 
														$SectionButton	= "<button type='submit' class='btn btn-success btn-sm'><i class='fa fa-file-pdf-o fa-fw fa-lg m-r-3'></i> Generar</button>";
													}else{ 
														$DisplayRcds 	= false;
														$SectionButton	= "N/A";
													}
												}else{
													$DisplayRcds 	= false;
													$SectionButton	= "N/A";
												}
											}
										}else{
											if(isset($_REQUEST["SecId"])){
												$CurrentSecId = $_REQUEST["SecId"];
											}else{
												$CurrentSecId = "solicitudes-validacion";
											}
											if($CurrentSecId === "solicitudes-validacion"){ 
												$DisplayRcds 	= false;
												$SectionButton	= "N/A";
											}else{ 
												$DisplayRcds 	= true;
												$SectionButton	= "N/A";
											}
										}
										break;
									case 15:
										$this->SectionId 	= "pago-realizado";
										$this->TipoAlta 	= "Solicitud Realizado";
										$SectionButton		= "N/A";
										break;
									case 17:
										switch ($this->data["TIPO_LIBERACION_ID"]) {
											case 3:
												if($_REQUEST["SecId"] === "solicitudes-validacion"){ 
													$DisplayRcds = false;
													$this->SectionId 	= "excencion-enfermedad";
													$this->TipoAlta 	= "Excenci&oacute;n por Enfermedad";
													$SectionButton		= "N/A";
												}else{ 
													$this->SectionId 	= "excencion-enfermedad";
													$this->TipoAlta 	= "Excenci&oacute;n por Enfermedad";
													$SectionButton		= "N/A";
												}
												break;
											default:
												$DisplayRcds = true;
												$this->TipoAlta = "Carta de Presentaci&oacute;n";
												switch ($GLOBALS["thisSPId"]) {
													case 1: $this->SectionId = "carta-presentacion-ss"; break;
													case 2: $this->SectionId = "carta-presentacion-sp"; break;
												}
												if($_REQUEST["SecId"] === "registros-tramite"){ 
													$DisplayRcds 		= false;
													$SectionButton		= "N/A";
												}else{ 
													$SectionButton		= "<button type='submit' class='btn btn-purple btn-sm'><i class='fa fa-file-pdf-o fa-fw fa-lg m-r-3'></i> Generar</button>";
												}
												break;
										}
										break;
									case 18:
										switch ($GLOBALS["thisSPId"]) {
											case 1: $this->SectionId = "carta-presentacion-ss"; break;
											case 2: $this->SectionId = "carta-presentacion-sp"; break;
											case 3: $this->SectionId = "solicitud-tit-iniciada"; break;
										}
										$this->TipoAlta 	= "Carta de Presentaci&oacute;n";
										$SectionButton		= "N/A";
										break;
									case 22:
										switch ($GLOBALS["thisSPId"]) {
											case 1: $this->SectionId = "carta-aceptacion-ss"; break;
											case 2: $this->SectionId = "carta-aceptacion-sp"; break;
										}
										$this->SectionId 	= "";
										$this->TipoAlta 	= "Servicio Social Aceptado";
										$SectionButton		= "<button type='submit' class='btn btn-success btn-sm'><i class='fa fa-check fa-fw fa-lg m-r-3'></i> Validar</button>";
										break;
								}
								if($this->data["FECHAREGISTRO"] <> ""){
									$this->RcdDate = self::formatDate($this->data["FECHAREGISTRO"]->format('Y-m-d'));
								}else{
									$this->RcdDate = "";
								}
								break;
						}

						if($DisplayButtons == 0){
							switch ($GLOBALS["thisSPId"]) {
								case 1:
								case 2:
									switch ($this->data["ESTATUS_ID"]) {
										case 18:
										case 19:
											$this->CartaAceptacion = self::getDbRowName("NOMBRE_DOCUMENTO", "ACC_ALUMNO_DOCUMENTOS_SP", "WHERE ID_DOCUMENTO = 7 AND PEOPLE_CODE_ID = '".$this->PeopleCodeId."' AND ESTATUS_ID NOT IN(25,26)", 1);
											if($this->CartaAceptacion <> "error"){
												$this->SectionId 	= "valida-carta-aceptacion";
												$this->TipoAlta 	= "Carta de Aceptaci&oacute;n Cargada";
												$DisplayButton		= "<button type='submit' class='btn btn-success btn-sm'><i class='fa fa-check fa-fw fa-lg m-r-3'></i> Validar</button>";
											}else{
												$DisplayRcds = false;
											}
											break;
										default:
											$DisplayButton = "N/A";
											break;
									}
									break;
								default:
									if($DisplayButtons == 0){
										$DisplayButton = "N/A";
									}else{
										$DisplayButton = $SectionButton;
									}
									break;
							}
						}else{
							$DisplayButton = $SectionButton;
						}

						if($DisplayRcds){
							$this->result .= "
								<tr class='SmallTable'>
									<td style='width:20px; text-align:center;'>".$this->data["ID"]."</td>
									<td>".$this->TipoAlta."</td>
									<td><a href='?ty=".$_REQUEST["ty"]."&SecId=historico&regId=".$this->data["Acc_Progress_Status_Id"]."'>".$this->PeopleCodeId."</a></td>
									<td>".$this->StudentName."</td>
									<td>".$this->StudentMiddleName."</td>
									<td>".$this->StudentLastName."</td>";
									switch ($GLOBALS["thisSPId"]) {
										case 1:
										case 3:
											$this->result .= "<td>".utf8_encode($this->TipoLiberacion)."</td>";
											break;
									}
									$this->result .= "
									<td>".$this->StatusName."</td>
									<td>".$StatusPagado."</td>
									<td>".$this->RcdDate."</td>
									<td style='width:200px;'>
										<form id='EnviaData' action='index.php' method='post'>
											<input type='hidden' id='ty' name='ty' value='".$_REQUEST["ty"]."'>
											<input type='hidden' id='SecId' name='SecId' value='".$this->SectionId."'>
											<input type='hidden' id='StudentId' name='StudentId' value='".$this->data["Acc_Progress_Status_Id"]."'>
											<input type='hidden' id='thisId' name='thisId' value='".$this->data["ID"]."'>
											".$DisplayButton."
										</form>
									</td>
								</tr>
							";
						}
					}
				}
				return $this->result;
			}

			public function RegistrosServiciosProfesionales($thisTable, $thisFilter, $DisplayButtons, $DisplayData){
				self::SP_Conn();
				self::xQueryI("SELECT * FROM ".$thisTable." ".$thisFilter);
				$this->result  = "";
				while ($this->data = sqlsrv_fetch_array($this->rQuery)) {
					$this->ThisRegId 		= self::getThisRcd("ACC_STDDEGREQ_SS_ID", "ACC_Progress_Status_SP", "WHERE Acc_Progress_Status_Id = ".$this->data["Acc_Progress_Status_Id"]);
					$this->PeopleCodeId		= self::getThisRcd("PEOPLE_CODE_ID", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
					$this->Cv_Carrera 		= self::getThisRcd("CV_CARRERA", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
					$this->FindDocsGen 		= self::getDbRowName("NOMBRE_DOCUMENTO", "ACC_Documentos_Generales", "WHERE PEOPLE_CODE_ID = '".$this->PeopleCodeId."' AND ESTATUS_ID IN(10,11,26)", 1);
					$this->FindDocsAc 		= self::getDbRowName("NOMBRE_DOCUMENTO", "ACC_ALUMNO_DOCUMENTOS_SP", "WHERE Acc_Progress_Status_Id = ".$this->data["Acc_Progress_Status_Id"]." AND ESTATUS_ID IN(10,11,26)", 1);
					$this->GetTotalDocs		= self::getTotalRcdsCount("ACC_ALUMNO_DOCUMENTOS_SP", "WHERE Acc_Progress_Status_Id = ".$this->data["Acc_Progress_Status_Id"]);
					$this->MatricSession	= self::getThisRcd("MATRIC_SESSION", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
					
					if(strtoupper($_SESSION["campus"]) === "NULL"){
						$MyCampus = true;
					}else{
						if($_SESSION["campus"] === $this->MatricSession){
							$MyCampus = true;
						}else{
							$MyCampus = false;
						}
					}

					if($MyCampus){
						if($this->data["FECHA_INICIO"] <> NULL){
							$thisEndDate = date("Y-m-d H:i:s");
							$this->RcdDateIni = self::formatDate($this->data["FECHA_INICIO"]->format('Y-m-d'));
							$this->RcdDateEnd = self::formatDate($this->data["FECHA_TERMINO"]->format('Y-m-d'));
							switch ($this->data["TIPO_LIBERACION_ID"]) {
								case 2:
									$this->TotalTime = self::calculaFechas($this->data["FECHA_INICIO"]->format('Y-m-d H:i:s'), $thisEndDate, $this->data["CVE_CARRERA"]);	
									break;
								default:
									$this->TotalTime = "Concluido";
									break;
							}
						}else{
							switch ($GLOBALS["thisSPId"]) {
								case 3:
									$this->TotalTime 	= "";
									$this->RcdDateIni 	= "En Progreso";
									break;
								default:
									$this->TotalTime 	= "";
									$this->RcdDateIni 	= "N/A";
									break;
							}
						}
						
						switch ($GLOBALS["thisSPId"]) {
							case 1:
								$this->ProcessHours	= "";
								$this->ThisHours 	= "";
								switch ($this->data["TIPO_LIBERACION_ID"]) {
									case 3:
									case 4:
										$this->TotalFiles = 1;
										break;
									default:
										$this->TotalFiles = 5;
										break;
								}
								break;
							case 2:
								$this->TotalFiles = 5;
								switch ($this->data["CVE_CARRERA"]) {
									case 15: //Administración de Empresas Turísticas
									case 28: //Gastronomía
										$this->ProcessHours	= 1400;
										break;
									default:
										$this->ProcessHours	= 400;
										break;
								}
								$this->ThisHours 	= self::getDbRowName("HORAS", "ACC_PRACTICAS_PROFESIONALES_SP", "WHERE Acc_Progress_Status_Id = ".$this->data["Acc_Progress_Status_Id"], 0);
								$this->TotalHours 	= self::getDbRowName("Horas", "ACC_Horas_SP", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId, 1);
								if($this->TotalHours === "error"){$this->TotalHours = "";}
								break;
							case 3:
								$this->TotalFiles = 9;
								break;
						}

						if($this->TotalTime === "Concluido"){
							$this->RcdDateEnd = $this->TotalTime."<br />".self::formatDate($this->data["FECHA_TERMINO"]->format('Y-m-d'));
							if($DisplayData == 1){$DisplayRcds = true;}else{$DisplayRcds = true;}
							if($this->FindDocsGen === "error" AND $this->FindDocsAc === "error"){
								if($this->data["ESTATUS_ID"] == 20){ 
									$this->SectionId	= "servicio-concluido";
									$DisplayButton 		= "N/A";
								}else{ 
									switch ($GLOBALS["thisSPId"]) {
										case 1: 
											if($this->GetTotalDocs >= $this->TotalFiles){
												$this->SectionId 	= "carta-liberacion-ss";
												$DisplayButton		= "<button type='submit' class='btn btn-primary btn-sm'><i class='fa fa-check fa-fw fa-lg m-r-3'></i> Finalizar</button>";
											}else{
												$this->SectionId	= "validar-documentos";
												$DisplayButton 		= "<button type='submit' class='btn btn-success btn-sm'><i class='fa fa-file fa-fw fa-lg m-r-3'></i> Validar</button>";
											}
											break;
										case 2: 
											if($this->ThisHours >= $this->ProcessHours){
												if($this->GetTotalDocs >= $this->TotalFiles){
													$this->SectionId 	= "carta-liberacion-sp";
													$DisplayButton		= "<button type='submit' class='btn btn-primary btn-sm'><i class='fa fa-check fa-fw fa-lg m-r-3'></i> Finalizar</button>";
												}else{
													$this->SectionId	= "validar-documentos";
													$DisplayButton 		= "<button type='submit' class='btn btn-success btn-sm'><i class='fa fa-file fa-fw fa-lg m-r-3'></i> Validar</button>";
												}
											}else{
												if($this->GetTotalDocs >= $this->TotalFiles){
													$this->SectionId 	= "carta-liberacion-sp";
													$DisplayButton		= "<button type='submit' class='btn btn-primary btn-sm'><i class='fa fa-check fa-fw fa-lg m-r-3'></i> Finalizar</button>";
												}else{
													$this->SectionId	= "validar-documentos";
													$DisplayButton 		= "<button type='submit' class='btn btn-success btn-sm'><i class='fa fa-file fa-fw fa-lg m-r-3'></i> Validar</button>";
												}
											}
											break;
									}
								}
							}else{ 
								switch ($GLOBALS["thisSPId"]) {
									case 1:
										$this->SectionId	= "validar-documentos";
										$DisplayButton 		= "<button type='submit' class='btn btn-success btn-sm'><i class='fa fa-file fa-fw fa-lg m-r-3'></i> Validar</button>";
										break;
									case 2:
										if($this->ThisHours == ""){
											$this->SectionId	= "";
											$DisplayButton 		= "N/A";
										}else{
											$this->SectionId	= "validar-documentos";
											$DisplayButton 		= "<button type='submit' class='btn btn-success btn-sm'><i class='fa fa-file fa-fw fa-lg m-r-3'></i> Validar</button>";
										}
										break;
								}
							}
						}else{
							switch ($GLOBALS["thisSPId"]) {
								case 3:
									$this->RcdDateEnd = "N/A";
									if($DisplayData == 0){$DisplayRcds = true;}else{$DisplayRcds = false;}
									switch ($this->data["ESTATUS_ID"]) {
										case 7:
											if($this->FindDocsGen === "error" AND $this->FindDocsAc === "error"){
												$this->SectionId 	= "";
												$DisplayButton		= "N/A";
											}else{
												switch ($this->GetTotalDocs) {
													case $this->TotalFiles:
														$this->SectionId	= "validar-documentos";
														$DisplayButton 		= "<button type='submit' class='btn btn-primary btn-sm'><i class='fa fa-file fa-fw fa-lg m-r-3'></i>Documentaci&oacute;n</button>";
														break;
													case $this->TotalFiles+4:
														$this->SectionId	= "validar-documentos";
														$DisplayButton 		= "<button type='submit' class='btn btn-warning btn-sm'><i class='fa fa-file fa-fw fa-lg m-r-3'></i>Cumplimiento</button>";
														break;
													default:
														$this->SectionId 	= "";
														$DisplayButton		= "N/A";
														break;
												}
											}
											break;
										case 19:
											$this->SectionId	= "validar-documentos";
											$DisplayButton 		= "<button type='submit' class='btn btn-success btn-sm'><i class='fa fa-check fa-fw fa-lg m-r-3'></i>Liberaci&oacute;n</button>";
											break;
										case 49:
											//$this->VpUnicoCert	= "P000011114_612352";
											$this->VpUnicoCert		= $this->PeopleCodeId."_".$this->Cv_Carrera;
											$this->TitStatus 		= self::getDbRowName("vp_estatus", "REG_Validacion", "WHERE vp_id_unico_cert = '".$this->VpUnicoCert."' AND vp_type = 'TIT'", 1);
											if($this->TitStatus === "error"){
												$this->SectionId	= "";
												$this->TitStatus	= "En Proceso";
												$DisplayButton 		= "N/A";
											}else{
												if(strtolower($this->TitStatus) === "xml autenticado"){
													$this->SectionId	= "generar-titulo";
													$DisplayButton 		= "<button type='submit' class='btn btn-purple btn-sm'><i class='fa fa-graduation-cap fa-fw fa-lg m-r-3'></i>&nbsp;&nbsp;Mostrar T&iacute;tulo</button>";
												}else{
													$this->SectionId	= "";
													$DisplayButton 		= "N/A";
												}
											}
											break;
										case 50:
											$this->SectionId	= "notificar-titulo";
											$DisplayButton 		= "<button type='submit' class='btn btn-success btn-sm'><i class='fa fa-file-text-o fa-fw fa-lg m-r-3'></i>Notificar T&iacute;tulo</button>";
											break;
										case 51:
											$this->SectionId	= "";
											$DisplayButton 		= "N/A";
											break;
									}
									break;
								default:
									$DisplayButton = "N/A";
									$this->SectionId = "servicio-en-progreso";
									if($this->data["FECHA_INICIO"] === NULL){
										$this->RcdDateEnd = "Por Iniciar";
									}else{
										$this->RcdDateEnd = "En Progreso<br>".$this->TotalTime;
									}
									if($DisplayData == 0){$DisplayRcds = true;}else{$DisplayRcds = false;}
									break;
							}
						}
						
						if($this->data["ESTATUS_ID"] == 21){ 
							$this->SectionId	= "servicio-cancelado";
							$this->RcdDateEnd 	= "Cancelado";
							$DisplayButton 		= "N/A";
						}

						if($DisplayRcds){
							$this->ThisRegId		= self::getThisRcd("ACC_STDDEGREQ_SS_ID", "ACC_Progress_Status_SP", "WHERE Acc_Progress_Status_Id = ".$this->data["Acc_Progress_Status_Id"]);
							$this->PeopleCodeId		= self::getThisRcd("PEOPLE_CODE_ID", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
							$this->ConvenioName		= self::getThisRcd("NOMBRE_EMPRESA", "ACC_CAT_CONVENIO_SP", "WHERE ID = ".$this->data["CONVENIO_ID"]);
							$this->TipoLiberacion	= self::getThisRcd("DESCRIPCION_TIPO_LIBERACION", "ACC_CAT_TIPO_LIBERACION_SS", "WHERE ID = ".$this->data["TIPO_LIBERACION_ID"]);
							$this->StudentName		= self::getThisRcd("PRIMER_NOMBRE", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
							$this->StudentLName1	= self::getThisRcd("APELLIDO_PATERNO", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
							$this->StudentLName2	= self::getThisRcd("APELLIDO_MATERNO", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
							$this->CarrerName		= self::getThisRcd("FORMAL_TITLE", "CODE_CURRICULUM", "WHERE CODE_VALUE_KEY = '".$this->data["CVE_CARRERA"]."'");
							$this->StatusName 		= self::getThisRcd("DESCRIPCION_ESTATUS", "ACC_CAT_ESTATUS_SPR_SP", "WHERE ESTATUS_ID = ".$this->data["ESTATUS_ID"]);
							$this->StatusName		= str_replace("[SERVICIO]", $GLOBALS['ThisServiceName'], $this->StatusName);
							
							if($this->ConvenioName === ""){$this->ConvenioName = "N/A";}
							
							$StudentName = $this->StudentName." ".$this->StudentLName1." ".$this->StudentLName2;
							
							switch ($this->data["PAGADO"]) {
								case 0:
									$StatusPagado = "<span style='color:#8c0003'><i class='fa fa-times fa-fw fa-lg m-r-3'></i></span> No";
									break;
								case 1:
									$StatusPagado = "<span style='color:#00ae00'><i class='fa fa-check fa-fw fa-lg m-r-3'></i></span> Si";
									break;
							}
							
							$this->result .= "
								<tr class='SmallTable'>
									<td style='text-align:left; width:200px;'><a href='?ty=".$_REQUEST["ty"]."&SecId=historico&regId=".$this->data["Acc_Progress_Status_Id"]."'>".$this->PeopleCodeId."</a><br />".$StudentName."</td>
									<td>".$this->CarrerName."</td>
									<td>".$this->ConvenioName."</td>
									<td>".$this->StatusName."</td>";
									switch ($GLOBALS["thisSPId"]) {
										case 3:
											break;
										default:
											$this->result .= "
												<td>".$this->RcdDateIni."</td>
												<td style='width:150px;'>".$this->RcdDateEnd."</td>
											";
											break;
									}
									$this->result .= "
									<td>".$StatusPagado."</td>";
									switch ($GLOBALS["thisSPId"]) {
										case 1: 
										case 3: 
											$this->result .= "<td>".$this->TipoLiberacion."</td>"; 
											break;
										case 2:
											if($this->ThisHours == ""){
												$this->ThisHours = "0 Hrs";
											}else{
												$this->ThisHours = $this->ThisHours." de ".$this->ProcessHours." Hrs";
											}
											if($this->TotalHours == ""){
												$this->TotalHours = "0 Hrs";
											}else{
												$this->TotalHours = $this->TotalHours." Hrs";
											}
											$this->result .= "
												<td>".$this->ThisHours."</td>
												<td>".$this->TotalHours."</td>
											";
											break;
									}
									if($GLOBALS["thisSPId"] == 3 AND $_REQUEST["SecId"] === "titulos-electronicos"){
										$this->result .= "
											<td>".$this->TitStatus."</td>
										";
									}
									$this->result .= "
									<td>
										<form id='EnviaData' action='index.php' method='post'>
											<input type='hidden' id='ty' name='ty' value='".$_REQUEST["ty"]."'>
											<input type='hidden' id='SecId' name='SecId' value='".$this->SectionId."'>
											<input type='hidden' id='StudentId' name='StudentId' value='".$this->data["Acc_Progress_Status_Id"]."'>
											<input type='hidden' id='thisId' name='thisId' value='".$this->data["ID"]."'>
											".$DisplayButton."
										</form>
									</td>
								</tr>
							";
						}
					}
				}
				return $this->result;
			}
			
			public function StatusHistorico($thisUserId, $thisServId){
				self::SP_Conn();
				self::xQueryI("SELECT * FROM ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP WHERE Acc_Progress_Status_Id = ".$thisUserId." AND SERVICIO_PROFESIONAL_ID = ".$thisServId." ORDER BY ID DESC");
				$this->result  = "
					<div class='info' style='padding:10px; margin-left:0px; margin-top:20px; text-align:left;'>
						<h5 class='page-header'>Registro Hist&oacute;rico de Alumno</h5>
						<div id='content' class='content'>
							<p>Mostrando Registro Hist&oacute;rico al d&iacute;a de hoy
							<ul class='timeline'>
				";
				$this->ThisRegId	= self::getThisRcd("ACC_STDDEGREQ_SS_ID", "ACC_Progress_Status_SP", "WHERE Acc_Progress_Status_Id = ".$thisUserId);
				$this->PeopleCodeId	= self::getThisRcd("PEOPLE_CODE_ID", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
				while ($this->data = sqlsrv_fetch_array($this->rQuery)) {
					$this->RcdDate = self::formatDate($this->data["FECHA_HISTORICO"]->format('Y-m-d'));
					if(strlen($this->data["TEACHER_ID"] <> 0)){
						$this->TeacherName 	= self::getThisRcd("usu_nombre", "reg_usu", "WHERE id = ".$this->data["TEACHER_ID"]);
					}else{
						$this->TeacherName 	= $_SESSION['rol'];
					}
					$this->StatusName = self::getThisRcd("DESCRIPCION_ESTATUS", "ACC_CAT_ESTATUS_SPR_SP", "WHERE ESTATUS_ID = ".$this->data["ESTATUS_ID"]);
					$this->StatusName = str_replace("[SERVICIO]", $GLOBALS['ThisServiceName'], $this->StatusName);
					switch ($this->data["REG_TIPO"]) {
						case 0:
							$this->thisTitCol 	= "#0054a4";
							$this->thisContCol 	= "#000000";
							$this->thisIconImg	= "student-icon.jpg";
							$this->thisClassId 	= "historico-alumno";
							$this->thisUserInfo	= "<i class='fa fa-graduation-cap fa-fw fa-lg m-r-3'></i> NOMBRE: | FOLIO CONTROL: ".utf8_encode($this->PeopleCodeId)." | PLANTEL: | CARRERA:";
							break;
						case 1:
							$this->thisTitCol 	= "#f1f2f3";
							$this->thisContCol 	= "#ffffff";
							$this->thisIconImg	= "logo-uin.jpg";
							$this->thisClassId 	= "historico-universidad";
							$this->thisUserInfo	= "<i class='fa fa-user fa-fw fa-lg m-r-3'></i> ATENDIDO POR: ".$this->TeacherName." | PLANTEL: ";
							break;
					}
					switch ($this->data["ESTATUS_ID"]) {
						case 1:
						case 4:
							$this->thisIcon_FAW	= "<i class='fa fa-warning fa-3' style='font-size:45px; color:#f4c520;'></i>";
							break;
						case 2:
						case 5:
						case 13:
							$this->thisIcon_FAW	= "<i class='fa fa-times fa-3' style='font-size:45px; color:#ce0005;'></i>";
							break;
						case 3:
						case 6:
							$this->thisIcon_FAW	= "<i class='fa fa-check fa-3' style='font-size:45px; color:#28a745;'></i>";
							break;
						case 15:
							$this->thisIcon_FAW	= "<i class='fa fa-dollar fa-3' style='font-size:45px; color:#28a745;'></i>";
							break;
						case 16:
							$this->thisIcon_FAW	= "<i class='fa fa-upload fa-3' style='font-size:45px; color:#8000ff;'></i>";
							break;
						case 11:
							$this->thisIcon_FAW	= "<i class='fa fa-eye fa-3' style='font-size:45px; color:#f1f2f3;'></i>";
							break;
						case 26:
							$this->thisIcon_FAW	= "<i class='fa fa-times fa-3' style='font-size:45px; color:#ce0005;'></i>";
							break;
						default:
							switch ($this->data["REG_TIPO"]) {
								case 0:
									$this->thisIcon_FAW	= "<i class='fa fa-file-pdf-o fa-3' style='font-size:45px; color:#0060bf;'></i>";
									break;
								case 1:
									$this->thisIcon_FAW	= "<i class='fa fa-file-pdf-o fa-3' style='font-size:45px; color:#f2f2f3;'></i>";
									break;
							}
							break;
					}
					$this->result .= "
						
							<div class='timeline-time'>
								<span class='date'>".$this->RcdDate."</span>
								<span class='time'>".$this->data["FECHA_HISTORICO"]->format('H:i')."</span>
							</div>
							<div class='timeline-icon'>
								<a href='javascript:;'>&nbsp;</a>
							</div>
							<div class='timeline-body ".$this->thisClassId."'>
								<div class='timeline-header'>
								<span class='userimage'><img src='img/".$this->thisIconImg."' alt=''></span>
									<span class='username' style='color:".$this->thisTitCol."'>".$this->StatusName."</span><br>
									<span style='color:".$this->thisContCol."'>".$this->thisUserInfo."</span>
									<span class='pull-right' style='color:".$this->thisContCol."'>".$this->thisIcon_FAW."</span>
								</div>
								<div class='timeline-content'>
									<p><span style='color:".$this->thisContCol."'>".$this->data["COMENTARIOS"]."</span></p>
								</div>
							</div>
						
					";
				}
				$this->result .= "
							</ul>
						</div>
					</div>
				";
				return $this->result;
			}

			public function HeaderAlumno($thisTitle, $thisId){
				$this->result = "
					<div id='invoice'>
						<div class='invoice overflow-auto'>";
							self::SP_Conn();
							$this->ThisRegId 	= self::getThisRcd("ACC_STDDEGREQ_SS_ID", "ACC_Progress_Status_SP", "WHERE Acc_Progress_Status_Id = ".$thisId);
							$this->PeopleCodeId	= self::getThisRcd("PEOPLE_CODE_ID", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
							self::xQueryI("SELECT * FROM ACC_STDDEGREQ_SS WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
							if ($this->data = sqlsrv_fetch_array($this->rQuery)) {
								$StudentName 	= utf8_encode($this->data["PRIMER_NOMBRE"]." ".$this->data["APELLIDO_PATERNO"]." ".$this->data["APELLIDO_MATERNO"]);
								$RcdDate		= self::formatDate($this->data["FECHA_RVOE"]->format('Y-m-d'));
								$this->result .= "
									<div style='min-width: 600px'>
										<div class='row contacts'>
											<div class='col invoice-to'>
												<div class='text-gray-light'><b>".$thisTitle.":</b><hr noshade=1></div>
												<h2 class='to'>".$StudentName."</h2>
												<div class='address'>Universidad Insurgentes Plantel ".$this->data["MATRIC_SESSION"]."</div>
												<div class='email'><a href='mailto:".$this->data["CORREO_ELECTRONICO"]."'>".$this->data["CORREO_ELECTRONICO"]."</a></div>
											</div>
											<div class='col invoice-details'>
												<h1 class='invoice-id'>".$this->PeopleCodeId."</h1>
												<h6 class='to'>".$this->data["RVOE"]."</h6>
												<div class='date'>Fecha de RVOE: ".$RcdDate."</div>
											</div>
										</div>
									</div>
								";
							}
						$this->result .= "
						</div>
					</div>";
				return $this->result;
			}

			public function DatosAlumno($thisUserId){
				$this->StudentImg 	= self::getThisRcd("NOMBRE_DOCUMENTO", "ACC_ALUMNO_DOCUMENTOS_SP", "WHERE ID_DOCUMENTO = 1");
				$this->StudentUrl 	= self::getThisRcd("RUTA_DOCUMENTO", "ACC_ALUMNO_DOCUMENTOS_SP", "WHERE ID_DOCUMENTO = 1");
				$this->ThisRegId 	= self::getThisRcd("ACC_STDDEGREQ_SS_ID", "ACC_Progress_Status_SP", "WHERE Acc_Progress_Status_Id = ".$thisUserId);
				$this->PeopleCodeId	= self::getThisRcd("PEOPLE_CODE_ID", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
				$this->result  		= "
				<div class='row mb-2' style='margin-top:20px;'>
					<div class='col-md-12'>
						<h3 class='page-header' style='text-align:left;'>".$this->PeopleCodeId."</h3>
						<div class='row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative'>
							<div class='col-auto d-none d-lg-block' style='width:400px; text-align:center; overflow:hidden;'>
								<img src='img/student-icon.jpg' style='width:100%;'>
							</div>";
							self::SP_Conn();
							self::xQueryI("SELECT * FROM ACC_STDDEGREQ_SS WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
							if ($this->data = sqlsrv_fetch_array($this->rQuery)) {
								$StudentName = utf8_encode($this->data["PRIMER_NOMBRE"]." ".$this->data["APELLIDO_PATERNO"]." ".$this->data["APELLIDO_MATERNO"]);
								$this->result .= "
									<div class='col p-4 d-flex flex-column position-static' style='text-align:left;'>
										<b class='d-inline-block mb-2 text-primary'>Mostrando los datos del Alumno:</b>
										<h3 class='mb-0 text-primary'>".$StudentName."</h3>
										<br />
										<b class='d-inline-block mb-2 text-primary'>Direcci&oacute;n</b>
										<h6 class='mb-0'>
											".$this->data["CALLE"].", #".$this->data["NUMERO_EXTERIOR"].", int: ".$this->data["NUMERO_INTERIOR"]."<br />
											Colonia:".$this->data["COLONIA"]."<br />
											Municipio: ".$this->data["DELEGACION"]."<br />
											C.P.: ".$this->data["CODIGO_POSTAL"]."X<br />
										</h6>
										<br />
										<b class='d-inline-block mb-2 text-primary'>Tel&eacute;fono</b>
										<h6 class='mb-0'>
											".$this->data["TELEFONO_CASA"]."<br />
										</h6>
										<br />
										<b class='d-inline-block mb-2 text-primary'>Correo Electr&oacute;nico</b>
										<h6 class='mb-0'>
											<a href='mailto:".$this->data["CORREO_ELECTRONICO"]."'>".$this->data["CORREO_ELECTRONICO"]."</a><br />
										</h6>
									</div>
								";
							}
						$this->result .= "
						</div>
					</div>
			  	</div>";
				return $this->result;
			}

			public function DatosPracticas($thisUserId){
				$this->ThisRegId 	= self::getThisRcd("ACC_STDDEGREQ_SS_ID", "ACC_Progress_Status_SP", "WHERE Acc_Progress_Status_Id = ".$thisUserId);
				$this->PeopleCodeId	= self::getThisRcd("PEOPLE_CODE_ID", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
				$this->StudentName	= self::getThisRcd("PRIMER_NOMBRE", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
				$this->LastName		= self::getThisRcd("APELLIDO_PATERNO", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
				$this->MotherName	= self::getThisRcd("APELLIDO_MATERNO", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
				$this->result  		= "
				<div class='row mb-2' style='margin-top:20px;'>
					<div class='col-md-12'>
						<h3 class='page-header' style='text-align:left;'>".$this->PeopleCodeId."</h3>
						<div class='row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative'>
							<div class='col-auto d-none d-lg-block' style='width:400px; margin-top:20px; text-align:center; overflow:hidden;'>
								<b class='d-inline-block mb-2 text-primary'>Listado de Pr&aacute;cticas Profesionales:</b>
								<h3 class='mb-0 text-primary'>".$this->StudentName." ".$this->LastName." ".$this->MotherName."</h3>
								<br />
								<img src='img/student-icon.jpg' style='width:100%;'>
							</div>";
							$TotalPracticas = 0;
							self::SP_Conn();
							self::xQueryI("SELECT * FROM ACC_PRACTICAS_PROFESIONALES_SP WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
							while ($this->data = sqlsrv_fetch_array($this->rQuery)) {
								$TotalPracticas = $TotalPracticas + 1;
								$this->result .= "
									<div class='col p-4 d-flex flex-column position-static' style='max-width:300px; margin-top:150px; text-align:left;'>
										<h6 class='mb-0'>
											<b>Datos del Asesor:</b><br>
											<b>Nombre:</b>".$this->data["NOMBRE_ASESOR"]."<br />
											<b>Cargo:</b> ".$this->data["CARGO_ASESOR"]."<br />
											<b>Profesi&oacute;n:</b> ".$this->data["PROFESION_ASESOR"]."<br />
											<b>Horas:</b> ".$this->data["HORAS"]."<br />
										</h6>
										<b class='d-inline-block mb-2 text-primary'>Actividades</b>
										<h6 class='mb-0'>
											".str_replace("|",", ",$this->data["ACTIVIDADES"])."<br />
										</h6>
									</div>
								";
							}
						$this->result .= "
						</div>
					</div>
			  	</div>";
				return $this->result;
			}

			public function ListadoDocumentos($thisTable, $thisFilter, $thisUserId, $thisType, $displayType){
				self::SP_Conn();
					$this->ThisRegId 		= self::getThisRcd("ACC_STDDEGREQ_SS_ID", "ACC_Progress_Status_SP", "WHERE Acc_Progress_Status_Id = ".$thisUserId);
					$this->PeopleCodeId		= self::getThisRcd("PEOPLE_CODE_ID", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
					$this->ProgressStatus	= self::getThisRcd("ESTATUS_ID", "ACC_SERVICIOS_PROFESIONALES_SP", "WHERE Acc_Progress_Status_Id = ".$thisUserId);
					$validateAllFiles		= "";
					switch ($thisType) {
						case "Generales":
							$StudentId 	= $this->PeopleCodeId;
							self::xQueryI("SELECT * FROM ".$thisTable." WHERE PEOPLE_CODE_ID = '".$this->PeopleCodeId."' ".$thisFilter);
							$GLOBALS["TotalDocsGen"] = self::getTotalRcdsCount($thisTable, "WHERE PEOPLE_CODE_ID = '".$this->PeopleCodeId."' ".$thisFilter." AND ESTATUS_ID = 25");
							break;
						case "Academicos":
							$StudentId  = $thisUserId;
							self::xQueryI("SELECT * FROM ".$thisTable." WHERE Acc_Progress_Status_Id = ".$thisUserId." ".$thisFilter);
							$GLOBALS["TotalDocsAc"] = self::getTotalRcdsCount($thisTable, "WHERE Acc_Progress_Status_Id = ".$thisUserId." ".$thisFilter." AND ESTATUS_ID = 25");
							break;
					}

					if(isset($GLOBALS["TotalDocsAc"])){
						$GLOBALS["TotalAuthDocs"] = $GLOBALS["TotalDocsGen"] + $GLOBALS["TotalDocsAc"];
					}else{
						if($thisType === "Academicos"){
							$GLOBALS["TotalAuthDocs"] = $GLOBALS["TotalDocsAc"];
						}else{
							$GLOBALS["TotalAuthDocs"] = "";
						}
					}

					if($GLOBALS["thisSPId"] == 3){
						if($GLOBALS["TotalAuthDocs"] >= 12){
							$validateAllFiles = " $('#ValidateAllFiles').attr('disabled', false); \n";
						}else{
							$validateAllFiles = " $('#ValidateAllFiles').attr('disabled', true); \n";
						}
					}

					if(isset($GLOBALS["TotalFiles"])){
						$GLOBALS['TotalFiles'] = $GLOBALS["TotalFiles"];
					}else{
						$GLOBALS['TotalFiles'] = 0;
					}
					
					$EnabledButton 	= "";
					$this->result 	= "";
					
					if($thisType === "Academicos"){
						$this->result .= "
							<input type='hidden' id='TotalAuthDocs' name='TotalAuthDocs' value='".$GLOBALS["TotalAuthDocs"]."'>
						";
					}
					
					while ($this->data = sqlsrv_fetch_array($this->rQuery)) {
						$GLOBALS['TotalFiles'] = $GLOBALS['TotalFiles'] + 1;

						if($GLOBALS['TotalFiles'] < 10){
							$GLOBALS['TotalFiles'] = "0".$GLOBALS['TotalFiles'];
						}
						
						switch ($this->data["ID_DOCUMENTO"]) {
							case 5:
							case 6:
							case 10:
							case 11:
							case 12:
							case 13:
							case 14:
							case 27:
							case 28:
							case 29:
							case 30:
							case 31:
								$DocPath = "../";
								$RutaDoc = $this->data["RUTA_DOCUMENTO"];
								break;
							default:
								$DocPath = "http://www.portaluin.mx";
								//$DocPath = "http://testportaluin.dnsalias.net";
								$RutaDoc = $this->data["RUTA_DOCUMENTO"];
								$RutaDoc = str_replace("\\", "/", $RutaDoc);
								break;
						}

						$DocPath 			= $DocPath."/".$RutaDoc;
						$this->DocId		= $this->data["ID_DOCUMENTO"];
						$this->DocName	 	= self::getThisRcd("DESCRIPCION_DOCUMENTOS", "ACC_CAT_DOCUMENTOS_SP", "WHERE ID = ".$this->data["ID_DOCUMENTO"]);
						$this->StatusName 	= self::getThisRcd("DESCRIPCION_ESTATUS", "ACC_CAT_ESTATUS_SPR_SP", "WHERE ESTATUS_ID = ".$this->data["ESTATUS_ID"]);

						switch ($this->data["ESTATUS_ID"]) {
							case 10:
							case 11:
								$DocCellColor = "#f9c700";
								break;
							case 16:
								$DocCellColor = "#cccccc";
								break;
							case 17:
							case 18:
							case 27:
								$DocCellColor = "#6f42c1; color:#FFFFFF";
								break;
							case 25:
								$DocCellColor = "#13b002";
								break;
							case 26:
								$DocCellColor = "#800000; color:#FFFFFF";
								break;
						}

						switch ($this->data["ESTATUS_ID"]) {
							case 25: 
								$EnabledButton	.= " $('#success_".$this->DocId."').attr('disabled', true); \n";
								break;
							case 26: 
								$EnabledButton	.= " $('#success_".$this->DocId."').attr('disabled', true); \n";
								$EnabledButton	.= " $('#decline_".$this->DocId."').attr('disabled', true); \n";
								$EnabledButton	.= " $('#pre_decline_".$this->DocId."').attr('disabled', true); \n"; 
								break;
						}

						$this->result .= "
							<tr id='MuestraDoc_".$this->DocId."' class='SmallTable'>";
								if($displayType == 1){ 
									$this->result .= "
										<td style='width:20px;'>".$GLOBALS["TotalFiles"]."</td>
										<td style='width:45%; text-align:left;'>
											<h6>".$this->DocName."</h6>
											<input type='hidden' id='DocumentName' name='DocumentName' value='".$this->DocName."'>
											<input type='hidden' id='start_date' name='start_date' value=''>
											<input type='hidden' id='end_date' name='end_date' value=''>
										</td>
										<td id='DocStatus_".$this->DocId."' style='width:15%; text-align:center; background-color:".$DocCellColor.";'><b>".$this->StatusName."</b></td>
										<td id='OpenFile_".$this->DocId."' style='width:40%; text-align:center;'>
											<a onclick=window.open('".$DocPath."/".$this->data["NOMBRE_DOCUMENTO"]."');><span id='viewfile_".$this->DocName."' class='btn btn-primary btn-sm' style='padding:5px;'><i class='fa fa-eye'></i> Ver Archivo</span></a>
											<button type='submit' id='success_".$this->DocId."' class='btn btn-success btn-sm ValidateRequest' Section='valida-archivo' StudentId='".$thisUserId."' StdRegId='".$this->ThisRegId."' PeopleCodeId='".$this->PeopleCodeId."' NewStatus='25' RegId='".$this->DocId."' RegName='".$thisType."' SPId='".$GLOBALS["thisSPId"]."'><i class='fa fa-check'></i> Autorizar</button>";
											switch ($GLOBALS["thisSPId"]) {
												case 3:
													$this->result .= "<button id='pre_decline_".$this->DocId."' data-fancybox='VerRazonRechazo' data-src='#VerRazonRechazo_".$this->DocId."' href='javascript:;' class='btn btn-danger btn-sm'><i class='fa fa-times'></i> Rechazar</button>";
													break;
												default:
													$this->result .= "<button type='submit' id='pre_decline_".$this->DocId."' class='btn btn-danger btn-sm ValidateRequest' Section='rechaza-archivo' StudentId='".$thisUserId."' StdRegId='".$this->ThisRegId."' PeopleCodeId='".$this->PeopleCodeId."' NewStatus='26' RegId='".$this->DocId."' RegName='".$thisType."' SPId='".$GLOBALS["thisSPId"]."'><i class='fa fa-times'></i> Rechazar</button>";
													break;
											}
											$this->result .= "
										</td>
									";
								}else{ 
									$this->result .= "
										<td style='width:20px;'>".$GLOBALS["TotalFiles"]."</td>
										<td style='width:80%; text-align:left;'>
											<h6>".$this->DocName."</h6>
											<input type='hidden' id='DocumentName' name='DocumentName' value='".$this->DocName."'>
										</td>";
											if($this->DocId == 32){
												$this->result .= "
													<td colspan='2' id='DocStatus_".$this->DocId."' style='width:10%; text-align:center; background-color:".$DocCellColor.";'><b>".$this->StatusName."</b></td>
												";
											}else{
												$this->result .= "
													<td id='DocStatus_".$this->DocId."' style='width:10%; text-align:center; background-color:".$DocCellColor.";'><b>".$this->StatusName."</b></td>
													<td id='OpenFile_".$this->DocId."' style=''>
														<a onclick=window.open('".$DocPath."/".$this->data["NOMBRE_DOCUMENTO"]."');><span id='viewfile_".$this->DocName."' class='btn btn-primary btn-sm' style='padding:5px;'><i class='fa fa-eye'></i> Ver Archivo</span></a>
													</td>
												";
											}
										$this->result .= "
									";
								}
								$this->result .= "
							</tr>
							<form id='SeleccionaModulo_".$this->DocId."'>
								<input type='hidden' id='start_date' name='start_date' value=''>
								<input type='hidden' id='end_date' name='end_date' value=''>
								<input type='hidden' id='DocumentName_".$this->DocId."' name='DocumentName_".$this->DocId."' value='".$this->DocName."'>
								<span id='VerRazonRechazo_".$this->DocId."' style='width:600px;' class='fancy_info'>
									<h5><b>Rechazar Documento:</b></h5><h5>".$this->DocName."</h5><br />
									<b>Escriba la raz&oacute;n del Rechazo:</b><br />
									<textarea id='AddComments_".$this->DocId."' name='AddComments_".$this->DocId."' class='form-control' rows='5'></textarea><br/>
									<div style='width:100%; text-align:center;>
										<button type='submit' id='decline_".$this->DocId."' class='btn btn-danger btn-bg ValidateRequest' Section='rechaza-archivo' StudentId='".$thisUserId."' StdRegId='".$this->ThisRegId."' PeopleCodeId='".$this->PeopleCodeId."' NewStatus='26' RegId='".$this->DocId."' RegName='".$thisType."' SPId='".$GLOBALS["thisSPId"]."'><i class='fa fa-times'></i> Rechazar Documento</button>
									</div>
								</span>
							</form>
						";
					}
					switch ($GLOBALS["thisSPId"]) {
						case 3:
							if($thisType === "Academicos" AND $this->ProgressStatus == 19){
								$this->result .= "
									<tr id='' style='height:200px; background-color:#ffffff;'>
										<td colspan='4' style='text-align:center;'>
											<button type='submit' id='ValidateAllFiles' class='btn btn-primary btn-sm ValidateRequest' Section='autorizacion-archivos' StudentId='".$thisUserId."' StdRegId='".$this->ThisRegId."' PeopleCodeId='".$this->PeopleCodeId."' NewStatus='49' RegId='' RegName='".$thisType."' SPId='".$GLOBALS["thisSPId"]."' style='margin-top:70px; height:50px;'><i class='fa fa-check'></i> Validar Todos los Documentos</button>
										</td>
									</tr>
								";
							}
							break;
					}
					$this->result .= "
						<script type='text/javascript'>
							$(document).ready(function() {
								".$EnabledButton."
								".$validateAllFiles."
							})
						</script>
						".MsgResult()."
					";
				return $this->result;
			}
			
			public function CargaDatosDb($thisType, $thisTable, $thisRejected, $thisAccepted, $NewStatus, $thisFilter, $thisId, $thisConfirmBtn, $thisRejectBtn, $thisAcceptBtn){
				self::SP_Conn();
				self::xQueryI("SELECT * FROM ".$thisTable." ".$thisFilter);
				if ($this->data = sqlsrv_fetch_array($this->rQuery)) {
					$this->RcdDate 				= self::formatDate($this->data["FECHAREGISTRO"]->format('Y-m-d'));
					$this->StatusName 			= self::getThisRcd("DESCRIPCION_ESTATUS", "ACC_CAT_ESTATUS_SPR_SP", "WHERE ESTATUS_ID = ".$this->data["ESTATUS_ID"]);

					$this->ThisRegId			= self::getThisRcd("ACC_STDDEGREQ_SS_ID", "ACC_Progress_Status_SP", "WHERE Acc_Progress_Status_Id = ".$this->data["Acc_Progress_Status_Id"]);
					$this->PeopleCodeId			= self::getThisRcd("PEOPLE_CODE_ID", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
					$this->StudentName			= self::getThisRcd("PRIMER_NOMBRE", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
					$this->StudentMiddleName	= self::getThisRcd("APELLIDO_PATERNO", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
					$this->StudentLastName		= self::getThisRcd("APELLIDO_MATERNO", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);

					echo "
						<div class='row mb-2' style='margin-top:20px;'>
							<div class='col-md-12'>
								<h5 class='page-header' style='text-align:left;'>Detalles de la Solicitud:</h5>
								<div class='row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative'>
									<div class='col-auto d-none d-lg-block' style='text-align:center;'>
										<svg class='bd-placeholder-img' width='200' height='250' xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMidYMid slice' focusable='false' role='img' aria-label='Nombre del Alumno'><title>Nombre del Alumno</title><rect width='100%' height='100%' fill='#55595c'></rect></svg>
									</div>
									<div class='col p-4 d-flex flex-column position-static' style='text-align:left; background-color:#f1f2f3;'>";
										switch ($thisType) {
											case 'nuevo-convenio':
												echo "
													<h6 class='page-header'><b>Alumno:</b><br />".$this->StudentName." ".$this->StudentMiddleName." ".$this->StudentLastName."</h6>
													<h6 class='card-title'><b>Solicitud:</b><br />".$this->StatusName."</h6>
													<h6 class='card-title'><b>Fecha de Solicitud:</b><br />".$this->RcdDate."</h6>
													<h6 class='card-title'><b>Petici&oacute;n del Alumno:</b></h6>
													<h6 class='card-title'>".utf8_encode($this->data["COMENTARIOS"])."</h6>
													<h6 class='card-title'><b>Datos de la Empresa:</b></h6>
													<h6 class='card-title'><b>Nombre de la Empresa:</b> ".utf8_encode($this->data["NOMBRE_EMPRESA"])."</h6>
													<h6 class='card-title'><b>Direcci&oacute;</b> ".utf8_encode($this->data["DIRECCION"])."</h6>
													<h6 class='card-title'><b>Persona de Contacto:</b> ".utf8_encode($this->data["PERSONA_CONTACTO"])."</h6>
													<h6 class='card-title'><b>Telefono:</b> ".utf8_encode($this->data["TELEFONO"])."</h6>
												";
												break;
											default:
												echo "
													<h6 class='page-header'><b>Alumno:</b><br />".$this->StudentName." ".$this->StudentMiddleName." ".$this->StudentLastName."</h6>
													<h6 class='card-title'><b>Solicitud:</b><br />".$this->StatusName."</h6>
													<h6 class='card-title'><b>Fecha de Solicitud:</b><br />".$this->RcdDate."</h6>
													<h6 class='card-title'><b>Petici&oacute;n del Alumno:</b></h6>
													<h6 class='card-title'>".utf8_encode($this->data["COMENTARIOS"])."</h6>
												";
												break;
										}
										echo "
									</div>
								</div>
							</div>
						</div>
						<div class='card' style='background-color:#f1f2f3;'>
							<div class='card-body'>
								<div style='text-align:center;'>
									<form id='SeleccionaModulo'>
										<input type='hidden' id='start_date' name='start_date' value=''>
										<input type='hidden' id='end_date' name='end_date' value=''>
										<input type='hidden' id='DocumentName' name='DocumentName' value=''>";
										switch ($this->data["ESTATUS_ID"]) {
											case 11:
												echo "
													<div class='form-group' style='text-align:left;'>
														<div id='NewComment' style='text-align:left;'>
															<h5>Desea Proporcionar un Comentario?</h5>:
															<textarea id='AddComments' name='AddComments' class='form-control' rows='5'></textarea>
														</div>
														<br />
														<div style='text-align:center;'>
															<button type='submit' id='BtnRejected' class='btn btn-danger ValidateRequest' Section='".$thisType."' StudentId='".$this->data["Acc_Progress_Status_Id"]."' StdRegId='".$this->ThisRegId."' PeopleCodeId='".$this->ThisRegId."' NewStatus='".$thisRejected."' RegId='".$this->data["ID"]."' RegName='' SPId='".$GLOBALS["thisSPId"]."'><i class='fa fa-times'></i> ".$thisRejectBtn."</button>
															<button type='submit' id='BtnSuccess' class='btn btn-success ValidateRequest' Section='".$thisType."' StudentId='".$this->data["Acc_Progress_Status_Id"]."' StdRegId='".$this->ThisRegId."' PeopleCodeId='".$this->ThisRegId."' NewStatus='".$thisAccepted."' RegId='".$this->data["ID"]."' RegName='' SPId='".$GLOBALS["thisSPId"]."'><i class='fa fa-check'></i> ".$thisAcceptBtn."</button>
														</div>
													</div>
												";
												break;
											default:
												echo "
													<div class='form-group' style='text-align:left;'>
														<div id='NewComment' style='text-align:left; display:none;'>
															<h5>Desea Proporcionar un Comentario?</h5>:
															<textarea id='AddComments' name='AddComments' class='form-control' rows='5'></textarea>
														</div>
														<br />
														<div style='text-align:center;'>
															<button type='submit' id='BtnRevision' class='btn btn-primary ValidateRequest' Section='".$thisType."' StudentId='".$this->data["Acc_Progress_Status_Id"]."' StdRegId='".$this->ThisRegId."' PeopleCodeId='".$this->ThisRegId."' NewStatus='".$NewStatus."' RegId='".$this->data["ID"]."' RegName='' SPId='".$GLOBALS["thisSPId"]."'><i class='fa fa-eye'></i> ".$thisConfirmBtn."</button>
															<button type='submit' id='BtnRejected' class='btn btn-danger ValidateRequest' Section='".$thisType."' StudentId='".$this->data["Acc_Progress_Status_Id"]."' StdRegId='".$this->ThisRegId."' PeopleCodeId='".$this->ThisRegId."' NewStatus='".$thisRejected."' RegId='".$this->data["ID"]."' RegName='' SPId='".$GLOBALS["thisSPId"]."' style='display:none;'><i class='fa fa-times'></i> ".$thisRejectBtn."</button>
															<button type='submit' id='BtnSuccess' class='btn btn-success ValidateRequest' Section='".$thisType."' StudentId='".$this->data["Acc_Progress_Status_Id"]."' StdRegId='".$this->ThisRegId."' PeopleCodeId='".$this->ThisRegId."' NewStatus='".$thisAccepted."' RegId='".$this->data["ID"]."' RegName='' SPId='".$GLOBALS["thisSPId"]."' style='display:none;'><i class='fa fa-check'></i> ".$thisAcceptBtn."</button>
														</div>
													</div>
												";
												break;
										}
									echo "
									</form>
								</div>
							</div>
						</div>
					";
				}
			}

			public function GeneraDocumentos($thisType, $thisId){
				self::SP_Conn();
				$this->ThisRegId			= self::getThisRcd("ACC_STDDEGREQ_SS_ID", "ACC_CSP_01_SP", "WHERE Acc_Progress_Status_Id = ".$thisId);
				$this->PeopleCodeId			= self::getThisRcd("PEOPLE_CODE_ID", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
				$this->ThisTipoLibreacion 	= self::getThisRcd("TIPO_LIBERACION_ID", "ACC_SERVICIOS_PROFESIONALES_SP", "WHERE Acc_Progress_Status_Id = ".$thisId);
				switch ($thisType) {
					case 'solicitud-ss-iniciada':
					case 'solicitud-sp-iniciada':
						$thisStatus     = 17;
						$ThisRegId 		= $this->ThisRegId;
						$thisFileName   = "ConstanciaCreditos_".$_REQUEST["ty"];
						
						$thisTitle1     = "GENERAR CONSTANCIA DE CR&Eacute;DITOS";
						$thisTitle2     = "Vista Previa de Constancia de Cr&eacute;ditos";
						
						$thisButton1    = "Generar Constancia de Cr&eacute;ditos";
						$thisBtnIcon1   = "file-pdf-o";
						$thisBtnClass1  = "btn-success";
						break;
					case 'carta-presentacion-ss':
						switch ($this->ThisTipoLibreacion) {
							case 4:
								$thisStatus     = 28;
								$ThisRegId 		= $this->ThisRegId;
								$thisFileName   = "ConstanciaLaboral_".$_REQUEST["ty"]; 
								
								$thisTitle1     = "GENERAR SOLICITUD DE CONSTANCIA LABORAL";
								$thisTitle2     = "Vista Previa de Solicitud de Constancia Laboral";

								$thisButton1    = "Generar Solicitud de Constancia Laboral";
								$thisBtnIcon1   = "file-pdf-o";
								$thisBtnClass1  = "btn-warning";
								break;
							default:
								$thisStatus     = 18;
								$ThisRegId 		= $this->ThisRegId;
								$thisFileName   = "CartaPresentacion_".$_REQUEST["ty"];
								
								$thisTitle1     = "GENERAR CARTA DE PRESENTACI&Oacute;N";
								$thisTitle2     = "Vista Previa de Carta de Presentaci&oacute;n";
								
								$thisButton1    = "Generar Carta de Presentaci&oacute;n";
								$thisBtnIcon1   = "file-pdf-o";
								$thisBtnClass1  = "btn-purple";
								break;
						}
						break;
					case 'carta-presentacion-sp':
						$thisStatus     = 18;
						$ThisRegId 		= $this->ThisRegId;
						$thisFileName   = "CartaPresentacion_".$_REQUEST["ty"];
						
						$thisTitle1     = "GENERAR CARTA DE PRESENTACI&Oacute;N";
						$thisTitle2     = "Vista Previa de Carta de Presentaci&oacute;n";
						
						$thisButton1    = "Generar Carta de Presentaci&oacute;n";
						$thisBtnIcon1   = "file-pdf-o";
						$thisBtnClass1  = "btn-purple";
						break;
					case 'carta-liberacion-ss':
					case 'carta-liberacion-sp':
						$thisStatus     = 27;
						$ThisRegId 		= $this->ThisRegId;
						$thisFileName   = "CartaLiberacion_".$_REQUEST["ty"];
						
						$thisTitle1     = "CONSTANCIA DE LIBERACI&Oacute;N";
						$thisTitle2     = "Vista Previa de Constancia de Liberaci&oacute;n";
						
						$thisButton1    = "Generar Constancia de Liberaci&oacute;n";
						$thisBtnIcon1   = "file-pdf-o";
						$thisBtnClass1  = "btn-primary";
						break;
					case 'carta-final-sp':
						$thisStatus     = 31;
						$ThisRegId 		= $this->ThisRegId;
						$thisFileName   = "CartaLiberacionFinal_".$_REQUEST["ty"];
						
						$thisTitle1     = "CONSTANCIA DE LIBERACI&Oacute;N FINAL";
						$thisTitle2     = "Vista Previa de Constancia de Liberaci&oacute;n Final";
						
						$thisButton1    = "Generar Constancia de Liberaci&oacute;n Final";
						$thisBtnIcon1   = "file-pdf-o";
						$thisBtnClass1  = "btn-success";
						break;
					case 'generar-titulo':
						$thisStatus     = 50;
						$ThisRegId 		= $this->ThisRegId;
						$thisFileName   = "TituloElectronico_".$this->PeopleCodeId;
						
						$thisTitle1     = "VISTA PREVIA DE T&Iacute;TULO ELECTR&Oacute;NICO";
						$thisTitle2     = "Vista Previa de T&iacute;tulo Electr&oacute;nico";
						
						$thisButton1    = "Generar T&iacute;tulo Electr&oacute;nico";
						$thisBtnIcon1   = "file-pdf-o";
						$thisBtnClass1  = "btn-purple";
						break;
					case 'notificar-titulo':
						$thisStatus     = 51;
						$thisFileName   = "";
						$ThisRegId 		= $this->ThisRegId;
						
						$thisTitle1     = "ENTREGA DE T&Iacute;TULO F&Iacute;SICO";
						$thisTitle2     = "Entrega de T&iacute;tulo F&iacute;sico";
						
						$thisButton1    = "Enviar Notificaci&oacute;n";
						$thisBtnIcon1   = "check";
						$thisBtnClass1  = "btn-success";
						break;
					case 'valida-carta-aceptacion':
						$ThisRegId 		= 7;
						$thisStatus     = 25;
						$thisFileName   = "";
						
						$thisTitle1     = "VALIDACI&Oacute;N DE CARTA DE ACEPTACI&Oacute;N";
						$thisTitle2     = "Validaci&oacute;n de la Carta de Aceptaci&oacute;n del Alumno";
						
						$thisButton1    = "Validar Documento";
						$thisBtnIcon1   = "check";
						$thisBtnClass1  = "btn-success";
						
						$this2ndStatus 	= 26;
						$thisButton2    = "Rechazar Documento";
						$thisBtnIcon2   = "times";
						$thisBtnClass2  = "btn-danger";
						break;
				}
				
				$thisAction		= $thisButton1;
				$thisFileName 	= $thisFileName."_".$thisId.".pdf";

				echo "
					<div class='info' style='padding:0px; text-align:left;'>
						".self::HeaderAlumno($thisTitle1, $thisId)."
						<div id='MuestraCartas' class='content'>
							<form id='ThisRequest'>
								<div class='info' style='width:100%; height:auto; background-color:#f1f2f3; padding:20px; margin:auto; text-align:center;'>
									<div id='FormatLetter' class='content' style='position:relative; width:90%; text-align:center; margin:auto;'>
										".self::FormatosCartas($thisType, $thisId, $thisAction)."
										<input type='hidden' id='DocumentName' name='DocumentName' value='".$thisTitle2."'>
									</div>";
									if($thisType <> "valida-carta-aceptacion"){
										echo "
											<input type='hidden' id='start_date' name='start_date' value=''>
											<input type='hidden' id='end_date' name='end_date' value=''>
										";
									}
										if($thisType <> "valida-carta-aceptacion"){
											echo "<div id='BtnLetters' style='position:relative; margin-top:150px;'>";
										}else{
											echo "<div id='BtnLetters' style='position:relative; margin-top:30px;'>";
										}
										if(isset($thisButton2)){echo "<button type='submit' class='btn ".$thisBtnClass2." ValidateRequest' Section='".$thisType."' StudentId='".$thisId."' StdRegId='".$this->ThisRegId."' PeopleCodeId='".$this->PeopleCodeId."' NewStatus='".$this2ndStatus."' RegId='".$ThisRegId."' RegName='".$thisFileName."' SPId='".$GLOBALS["thisSPId"]."' style='z-index:1000; position: relative;'><i class='fa fa-".$thisBtnIcon2."'></i> ".$thisButton2."</button>";}
										echo "<button type='submit' class='btn ".$thisBtnClass1." ValidateRequest' Section='".$thisType."' StudentId='".$thisId."' StdRegId='".$this->ThisRegId."' PeopleCodeId='".$this->PeopleCodeId."' NewStatus='".$thisStatus."' RegId='".$ThisRegId."' RegName='".$thisFileName."' SPId='".$GLOBALS["thisSPId"]."' style='z-index:1000; position: relative;'><i class='fa fa-".$thisBtnIcon1."'></i> ".$thisButton1."</button>
									</div>
								</div>
							</form>
							".MsgResult()."
						</div>
					</div>
				";
			}
			
			public function UpdateInfo($thisType, $thisId){
				$obj = new ServProf;
				switch ($thisType) {
					case 'actualiza-data':
						$thisRejected       = 2;
						$thisAccepted       = 3;
						$thisRevision       = 11;
						
						$thisTable          = "ACC_CAMBIO_DATOS_SPR_SP";
						$thisSqlFilter      = "WHERE ID = ".$thisId;
						$thisTitle          = "Solicitud de Actualizaci&oacute;n de Datos del Alumno:";
						$thisDescription    = "La informaci&oacute;n mostrada a continuaci&oacute;n deber&aacute; ser actualizada directamente en la Base de Datos del Power Campus";
						$thisConfirmBtn     = "Comenzar Revisi&oacute;n";
						$thisRejectBtn      = "Rechazar informaci&oacute;n";
						$thisAcceptBtn      = "Confirmar informaci&oacute;n";
					break;
					case 'nuevo-convenio':
						$thisRejected       = 5;
						$thisAccepted       = 6;
						$thisRevision       = 11;
		
						$thisTable          = "ACC_CAMBIO_DATOS_SPR_SP";
						$thisSqlFilter      = "WHERE ID = ".$thisId;
						$thisTitle          = "Solicitud de Alta de nuevo Convenio:";
						$thisDescription    = "La informaci&oacute;n mostrada a continuaci&oacute;n deber&aacute; ser actualizada directamente en la Base de Datos del Power Campus";
						$thisConfirmBtn     = "Comenzar Revisi&oacute;n";
						$thisRejectBtn      = "Rechazar Convenio";
						$thisAcceptBtn      = "Confirmar Convenio";
					break;
				}
				echo "
					<div class='info' style='padding:10px; margin-left:0px; margin-top:0px; text-align:left;'>
						<h3 class='page-header'>".$thisTitle."</h3>
						<div id='content' class='content'>
							<p>".$thisDescription."</p>
						</div>
					</div>
					<div id='ThisRequest' class='row' style='margin-top:10px;'>
						<div class='col-md-12 col-sm-6'>";
							switch ($thisType) {
								case 'actualiza-data':
								case 'nuevo-convenio':
									$obj->CargaDatosDb($thisType, $thisTable, $thisRejected, $thisAccepted, $thisRevision, $thisSqlFilter, $thisId, $thisConfirmBtn, $thisRejectBtn, $thisAcceptBtn);
								break;
							}
						echo "
						</div>
					</div>
					".MsgResult()."
				";
			}
			
			public function FormatosCartas($thisType, $thisId, $thisAction){
				self::SP_Conn();
				$this->ThisRegId 		= self::getThisRcd("ACC_STDDEGREQ_SS_ID", "ACC_CSP_01_SP", "WHERE Acc_Progress_Status_Id = ".$thisId);
				$this->ThisPeopleCodeId = self::getThisRcd("PEOPLE_ID", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
				$this->ThisGenero 		= self::getThisRcd("SEXO", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
				
				self::xQueryI("EXECUTE Acc_datos_cartas_sp @ACC_STDDEGREQ_SS_ID = ".$this->ThisRegId);
				while ($this->data = sqlsrv_fetch_array($this->rQuery)) {
					$SPData_Director 	= $this->data["Director"];
					$SPData_Direccion 	= $this->data["Direccion"];
					$SPData_Delegacion 	= utf8_encode($this->data["Delegacion"]);
					$SPData_Estado 		= utf8_encode($this->data["Estado"]);
					$SPData_RVOE 		= $this->data["Rvoe"];
					$SPData_Matricula 	= $this->data["Matricula"];
					$SPData_Cv_Carrera	= $this->data["Cv_carrera"];
					$SPData_dia 		= $this->data["Dia"];
					$SPData_mes 		= $this->data["Mes"];
					$SPData_anho 		= $this->data["Anho"];
					$SPData_FormalTitle = $this->data["Carrera"];
					$SPData_Porcentaje 	= $this->data["Porcentaje"];
					$SPData_Promedio 	= $this->data["Promedio"];
					$SPData_Turno 		= $this->data["Turno"];
					$SPData_CicloIng 	= $this->data["Ciclo_de_ingreso"];

					if($SPData_Estado === "Estado de México"){
						$SPData_Estado = $SPData_Delegacion.", ".$SPData_Estado;
					}
				}
				
				switch ($this->ThisGenero) {
					case 'M':
						$TxtGen_1 = "o";
						$TxtGen_2 = "él";
						$TxtGen_3 = "del";
						$TxtGen_4 = "Sr.";
						$TxtGen_5 = "el";
						$TxtGen_6 = "al";
						break;
					case 'F':
						$TxtGen_1 = "a";
						$TxtGen_2 = "la";
						$TxtGen_3 = "de la";
						$TxtGen_4 = "Srita.";
						$TxtGen_5 = "ella";
						$TxtGen_6 = "a la";
						break;
				}
				
				if($thisType === "notificar-titulo"){
					$this->result = "";
				}else{
					$this->result = "";
					//$this->result = "<div class='water-mark'>&nbsp;</div>";
				}

				switch ($thisType) {
					case 'solicitud-ss-iniciada':
					case 'solicitud-sp-iniciada':
						self::xQueryI("SELECT * FROM ACC_CSP_01_SP WHERE Acc_Progress_Status_Id = ".$thisId);
						if ($this->data = sqlsrv_fetch_array($this->rQuery)) {
							$this->RcdDate = self::formatDate($this->data["FECHA"]->format('Y-m-d'));
							$this->result .= "
								<div style='width:90%; margin:auto; text-align:justify; font-size:14px;'>
									<div class='row' style='width:100%; margin-bottom:10px;'>
										<div class='col-md-6' style='width:500px; text-align:center;'></div>
											<div class='col-md-6' style='width:300px; text-align:right; float:right;'>
												<img src='img/uin-logo.png' style='width:100%; margin-bottom:10px;'><br />
												<span style='color:#0054a4; font-size:20px;'><b>PLANTEL ".$this->data["PLATEL"]."</b></span><br />
											</div>
										</div>
										<p style='text-align:center; font-weight:bold; margin-top:40px;'>
											<b>".$SPData_Estado."</b> a <b><u>".date("d")."</u></b> de <b><u>".self::monthName(date("m"), 2)."</u></b> de <b><u>".date("Y")."</u></b>.
										</p>
										<br /><br />
										<p style='text-align:left; left:0px; font-weight:bold;'>
											<b>".strtoupper(utf8_encode($this->data["DIRIGIDO_A"]))."</b><br />
											<b>".strtoupper(utf8_encode($this->data["PUESTO_DESEMPENADO"]))."</b><br />
											<b>P R E S E N T E</b> 
										</p>
										<br /><br />
										<div style='text-align:center;'>
											<span style='color:#000000; font-size:20px;'><b>C&nbsp;O&nbsp;N&nbsp;S&nbsp;T&nbsp;A&nbsp;N&nbsp;C&nbsp;I&nbsp;A&nbsp;&nbsp;&nbsp;D&nbsp;E&nbsp;&nbsp;&nbsp;C&nbsp;R&nbsp;É&nbsp;D&nbsp;I&nbsp;T&nbsp;O&nbsp;S</b></span>
										</div>
										<br /><br />
										<p style='text-align:justify;'>
											Por medio de la presente hago constar que ".$TxtGen_2." alumn".$TxtGen_1." <b><u>".strtoupper(utf8_encode($this->data["NOMBRE_ALUMNO"]))." ".strtoupper(utf8_encode($this->data["APATERNO"]))." ".strtoupper(utf8_encode($this->data["AMATERNO"]))."</u></b> inscrit".$TxtGen_1." 
											en la <b><u><b>".strtoupper(utf8_encode($SPData_FormalTitle))."</b></u></b> con Reconocimiento de Validez Oficial de Estudios de la S.E.P., seg&uacute;n Acuerdo No.<b>".$SPData_RVOE."</b> 
											de fecha <u><b>".$SPData_dia." de ".self::monthName($SPData_mes, 1)." de ".$SPData_anho."</b></u> y con n&uacute;mero de matr&iacute;cula <b>".$SPData_Matricula."</b>, tiene cubierto el <b><u>".$SPData_Porcentaje."</u></b>% de cr&eacute;ditos, con un promedio 
											general de: <b><u>".$SPData_Promedio."</u></b>
											<br /><br />
											Se extiende la presente a petici&oacute;n ".$TxtGen_3." interesad".$TxtGen_1." para los fines que a ".$TxtGen_2." interesad".$TxtGen_1." convengan.
										</p>
										<div style='text-align:center; font-weight:bold; margin-top:70px;'>
											<b>A T E N T A M E N T E</b><br />
											“Sapientia, Superatio et Progressus”
											<br /><br />
											<div style='margin-top:110px;'><b>".utf8_encode($SPData_Director)."</b></div>
											Director(a) del Plantel
											<br /><br />
											<div style='text-align:center; font-weight:bold;'>
												".utf8_encode($SPData_Direccion)."
											</div>
										</div>
										<input type='hidden' id='AddComments' name='AddComments' value='".$thisAction."'>
								</div>
							";
						}
						break;
					case 'carta-presentacion-ss':
						self::xQueryI("SELECT * FROM ACC_CSP_01_SP WHERE Acc_Progress_Status_Id = ".$thisId);
						if ($this->data = sqlsrv_fetch_array($this->rQuery)) {
							$this->RcdDate 				= self::formatDate($this->data["FECHA"]->format('Y-m-d'));
							$this->ThisTipoLibreacion 	= self::getThisRcd("TIPO_LIBERACION_ID", "ACC_SERVICIOS_PROFESIONALES_SP", "WHERE Acc_Progress_Status_Id = ".$thisId);
							$this->ThisFechaRVOE		= "";
							switch ($this->ThisTipoLibreacion) {
								case 4:
									$this->result .= "
										<div style='width:95%; margin:auto; text-align:justify; font-size:12px; line-height: 1.5;'>
											<div class='row' style='width:100%; margin-bottom:5px;'>
												<div class='col-md-7' style='width:300px; float:left;'>
													<img src='img/uin-logo.png' style='width:300px; margin-bottom:10px;'><br />
													<span style='color:#0054a4; font-size:20px;'><b>PLANTEL ".$this->data["PLATEL"]."</b></span>
													<br /><br />
												</div>
												<div class='col-md-5' style='width:400px; float:right; top:50px; text-align:right;'>
													<span style='font-size:17px;'><b>SOLICITUD DE CONSTANCIA LABORAL</b></span>.
													<br />
													<b>".$SPData_Estado."</b> a <b><u>".date("d")."</u></b> de <b><u>".self::monthName(date("m"), 2)."</u></b> de <b><u>".date("Y")."</u></b>.
												</div>
											</div>
											<div style='text-align:justify;'>
												<b>".strtoupper(utf8_encode($this->data["DIRIGIDO_A"]))."</b><br /><b>".strtoupper(utf8_encode($this->data["PUESTO_DESEMPENADO"]))."</b><br />
												De: <b>".strtoupper(utf8_encode($this->data["NOMBRE_EMPRESA"]))."</b>.
												<br /><b>P R E S E N T E</b>
												</p><br />
												Para los efectos correspondientes, comunico a usted que C. <b>".strtoupper(utf8_encode($this->data["NOMBRE_ALUMNO"]))." ".strtoupper(utf8_encode($this->data["APATERNO"]))." ".strtoupper(utf8_encode($this->data["AMATERNO"]))."</b>, 
												estudiante de esta Universidad Plantel <b>".strtoupper($this->data["PLATEL"])."</b>, de la <b>".strtoupper(utf8_encode($SPData_FormalTitle))."</b>, con n&uacute;mero de Matr&iacute;cula <b>".$SPData_Matricula."</b>, 
												quien ha obtenido el <b>".$SPData_Porcentaje."%</b> de un total de <b>100%</b>, impartida seg&uacute;n acuerdo de RVOE de la S.E.P. No. <b>".$SPData_RVOE."</b> de <b>".$SPData_dia." de ".self::monthName($SPData_mes, 1)." de ".$SPData_anho."</b>.
												<br /><br /> 
												Que ".$TxtGen_2." alumn".$TxtGen_1." de esta Universidad, queda exento del Servicio Social, esto con fundamento en el art&iacute;culo 91 del Reglamento de la Ley Reglamentaria del Art&iacute;culo 5o. Constitucional, 
												Relativo al Ejercicio de las Profesiones en la Ciudad de M&eacute;xico que a la letra dice:
												<br /><br /> 
												Los estudiantes y profesionistas trabajadores de la Federaci&oacute;n y del Gobierno de la Ciudad de M&eacute;xico no estar&aacute;n obligados a prestar ning&uacute;n servicio social distinto al desempeño de sus funciones. 
												El que presten voluntariamente dar&aacute; lugar a que se haga la anotaci&oacute;n respectiva en su hoja de servicios
												<br /><br /> 
												<div style='text-align:left;'>
													Por lo anterior, solicito a usted se expida el siguiente documento en hoja membretada, sellada y firmada:
													<ul>
														Constancia laboral expedida por el &aacute;rea de Recursos Humanos o del Departamento de Personal, 
														<br />dirigida a: <b>".utf8_encode($SPData_Director)."</b>,
														<br />Director(a) del Plantel
													</ul>
													<b>En la que se señale lo siguiente:</b><br />
													<ul>
														Nombre completo del alumn".$TxtGen_1."
														Licenciatura que est&aacute; estudiando
														Cuatrimestre que est&aacute; cursando
														Plantel al que pertenece
														Cargo que desempeña o nombramiento
														Labores que realiza
														Fecha de ingreso a la instituci&oacute;n.
														Se debe observar, que tiene una antigüedad m&iacute;nima de 6 meses laborando en la Dependencia.
													</ul>
												</div>
											</div>
											Sin otro particular de momento, aprovecho la ocasi&oacute;n para enviarle un cordial saludo.
											<br />
											<div style='text-align:left; font-weight:bold;'>
												<b>A T E N T A M E N T E</b><br />
												“Sapientia, Superatio et Progressus”
												<br /><br />
												<div style='margin-top:50px;'><b>".utf8_encode($SPData_Director)."</b></div>
												Director(a) del Plantel
											</div>
											<br /><br />
											<div style='text-align:center; font-weight:bold;'>
												".utf8_encode($SPData_Direccion)."
											</div>
											<input type='hidden' id='AddComments' name='AddComments' value='".$thisAction."'>
										</div>
									";
									break;
								default:
									$this->result .= "
										<div style='width:95%; margin:auto; text-align:justify; font-size:12px; line-height: 1.5;'>
											<div class='row' style='width:100%; margin-bottom:5px;'>
												<div class='col-md-7' style='width:300px; float:left;'>
													<img src='img/uin-logo.png' style='width:300px; margin-bottom:10px;'><br />
													<span style='color:#0054a4; font-size:20px;'><b>PLANTEL ".$this->data["PLATEL"]."</b></span>
													<br /><br />
												</div>
												<div class='col-md-4' style='width:300px; float:right; top:50px;'>
													<span style='font-size:20px;'><b>ESCRITO DE PRESENTACIÓN</b></span>.
													<br />
													<b>".$SPData_Estado."</b> a <b><u>".date("d")."</u></b> de <b><u>".self::monthName(date("m"), 2)."</u></b> de <b><u>".date("Y")."</u></b>.
												</div>
											</div>
											<div style='text-align:justify;'>
												<b>".strtoupper(utf8_encode($this->data["DIRIGIDO_A"]))."</b><br /><b>".strtoupper(utf8_encode($this->data["PUESTO_DESEMPENADO"]))."</b><br />
												De: <b>".strtoupper(utf8_encode($this->data["NOMBRE_EMPRESA"]))."</b>.
												<br /><b>P R E S E N T E</b>
												</p><br />
												Con el fin de cumplir con el requisito establecido en el Art&iacute;culo 5º Constitucional y en el Reglamento de Profesiones y Disposiciones Conexas de la Direcci&oacute;n General 
												de Profesiones dependiente de la Secretar&iacute;a de Educaci&oacute;n P&uacute;blica, con respecto a la prestaci&oacute;n del <b>Servicio Social</b>, me permito presentar a usted ".$TxtGen_6.":
												<br /><br />
												C. <b>".strtoupper(utf8_encode($this->data["NOMBRE_ALUMNO"]))." ".strtoupper(utf8_encode($this->data["APATERNO"]))." ".strtoupper(utf8_encode($this->data["AMATERNO"]))."</b>, alumn".$TxtGen_1." de la 
												<b>".strtoupper(utf8_encode($SPData_FormalTitle))."</b>, impartida seg&uacute;n Acuerdo No. <b>".$SPData_RVOE."</b> de fecha <b>".$SPData_dia." de ".self::monthName($SPData_mes, 1)." de ".$SPData_anho."</b> y quien 
												ha obtenido el <b>".$SPData_Porcentaje."%</b> de cr&eacute;ditos de un total de 100% y un promedio general de: <b>".$SPData_Promedio."</b>.  
												<br /><br />
												No. de Matr&iacute;cula: <b>".$SPData_Matricula."</b> Turno: ".strtoupper($SPData_Turno).". 
												<br /><br />
												".$TxtGen_2." ".$TxtGen_4." <b>".$this->data["APATERNO"]."</b>, tiene inter&eacute;s en desarrollar su Servicio Social en esta Empresa/Dependencia, desempeñando las labores que le sean 
												encomendadas y que est&eacute;n directamente relacionadas con su preparaci&oacute;n acad&eacute;mica. Es conveniente señalar que la duraci&oacute;n del servicio social ser&aacute; de 480 hrs. a cumplir 
												en un per&iacute;odo de seis meses como m&iacute;nimo.
												<br /><br />
												Mucho agradecer&eacute; a Usted que, una vez aceptada nuestra petici&oacute;n, env&iacute;e a esta Direcci&oacute;n por conducto de ".$TxtGen_2." ".$TxtGen_4." <b>".$this->data["APATERNO"]."</b>, la carta 
												en la que se indique la siguiente informaci&oacute;n:
												<table class='table' style='width:100%;'>
													<tr>
														<td style='width:50%;'>Nombre completo ".$TxtGen_3." alumn".$TxtGen_1."</td>
														<td style='width:50%;'>Total de horas a cubrir 480 hrs.</td>
													</tr>
													<tr>
														<td style='width:50%;'>Nombre de carrera que cursa</td>
														<td style='width:50%;'>Aceptad".$TxtGen_1." para realizar el Servicio Social</td>
													</tr>
													<tr>
														<td style='width:50%;'>Número de matrícula</td>
														<td style='width:50%;'>Horario</td>
													</tr>
													<tr>
														<td style='width:50%;'>Fecha de inicio y término del Servicio Social</td>
														<td style='width:50%;'>Actividades a desarrollar</td>
													</tr>
												</table>
												<br />
												Al respecto, solicito atentamente que este escrito sea enviado dentro de los 15 d&iacute;as h&aacute;biles siguientes a la fecha de esta petici&oacute;n, ya que de acuerdo al Reglamento 
												de Servicio Social, no ser&aacute; reconocida ninguna prestaci&oacute;n de servicio social que no est&eacute; debidamente documentada, registrada y autorizada por la Direcci&oacute;n de este plantel.
												<br /><br />
												<b><i>Cabe señalar que la carta de t&eacute;rmino debe contener los datos arriba señalados; con la diferencia de que se manifestar&aacute; que el alumno “concluy&oacute;” o “termin&oacute;” 
												satisfactoriamente el Servicio Social.</i></b>
												<br /><br />
												Sin otro particular de momento, aprovecho la ocasi&oacute;n para enviarle un cordial saludo.
											</div>
											<br />
											<div style='text-align:left; font-weight:bold;'>
												<b>A T E N T A M E N T E</b><br />
												“Sapientia, Superatio et Progressus”
												<br /><br />
												<div style='margin-top:50px;'><b>".utf8_encode($SPData_Director)."</b></div>
												Director(a) del Plantel
											</div>
											<br /><br />
											<div style='text-align:center; font-weight:bold;'>
												".utf8_encode($SPData_Direccion)."
											</div>
											<input type='hidden' id='AddComments' name='AddComments' value='".$thisAction."'>
										</div>
									";
									break;
							}
						}
						break;
					case 'carta-presentacion-sp':
						self::xQueryI("SELECT * FROM ACC_CSP_01_SP WHERE Acc_Progress_Status_Id = ".$thisId);
						if ($this->data = sqlsrv_fetch_array($this->rQuery)) {
							$this->RcdDate 				= self::formatDate($this->data["FECHA"]->format('Y-m-d'));
							$this->ThisTipoLibreacion 	= self::getThisRcd("TIPO_LIBERACION_ID", "ACC_SERVICIOS_PROFESIONALES_SP", "WHERE Acc_Progress_Status_Id = ".$thisId);
							$this->ThisFechaRVOE		= "";
							$this->result .= "
								<div style='width:95%; margin:auto; text-align:justify; font-size:12px; line-height: 1.5;'>
									<div class='row' style='width:100%; margin-bottom:5px;'>
										<div class='col-md-7' style='width:300px; float:left;'>
											<img src='img/uin-logo.png' style='width:300px; margin-bottom:10px;'><br />
											<span style='color:#0054a4; font-size:20px;'><b>PLANTEL ".$this->data["PLATEL"]."</b></span>
											<br />
										</div>
										<div class='col-md-4' style='width:300px; float:right; top:50px;'>
											<span style='font-size:20px;'><b>ESCRITO DE PRESENTACIÓN</b></span>.
											<br />
											<b>".$SPData_Estado."</b> a <b><u>".date("d")."</u></b> de <b><u>".self::monthName(date("m"), 2)."</u></b> de <b><u>".date("Y")."</u></b>.
										</div>
									</div>
									<div style='text-align:justify;'>
										<b>".strtoupper(utf8_encode($this->data["DIRIGIDO_A"]))."</b><br /><b>".strtoupper(utf8_encode($this->data["PUESTO_DESEMPENADO"]))."</b><br />
										De: <b>".strtoupper(utf8_encode($this->data["NOMBRE_EMPRESA"]))."</b>.
										<br /><b>P R E S E N T E</b>
										</p><br />
										Con el fin de cumplir con el requisito establecido por esta Universidad con respecto a la presentación de las 
										Pr&aacute;cticas Profesionales, me permito presentar a usted ".$TxtGen_6.":
										<br /><br />
										C. <b>".strtoupper(utf8_encode($this->data["NOMBRE_ALUMNO"]))." ".strtoupper(utf8_encode($this->data["APATERNO"]))." ".strtoupper(utf8_encode($this->data["AMATERNO"]))."</b>, 
										alumn".$TxtGen_1." de la <b>".strtoupper(utf8_encode($SPData_FormalTitle))."</b>, 
										impartida según Acuerdo No. <b>".$SPData_RVOE."</b> de fecha <b>".$SPData_dia." de ".self::monthName($SPData_mes, 1)." de ".$SPData_anho."</b> y quien ha obtenido el <b>".$SPData_Porcentaje."%</b> de cr&eacute;ditos de un total de 100% 
										y un promedio general de: <b>".$SPData_Promedio."</b>.
										<br /><br />
										No. de Matrícula: <b>".$SPData_Matricula."</b> Turno: ".strtoupper($SPData_Turno).". 
										<br /><br />
										".$TxtGen_2." ".$TxtGen_4." <b>".$this->data["APATERNO"]."</b>, tiene el interés en desarrollar sus Pr&aacute;cticas Profesionales en esta 
										Empresa/Dependencia del Sector Privado/Público, desempeñando las labores que le sean encomendadas y que estén 
										directamente relacionadas con su preparación académica. Es conveniente señalar que la duración de las 
										Pr&aacute;cticas Profesionales será de 400 horas a cumplir en un periodo de cuatro meses como mínimo.
										<br /><br />
										Mucho agradeceré a Usted que, una vez aceptada nuestra petición, envíe a esta Dirección por conducto de 
										".$TxtGen_2." ".$TxtGen_4." <b>".$this->data["APATERNO"]."</b>, la carta en la que se indique la siguiente información:
										<table class='table' style='width:100%;'>
											<tr>
												<td style='width:50%;'>Nombre completo ".$TxtGen_3." alumn".$TxtGen_1."</td>
												<td style='width:50%;'>Total de horas a cubrir 480 hrs.</td>
											</tr>
											<tr>
												<td style='width:50%;'>Nombre de carrera que cursa</td>
												<td style='width:50%;'>Aceptad".$TxtGen_1." para realizar las Pr&aacute;cticas Profesionales</td>
											</tr>
											<tr>
												<td style='width:50%;'>Número de matrícula</td>
												<td style='width:50%;'>Horario</td>
											</tr>
											<tr>
												<td style='width:50%;'>Fecha de inicio y término de las Pr&aacute;cticas Profesionales</td>
												<td style='width:50%;'>Actividades a desarrollar</td>
											</tr>
										</table>
										<br />
										Al respecto, solicito atentamente que este escrito sea enviado dentro de los 15 días hábiles siguientes a la 
										fecha de esta petición, ya que de acuerdo a nuestra normatividad, no será reconocida ninguna prestación de 
										Pr&aacute;cticas Profesionales que no esté debidamente documentada, registrada y autorizada por la Dirección de este 
										plantel.
										<br /><br />
										<b><i>Cabe señalar que la carta de término debe contener los datos arriba señalados; con la diferencia de que se 
										manifestará que el alumno “concluyó” o “terminó” satisfactoriamente las Pr&aacute;cticas Profesionales.</i></b>
										<br /><br />
										Sin otro particular de momento, aprovecho la ocasión para enviarle un cordial saludo.
									</div>
									<br />
									<div style='text-align:left; font-weight:bold;'>
										<b>A T E N T A M E N T E</b><br />
										“Sapientia, Superatio et Progressus”
										<br /><br />
										<div style='margin-top:30px;'><b>".utf8_encode($SPData_Director)."</b></div>
										Director(a) del Plantel
									</div>
									<br />
									<div style='text-align:center; font-weight:bold;'>
										".utf8_encode($SPData_Direccion)."
									</div>
									<input type='hidden' id='AddComments' name='AddComments' value='".$thisAction."'>
								</div>
							";
						}
						break;
					case 'carta-liberacion-ss':
						self::xQueryI("SELECT * FROM ACC_CSP_01_SP WHERE Acc_Progress_Status_Id = ".$thisId);
						if ($this->data = sqlsrv_fetch_array($this->rQuery)) {
							$this->RcdDate 				= self::formatDate($this->data["FECHA"]->format('Y-m-d'));
							$this->ThisTipoLibreacion 	= self::getThisRcd("TIPO_LIBERACION_ID", "ACC_SERVICIOS_PROFESIONALES_SP", "WHERE Acc_Progress_Status_Id = ".$thisId);
							switch ($this->ThisTipoLibreacion) {
								case 4:
									$this->result .= "
										<div style='width:90%; margin:auto; text-align:justify; font-size:14px; line-height:1.5;'>
											<div class='row' style='width:100%;'>
												<div class='col-md-8' style='width:500px; float:left;'></div>
												<div class='col-md-4' style='width:300px; float:right; top:120px; text-align:right;'>
													<span style='color:#0054a4; font-size:20px;'><b>PLANTEL ".$this->data["PLATEL"]."</b></span>
												</div>
											</div>
											<br /><br />
											<div style='width:100%; text-align:left; margin-top:120px;'>
												<span style='color:#000000; font-size:19px;'><b>
													DIRECCIÓN GENERAL DE PROFESIONES<br />
													SECRETARÍA DE EDUCACIÓN PÚBLICA
												</b></span>
											</div>
											<br /><br />
											<div class='col-md-6' style='width:600px; float:right; text-align:right;'>
												<span style='color:#000000; font-size:18px;'><b>Asunto: Constancia de Liberación de<br />Servicio Social</b></span>
											</div>
											<br /><br />
											<p style='text-align:justify; margin-top:40px;'>
												De conformidad con lo dispuesto en el reglamento de la Ley Reglamentaria del Artículo 5º Constitucional relativo al ejercicio de las profesiones en el Distrito Federal y en cumplimiento con el mismo, la Dirección de este plantel, 
												hace constar el C. <b>".strtoupper(utf8_encode($this->data["NOMBRE_ALUMNO"]))." ".strtoupper(utf8_encode($this->data["APATERNO"]))." ".strtoupper(utf8_encode($this->data["AMATERNO"]))."</b>, alumno de la ".strtoupper(utf8_encode($SPData_FormalTitle)).", 
												con Reconocimiento de Validez Oficial de Estudios de la S.E.P., seg&uacute;n Acuerdo No. <b>".$SPData_RVOE."</b> de fecha <b>".$SPData_dia." de ".self::monthName($SPData_mes, 1)." de ".$SPData_anho."</b>, que se imparte en la 
												<b>UNIVERSIDAD INSURGENTES PLANTEL ".strtoupper($this->data["PLATEL"])."</b>, no está obligado a prestar ningún Servicio Social, como indica el Artículo 91, Capítulo VIII, del Reglamento citado; que a la letra dice:
												<br /><br />
												''Los estudiantes y profesionistas trabajadores de la Federación y del Gobierno del Distrito Federal no estarán obligados a prestar ningún servicio social distinto del desempeño de sus funciones. El que presten voluntariamente dará lugar a que se haga la anotación respectiva en su hoja de servicio''.
												<br /><br />
												Lo anterior en virtud de que el C. <b>".strtoupper(utf8_encode($this->data["NOMBRE_ALUMNO"]))." ".strtoupper(utf8_encode($this->data["APATERNO"]))." ".strtoupper(utf8_encode($this->data["AMATERNO"]))."</b><br />
												Se desempeña laboralmente en: ".strtoupper(utf8_encode($this->data["NOMBRE_EMPRESA"])).".
												<br /><br />
												Se expide la presente constancia, a los <b>".date("d")." días del mes de  ".self::monthName(date("m"), 2)." de ".date("Y")."</b>, para los fines que a ".$TxtGen_2." interesad".$TxtGen_1." convengan.
											</p>
											<div style='text-align:center; font-weight:bold; margin-top:50px;'>
												<b>A T E N T A M E N T E</b><br />
												<br /><br />
												<div style='margin-top:60px;'><b>".utf8_encode($SPData_Director)."</b></div>
												Director(a) del Plantel
												<br /><br />
												<div style='text-align:center; font-weight:bold;'>
													".utf8_encode($SPData_Direccion)."
												</div>
											</div>
											<input type='hidden' id='AddComments' name='AddComments' value='".$thisAction."'>
										</div>
									";
									break;
								default:
									$this->result .= "
										<div style='width:90%; margin:auto; text-align:justify; font-size:14px; line-height:1.5;'>
											<div class='row' style='width:100%;'>
												<div class='col-md-8' style='width:500px; float:left;'></div>
												<div class='col-md-4' style='width:300px; float:right; top:120px; text-align:right;'>
													<span style='color:#0054a4; font-size:20px;'><b>PLANTEL ".$this->data["PLATEL"]."</b></span>
												</div>
											</div>
											<br /><br />
											<div style='width:100%; text-align:left; margin-top:120px;'>
												<span style='color:#000000; font-size:19px;'><b>
													DIRECCIÓN GENERAL DE PROFESIONES<br />
													SECRETARÍA DE EDUCACIÓN PÚBLICA
												</b></span>
											</div>
											<br /><br />
											<div class='col-md-6' style='width:600px; float:right; text-align:right;'>
												<span style='color:#000000; font-size:18px;'><b>Asunto: Constancia de Liberación de<br />Servicio Social</b></span>
											</div>
											<br /><br />
											<p style='text-align:justify; margin-top:40px;'>
											En apego a lo establecido en el artículo 55 de la Ley Reglamentaria del Artículo 5º Constitucional, la Dirección de este plantel, hace constar que ".$TxtGen_2." 
												C. <b>".strtoupper(utf8_encode($this->data["NOMBRE_ALUMNO"]))." ".strtoupper(utf8_encode($this->data["APATERNO"]))." ".strtoupper(utf8_encode($this->data["AMATERNO"]))."</b>, alumno de la ".strtoupper(utf8_encode($SPData_FormalTitle)).", 
												con Reconocimiento de Validez Oficial de Estudios de la S.E.P., seg&uacute;n Acuerdo No. <b>".$SPData_RVOE."</b> de fecha <b>".$SPData_dia." de ".self::monthName($SPData_mes, 1)." de ".$SPData_anho."</b>, que se imparte en la 
												UNIVERSIDAD INSURGENTES PLANTEL <b>".strtoupper($this->data["PLATEL"])."</b>, realizó su Servicio Social cubriendo un total de 480 horas.
												<br /><br />
												Se expide la presente constancia, a los <b>".date("d")." días del mes de  ".self::monthName(date("m"), 2)." de ".date("Y")."</b>, para los fines que a ".$TxtGen_2." interesad".$TxtGen_1." convengan.
											</p>
											<div style='text-align:center; font-weight:bold; margin-top:50px;'>
												<b>A T E N T A M E N T E</b><br />
												<br /><br />
												<div style='margin-top:60px;'><b>".utf8_encode($SPData_Director)."</b></div>
												Director(a) del Plantel
												<br /><br />
												<div style='text-align:center; font-weight:bold;'>
													".utf8_encode($SPData_Direccion)."
												</div>
											</div>
											<input type='hidden' id='AddComments' name='AddComments' value='".$thisAction."'>
										</div>
									";
									break;
							}
						}
						break;
					case 'carta-liberacion-sp':
						self::xQueryI("SELECT * FROM ACC_CSP_01_SP WHERE Acc_Progress_Status_Id = ".$thisId);
						if ($this->data = sqlsrv_fetch_array($this->rQuery)) {
							$this->RcdDate 		= self::formatDate($this->data["FECHA"]->format('Y-m-d'));
							$this->Actividades	= self::getThisRcd("ACTIVIDADES", "ACC_PRACTICAS_PROFESIONALES_SP", "WHERE Acc_Progress_Status_Id = ".$thisId);
							$this->FechaInicio	= self::getDbRowName("FECHA_INICIO", "ACC_SERVICIOS_PROFESIONALES_SP", "WHERE Acc_Progress_Status_Id = ".$thisId, 0);
							$this->FechaTermino	= self::getDbRowName("FECHA_TERMINO", "ACC_SERVICIOS_PROFESIONALES_SP", "WHERE Acc_Progress_Status_Id = ".$thisId, 0);
							
							$this->Actividades 	= str_replace("|",", ",$this->Actividades);

							$this->FechaInicio	= self::getDbRowName("FECHA_INICIO", "ACC_SERVICIOS_PROFESIONALES_SP", "WHERE Acc_Progress_Status_Id = 1", 0);
					
							$this->FechaInicio 	= $this->FechaInicio->format('Y-m-d');
							$this->FechaTermino = $this->FechaTermino->format('Y-m-d');
							
							$this->DiaInicio 	= substr(strtoupper($this->FechaInicio), 8, 2);
							$this->MesInicio 	= substr(strtoupper($this->FechaInicio), 5, 2);
							$this->AnioInicio 	= substr(strtoupper($this->FechaInicio), 0, 4);
							
							$this->DiaTermino 	= substr(strtoupper($this->FechaTermino), 8, 2);
							$this->MesTermino 	= substr(strtoupper($this->FechaTermino), 5, 2);
							$this->AnioTermino 	= substr(strtoupper($this->FechaTermino), 0, 4);
		
							$this->MesInicio 	= self::monthName($this->MesInicio, 2);
							$this->MesTermino 	= self::monthName($this->MesTermino, 2);

							$this->result .= "
								<div style='width:90%; margin:auto; text-align:justify; font-size:14px;'>
									<div class='row' style='width:100%;'>
										<div class='col-md-8' style='width:500px; float:left;'></div>
										<div class='col-md-4' style='width:300px; float:right; top:120px; text-align:right;'>
											<span style='color:#0054a4; font-size:20px;'><b>PLANTEL ".$this->data["PLATEL"]."</b></span>
										</div>
									</div>
									<br /><br />
									<div style='width:100%; text-align:left; margin-top:120px;'>
										<span style='color:#000000; font-size:19px;'><b>
											DIRECCIÓN GENERAL DE PROFESIONES<br />
											SECRETARÍA DE EDUCACIÓN PÚBLICA
										</b></span>
									</div>
									<br /><br />
									<div class='col-md-6' style='width:600px; float:right; text-align:right;'>
										<span style='color:#000000; font-size:18px;'><b>Asunto: Constancia de Pr&aacute;cticas Profesionales</b></span>
									</div>
									<br /><br />
									<p style='text-align:justify;'>
										La Dirección de la Universidad Insurgentes PLANTEL <b>".$this->data["PLATEL"]."</b>, con base al control de pasantes de la <b>".strtoupper(utf8_encode($SPData_FormalTitle))."</b>, hace constar que ".$TxtGen_2.":
										<br /><br />
										C. <b>".strtoupper(utf8_encode($this->data["NOMBRE_ALUMNO"]))." ".strtoupper(utf8_encode($this->data["APATERNO"]))." ".strtoupper(utf8_encode($this->data["AMATERNO"]))."</b>.
										Concluyó satisfactoriamente sus Pr&aacute;cticas Profesionales en:
										<br /><br />
										<b>".strtoupper(utf8_encode($this->data["NOMBRE_EMPRESA"]))."</b>.
										<br /><br />
										Consistiendo en:<b>".$this->Actividades."</b>.
										<br /><br />
										Durante el lapso del <b>".$this->DiaInicio."</b> del mes de <b>".$this->MesInicio."</b> de <b>".$this->AnioInicio."</b> al <b>".$this->DiaTermino."</b> del mes de <b>".$this->MesTermino."</b> de <b>".$this->AnioTermino."</b>.
										<br /><br />
										Se expide la presente constancia, a los <b>".date("d")." días del mes de  ".self::monthName(date("m"), 2)." de ".date("Y")."</b>, para los fines que a ".$TxtGen_2." interesad".$TxtGen_1." convengan.
									</p>
									<div style='text-align:center; font-weight:bold; margin-top:70px;'>
										<b>A T E N T A M E N T E</b><br />
										“Sapientia, Superatio et Progressus”
										<br /><br />
										<div style='margin-top:80px;'><b>".utf8_encode($SPData_Director)."</b></div>
										Director(a) del Plantel
										<br /><br />
										<div style='text-align:center; font-weight:bold;'>
											".utf8_encode($SPData_Direccion)."
										</div>
									</div>
									<input type='hidden' id='AddComments' name='AddComments' value='".$thisAction."'>
								</div>
							";
						}
						break;
					case 'carta-final-sp':
						self::xQueryI("SELECT * FROM ACC_CSP_01_SP WHERE Acc_Progress_Status_Id = ".$thisId);
						if ($this->data = sqlsrv_fetch_array($this->rQuery)) {
							$this->RcdDate = self::formatDate($this->data["FECHA"]->format('Y-m-d'));
							$this->result .= "
								<div style='width:90%; margin:auto; text-align:justify; font-size:14px;'>
									<div class='row' style='width:100%;'>
										<div class='col-md-8' style='width:500px; float:left;'></div>
										<div class='col-md-4' style='width:300px; float:right; top:120px; text-align:right;'>
											<span style='color:#0054a4; font-size:20px;'><b>PLANTEL ".$this->data["PLATEL"]."</b></span>
										</div>
									</div>
									<br /><br />
									<div style='width:100%; text-align:left; margin-top:120px;'>
										<span style='color:#000000; font-size:19px;'><b>
											DIRECCIÓN GENERAL DE PROFESIONES<br />
											SECRETARÍA DE EDUCACIÓN PÚBLICA
										</b></span>
									</div>
									<br /><br />
									<div class='col-md-6' style='width:600px; float:right; text-align:right;'>
										<span style='color:#000000; font-size:18px;'><b>Asunto: Constancia de Pr&aacute;cticas Profesionales</b></span>
									</div>
									<br /><br />
									<p style='text-align:justify;'>
										CONTENIDO DE LA CARTA FINAL FALTA POR DEFINIR
									</p>
									<div style='text-align:center; font-weight:bold; margin-top:70px;'>
										<b>A T E N T A M E N T E</b><br />
										“Sapientia, Superatio et Progressus”
										<br /><br />
										<div style='margin-top:80px;'><b>".utf8_encode($SPData_Director)."</b></div>
										Director(a) del Plantel
										<br /><br />
										<div style='text-align:center; font-weight:bold;'>
											".utf8_encode($SPData_Direccion)."
										</div>
									</div>
									<input type='hidden' id='AddComments' name='AddComments' value='".$thisAction."'>
								</div>
							";
						}
						break;
					case 'generar-titulo':
						//$this->VpUnicoCert	= "P000011114_612352";
						$this->VpUnicoCert		= $this->ThisPeopleCodeId."_".$SPData_Cv_Carrera;
						$this->FilePath			= "../XMLsTitulos/Autenticados/xml";
						$this->FileName			= $this->FilePath."/P".$this->VpUnicoCert.".xml";
						if(file_exists($this->FileName)){
							$xml 					= simplexml_load_file($this->FileName) or die("Error: No es posible leer el Archivo Seleccionado");
							$FolioControl			= $xml[0]['folioControl'];
							$NombreInstitucion		= $xml[0]->Institucion['nombreInstitucion'];
							
							$SelloResponsable		= $xml[0]->FirmaResponsables->FirmaResponsable[0]['sello'];
							$NombreResponsable		= $xml[0]->FirmaResponsables->FirmaResponsable[0]['nombre'];
							$PrimerApResponsable	= $xml[0]->FirmaResponsables->FirmaResponsable[0]['primerApellido'];
							$SegundoApResponsable	= $xml[0]->FirmaResponsables->FirmaResponsable[0]['segundoApellido'];
							$NoCertResponsable		= $xml[0]->FirmaResponsables->FirmaResponsable[0]['noCertificadoResponsable'];
							
							$SelloSecretarioGral	= $xml[0]->FirmaResponsables->FirmaResponsable[1]['sello'];
							$NombreSecretarioGral	= $xml[0]->FirmaResponsables->FirmaResponsable[1]['nombre'];
							$PrimerApSecretarioGral	= $xml[0]->FirmaResponsables->FirmaResponsable[1]['primerApellido'];
							$SegundoApSecGral		= $xml[0]->FirmaResponsables->FirmaResponsable[1]['segundoApellido'];
							$NoCertSecretarioGral	= $xml[0]->FirmaResponsables->FirmaResponsable[1]['noCertificadoResponsable'];

							$FechaExpedicion		= $xml[0]->Expedicion['fechaExpedicion'];
							$EntidadFederativa		= $xml[0]->Expedicion['entidadFederativa'];
							$FechaExProf			= $xml[0]->Expedicion['fechaExamenProfesional'];
							$FechaExcencionExProf	= $xml[0]->Expedicion['fechaExencionExamenProfesional'];
							
							$CveCarrera				= $xml[0]->Carrera['cveCarrera'];
							$NombreCarrera			= $xml[0]->Carrera['nombreCarrera'];
							$FechaInicio			= $xml[0]->Carrera['fechaInicio'];
							$FechaTerminacion		= $xml[0]->Carrera['fechaTerminacion'];
							$TipoAutorizacion		= $xml[0]->Carrera['autorizacionReconocimiento'];
							$NumeroReconocimiento	= $xml[0]->Carrera['numeroRvoe'];

							$NombreProfesionista	= $xml[0]->Profesionista['nombre'];
							$PrimerApellidoProf		= $xml[0]->Profesionista['primerApellido'];
							$SegundoApellidoProf	= $xml[0]->Profesionista['segundoApellido'];
							$CurpProfesionista		= $xml[0]->Profesionista['curp'];
							
							$FolioDigital			= $xml[0]->Autenticacion['folioDigital'];
							$SelloTitulo			= $xml[0]->Autenticacion['selloTitulo'];
							$SelloAutenticacion		= $xml[0]->Autenticacion['selloAutenticacion'];
							$NoCertAutoridad		= $xml[0]->Autenticacion['noCertificadoAutoridad'];
							$FechaAutenticacion		= $xml[0]->Autenticacion['fechaAutenticacion'];

							if(strlen($SelloAutenticacion) >= 300){
								if($FolioControl == $this->VpUnicoCert){
									$DocumentoAutenticado = true;
								}else{
									$DocumentoAutenticado = false;
									$NotificacionErrorDoc = "El Folio del Documento no Corresponde con el ID del Alumno";
								}
							}else{
								$DocumentoAutenticado = false;
								$NotificacionErrorDoc = "El Documento no ha sido Autenticado ".$SelloAutenticacion;
							}
						}else{
							$DocumentoAutenticado = false;
							$NotificacionErrorDoc = "T&iacute;tulo Electr&oacute;nico no encontrado";
						}

						if($DocumentoAutenticado){
							$this->result .= "
								<div style='width:100%; margin:auto; text-align:left; font-size:12px; line-height:1.5;'>
									<div class='row' style='width:100%; margin:auto; justify-content:center; margin-bottom:10px;'>
										<div class='col-md-4' style='width:300px; float:left;'>
											<img src='img/uin-logo.png' style='width:100%; margin-bottom:10px;'>
										</div>
										<div class='col-md-6' style='width:300px; float:right;'>
											<b>FOLIO:</b> ".$FolioDigital."<br />
											<b>FECHA DE EXPEDICIÓN DEL T&Iacute;TULO:</b> ".$FechaExpedicion."<br />
											<b>ENTIDAD FEDERATIVA:</b> ".$EntidadFederativa."<br />
										</div>
									</div>
									<div class='row' style='width:100%; margin-bottom:10px;'>
										<div class='col-md-3' style='width:150px; float:left;'>
											<div style='width:140px; height:150px; border: 4px dotted blue; text-align:center; vertical-align:middle;'><span style='position:relative; top:44%;'>FOTO</b></div>
										</div>
										<div class='col-md-8' style='width:450px; float:right; font-size:14px;'>
											<b>NOMBRE DE LA INSTITUCI&Oacute;N:</b><br />".$NombreInstitucion."
											<br /><br />
											<b>NOMBRE DEL PROFESIONISTA:</b> ".$NombreProfesionista." ".$PrimerApellidoProf." ".$SegundoApellidoProf."
											<br /><br />
											<b>CURP DEL PROFESIONISTA:</b> ".$CurpProfesionista."
											<br /><br />
										</div>
									</div>
									<div class='row' style='width:100%; margin-bottom:10px;'>
										<div class='col-md-12 col-sm-12'>
											<b>CURSO Y ACREDITACI&Oacute;N:</b> ".$CveCarrera." - ".$NombreCarrera."
											<div class='row'>
												<div class='col-md-6' style='width:250px; float:left;'>
													<b>FECHA DE INICIO</b> ".$FechaInicio."
												</div>
												<div class='col-md-6' style='width:250px; float:right;'>
													<b>FECHA DE TERMINACI&Oacute;N</b> ".$FechaTerminacion."
												</div>
											</div>
											<br />
											<b>FECHA DE EXAMEN PROFESIONAL O FECHA DE EXENCI&Oacute;N DE EXAMEN PROFESIONAL:</b> ".$FechaExProf." ".$FechaExcencionExProf."
											<div class='row'>
												<div class='col-md-6' style='width:250px; float:left;'>
													<b>TIPO DE AUTORIZACI&Oacute;N:</b> ".$TipoAutorizacion."
												</div>
												<div class='col-md-6' style='width:250px; float:right;'>
													<b>N&Uacute;MERO RECON. DE VALIDEZ OFICIAL</b> ".$NumeroReconocimiento."
												</div>
											</div>
										</div>
									</div>
									<div class='row' style='width:100%; margin-bottom:20px;'>
										<div class='col-md-4' style='width:200px; float:left; margin-top:90px;'>
											&nbsp;&nbsp;&nbsp;&nbsp;
											&nbsp;&nbsp;&nbsp;&nbsp;
											<b>SECRETARIO GENERAL</b><br />
											<div style='width:200px; height:200px; border: 4px dotted gray;'><img src='http://chart.googleapis.com/chart?chs=300x300&cht=qr&amp;chl=http://www.siged.sep.gob.mx/titulos/autenticacion/".$FolioDigital."' style='width:100%;' /></div>
										</div>";
										if(isset($GLOBALS["geCssStyle"])){
											$this->result .= "<div class='col-md-8' style='width:420px; margin-top:-90px; float:right;'>";
										}else{
											$this->result .= "<div class='col-md-8' style='width:420px; float:right;'>";
										}
										$this->result .= "
											<b>RECTORA:</b> ".$NombreResponsable." ".$PrimerApResponsable." ".$SegundoApResponsable."<br />
											<b>NO. CERTIFICADO FIRMANTE INSTITUCI&Oacute;N:</b> ".$NoCertResponsable."<br />
											<b>SELLO:</b><br />
											<span style='word-wrap:break-word; font-size:11px;'>".$SelloResponsable."</span><br />
											<b>SECRETARIO GENERAL:</b> ".$NombreSecretarioGral." ".$PrimerApSecretarioGral." ".$SegundoApSecGral."<br />
											<b>NO. CERTIFICADO FIRMANTE INSTITUCI&Oacute;N:</b> ".$NoCertSecretarioGral."<br />
											<b>SELLO:</b> <span style='word-wrap:break-word; font-size:11px;'>".$SelloSecretarioGral."</span><br />
										</div>
									</div>
									<div class='row' style='width:100%;'>
										<div class='col-md-12' style='width:100%; float:left;'>
											<b>SELLO DE AUTENTICACI&Oacute;N:</b><br />
											<span style='word-wrap:break-word; font-size:11px;'>".$SelloAutenticacion."</span><br />
											<b>NO. CERTIFICADO AUTORIDAD:</b> ".$NoCertAutoridad."<br />
											<b>SELLO T&Iacute;TULO:</b><br />
											<span style='word-wrap:break-word; font-size:11px;'>".$SelloTitulo."</span>
											<br /><br />
											<b>FECHA DE AUTENTICACI&Oacute;N:</b> <span style='word-wrap:break-word; font-size:11px;'>".$FechaAutenticacion."</span>
										</div>
									</div>
									<input type='hidden' id='AddComments' name='AddComments' value='".$thisAction."'>
								</div>
							";
						}else{
							$this->result .= "
								<div style='width:70%; margin:auto; text-align:center; line-height: 1.5;'>
									<img src='img/uin-logo.png' style='width:50%; margin-bottom:30px;'>
									<br /><br />
									<h5>".$NotificacionErrorDoc."</h5>
									<input type='hidden' id='AddComments' name='AddComments' value='".$thisAction."'>
								</div>
							";
						}
						break;
					case 'notificar-titulo':
						$this->result .= "
							<div class='row' style='text-align:center;'>
								<div class='col-md-12 col-sm-6'>
									<div class='card-body' style='vertical-align:middle;'>
										<img src='img/uin-logo.png' style='width:50%; margin-bottom:30px;'>
										<br /><br />
										<div id='NewComment' style='text-align:left;'>
											<h5>Ingrese los Detalles de la Entrega del T&iacute;tulo en F&iacute;sico:</h5>:
											<textarea id='AddComments' name='AddComments' class='form-control' rows='5' placeholder='Ingrese los datos de la entrega del documento' required></textarea>
										</div>
										<br /><br />
										<span id='PrintMsg'>SE NOTIFICARA AL ALUMNO QUE SU TITULO FISICO SE ENCUENTRA DISPONIBLE</b>
									</div>
								</div>
							</div>
						";
						break;
					case 'valida-carta-aceptacion':
						$DocPath		= "http://www.portaluin.mx";
						//$DocPath 		= "http://testportaluin.dnsalias.net";
						$RutaDoc 		= self::getDbRowName("RUTA_DOCUMENTO", "ACC_ALUMNO_DOCUMENTOS_SP", "WHERE ID_DOCUMENTO = 7 AND PEOPLE_CODE_ID = 'P".$SPData_Matricula."'", 1);
						$NombreDoc 		= self::getDbRowName("NOMBRE_DOCUMENTO", "ACC_ALUMNO_DOCUMENTOS_SP", "WHERE ID_DOCUMENTO = 7 AND PEOPLE_CODE_ID = 'P".$SPData_Matricula."'", 1);
						$TipoCurriculum = self::getDbRowName("CURRICULUM", "ACC_STDDEGREQ_SS", "WHERE PEOPLE_CODE_ID = 'P".$SPData_Matricula."'", 1);
						$RutaDoc 		= str_replace("\\", "/", $RutaDoc);
						switch ($GLOBALS['thisSPId']) {
							case 1:
								$ConteoTiempo = 6;
								break;
							case 2:
								switch ($TipoCurriculum) {
									case 15:
									case 18:
										$ConteoTiempo = 1;
										break;
									default:
										$ConteoTiempo = 4;
										break;
								}
								break;
						}
						$this->result .= "
							<div class='row' style='text-align:center;'>
								<div class='col-md-12 col-sm-6'>
									<div class='card-body' style='vertical-align:middle;'>
										<img src='img/uin-logo.png' style='width:50%; margin-bottom:30px;'>
										<br /><br />
										<h5>Revise detenidamente el Documento para proceder a su Validaci&oacute;n:</h5>:
										<iframe width='100%' height='800px;' src='".$DocPath."/".$RutaDoc."/".$NombreDoc."'></iframe>
										<br /><br />
										<div id='RegDate' style='background-color:#ffffff; padding:20px;'>
											<h5>Ingrese la Fecha de Inicio del Proceso:</h5>:
											<script src='../js/date-time-picker.min.js'></script>
											<div class='row'>
												<div class='col-sm-3'>&nbsp;</div>
												<div class='col-sm-3'>
												<span class='navbar-text'><b>Fecha de Inicio:</b></span>
													<input id='start_date' name='start_date' class='form-control' value='' ConteoTiempo='".$ConteoTiempo."' placeholder='Ingrese la Fecha de Inicio' required />
												</div>
												<div class='col-sm-3'>
													<span class='navbar-text'><b>Fecha Final Aproximada:</b></span>
													<input id='end_date' name='end_date' class='form-control' value='' ConteoTiempo='".$ConteoTiempo."' placeholder='Ajuste Automático' disabled>
												</div>
											</div>
										</div>
										<br /><br />
										<h5>Ingrese sus Comentarios u Observaciones:</h5>:
										<div id='NewComment' style='text-align:left;'>
											<textarea id='AddComments' name='AddComments' class='form-control' rows='5' placeholder='Ingrese los Comentarios que determinen el resultado de la Validaci&oacute;n' required /></textarea>
										</div>
									</div>
								</div>
							</div>
						";
						break;
				}
				return $this->result;
			}

			public function ListadoConvenios($thisType, $thisFilter){
				self::SP_Conn();
				self::xQueryI("SELECT * FROM ACC_CAT_CONVENIO_SP ".$thisFilter);
					$this->result = "";	
					while ($this->data = sqlsrv_fetch_array($this->rQuery)) {
						switch ($thisType) {
							case 'convenios-autorizados':
								$ThisBtn = "<a href='?ty=convenios&SecId=editar-convenio&thisid=".$this->data["ID"]."'><span id='ver_convenio_".$this->data["ID"]."' class='btn btn-primary btn-sm' style='padding:5px;'><i class='fa fa-edit'></i> Editar Convenio</span></a> <a href='#'><span id='autorizar_convenio_".$this->data["ID"]."' class='btn btn-danger btn-sm ValidaRegistro' Section='desactiva-convenio' RegId='".$this->data["ID"]."' style='padding:5px;'><i class='fa fa-times'></i> Desactivar</span></a>";
								break;
							case 'convenios-proceso':
								$ThisBtn = "<a href='?ty=convenios&SecId=editar-convenio&thisid=".$this->data["ID"]."'><span id='ver_convenio_".$this->data["ID"]."' class='btn btn-primary btn-sm' style='padding:5px;'><i class='fa fa-edit'></i> Editar Convenio</span></a> <a href='#'><span id='autorizar_convenio_".$this->data["ID"]."' class='btn btn-success btn-sm ValidaRegistro' Section='autoriza-convenio' RegId='".$this->data["ID"]."' style='padding:5px;'><i class='fa fa-check'></i> Autorizar</span></a>";
								break;
						}
						$this->result .= "
							<tr id='Registro_".$this->data["ID"]."'>
								<td>".$this->data["ID"]."</td>
								<td>".utf8_encode($this->data["NOMBRE_EMPRESA"])."</td>
								<td>".utf8_encode($this->data["DIRECCION"])."</td>
								<td>".$this->data["TELEFONO"]."</td>
								<td>".$this->data["SECTOR"]."</td>
								<td>".$this->data["CODIGO_POSTAL"]."</td>
								<td style='width:280px;'>".$ThisBtn."</td>
							</tr>
						";
					}
				return $this->result;
			}

			public function FormularioConvenio($thisFilter){
				self::SP_Conn();
				self::xQueryI("SELECT * FROM ACC_CAT_CONVENIO_SP ".$thisFilter);
					if ($this->data = sqlsrv_fetch_array($this->rQuery)) {
						$ThisSection		= "editar-convenio";
						$BtnText			= "Actualizar Convenio";
						$IdConvenio 		= $this->data["ID"];
						$NombreEmpresa 		= utf8_encode($this->data["NOMBRE_EMPRESA"]);
						$Responsable 		= utf8_encode($this->data["REPONSABLE"]);
						$CargoResponsable 	= utf8_encode($this->data["CARGO_RESPONSABLE"]);
						$ResponsableLegal 	= utf8_encode($this->data["RESPONSABLE_LEGAL"]);
						$ResponsableSS 		= utf8_encode($this->data["RESPONSABLE_SERVICIO_SOCIAL"]);
						$Direccion 			= utf8_encode($this->data["DIRECCION"]);
						$Colonia 			= utf8_encode($this->data["COLONIA"]);
						$Municipio			= utf8_encode($this->data["MUNICIPIO"]);
						$EntidadFederativa	= utf8_encode($this->data["ID_ENTIDAD_FEDERATIVA"]);
						$CP 				= $this->data["CODIGO_POSTAL"];
						$Sector				= $this->data["SECTOR"];
						$Telefono			= $this->data["TELEFONO"];
						$Extencion			= $this->data["EXTENCION"];
						$Horario			= $this->data["HORARIO_SERVICIO"];
						$TipoPrestacion		= $this->data["CAT_TIPO_PRESTACION"];
					}else{
						$ThisSection		= "agregar-convenio";
						$BtnText			= "Crear Convenio";
						$IdConvenio 		= "";
						$NombreEmpresa 		= "";
						$Responsable 		= "";
						$CargoResponsable 	= "";
						$ResponsableLegal	= "";
						$ResponsableSS		= "";
						$Direccion 			= "";
						$Colonia 			= "";
						$Municipio			= "";
						$EntidadFederativa	= "";
						$CP 				= "";
						$Sector				= "";
						$Telefono			= "";
						$Extencion			= "";
						$Horario			= "";
						$TipoPrestacion		= "";
					}
					echo "
						<div id='ThisRequest' class='container-fluid bg-light py-3'>
							<form id='GlobalForm' type='".$ThisSection."'>
								<div class='messages'></div>
								<div class='controls'>
									<div class='row'>
										<div class='col-sm-4'>
											<div class='form-group' style='text-align:left;'>
												<label for='NOMBRE_EMPRESA'>Nombre de la Empresa</label>
												<input id='NOMBRE_EMPRESA' type='text' name='NOMBRE_EMPRESA' value='".$NombreEmpresa."' class='form-control' placeholder='Ingrese el Nombre o Raz&oacute;n Social de la Empresa' required='required' data-error=''>
												<input type='hidden' id='CONVENIO_ID' name='CONVENIO_ID' value='".$IdConvenio."'>
												<div class='help-block with-errors'></div>
											</div>
										</div>
										<div class='col-sm-4'>
											<div class='form-group' style='text-align:left;'>
												<label for='REPONSABLE'>Persona Responsable</label>
												<input id='REPONSABLE' type='text' name='REPONSABLE' value='".$Responsable."' class='form-control' placeholder='Ingrese el Nombre del Responsable' required='required' data-error=''>
												<div class='help-block with-errors'></div>
											</div>
										</div>
										<div class='col-sm-4'>
											<div class='form-group' style='text-align:left;'>
												<label for='CARGO_RESPONSABLE'>Cargo</label>
												<input id='CARGO_RESPONSABLE' type='text' name='CARGO_RESPONSABLE' value='".$CargoResponsable."' class='form-control' placeholder='Ingrese el Cargo de la Persona Responsable' required='required' data-error=''>
												<div class='help-block with-errors'></div>
											</div>
										</div>
									</div>
								</div>
								<div class='clearfix'></div>
								<div class='controls'>
									<div class='row'>
										<div class='col-sm-6'>
											<div class='form-group' style='text-align:left;'>
												<label for='RESPONSABLE_LEGAL'>Responsable Legal</label>
												<input id='RESPONSABLE_LEGAL' type='text' name='RESPONSABLE_LEGAL' value='".$ResponsableLegal."' class='form-control' placeholder='Ingrese la Direcci&oacute;n de la Empresa' required='required' data-error=''>
												<div class='help-block with-errors'></div>
											</div>
										</div>
										<div class='col-sm-6'>
											<div class='form-group' style='text-align:left;'>
												<label for='RESPONSABLE_SERVICIO_SOCIAL'>Responsable del Programa de Servicio Social</label>
												<input id='RESPONSABLE_SERVICIO_SOCIAL' type='text' name='RESPONSABLE_SERVICIO_SOCIAL' value='".$ResponsableSS."' class='form-control' placeholder='Ingrese la Colonia de la Empresa' required='required' data-error=''>
												<div class='help-block with-errors'></div>
											</div>
										</div>
									</div>
								</div>
								<div class='clearfix'></div>
								<div class='controls'>
									<div class='row'>
										<div class='col-sm-8'>
											<div class='form-group' style='text-align:left;'>
												<label for='DIRECCION'>Direcci&oacute;n de la Empresa</label>
												<input id='DIRECCION' type='text' name='DIRECCION' value='".$Direccion."' class='form-control' placeholder='Ingrese la Direcci&oacute;n de la Empresa' required='required' data-error=''>
												<div class='help-block with-errors'></div>
											</div>
										</div>
										<div class='col-sm-4'>
											<div class='form-group' style='text-align:left;'>
												<label for='COLONIA'>Colonia</label>
												<input id='COLONIA' type='text' name='COLONIA' value='".$Colonia."' class='form-control' placeholder='Ingrese la Colonia de la Empresa' required='required' data-error=''>
												<div class='help-block with-errors'></div>
											</div>
										</div>
									</div>
								</div>
								<div class='clearfix'></div>
								<div class='controls'>
									<div class='row'>
										<div class='col-sm-4'>
											<div class='form-group' style='text-align:left;'>
												<label for='MUNICIPIO'>Municipio</label>
												<input id='MUNICIPIO' type='text' name='MUNICIPIO' value='".$Municipio."' class='form-control' placeholder='Ingrese la Direcci&oacute;n de la Empresa' required='required' data-error=''>
												<div class='help-block with-errors'></div>
											</div>
										</div>
										<div class='col-sm-4'>
											<div class='form-group' style='text-align:left;'>
												<label for='ENTIDAD_FEDERATIVA'>Entidad Federativa</label>
												<input id='ENTIDAD_FEDERATIVA' type='text' name='ENTIDAD_FEDERATIVA' value='".$EntidadFederativa."' class='form-control' placeholder='Ingrese la Entidad Federativa' required='required' data-error=''>
												<div class='help-block with-errors'></div>
											</div>
										</div>
										<div class='col-sm-2'>
											<div class='form-group' style='text-align:left;'>
												<label for='CODIGO_POSTAL'>C&oacute;digo Postal</label>
												<input id='CODIGO_POSTAL' type='number' name='CODIGO_POSTAL' value='".$CP."' class='form-control' placeholder='' required='required' data-error=''>
												<div class='help-block with-errors'></div>
											</div>
										</div>
										<div class='col-sm-2'>
											<div class='form-group' style='text-align:left;'>
												<label for='SECTOR'>Sector</label>";
												if($Sector === "PUBLICO"){$thisChecked_1 = "Selected";}else{$thisChecked_1 = "";}
												if($Sector === "PRIVADO"){$thisChecked_2 = "Selected";}else{$thisChecked_2 = "";}
												echo "
												<select class='form-control' id='SECTOR' name='SECTOR'>
													<option value='PUBLICO' ".$thisChecked_1.">P&uacute;blico</option>
													<option value='PRIVADO' ".$thisChecked_2.">Privado</option>
												</select>
												<div class='help-block with-errors'></div>
											</div>
										</div>
									</div>
								</div>
								<div class='clearfix'></div>
								<div class='controls'>
									<div class='row'>
										<div class='col-sm-3'>
											<div class='form-group' style='text-align:left;'>
												<label for='TELEFONO'>Tel&eacute;fono</label>
												<input id='TELEFONO' type='number' name='TELEFONO' value='".$Telefono."' class='form-control' placeholder='Ingrese el Tel&eacute;fono' required='required' data-error=''>
												<div class='help-block with-errors'></div>
											</div>
										</div>
										<div class='col-sm-2'>
											<div class='form-group' style='text-align:left;'>
												<label for='EXTENCION'>Extensi&oacute;n</label>
												<input id='EXTENCION' type='number' name='EXTENCION' value='".$Extencion."' class='form-control' placeholder='' required='required' data-error=''>
												<div class='help-block with-errors'></div>
											</div>
										</div>
										<div class='col-sm-4'>
											<div class='form-group' style='text-align:left;'>
												<label for='HORARIO_SERVICIO'>Horario de Servicio</label>
												<input id='HORARIO_SERVICIO' type='text' name='HORARIO_SERVICIO' value='".$Horario."' class='form-control' placeholder='' required='required' data-error=''>
												<div class='help-block with-errors'></div>
											</div>
										</div>
										<div class='col-sm-3'>
											<div class='form-group' style='text-align:left;'>
												<label for='CAT_TIPO_PRESTACION'>Tipo de Prestaci&oacute;n</label>
												<select class='form-control' id='CAT_TIPO_PRESTACION' name='CAT_TIPO_PRESTACION'>
													<option value=''>Seleccionar</option>";
													self::xQueryI("SELECT * FROM ACC_CAT_TIPO_PRESTACION_SS");
													while ($this->data = sqlsrv_fetch_array($this->rQuery)) {
														if($TipoPrestacion == $this->data["ID"]){
															$ThisChecked = "Selected";
														}else{
															$ThisChecked = "";
														}
														echo "
															<option value='".$this->data["ID"]."' ".$ThisChecked.">".utf8_encode($this->data["DESCRIPCION_TIPO_AYUDA"])."</option>
														";
													}
													echo "
												</select>
												<div class='help-block with-errors'></div>
											</div>
										</div>
									</div>
								</div>
								<div class='clearfix'></div>
								<br /><br />
								<div class='row'>
									<div class='col-md-12'>
										<button type='submit' class='btn btn-primary'><i class='fa fa-save'></i> ".$BtnText."</button>
									</div>
								</div>
							</form>
						</div>
						<div id='MsgResult' class='row' style='display:none; margin-top:50px; text-align:center;'>
							<div class='col-md-12 col-sm-6'>
								<div class='card' style='height:300px; background-color:#f1f2f3;'>
									<div class='card-body'>
										<span id='PrintMsg'></span>
									</div>
								</div>
							</div>
						</div>
					";
			}

			public function getThisRcd($thisRow, $thisTable, $thisFilter){
				$sql 		= "SELECT ".$thisRow." AS RowName FROM ".$thisTable." ".$thisFilter;
				$res		= sqlsrv_query($this->cnn, $sql);
				$thisData 	= sqlsrv_fetch_array($res);
		        $GetRowName = utf8_encode($thisData["RowName"]);
				return $GetRowName;
				sqlsrv_close($this->cnn);
			}
			
			public function getDbRowName($thisRow, $thisTable, $thisFilter, $displayError){
				$sql 		= "SELECT ".$thisRow." AS RowName FROM ".$thisTable." ".$thisFilter;
				$res 		= sqlsrv_query($this->cnn, $sql);
				$thisData 	= sqlsrv_fetch_array($res);
		        $GetRowName = $thisData["RowName"];
		        if($displayError == 1){
		        	if(strlen($GetRowName) == 0){$GetRowName = "error";}
		        }
				return $GetRowName;
				sqlsrv_close($this->cnn);
			}

			public function getTotalRcdsCount($thisTable, $thisFilter){
				$sql 		= "SELECT * FROM ".$thisTable." ".$thisFilter;
				$res 		= sqlsrv_query($this->cnn, $sql);
				$TotalFiles = 0;
				while ($data = sqlsrv_fetch_array($res)) {
					$TotalFiles = $TotalFiles + 1;
				}
				return $TotalFiles;
				sqlsrv_close($this->cnn);
			}
		
			public function globalStatusUpdate($thisTable, $ProgressId, $PeopleCodeId, $TeacherId, $RegId, $RegName, $NewStatus, $thisComments, $thisSPId, $thisFilter){
				switch ($NewStatus) {
					case 17: $DocStatus = 5; break;
					case 18: $DocStatus = 6; break;
					case 27: $DocStatus = 10; break;
					case 28: $DocStatus = 11; break;
					case 31: $DocStatus = 14; break;
					case 50: $DocStatus = 31; break;
					case 51: $DocStatus = 32; break;
				}
				switch ($NewStatus) {
					case 11:
						$ThisEstatusIdSS = $NewStatus;
						$thisComments = "Se ha comenzado el Proceso de Revisi&oacute;n";
						break;
					case 27:
					case 31:
						$ThisEstatusIdSS = 20;
						break;
					default:
						$ThisEstatusIdSS = $NewStatus;
						break;
				}
				
				if(isset($DocStatus)){
					$ThisPeopleCodeId = $RegId;
				}else{
					$ThisPeopleCodeId = $PeopleCodeId;
				}
				
				$ThisResult 	= " | 1 | ";
				$thisComments	= self::sanear_Acents($thisComments);
				$ThisUpdate		= "UPDATE ".$thisTable." SET ESTATUS_ID = ".$ThisEstatusIdSS." ".$thisFilter.";";
				$ThisUpdate		.= "INSERT INTO ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP (Acc_Progress_Status_Id, ACC_STDDEGREQ_SS_ID, SERVICIO_PROFESIONAL_ID, REG_TIPO, TEACHER_ID, ESTATUS_ID, COMENTARIOS, FECHA_HISTORICO) VALUES (".$ProgressId.",".$ThisPeopleCodeId.",".$thisSPId.",1,'".$TeacherId."',".$NewStatus.",'".$thisComments."','".date('Y-d-m H:i:s.000')."');"; 
				
				switch ($ThisEstatusIdSS) {
					case 20:
						$ThisUpdate .= "UPDATE ACC_Progress_Status_SP SET Release = 1 WHERE Acc_Progress_Status_Id = ".$ProgressId.";";
						switch ($thisSPId) {
							case 2:
								self::SP_Conn();
								$CurrentHours 	= self::getDbRowName("Horas", "ACC_Horas_SP", "WHERE ACC_STDDEGREQ_SS_ID = ".$RegId, 1);
								$NewHours 		= self::getDbRowName("HORAS", "ACC_PRACTICAS_PROFESIONALES_SP", "WHERE Acc_Progress_Status_Id = ".$ProgressId, 1);
								if($CurrentHours <> "error"){
									$UpdateHours = $CurrentHours + $NewHours;
								}else{
									$UpdateHours = $NewHours;
								}
								if($UpdateHours >= 1400){
									if($NewStatus == 27){
										$ThisResult = " | 3 | ";
									}
								}
								if($NewStatus <> 31){
									$ThisUpdate .= "UPDATE ACC_Horas_SP SET ACC_STDDEGREQ_SS_ID = ".$RegId.", Horas = ".$UpdateHours.", Status = 1 WHERE ACC_STDDEGREQ_SS_ID = ".$RegId." IF @@ROWCOUNT=0 INSERT INTO ACC_Horas_SP (ACC_STDDEGREQ_SS_ID, Horas, Status) VALUES (".$RegId.",".$UpdateHours.",1);";
								}
								break;
						}
						break;
				}
				if(isset($DocStatus)){
					$ThisUpdate .= "INSERT INTO ACC_ALUMNO_DOCUMENTOS_SP (Acc_Progress_Status_Id, PEOPLE_CODE_ID, ID_DOCUMENTO, SERVICIO_PROFESIONAL_ID, RUTA_DOCUMENTO, NOMBRE_DOCUMENTO, ESTATUS_ID) VALUES (".$ProgressId.",'".$PeopleCodeId."',".$DocStatus.",".$thisSPId.",'servicios-profesionales/docs/".$PeopleCodeId."','".$RegName."',".$NewStatus.");";
				}
				if(self::xQueryI($ThisUpdate)){
					$StudentUsrId	= str_replace("P","",$PeopleCodeId);
					$StudentEmail 	= self::getThisRcd("UserName", "AspNetUsers", "WHERE Student = '".$StudentUsrId."'");
					$StatusName		= self::getThisRcd("DESCRIPCION_ESTATUS", "ACC_CAT_ESTATUS_SPR_SP", "WHERE ESTATUS_ID = ".$NewStatus);
					$MatricSession	= self::getThisRcd("MATRIC_SESSION", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$ProgressId);
					sendMailData($thisSPId, $StudentEmail, $StatusName, $thisComments, $MatricSession);
					echo $ThisResult;
				}else{
					echo "Ha ocurrido un error interno, favor de intentarlo nuevamente";
				}
				switch ($thisTable) {
					case 'ACC_CAMBIO_DATOS_SPR_SP':
						if($NewStatus == 3){
							$this->PeopleCodeId = self::getThisRcd("PEOPLE_CODE_ID", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$PeopleCodeId);
							$this->UpdateSP = self::updateSPData($this->PeopleCodeId, $thisSPId);	
						}
						break;
				}
			}

			public function updateSPData($ThisStudentId, $thisSPId){
				self::SP_Conn();
				self::xQueryI("SELECT * FROM ACC_STDDEGREQ_SS WHERE PEOPLE_CODE_ID = '".$ThisStudentId."'");
				if ($this->SP_Data = sqlsrv_fetch_array($this->rQuery)) {
					$ThisUpdate = "EXEC ACC_serviciosocialdetalle_SS @people_code_id='".$ThisStudentId."', @MATRIC_YEAR='".$this->SP_Data["MATRIC_YEAR"]."', @MATRIC_TERM='".$this->SP_Data["MATRIC_TERM"]."', @RECORD_TYPE='".$this->SP_Data["RECORD_TYPE"]."', @PROGRAM='".$this->SP_Data["PROGRAM"]."', @DEGREE='".$this->SP_Data["DEGREE"]."', @CURRICULUM='".$this->SP_Data["CURRICULUM"]."', @MatricSession='".$this->SP_Data["MATRIC_SESSION"]."', @Servicio_profesional_ID='".$thisSPId."', @TIPO=2";
					self::xQueryI($ThisUpdate);
				}
			}

			public function globalDataInsert($thisTable, $ThisRows, $ThisValues){
				$ThisUpdate = "INSERT INTO ".$thisTable." (".$ThisRows.") VALUES (".$ThisValues.");";
				if(self::xQueryI($ThisUpdate)){
					echo " | 1 | ";
				}else{
					echo "Ha ocurrido un error interno, favor de intentarlo nuevamente";
				}
			}

			public function cancelService($thisProgressId){
				$ThisUpdate = "UPDATE ACC_SERVICIOS_PROFESIONALES_SP SET ESTATUS_ID=21;";
				$ThisUpdate .= "UPDATE Acc_Progress_Status_SP SET ESTATUS_ID=21;";
				if(self::xQueryI($ThisUpdate)){
					echo " | 1 | ";
				}else{
					echo "Ha ocurrido un error interno, favor de intentarlo nuevamente";
				}
			}

			public function globalDataUpdate($thisTable, $ThisRows, $thisFilter){
				$ThisUpdate = "UPDATE ".$thisTable." SET ".$ThisRows." ".$thisFilter.";";
				if(self::xQueryI($ThisUpdate)){
					echo " | 1 | ";
				}else{
					echo "Ha ocurrido un error interno, favor de intentarlo nuevamente";
				}
			}

			public function globalFileUpdate($thisTable, $ProgressId, $PeopleCodeId, $TeacherId, $RegId, $RegName, $NewStatus, $thisComments, $thisSPId, $thisDateIni, $thisDateEnd, $ThisRows, $thisFilter){
				$thisComments 	= self::utf8_decode($thisComments);
				$ThisUpdate 	= "UPDATE ".$thisTable." SET ".$ThisRows." ".$thisFilter.";";
				$ThisUpdate		.= "INSERT INTO ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP (Acc_Progress_Status_Id, ACC_STDDEGREQ_SS_ID, SERVICIO_PROFESIONAL_ID, REG_TIPO, TEACHER_ID, ESTATUS_ID, COMENTARIOS, FECHA_HISTORICO) VALUES (".$ProgressId.",".$PeopleCodeId.",".$thisSPId.",1,'".$TeacherId."',".$NewStatus.",'".$thisComments."','".date('Y-d-m H:i:s.000')."');"; 
				switch ($RegId) {
					case 7:
						$SetServiceDate = true;
						break;
					default:
						$SetServiceDate = false;
						break;
				}
				if($SetServiceDate){
					$thisDateIni = date('Y-d-m', strtotime($thisDateIni))." 00:00:00.000";
					$thisDateEnd = date('Y-d-m', strtotime($thisDateEnd))." 00:00:00.000";
					$ThisUpdate .= "UPDATE ACC_SERVICIOS_PROFESIONALES_SP SET FECHA_INICIO='".$thisDateIni."', FECHA_TERMINO='".$thisDateEnd."' WHERE Acc_Progress_Status_Id = ".$ProgressId;
				}
				if(self::xQueryI($ThisUpdate)){
					switch ($RegId) {
						case 98:
						case 24:
						case 27:
						case 30:
						case 7:
							$SendMail = true;
							break;
						default:
							$SendMail = false;
							break;
					}
					if($SendMail){
						$StudentUsrId	= str_replace("P","",$PeopleCodeId);
						$StudentEmail 	= self::getThisRcd("UserName", "AspNetUsers", "WHERE Student = '".$StudentUsrId."'");
						$StatusName		= self::getThisRcd("DESCRIPCION_ESTATUS", "ACC_CAT_ESTATUS_SPR_SP", "WHERE ESTATUS_ID = ".$NewStatus);
						$MatricSession	= self::getThisRcd("MATRIC_SESSION", "ACC_STDDEGREQ_SS", "WHERE ACC_STDDEGREQ_SS_ID = ".$ProgressId);
						sendMailData($thisSPId, $StudentEmail, $StatusName, "Tus Documentos est&aacute;n siendo validados.<br />Favor de estar al Pendiente para poder continuar con tu Proceso", $MatricSession);
					}
					echo " | 1 | ";
				}else{
					echo "Ha ocurrido un error interno, favor de intentarlo nuevamente";
				}
			}
			
			public function calculaFechas($thisIni, $thisEnd, $thisCarrer){
				$thisIni 		= new DateTime($thisIni);
				$thisEnd 		= new DateTime($thisEnd);
				$intervalo 		= $thisEnd->diff($thisIni);
				$thisResult		= $intervalo->format('%Y Años, %m Meses, %d d&iacute;as');
				$thisResult 	= str_replace("00 Años, ", "", $thisResult);
				$thisResult 	= str_replace("0 Meses, ", "", $thisResult);
				
				$totalYears		= $intervalo->format('%Y');
				$totalMonths	= $intervalo->format('%m');
				$totalDays		= $intervalo->format('%d');

				switch ($GLOBALS["thisSPId"]) {
					case 1:
						$TotalTime = 6;
						break;
					case 2: 
						switch ($thisCarrer) {
							case 15: //Administración de Empresas Turísticas
							case 28: //Gastronomía
								$TotalTime = 1; 
								break;
							default:
								$TotalTime = 3; 
								break;
						}
						break;
				}
				
				if($totalYears == 00 AND $totalMonths >= $TotalTime){
					$thisResult = "Concluido";
				}else if($totalYears <> 00){
					$thisResult = "Concluido";
				}

				return $thisResult;
			}

			function monthName($thisMonth, $thisType){
				setlocale(LC_TIME, 'spanish');
				$nombreMes  = strftime("%B",mktime(0, 0, 0, $thisMonth, 1, 2000));
				$nombreMes  = ucwords($nombreMes);
				switch ($thisType) {
					case 0:
						$nombreMes = substr(strtoupper($nombreMes), 0, 3);
						break;
					case 2:
						$nombreMes = $nombreMes;
						break;
					default:
						$nombreMes = strtoupper($nombreMes);
						break;
				}
				return $nombreMes;
			}

			function formatDate($thisDate){
				setlocale(LC_TIME, 'spanish');
				$thisYear			= substr($thisDate,0, 4);
				$thisMonth 			= self::monthName(substr($thisDate,5, 2), 0);
				$thisDay			= substr($thisDate,8, 2);
				$FrtDate			= $thisDay."/".$thisMonth."/".$thisYear;
				return $FrtDate;
			}
	
			function sanear_Acents($string){
				$this->string = trim($string);
				$this->string = str_replace(
					array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
					array('&aacute;', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
					$this->string
				);
				$this->string = str_replace(
					array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
					array('&eacute;', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
					$this->string
				);
				$this->string = str_replace(
					array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
					array('&iacute;', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
					$this->string
				);
				$this->string = str_replace(
					array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
					array('&oacute;', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
					$this->string
				);
				$this->string = str_replace(
					array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
					array('&uacute;', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
					$this->string
				);
				$this->string = str_replace(
					array('ç', 'Ç'),
					array('c', 'C'),
					$this->string
				);
				return $this->string;
			}
		// ****************************************************************************************
	}
	
    function InfoAlumno($thisId){
        $obj = new ServProf;
        echo "
            <div class='info' style='padding:0px; text-align:left;'>
                ".$obj->HeaderAlumno("DATOS DEL ALUMNO", $thisId)."
                <div id='content' class='content'>
                    <p>Seleccione desde las pestañas de navegaci&oacute;n la opci&oacute;n necesaria para validar el registro:
                </div>
            </div>
            <nav>
                <div class='nav nav-tabs' id='nav-tab' role='tablist'>
                    <a class='nav-item nav-link active' id='nav-historico-tab' data-toggle='tab' href='#nav-historico' role='tab' aria-controls='nav-historico' aria-selected='true'>Hist&oacute;rico</a>
					<a class='nav-item nav-link' id='nav-info-tab' data-toggle='tab' href='#nav-info' role='tab' aria-controls='nav-info' aria-selected='false'>Informaci&oacute;n</a>";
					if($GLOBALS["thisSPId"] == 2){
						echo "<a class='nav-item nav-link' id='nav-practicas-tab' data-toggle='tab' href='#nav-practicas' role='tab' aria-controls='nav-practicas' aria-selected='false'>Pr&aacute;cticas</a>";
					}
					echo "
                    <a class='nav-item nav-link' id='nav-documentos-oficiales-tab' data-toggle='tab' href='#nav-documentos-oficiales' role='tab' aria-controls='nav-documentos-oficiales' aria-selected='false'>Documentos Oficiales</a>
                    <a class='nav-item nav-link' id='nav-documentos-academicos-tab' data-toggle='tab' href='#nav-documentos-academicos' role='tab' aria-controls='nav-documentos-academicos' aria-selected='false'>Documentos Acad&eacute;micos</a>
                </div>
            </nav>
            <div class='tab-content' id='nav-tabContent'>
                <div class='tab-pane fade show active' id='nav-historico' role='tabpanel' aria-labelledby='nav-historico-tab'>
                    ".$obj->StatusHistorico($thisId, $GLOBALS["thisSPId"])."
                </div>
                <div class='tab-pane fade' id='nav-info' role='tabpanel' aria-labelledby='nav-info-tab'>
                    ".$obj->DatosAlumno($thisId)."
                </div>";
				if($GLOBALS["thisSPId"] == 2){
					echo "
						<div class='tab-pane fade' id='nav-practicas' role='tabpanel' style='text-align:left;' aria-labelledby='nav-practicas-tab'>
                    		".$obj->DatosPracticas($thisId)."
						</div>
					";
				}
				echo "
                <div class='tab-pane fade' id='nav-documentos-oficiales' role='tabpanel' style='text-align:left;' aria-labelledby='nav-documentos-oficiales-tab'>
                    <br /><br />
                    <table id='TablaDocumentos' class='table table-rounded table-striped table-sm' cellspacing='0'>
                        <tr class='bg-primary text-white' style='text-align:center;'>
                            <th>#</th>
                            <th class=''>Nombre del Archivo</th>
                            <th class=''>Estatus</th>
                            <th class=''>Acciones</th>
                        </tr>
                        ".$obj->ListadoDocumentos("ACC_Documentos_Generales", "", $thisId, "Generales", 0)."
					</table>
                </div>
                <div class='tab-pane fade' id='nav-documentos-academicos' role='tabpanel' style='text-align:left;' aria-labelledby='nav-documentos-academicos-tab'>
                    <br /><br />
                    <table id='TablaDocumentos' class='table table-rounded table-striped table-sm' cellspacing='0'>
                        <tr class='bg-primary text-white' style='text-align:center;'>
                            <th>#</th>
                            <th class=''>Nombre del Archivo</th>
                            <th class=''>Estatus</th>
                            <th class=''>Acciones</th>
                        </tr>
                        ".$obj->ListadoDocumentos("ACC_ALUMNO_DOCUMENTOS_SP", "AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"], $thisId, "Academicos", 0)."
					</table>
                </div>
            </div>
        ";
    }

    function ValidaDocumentos($thisId){ 
        $obj = new ServProf;
        echo "
            <div class='info' style='padding:0px; text-align:left;'>
                ".$obj->HeaderAlumno("DATOS DEL ALUMNO", $thisId)."
                <div id='content' class='content'>
                    <p>Seleccione desde las pestañas de Navegaci&oacute;n la opci&oacute;n necesaria para validar el registro:
                </div>
            </div>
            <table id='TablaDocumentos' class='table table-rounded table-striped table-sm' cellspacing='0'>
                <tr class='bg-primary text-white' style='text-align:center;'>
                    <th>#</th>
                    <th>Nombre del Archivo</th>
                    <th>Estatus</th>
                    <th>Acciones</th>
                </tr>
                ".$obj->ListadoDocumentos("ACC_Documentos_Generales", "AND ESTATUS_ID IN(10,11,25,26)", $thisId, "Generales", 1)."
                ".$obj->ListadoDocumentos("ACC_ALUMNO_DOCUMENTOS_SP", "AND ESTATUS_ID IN(10,11,25,26) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"], $thisId, "Academicos", 1)."
            </table>
        ";
    }

    function sendMailData($thisSpType, $sendTo, $thisSubject, $thisMsg, $thisMatricSession){
		require_once("php-mailer/class.phpmailer.php");
		require_once("php-mailer/class.smtp.php");
	
		$nombre         = "Servicios Escolares";
		$thisSubject	= "Universidad Insurgentes - ".$thisSubject;
		$telefono       = "555-55-55";
		$MsgHTML 		= nl2br($thisMsg);

		switch ($thisSpType) {
			case 1: $SectionName = "Servicio Social"; break;
			case 2: $SectionName = "Pr&aacute;cticas Profesionales"; break;
			case 3: $SectionName = "Titulaci&oacute;n"; break;
			case 4: $SectionName = "Convenio"; break;
		}

		$mail = new PHPMailer();

		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->Port 	= 587; 

		$mail->IsHTML(true); 
		$mail->CharSet = "utf-8";

		/*
			$mail->Host 	= "mail.accessmedia.com.mx";
			$mail->Username = "envios@accessmedia.com.mx";
			$mail->Password = "RL9W7a4W4Nvi";
		*/

		$mail->Host = "smtp.gmail.com";
		switch ($thisMatricSession){
			case 'TLALPAN':
				$mail->Username = "noreply.tlalpan@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'VIADUCTO':
				$mail->Username = "noreply.viaducto@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'XOLA':
				$mail->Username = "noreply.xola@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'NORTE':
				$mail->Username = "noreply.norte@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'CHALCO':
				$mail->Username = "noreply.chalco@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'ECATEPEC':
				$mail->Username = "noreply.ecatepec@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'TOLUCA':
				$mail->Username = "noreply.toluca@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'TOREO':
				$mail->Username = "noreply.toreo@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'TLAHUAC':
				$mail->Username = "noreply.tlahuac@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'CENTRO':
				$mail->Username = "noreply.centro@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'TLALNEPANT':
				$mail->Username = "noreply.tlalnepantla@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'VIAMORELOS':
				$mail->Username = "noreply.viamorelos@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'IZTAPALAPA':
				$mail->Username = "noreply.iztapalapa@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'ERMITA':
				$mail->Username = "noreply.ermita@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'CUAUTITLAN':
				$mail->Username = "noreply.cuautitlan@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'SAN ANGEL':
				$mail->Username = "noreply.sanangel@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'SUR1':
				$mail->Username = "noreply.sur1@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'LEON':
				$mail->Username = "noreply.leon@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'ORIENTE':
				$mail->Username = "noreply.oriente@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'VILLADECOX':
				$mail->Username = "noreply.villa@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'SUR2':
				$mail->Username = "noreply.sur2@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'COACALCO':
				$mail->Username = "noreply.coacalco@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			case 'IESCA':
				$mail->Username = "noreply.iesca@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
			default:
				$mail->Username = "noreply.viaducto@uinenlinea.mx";
				$mail->Password = "Uin@portal_2019";
				break;
		}
		
		$email = $mail->Username;

		$mail->From     = $email;
		$mail->FromName = $nombre;
		$mail->AddAddress($sendTo);
		
		$mail->Subject 	= $thisSubject;
		$mail->Body 	= "
		<html> 
			<body> 
				<img src='".$GLOBALS['HomeLink']."servicios-profesionales/img/uin-logo.png' style='width:100%; margin-bottom:10px;'><br>
				<h1>Haz recibido una nueva Notificaci&oacute;n en tu Proceso de: ".$SectionName."</h1>
				<p>Departamento de <b>".$nombre."</b>:</p>
				<p><b>E-Mail:</b>: ".$email."</p>
				<p><b>Tel&eacute;fono:</b>: ".$telefono."</p>
				<hr noshade='1'>
				Te Notifica que:
				<p><b>Asunto:</b>: ".$thisSubject."</p>
				<p><b>Mensaje:</b>:<br>
				".$MsgHTML."</p>
			</body> 
		</html>
		<br />";
		$mail->AltBody = "{$thisMsg} \n\n ";
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);
		
		if(strlen($sendTo) > 10){
			$MailStatus = $mail->Send(); 
			if($MailStatus){
				echo "El correo fue enviado correctamente.";
			} else {
				echo "Ocurrio un error inesperado.";
			}
		}else{
			echo "El correo del destinatario no existe";
		}
	}
	
    function MsgResult(){
        $ThisResult = "
            <div id='MsgResult' class='row' style='display:none; margin-top:50px; text-align:center;'>
                <div class='col-md-12 col-sm-6'>
                    <div class='card' style='height:300px; background-color:#f1f2f3;'>
                        <div class='card-body'>
                            <span id='PrintMsg'></span>
                        </div>
                    </div>
                </div>
            </div>
        ";
        return $ThisResult;
	}
?>
