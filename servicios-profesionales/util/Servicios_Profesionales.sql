USE [Campus]
GO
ALTER TABLE [dbo].[Acc_Solicitudes_Titulacion_SP] DROP CONSTRAINT [FK_Acc_Solicitudes_Titulacion_SP_Acc_Cat_Modalidades_Tipo_SP]
GO
ALTER TABLE [dbo].[ACC_SERVICIOS_PROFESIONALES_SP] DROP CONSTRAINT [FK__ACC_SERVI__ESTAT__45A74A70]
GO
ALTER TABLE [dbo].[ACC_Documentos_Generales] DROP CONSTRAINT [FK_DO__PEOPL__6DEA45F4]
GO
ALTER TABLE [dbo].[ACC_ALUMNO_DOCUMENTOS_SP] DROP CONSTRAINT [FK__ALUMNO_DO__PEOPL__6DEA45F4]
GO
ALTER TABLE [dbo].[Acc_Progress_Status_SP] DROP CONSTRAINT [DF_Acc_Progress_Status_SS_Release]
GO
ALTER TABLE [dbo].[Acc_Progress_Status_SP] DROP CONSTRAINT [DF_Acc_Progress_Status_SS_Compliance]
GO
ALTER TABLE [dbo].[Acc_Progress_Status_SP] DROP CONSTRAINT [DF_Acc_Progress_Status_SS_Documentation]
GO
ALTER TABLE [dbo].[Acc_Progress_Status_SP] DROP CONSTRAINT [DF_Acc_Progress_Status_SS_Payment]
GO
ALTER TABLE [dbo].[Acc_Progress_Status_SP] DROP CONSTRAINT [DF_Acc_Progress_Status_SS_RequestDone]
GO
ALTER TABLE [dbo].[Acc_Progress_Status_SP] DROP CONSTRAINT [DF_Acc_Progress_Status_SS_StartRequest]
GO
ALTER TABLE [dbo].[Acc_Progress_Status_SP] DROP CONSTRAINT [DF_Acc_Progress_Status_SS_ProcessStartted]
GO
ALTER TABLE [dbo].[ACC_PRACTICAS_PROFESIONALES_SP] DROP CONSTRAINT [DF_ACC_PRACTICAS_PROFESIONALES_ID_Status]
GO
ALTER TABLE [dbo].[ACC_Horas_SP] DROP CONSTRAINT [DF_ACC_Horas_SP_Status]
GO
ALTER TABLE [dbo].[ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP] DROP CONSTRAINT [DF_ACC_HISTORICO_SERVICIO_SOCIAL_SS_Visto_Alumno]
GO
ALTER TABLE [dbo].[Acc_Cat_Modalidades_Tipo_SP] DROP CONSTRAINT [DF_Acc_Cat_Modalidades_Tipo_SP_Status]
GO
/****** Object:  Table [dbo].[ACC_STDDEGREQ_SS]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[ACC_STDDEGREQ_SS]
GO
/****** Object:  Table [dbo].[Acc_Solicitudes_Titulacion_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[Acc_Solicitudes_Titulacion_SP]
GO
/****** Object:  Table [dbo].[ACC_SERVICIOS_PROFESIONALES_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[ACC_SERVICIOS_PROFESIONALES_SP]
GO
/****** Object:  Table [dbo].[Acc_Progress_Status_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[Acc_Progress_Status_SP]
GO
/****** Object:  Table [dbo].[ACC_PRACTICAS_PROFESIONALES_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[ACC_PRACTICAS_PROFESIONALES_SP]
GO
/****** Object:  Table [dbo].[ACC_Horas_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[ACC_Horas_SP]
GO
/****** Object:  Table [dbo].[ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP]
GO
/****** Object:  Table [dbo].[ACC_Documentos_Generales]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[ACC_Documentos_Generales]
GO
/****** Object:  Table [dbo].[ACC_CSP_01_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[ACC_CSP_01_SP]
GO
/****** Object:  Table [dbo].[ACC_CAT_TIPO_PRESTACION_SS]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[ACC_CAT_TIPO_PRESTACION_SS]
GO
/****** Object:  Table [dbo].[ACC_CAT_TIPO_LIBERACION_SS]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[ACC_CAT_TIPO_LIBERACION_SS]
GO
/****** Object:  Table [dbo].[ACC_CAT_SERVICIO_PROFESIONAL_SS]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[ACC_CAT_SERVICIO_PROFESIONAL_SS]
GO
/****** Object:  Table [dbo].[Acc_Cat_Modalidades_Tipo_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[Acc_Cat_Modalidades_Tipo_SP]
GO
/****** Object:  Table [dbo].[ACC_CAT_ESTATUS_SPR_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[ACC_CAT_ESTATUS_SPR_SP]
GO
/****** Object:  Table [dbo].[ACC_CAT_DOCUMENTOS_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[ACC_CAT_DOCUMENTOS_SP]
GO
/****** Object:  Table [dbo].[ACC_CAT_CONVENIO_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[ACC_CAT_CONVENIO_SP]
GO
/****** Object:  Table [dbo].[ACC_CAMBIO_DATOS_SPR_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[ACC_CAMBIO_DATOS_SPR_SP]
GO
/****** Object:  Table [dbo].[ACC_ALUMNO_DOCUMENTOS_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP TABLE [dbo].[ACC_ALUMNO_DOCUMENTOS_SP]
GO
/****** Object:  StoredProcedure [dbo].[ACC_spcreditosobtenidos_SS]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP PROCEDURE [dbo].[ACC_spcreditosobtenidos_SS]
GO
/****** Object:  StoredProcedure [dbo].[ACC_serviciosocialdetalle_SS]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP PROCEDURE [dbo].[ACC_serviciosocialdetalle_SS]
GO
/****** Object:  StoredProcedure [dbo].[ACC_serviciosocial_SS]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP PROCEDURE [dbo].[ACC_serviciosocial_SS]
GO
/****** Object:  StoredProcedure [dbo].[ACC_Obtener_Concepto_Pago_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP PROCEDURE [dbo].[ACC_Obtener_Concepto_Pago_SP]
GO
/****** Object:  StoredProcedure [dbo].[ACC_Insertar_Conceptos_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP PROCEDURE [dbo].[ACC_Insertar_Conceptos_SP]
GO
/****** Object:  StoredProcedure [dbo].[Acc_datos_cartas_sp]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP PROCEDURE [dbo].[Acc_datos_cartas_sp]
GO
/****** Object:  StoredProcedure [dbo].[ACC_CHECK_PROGRESS_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
DROP PROCEDURE [dbo].[ACC_CHECK_PROGRESS_SP]
GO
/****** Object:  StoredProcedure [dbo].[ACC_CHECK_PROGRESS_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ACC_CHECK_PROGRESS_SP] @PEOPLE_CODE_ID NVARCHAR(10), @Servicio_Profesional_Id_i INT 
AS

declare 
@ACC_STDDEGREQ_SS_ID_i nvarchar(10)
,@chargecredit int
,@ProcessStartted  int
,@StartRequest int
,@RequestDone int
,@Payment int
,@Documentation int
,@Compliance int
,@Release int
,@ESTATUS_ID int
,@people_id_i nvarchar(10)
,@academic_session_i NVARCHAR(10)

	SELECT  
		   @ProcessStartted=ProcessStartted  
		   ,@StartRequest= StartRequest
		   ,@RequestDone= RequestDone
		   ,@Payment= Payment
		   ,@Documentation= Documentation
		   ,@Compliance= Compliance
		   ,@Release= Release
		   ,@ESTATUS_ID = ESTATUS_ID
		   ,@chargecredit=CHARGECREDITNUMBER 
		   ,@ACC_STDDEGREQ_SS_ID_i =STD.ACC_STDDEGREQ_SS_ID 
		   ,@people_id_i=STD.PEOPLE_ID
		   ,@academic_session_i=std.MATRIC_SESSION
		   
	  FROM Acc_Progress_Status_SP  PRO
INNER JOIN ACC_STDDEGREQ_SS STD
		ON PRO.ACC_STDDEGREQ_SS_ID = STD.ACC_STDDEGREQ_SS_ID
     WHERE PRO.PEOPLE_CODE_ID = @PEOPLE_CODE_ID AND  ESTATUS_ID = '19' 
       AND STD.Servicio_Profesional_Id =  @Servicio_Profesional_Id_i ;

	   if(@ESTATUS_ID IS NOT NULL)
	   BEGIN
	   select 
			 @ProcessStartted AS ProcessStartted
			,@StartRequest AS StartRequest
			,@RequestDone AS RequestDone
			,@Payment AS Payment 
			,@Documentation AS Documentation 
			,@Compliance AS Compliance
			,@Release AS Release 
			,@ESTATUS_ID AS ESTATUS_ID

	   END
	   IF(@ESTATUS_ID = '19')
	   BEGIN 
	   
	   if exists (select * from CHARGECREDIT as cgc where cgc.CHARGECREDITNUMBER=@chargecredit and cgc.VOID_FLAG='A' )
		begin
		update ACC_STDDEGREQ_SS SET ACC_STDDEGREQ_SS.CHARGECREDITNUMBER= NULL 
		WHERE ACC_STDDEGREQ_SS.ACC_STDDEGREQ_SS_ID=@ACC_STDDEGREQ_SS_ID_i
		UPDATE Acc_Progress_Status_SP 
		SET Payment=0, Documentation=0,Compliance=0, Release=0 
		WHERE ACC_STDDEGREQ_SS_ID=@ACC_STDDEGREQ_SS_ID_i  
		
		exec [dbo].[ACC_Insertar_Conceptos_SP] @people_id=@people_id_i, @academic_session=@academic_session_i, @ACC_STDDEGREQ_SS_ID=@ACC_STDDEGREQ_SS_ID_i,@Servicio_Profesional_ID=@Servicio_Profesional_Id_i
		end 
		END
	

GO
/****** Object:  StoredProcedure [dbo].[Acc_datos_cartas_sp]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Acc_datos_cartas_sp] @ACC_STDDEGREQ_SS_ID int 
AS


SELECT   top(1)     
 cpd.PersonnelFullName as Director
, ISNULL([add].ADDRESS_LINE_1, N'') + ' ' + ISNULL([add].HOUSE_NUMBER, N'') + ' ' + ISNULL([add].ADDRESS_LINE_2, N'') + ' ' + ISNULL([add].ADDRESS_LINE_3,N'') + ' ' + ISNULL(cc.LONG_DESC, N'')+ ' ' + ISNULL(cdc.MEDIUM_DESC, N'')  + ' ' + ISNULL([add].ZIP_CODE, N'') +' Télefono '+(select top(1) dg.PhoneNumber from  OrganizationPhone as dg where dg.OrganizationId=org.OrganizationId) as Direccion
						 ,ISNULL(cc.LONG_DESC, N'') as Delegacion
						 ,ISNULL(cdc.MEDIUM_DESC, N'') as Estado
						 ,acc.RVOE AS Rvoe
						 ,acc.PEOPLE_Id as Matricula
						 ,day (acc.FECHA_RVOE) as Dia
						 ,month (acc.FECHA_RVOE) as Mes 
						 ,year (acc.FECHA_RVOE) as Anho
						 ,acc.PRIMER_NOMBRE+' '+acc.SEGUNDO_NOMBRE+' '+acc.APELLIDO_PATERNO+' '+acc.APELLIDO_MATERNO as Nombre_del_Alumno
						 ,cu.FORMAL_TITLE Carrera
						 ,acc.PORCENTAJE as Porcentaje
						 ,aCC.PROMEDIO as Promedio
						 ,ACC.TURNO as Turno
						 ,[add].EMAIL_ADDRESS as Correo
						 ,acc.MATRIC_TERM_PROGRESS  AS Ciclos
						 ,ACC.CICLO_INGRESO as Ciclo_de_ingreso
						 ,(select top(1) cc.CLAVE_CARRERA from CARRERA_CERTIFICADO as cc where cc.CURRICULUM=ACC.CURRICULUM) Cv_carrera
						        
FROM            CODE_COUNTY AS cc with (nolock)RIGHT OUTER JOIN
                         ADDRESS AS [add] with (nolock) ON cc.CODE_VALUE_KEY = [add].COUNTY   RIGHT OUTER JOIN
                         CampusPersonnel AS cpd with (nolock) INNER JOIN
                         ORGANIZATION AS org with (nolock) ON cpd.OrganizationId = org.OrganizationId INNER JOIN
                         CODE_ACASESSION AS ac with (nolock) ON org.ORG_IDENTIFIER = ac.CODE_XVAL INNER JOIN
                         ACC_STDDEGREQ_SS AS ACC with (nolock) ON ac.CODE_VALUE_KEY = ACC.MATRIC_SESSION AND cpd.Curriculum = ACC.CURRICULUM ON [add].PEOPLE_ORG_CODE_ID = org.ORG_CODE_ID LEFT OUTER JOIN
                         CODE_STATE AS cdc with (nolock) ON [add].STATE = cdc.CODE_VALUE_KEY LEFT OUTER JOIN
                         CODE_CURRICULUM AS cu with (nolock) ON ACC.CURRICULUM = cu.CODE_VALUE_KEY
WHERE        (cpd.PersonnelType = 1) 
------------------------**PARAMETRO***---------------
AND (ACC.ACC_STDDEGREQ_SS_ID = @ACC_STDDEGREQ_SS_ID)---id de  select ACC_STDDEGREQ_SS_ID  from ACC_STDDEGREQ_SS
-------------------------****---------------
--and [add].ADDRESS_TYPE='OTRO'

GO
/****** Object:  StoredProcedure [dbo].[ACC_Insertar_Conceptos_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ACC_Insertar_Conceptos_SP]  @people_id nvarchar(10), @academic_session NVARCHAR(10), @ACC_STDDEGREQ_SS_ID INT , @Servicio_Profesional_ID int

AS

IF EXISTS(SELECT CHARGECREDITNUMBER FROM ACC_STDDEGREQ_SS WHERE ACC_STDDEGREQ_SS_ID = @ACC_STDDEGREQ_SS_ID AND CHARGECREDITNUMBER IS NOT NULL)
BEGIN 
RETURN
END
DECLARE 
			@academic_year		NVARCHAR(4),	--AÑO		
			@academic_term		NVARCHAR(10),	--PERIODO
			--@academic_session	NVARCHAR(10),	--SESIÓN
			@peopleorgcodeid    NVARCHAR(10),
			@batchnumber		NVARCHAR(20),
			--@people_id			NVARCHAR (9),	-- ID ALUMNO
			@degree				NVARCHAR(10),
			@charge_credit_code NVARCHAR(10),	-- CÓDIGO DE CONCEPTO
			@due_date			DATETIME,
			@chargecreditcodedesc NVARCHAR(20),
			@entry_date			DATETIME,
			@statement_type		NVARCHAR(3),
			@sumary_type		NVARCHAR(6),
			@pbdate				DATETIME,
			@pbtime				DATETIME,
			@amount				NUMERIC(18,6),	-- MONTO
			@paymentoption		NVARCHAR(1),
			@payplaneligible	NVARCHAR(1),
			@rateamount			NUMERIC(18,6),
			@percreditflat		NVARCHAR(1),
			@chargecreditcodeid INT,
			@chargecreditnumber INTEGER,
			@CHARGE_CREDIT_type NVARCHAR(1),	--CARGO/ABONO
			@cont_table			INT
			, @aca_year nvarchar(4)
			, @aca_term nvarchar(10)
			, @aca_session nvarchar(10)
			, @ChargeCreditId INT 
			, @return_value int
			EXEC	@return_value = [dbo].[AX_spGetYTS_x_pid] @peopleid = @people_id, @aca_year = @aca_year OUTPUT, @aca_term = @aca_term OUTPUT
			, @aca_session = @aca_session OUTPUT 
			select  @aca_year ,@aca_term
			,@aca_session

			SELECT @degree=SS.DEGREE FROM ACC_STDDEGREQ_SS AS SS WHERE SS.ACC_STDDEGREQ_SS_ID=@ACC_STDDEGREQ_SS_ID

declare @CHARGE_CREDIT TABLE 
	(
		CHARGECREDITNUMBER		INT,
		PEOPLE_ORG_CODE_ID		NVARCHAR(10),
		PEOPLE_ORG_CODE			NVARCHAR(1),
		PEOPLE_ORG_ID			NVARCHAR(9),
		ACADEMIC_YEAR			NVARCHAR(9),
		ACADEMIC_TERM			NVARCHAR(10),
		ACADEMIC_SESSION		NVARCHAR(10),
		CHARGE_CREDIT_CODE		NVARCHAR(10),
		CHARGE_CREDIT_TYPE		NVARCHAR(1),
		AMOUNT					NUMERIC(18,6),
		PAID_AMOUNT				NUMERIC(18,6),
		BALANCE_AMOUNT			NUMERIC(18,6),
		NOTE					NVARCHAR(MAX),
		PREBILL_FLAG			NVARCHAR(1),
		TAXABLE					NVARCHAR(1),
		FIN_AID_ELIGIBLE		NVARCHAR(1),
		REFUND_ELIGIBLE			NVARCHAR(1),
		BATCH					NVARCHAR(20),
		PRINTED_ON_STMT			NVARCHAR(1),
		FIRST_STMT_DATE			DATETIME,
		DUE_DATE				DATETIME,
		CRG_CRD_DESC			NVARCHAR(40),
		ENTRY_DATE				DATETIME,
		JOURNAL_SOURCE			NVARCHAR(10),
		JOURNAL_ENTRY			NVARCHAR(10),
		ANTICIPATED_FLAG		NVARCHAR(1),
		SCHED_DISB_DATE			DATETIME,
		ASSESSED_FLAG			VARCHAR(1),
		VOID_FLAG				VARCHAR(1),
		POSTED_FLAG				VARCHAR(1),
		POSTED_DATE				DATETIME,
		FISCAL_YEAR				VARCHAR(4),
		FISCAL_PERIOD			VARCHAR(10),
		PROJECT_ID				VARCHAR(10),
		STATEMENT_TYPE			VARCHAR(3),
		SUMMARY_TYPE			VARCHAR(6),
		RECEIPT_NUMBER			INT,
		DETAIL_FLAG				VARCHAR(1),
		CREATE_DATE				DATETIME,
		CREATE_TIME				DATETIME,
		CREATE_OPID				VARCHAR(8),
		CREATE_TERMINAL			VARCHAR(4),	
		REVISION_DATE			DATETIME,
		REVISION_TIME			DATETIME,
		REVISION_OPID			VARCHAR(8),
		REVISION_TERMINAL		VARCHAR(4),
		ABT_JOIN				VARCHAR(1),
		TRUE_ACADEMIC_YEAR		VARCHAR(4),
		BEGINNING_BAL_FLAG		VARCHAR(1),	
		VOID_DATE				DATETIME,
		REVERSED				VARCHAR(1),
		REVERSED_NUMBER			INT,
		PAYPLAN_FLAG			VARCHAR(1),
		REFUND_EXPORT			VARCHAR(1),
		REFUND_EXPORT_DATE		DATETIME,
		REFUND_EXPORT_OPID		VARCHAR(8),
		REFUND_CHECK_NUM		INT,
		REFUND_CHECK_DATE		DATETIME,
		SPONSOR_ID				VARCHAR(10),
		SPONSORED_ID			VARCHAR(10),
		AGREEMENT_NUMBER		INT,
		PAYMENT_OPTION			VARCHAR(1),
		PAYPLAN_ELIGIBLE		VARCHAR(1),
		TYPE_1098				VARCHAR(1),
		RATE_AMOUNT				NUMERIC(18,6),
		PER_CREDIT_FLAT			NCHAR(1)
	)


	
	SET @entry_date= dbo.fnMakeDate( getdate() )
	--	SET NOCOUNT ON
	--SET CONCAT_NULL_YIELDS_NULL OFF


	-- CONVERT THE BATCH_SEQUENCE INTO THE BATCH_NUMBER (MMDDYYYY+BATCH_SEQUENCE)
	-- Calcula el Batch Number
	SELECT @batchnumber = REPLACE( CONVERT( NVARCHAR(10), GETDATE(), 101 ), '/', '' ) + CONVERT( NVARCHAR(6), 9990 )
		
		-- Establece valores iniciales
		
		SET @cont_table = 0
		--Obtiene el Id del Alumno (PersonId)
	
		SELECT @peopleorgcodeid = 'P'+@people_id
		
		--Establece Variables Date y Time
		exec Sp_getpbdate @pbdate output
		exec sp_getpbtime @pbtime output

				   	--Selecciona el año/periodo/sesión
		/*SELECT	@academic_year		= '2020', 
				@academic_term		= 'cuatri1', 
				@academic_session	= 'TLALPAN'*/
	    
		SELECT TOP(1) 
		@academic_year		= CC.ACADEMIC_YEAR, 
		@academic_term		= CC.ACADEMIC_TERM
		FROM ACADEMICCALENDAR AS CC WHERE CC.START_DATE<GETDATE()
		AND CC.ACADEMIC_SESSION=@academic_session
		ORDER BY START_DATE DESC

		if(@Servicio_Profesional_ID=1)---obtiene concepto para servicio social
		begin

				select @charge_credit_code=(
				SELECT TOP(1) TT.CODE_VALUE_KEY FROM (
				SELECT 
					(CASE T1.PL WHEN 'AZ' THEN 'ECATEPEC'
					WHEN 'CH' THEN 'CHALCO'
					WHEN 'CN' THEN 'CENTRO'
					WHEN 'CO' THEN 'COACALCO'
					WHEN 'CT' THEN 'CHAPULT'
					WHEN 'CU' THEN 'CUAUTITLAN'
					WHEN 'ER' THEN 'ERMITA'
					WHEN 'IZ' THEN 'IZTAPALAPA'
					WHEN 'NO' THEN 'NORTE'
					WHEN 'OT' THEN 'ORIENTE'
					WHEN 'S1' THEN 'SUR1'
					WHEN 'S2' THEN 'SUR2'
					WHEN 'SA' THEN 'SAN ANGEL'
					WHEN 'TC' THEN 'TOLUCA'
					WHEN 'TH' THEN 'TLAHUAC'
					WHEN 'TO' THEN 'TOREO'
					WHEN 'TN' THEN 'TLALNEPANT'
					WHEN 'VC' THEN 'VILLADECOX'
					WHEN 'VD' THEN 'VIADUCTO'
					WHEN 'VM' THEN 'VIAMORELOS'
					WHEN 'XO' THEN 'XOLA'
					WHEN 'TL' THEN 'TLALPAN'
					WHEN 'LE' THEN 'LEON'
					END
					) AS PLANTEL
					,T1.*
					 FROM (
					select 
					LEFT (CODE_VALUE_KEY,2)AS PL
					,CODE_VALUE_KEY
					,DISTRIBUTION_ORDER
					,AMOUNT 
					from CODE_CHARGECREDIT where (LONG_DESC LIKE '%SOCIAL%' OR MEDIUM_DESC LIKE '%SERV SOC%')
					AND ISNUMERIC(LEFT (CODE_VALUE_KEY,2))=0
					AND [TYPE]='C'
					AND [STATUS]='A'
					union all

					select 
					right (CODE_VALUE_KEY,2) AS PL
					,CODE_VALUE_KEY
					,DISTRIBUTION_ORDER
					,AMOUNT 
					from CODE_CHARGECREDIT where LONG_DESC LIKE '%SOCIAL%'
					AND ISNUMERIC(right (CODE_VALUE_KEY,2))=0
					AND [TYPE]='C'
					AND [STATUS]='A'
					) AS T1
					) AS TT
					where TT.PLANTEL=@academic_session
				)
			end

			if(@Servicio_Profesional_ID=2)---obtiene concepto para servicio social
		begin

				select @charge_credit_code=(
				SELECT TOP(1) TT.CODE_VALUE_KEY FROM (
				SELECT 
					(CASE T1.PL WHEN 'AZ' THEN 'ECATEPEC'
					WHEN 'CH' THEN 'CHALCO'
					WHEN 'CN' THEN 'CENTRO'
					WHEN 'CO' THEN 'COACALCO'
					WHEN 'CT' THEN 'CHAPULT'
					WHEN 'CU' THEN 'CUAUTITLAN'
					WHEN 'ER' THEN 'ERMITA'
					WHEN 'IZ' THEN 'IZTAPALAPA'
					WHEN 'NO' THEN 'NORTE'
					WHEN 'OT' THEN 'ORIENTE'
					WHEN 'S1' THEN 'SUR1'
					WHEN 'S2' THEN 'SUR2'
					WHEN 'SA' THEN 'SAN ANGEL'
					WHEN 'TC' THEN 'TOLUCA'
					WHEN 'TH' THEN 'TLAHUAC'
					WHEN 'TO' THEN 'TOREO'
					WHEN 'TN' THEN 'TLALNEPANT'
					WHEN 'VC' THEN 'VILLADECOX'
					WHEN 'VD' THEN 'VIADUCTO'
					WHEN 'VM' THEN 'VIAMORELOS'
					WHEN 'XO' THEN 'XOLA'
					WHEN 'TL' THEN 'TLALPAN'
					--WHEN 'LE' THEN 'LEON' leon no tiene pago de practicas profesionales
					END
					) AS PLANTEL
					,T1.*
					 FROM (
					select 
					LEFT (CODE_VALUE_KEY,2)AS PL
					,CODE_VALUE_KEY
					,DISTRIBUTION_ORDER
					,AMOUNT 
					from CODE_CHARGECREDIT where CODE_VALUE_KEY LIKE '%-25%' AND CODE_VALUE_KEY<>'LE-25'
					AND ISNUMERIC(LEFT (CODE_VALUE_KEY,2))=0
					AND [TYPE]='C'
					AND [STATUS]='A'
					union all

					select 
					right (CODE_VALUE_KEY,2) AS PL
					,CODE_VALUE_KEY
					,DISTRIBUTION_ORDER
					,AMOUNT 
					from CODE_CHARGECREDIT where CODE_VALUE_KEY LIKE '%25-%'
					AND ISNUMERIC(right (CODE_VALUE_KEY,2))=0
					AND [TYPE]='C'
					AND [STATUS]='A'
					) AS T1
					) AS TT
					where TT.PLANTEL=@academic_session
				)
			end
			
			if(@Servicio_Profesional_ID=3 and @degree='LIC' )---obtiene concepto para servicio social
		begin
					select @charge_credit_code=(
				SELECT TOP(1) TT.CODE_VALUE_KEY FROM (
				SELECT 
					(CASE T1.PL WHEN 'AZ' THEN 'ECATEPEC'
					WHEN 'CH' THEN 'CHALCO'
					WHEN 'CN' THEN 'CENTRO'
					WHEN 'CO' THEN 'COACALCO'
					WHEN 'CT' THEN 'CHAPULT'
					WHEN 'CU' THEN 'CUAUTITLAN'
					WHEN 'ER' THEN 'ERMITA'
					WHEN 'IZ' THEN 'IZTAPALAPA'
					WHEN 'NO' THEN 'NORTE'
					WHEN 'OT' THEN 'ORIENTE'
					WHEN 'S1' THEN 'SUR1'
					WHEN 'S2' THEN 'SUR2'
					WHEN 'SA' THEN 'SAN ANGEL'
					WHEN 'TC' THEN 'TOLUCA'
					WHEN 'TH' THEN 'TLAHUAC'
					WHEN 'TO' THEN 'TOREO'
					WHEN 'TN' THEN 'TLALNEPANT'
					WHEN 'VC' THEN 'VILLADECOX'
					WHEN 'VD' THEN 'VIADUCTO'
					WHEN 'VM' THEN 'VIAMORELOS'
					WHEN 'XO' THEN 'XOLA'
					WHEN 'TL' THEN 'TLALPAN'
					--WHEN 'LE' THEN 'LEON' leon no tiene pago de practicas profesionales
					END
					) AS PLANTEL
					,T1.*
					 FROM (

		select 
					LEFT (CODE_VALUE_KEY,2)AS PL
					,CODE_VALUE_KEY
					,DISTRIBUTION_ORDER
					,AMOUNT 
					from CODE_CHARGECREDIT where  ((CODE_VALUE_KEY LIKE '%-29%' AND CODE_VALUE_KEY<>'LE-29')OR(CODE_VALUE_KEY='LE-41'))
					AND ISNUMERIC(LEFT (CODE_VALUE_KEY,2))=0
					AND [TYPE]='C'
					AND [STATUS]='A'
					and len(CODE_VALUE_KEY)=5
					union all

		select 
					right (CODE_VALUE_KEY,2) AS PL
					,CODE_VALUE_KEY
					,DISTRIBUTION_ORDER
					,AMOUNT 
					from CODE_CHARGECREDIT where CODE_VALUE_KEY LIKE '%29-%'
					AND ISNUMERIC(right (CODE_VALUE_KEY,2))=0
					AND [TYPE]='C'
					AND [STATUS]='A'
					and len(CODE_VALUE_KEY)=5
					) AS T1
					) AS TT
					where TT.PLANTEL=@academic_session
				)

		end

			if(@Servicio_Profesional_ID=3 and @degree='MAEST' )---obtiene concepto para servicio social
		begin
					select @charge_credit_code=(
				SELECT TOP(1) TT.CODE_VALUE_KEY FROM (
				SELECT 
					(CASE T1.PL WHEN 'AZ' THEN 'ECATEPEC'
					WHEN 'CH' THEN 'CHALCO'
					WHEN 'CN' THEN 'CENTRO'
					WHEN 'CO' THEN 'COACALCO'
					WHEN 'CT' THEN 'CHAPULT'
					WHEN 'CU' THEN 'CUAUTITLAN'
					WHEN 'ER' THEN 'ERMITA'
					WHEN 'IZ' THEN 'IZTAPALAPA'
					WHEN 'NO' THEN 'NORTE'
					WHEN 'OT' THEN 'ORIENTE'
					WHEN 'S1' THEN 'SUR1'
					WHEN 'S2' THEN 'SUR2'
					WHEN 'SA' THEN 'SAN ANGEL'
					WHEN 'TC' THEN 'TOLUCA'
					WHEN 'TH' THEN 'TLAHUAC'
					WHEN 'TO' THEN 'TOREO'
					WHEN 'TN' THEN 'TLALNEPANT'
					WHEN 'VC' THEN 'VILLADECOX'
					WHEN 'VD' THEN 'VIADUCTO'
					WHEN 'VM' THEN 'VIAMORELOS'
					WHEN 'XO' THEN 'XOLA'
					WHEN 'TL' THEN 'TLALPAN'
					--WHEN 'LE' THEN 'LEON' leon no tiene pago de practicas profesionales
					END
					) AS PLANTEL
					,T1.*
					 FROM (

		select 
					LEFT (CODE_VALUE_KEY,2)AS PL
					,CODE_VALUE_KEY
					,DISTRIBUTION_ORDER
					,AMOUNT 
					from CODE_CHARGECREDIT where ((CODE_VALUE_KEY LIKE '%-33%' AND CODE_VALUE_KEY<>'LE-33')OR(CODE_VALUE_KEY='LE-53'))
					AND ISNUMERIC(LEFT (CODE_VALUE_KEY,2))=0
					AND [TYPE]='C'
					AND [STATUS]='A'
					and len(CODE_VALUE_KEY)=5
					union all

		select 
					right (CODE_VALUE_KEY,2) AS PL
					,CODE_VALUE_KEY
					,DISTRIBUTION_ORDER
					,AMOUNT 
					from CODE_CHARGECREDIT where CODE_VALUE_KEY LIKE '%33-%'
					AND ISNUMERIC(right (CODE_VALUE_KEY,2))=0
					AND [TYPE]='C'
					AND [STATUS]='A'
					and len(CODE_VALUE_KEY)=5
					) AS T1
					) AS TT
					where TT.PLANTEL=@academic_session
				)

		end
		if(@Servicio_Profesional_ID=3 and @degree='ESPEC' )---obtiene concepto para servicio social
		begin
					select @charge_credit_code=(
				SELECT TOP(1) TT.CODE_VALUE_KEY FROM (
				SELECT 
					(CASE T1.PL WHEN 'AZ' THEN 'ECATEPEC'
					WHEN 'CH' THEN 'CHALCO'
					WHEN 'CN' THEN 'CENTRO'
					WHEN 'CO' THEN 'COACALCO'
					WHEN 'CT' THEN 'CHAPULT'
					WHEN 'CU' THEN 'CUAUTITLAN'
					WHEN 'ER' THEN 'ERMITA'
					WHEN 'IZ' THEN 'IZTAPALAPA'
					WHEN 'NO' THEN 'NORTE'
					WHEN 'OT' THEN 'ORIENTE'
					WHEN 'S1' THEN 'SUR1'
					WHEN 'S2' THEN 'SUR2'
					WHEN 'SA' THEN 'SAN ANGEL'
					WHEN 'TC' THEN 'TOLUCA'
					WHEN 'TH' THEN 'TLAHUAC'
					WHEN 'TO' THEN 'TOREO'
					WHEN 'TN' THEN 'TLALNEPANT'
					WHEN 'VC' THEN 'VILLADECOX'
					WHEN 'VD' THEN 'VIADUCTO'
					WHEN 'VM' THEN 'VIAMORELOS'
					WHEN 'XO' THEN 'XOLA'
					WHEN 'TL' THEN 'TLALPAN'
					--WHEN 'LE' THEN 'LEON' leon no tiene pago de practicas profesionales
					END
					) AS PLANTEL
					,T1.*
					 FROM (

		select 
					LEFT (CODE_VALUE_KEY,2)AS PL
					,CODE_VALUE_KEY
					,DISTRIBUTION_ORDER
					,AMOUNT 
					from CODE_CHARGECREDIT where ((CODE_VALUE_KEY LIKE '%-31%' AND CODE_VALUE_KEY<>'LE-31'))
					AND ISNUMERIC(LEFT (CODE_VALUE_KEY,2))=0
					AND [TYPE]='C'
					AND [STATUS]='A'
					and len(CODE_VALUE_KEY)=5
					union all

		select 
					right (CODE_VALUE_KEY,2) AS PL
					,CODE_VALUE_KEY
					,DISTRIBUTION_ORDER
					,AMOUNT 
					from CODE_CHARGECREDIT where CODE_VALUE_KEY LIKE '%31-%'
					AND ISNUMERIC(right (CODE_VALUE_KEY,2))=0
					AND [TYPE]='C'
					AND [STATUS]='A'
					and len(CODE_VALUE_KEY)=5
					) AS T1
					) AS TT
					where TT.PLANTEL=@academic_session
				)

		end

		--Valida que el concepto exista
				SELECT	@CHARGE_CREDIT_type = [TYPE], 
						@chargecreditcodeid = ChargeCreditCodeId
				  FROM	CODE_CHARGECREDIT 
				 WHERE	  CODE_CHARGECREDIT.CODE_VALUE_KEY = @charge_credit_code
				   AND	CODE_CHARGECREDIT.[STATUS]='A'

				--Obtiene datos del Concepto de Cargo
		SELECT	--@chargecreditcode = ccc.Code_Value_Key,
				@chargecreditcodedesc = ccc.MEDIUM_DESC,
				@paymentoption = ccc.PAYMENT_OPTION, --default value
				@payplaneligible = ccc.PAYPLAN_ELIGIBLE, --default value
				@rateamount = ccc.Amount,
				@percreditflat = ccc.PER_CREDIT_FLAT,
				@statement_type = ccc.STATEMENT_TYPE,
				@sumary_type = ccc.SUMMARY_TYPE,
				@amount=CCC.AMOUNT
		FROM	dbo.CODE_CHARGECREDIT ccc with( nolock )
		WHERE	ChargeCreditCodeId = @chargecreditcodeid;

		/*
		SELECT 
		@cont_table,
		@academic_year			,
			@academic_term		,
			@academic_session	,
			@peopleorgcodeid    ,
			@batchnumber		,
			@people_id			,
			@charge_credit_code,
			@due_date			,
			@chargecreditcodedesc,
			@entry_date			,
			@statement_type		,
			@sumary_type		,
			@pbdate				,
			@pbtime				,
			@amount				,
			@paymentoption		,
			@payplaneligible	,
			@rateamount			,
			@percreditflat		,
			@chargecreditcodeid ,
			@CHARGE_CREDIT_type ,
			@cont_table			

			*/
		



						
IF(@people_id IS NOT NULL OR @academic_session IS NOT NULL)

BEGIN 

INSERT INTO @CHARGE_CREDIT( CHARGECREDITNUMBER,	PEOPLE_ORG_CODE_ID,	PEOPLE_ORG_CODE, PEOPLE_ORG_ID,	
									ACADEMIC_YEAR,	ACADEMIC_TERM,	ACADEMIC_SESSION,	CHARGE_CREDIT_CODE,
									CHARGE_CREDIT_TYPE,	AMOUNT,	PAID_AMOUNT, BALANCE_AMOUNT, NOTE, PREBILL_FLAG,
									TAXABLE, FIN_AID_ELIGIBLE, REFUND_ELIGIBLE,	BATCH,	PRINTED_ON_STMT,	
									FIRST_STMT_DATE, DUE_DATE, CRG_CRD_DESC, ENTRY_DATE, JOURNAL_SOURCE, 
									JOURNAL_ENTRY, ANTICIPATED_FLAG, SCHED_DISB_DATE, ASSESSED_FLAG, VOID_FLAG,	
									POSTED_FLAG, POSTED_DATE, FISCAL_YEAR, FISCAL_PERIOD, PROJECT_ID, STATEMENT_TYPE,
									SUMMARY_TYPE, RECEIPT_NUMBER, DETAIL_FLAG, CREATE_DATE, CREATE_TIME, CREATE_OPID,
									CREATE_TERMINAL, REVISION_DATE, REVISION_TIME, REVISION_OPID, REVISION_TERMINAL,
									ABT_JOIN, TRUE_ACADEMIC_YEAR, BEGINNING_BAL_FLAG, VOID_DATE, REVERSED, REVERSED_NUMBER,
									PAYPLAN_FLAG, REFUND_EXPORT, REFUND_EXPORT_DATE, REFUND_EXPORT_OPID, REFUND_CHECK_NUM,
									REFUND_CHECK_DATE, SPONSOR_ID, SPONSORED_ID, AGREEMENT_NUMBER, PAYMENT_OPTION, 
									PAYPLAN_ELIGIBLE, TYPE_1098, RATE_AMOUNT, PER_CREDIT_FLAT)
		VALUES( @cont_table, @peopleorgcodeid, 'P', @people_id, 
				@academic_year, @academic_term, @academic_session, @charge_credit_code,
		   		@CHARGE_CREDIT_type, @amount, 0.0, @amount, '', 'N', 
				'N', 'N', 'N', @batchnumber, 'N',
				NULL,  @due_date, @chargecreditcodedesc, @entry_date, '',
				'','N', NULL, 'Y', 'N',
				'N', NULL, '', '', NULL, @statement_type,
				@sumary_type, 0, 'N', @pbdate, @pbtime, 'APPLCANT',
				'0001', @pbdate, @pbtime, 'APPLCANT', '0001',
				'*', '', 'N', NULL, 'N', NULL,
				'N', 'N', NULL, NULL, 0, 
				NULL, NULL, NULL, NULL, @paymentoption, 
				@payplaneligible, NULL, @rateamount, @percreditflat 
				)
	
		-- Guarda Saldo

--Actualiza CHARGECREDITNUMBER
EXEC sp_get_next_chargecreditnum @chargecreditnumber OUTPUT,'APPLCANT', '0001';
		UPDATE	@CHARGE_CREDIT
		   SET	CHARGECREDITNUMBER = @chargecreditnumber
		 WHERE	CHARGECREDITNUMBER = @cont_table

		 --SELECT *FROM @CHARGE_CREDIT ;
		 --SELECT TOP 1 	CHARGECREDITNUMBER,	PEOPLE_ORG_CODE_ID,	PEOPLE_ORG_CODE, PEOPLE_ORG_ID,	
			--						ACADEMIC_YEAR,	ACADEMIC_TERM,	ACADEMIC_SESSION,	CHARGE_CREDIT_CODE,
			--						CHARGE_CREDIT_TYPE,	AMOUNT,	PAID_AMOUNT, BALANCE_AMOUNT, NOTE, PREBILL_FLAG,
			--						TAXABLE, FIN_AID_ELIGIBLE, REFUND_ELIGIBLE,	BATCH,	PRINTED_ON_STMT,	
			--						FIRST_STMT_DATE, DUE_DATE, CRG_CRD_DESC, ENTRY_DATE, JOURNAL_SOURCE, 
			--						JOURNAL_ENTRY, ANTICIPATED_FLAG, SCHED_DISB_DATE, ASSESSED_FLAG, VOID_FLAG,	
			--						POSTED_FLAG, POSTED_DATE, FISCAL_YEAR, FISCAL_PERIOD, PROJECT_ID, STATEMENT_TYPE,
			--						SUMMARY_TYPE, RECEIPT_NUMBER, DETAIL_FLAG, CREATE_DATE, CREATE_TIME, CREATE_OPID,
			--						CREATE_TERMINAL, REVISION_DATE, REVISION_TIME, REVISION_OPID, REVISION_TERMINAL,
			--						ABT_JOIN, TRUE_ACADEMIC_YEAR, BEGINNING_BAL_FLAG, VOID_DATE, REVERSED, REVERSED_NUMBER,
			--						PAYPLAN_FLAG, REFUND_EXPORT, REFUND_EXPORT_DATE, REFUND_EXPORT_OPID, REFUND_CHECK_NUM,
			--						REFUND_CHECK_DATE, SPONSOR_ID, SPONSORED_ID, AGREEMENT_NUMBER, PAYMENT_OPTION, 
			--						PAYPLAN_ELIGIBLE, TYPE_1098, RATE_AMOUNT, PER_CREDIT_FLAT  FROM CHARGECREDIT ;




	INSERT INTO CHARGECREDIT(	CHARGECREDITNUMBER,	PEOPLE_ORG_CODE_ID,	PEOPLE_ORG_CODE, PEOPLE_ORG_ID,	
									ACADEMIC_YEAR,	ACADEMIC_TERM,	ACADEMIC_SESSION,	CHARGE_CREDIT_CODE,
									CHARGE_CREDIT_TYPE,	AMOUNT,	PAID_AMOUNT, BALANCE_AMOUNT, NOTE, PREBILL_FLAG,
									TAXABLE, FIN_AID_ELIGIBLE, REFUND_ELIGIBLE,	BATCH,	PRINTED_ON_STMT,	
									FIRST_STMT_DATE, DUE_DATE, CRG_CRD_DESC, ENTRY_DATE, JOURNAL_SOURCE, 
									JOURNAL_ENTRY, ANTICIPATED_FLAG, SCHED_DISB_DATE, ASSESSED_FLAG, VOID_FLAG,	
									POSTED_FLAG, POSTED_DATE, FISCAL_YEAR, FISCAL_PERIOD, PROJECT_ID, STATEMENT_TYPE,
									SUMMARY_TYPE, RECEIPT_NUMBER, DETAIL_FLAG, CREATE_DATE, CREATE_TIME, CREATE_OPID,
									CREATE_TERMINAL, REVISION_DATE, REVISION_TIME, REVISION_OPID, REVISION_TERMINAL,
									ABT_JOIN, TRUE_ACADEMIC_YEAR, BEGINNING_BAL_FLAG, VOID_DATE, REVERSED, REVERSED_NUMBER,
									PAYPLAN_FLAG, REFUND_EXPORT, REFUND_EXPORT_DATE, REFUND_EXPORT_OPID, REFUND_CHECK_NUM,
									REFUND_CHECK_DATE, SPONSOR_ID, SPONSORED_ID, AGREEMENT_NUMBER, PAYMENT_OPTION, 
									PAYPLAN_ELIGIBLE, TYPE_1098, RATE_AMOUNT, PER_CREDIT_FLAT )
		SELECT	* 
		  FROM	@CHARGE_CREDIT
		 WHERE	CHARGECREDITNUMBER = @chargecreditnumber;
		 UPDATE ACC_STDDEGREQ_SS SET CHARGECREDITNUMBER = @chargecreditnumber WHERE ACC_STDDEGREQ_SS_ID  = @ACC_STDDEGREQ_SS_ID ;

END






GO
/****** Object:  StoredProcedure [dbo].[ACC_Obtener_Concepto_Pago_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ACC_Obtener_Concepto_Pago_SP] @PEOPLE_CODE_ID VARCHAR(10)
AS
	DECLARE	@return_value int, 		
			@aca_year nvarchar(4),
	 		@aca_term nvarchar(10), 
			@aca_session nvarchar(10), 		
			@ChargeCreditId INT 
	EXEC	@return_value = [dbo].[AX_spGetYTS_x_pid] 		
			@peopleid = @PEOPLE_CODE_ID, 		
			@aca_year = @aca_year OUTPUT, 		
			@aca_term = @aca_term OUTPUT, 		
			@aca_session = @aca_session OUTPUT 
	 SELECT AC.ACADEMIC_YEAR, AC.ACADEMIC_TERM, 
	        AC.ACADEMIC_SESSION,MPT.PaymentType Id, 
			MPT.PaymentTypeCode Code, MPT.PaymentTypeDescription Description , 
			MPC.CampusId , MPC.ChargeCreditId , AC.SessionPeriodId , TP.TermPeriodId  
	   FROM ACADEMICCALENDAR AC 
 INNER JOIN MakePaymentPeriod MPP ON AC.SessionPeriodId = MPP.SessionPeriodId 
 INNER JOIN MakePaymentPeriodComplement MPC ON MPP.MakePaymentPeriodId = MPC.MakePaymentPeriodId AND AC.SessionPeriodId = MPC.SessionPeriodId 
 INNER JOIN MakePaymentType MPT ON MPC.PaymentType = MPT.PaymentType 
 INNER JOIN TermPeriod TP ON AC.ACADEMIC_YEAR = TP.AcademicYear  AND AC.ACADEMIC_TERM = TP.AcademicTerm 
      WHERE AC.ACADEMIC_YEAR = @aca_year AND AC.ACADEMIC_TERM = @aca_term AND AC.ACADEMIC_SESSION = @aca_session 
	    AND MPT.PaymentTypeCode  =  02


GO
/****** Object:  StoredProcedure [dbo].[ACC_serviciosocial_SS]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ACC_serviciosocial_SS]  @people_code_id nvarchar(10)  , @Servicio_profesional_ID int 
     
AS  

DECLARE @res varchar(max)= NULL
,@curriculum nvarchar(10)
,@porcentaje nvarchar(10)
,@date datetime
, @time datetime
select  @date = dbo.fnMakeDate( getdate() )
	, @time = dbo.fnMakeTime( getdate() )

--TABLA TEMPORAL CON UNA COLUMNA
DECLARE @t TABLE ( PEOPLE_CODE_ID nvarchar(10),PEOPLE_ID NVARCHAR(9),CURRICULUM NVARCHAR(10),MATRIC_YEAR NVARCHAR(4),MATRIC_TERM NVARCHAR(10),MATRIC_SESSION NVARCHAR(15),DEGREE NVARCHAR(10),PROGRAM NVARCHAR(10),RECORD_TYPE NVARCHAR(10),CREDCUBIERTOS numeric(8,4),CREDPLAN numeric(8,4),porcentaje NUMERIC(8,2))

INSERT INTO @t 
EXEC dbo.ACC_spcreditosobtenidos_SS @people = @people_code_id
--ASIGNANDO EL VALOR DE LA COLUMNA A LA VARIABLE.
if(@Servicio_profesional_ID in (1,2))
begin 
select t1.* from (
			SELECT td.*,(SELECT TOP(1)  cc.FORMAL_TITLE 
								FROM CODE_CURRICULUM AS CC WITH(NOLOCK) 
								WHERE CC.CODE_VALUE_KEY=td.CURRICULUM ) as carrera_nom 
					,isnull((SELECT TOP(1) TS.CHECK_SS FROM ACC_STDDEGREQ_SS AS TS WITH(NOLOCK) 
								WHERE TS.PEOPLE_CODE_ID=TD.PEOPLE_CODE_ID 
								AND TS.MATRIC_YEAR=TD.MATRIC_YEAR
								AND TS.MATRIC_TERM=TD.MATRIC_TERM
								AND TS.CURRICULUM=TD.CURRICULUM
								AND TS.MATRIC_SESSION=TD.MATRIC_SESSION
								AND TS.DEGREE=TD.DEGREE
								AND TS.PROGRAM=TD.PROGRAM
								and ts.Servicio_Profesional_Id=@Servicio_profesional_ID
					),0) AS check_ss
					
					,isnull((SELECT TOP(1) ss.ESTATUS_ID 
								FROM ACC_STDDEGREQ_SS AS TS WITH(NOLOCK) 
								left join  Acc_Progress_Status_SP as ss on 
								ts.ACC_STDDEGREQ_SS_ID= ss.ACC_STDDEGREQ_SS_ID 
								WHERE TS.PEOPLE_CODE_ID=TD.PEOPLE_CODE_ID 
								AND TS.MATRIC_YEAR=TD.MATRIC_YEAR
								AND TS.MATRIC_TERM=TD.MATRIC_TERM
								AND TS.CURRICULUM=TD.CURRICULUM
								AND TS.MATRIC_SESSION=TD.MATRIC_SESSION
								AND TS.DEGREE=TD.DEGREE
								AND TS.PROGRAM=TD.PROGRAM
								and ts.Servicio_Profesional_Id=@Servicio_profesional_ID
								and ss.ESTATUS_ID=20
					),0) AS check_progres

					,ISNULL((SELECT   ( CASE  WHEN TS.CURRICULUM in ('15','28') AND SUM(sh.Horas)>=1400 
										THEN '1'
										WHEN  TS.CURRICULUM NOT in ('15','28') AND SUM(sh.Horas)>=400
										THEN '1'
										ELSE '0'
										END) AS DTTS
						FROM  ACC_Horas_SP AS sh INNER JOIN
                         ACC_STDDEGREQ_SS AS ts ON sh.ACC_STDDEGREQ_SS_ID = ts.ACC_STDDEGREQ_SS_ID
						 WHERE TS.PEOPLE_CODE_ID=TD.PEOPLE_CODE_ID 
								AND TS.MATRIC_YEAR=TD.MATRIC_YEAR
								AND TS.MATRIC_TERM=TD.MATRIC_TERM
								AND TS.CURRICULUM=TD.CURRICULUM
								AND TS.MATRIC_SESSION=TD.MATRIC_SESSION
								AND TS.DEGREE=TD.DEGREE
								AND TS.PROGRAM=TD.PROGRAM
								and ts.Servicio_Profesional_Id=@Servicio_profesional_ID
								GROUP BY TS.CURRICULUM
							

								),'0') AS Horas
								
				FROM @t as td 
				where td.DEGREE='LIC'
				) as t1 --where t1.check_ss<>1
					where (t1.check_progres<>20 AND @Servicio_profesional_ID IN ('1','3')) OR ( Horas='0' AND @Servicio_profesional_ID='2')


/*UNION ALL
SELECT TD.PEOPLE_CODE_ID,TD.PEOPLE_ID,'24' AS CURRICULUM, TD.MATRIC_YEAR,TD.MATRIC_TERM,TD.MATRIC_SESSION,TD.DEGREE,TD.PROGRAM,TD.RECORD_TYPE,TD.CREDCUBIERTOS,TD.CREDPLAN,TD.porcentaje,(SELECT TOP(1)  cc.FORMAL_TITLE FROM CODE_CURRICULUM AS CC WITH(NOLOCK) WHERE CC.CODE_VALUE_KEY='24') as carrera_nom FROM @t as td
*/
if exists (SELECT * FROM @t)
	begin	

			IF NOT EXISTS (SELECT * FROM ACC_STDDEGREQ_SS AS ST INNER JOIN  @t AS T1 ON T1.PEOPLE_CODE_ID=ST.PEOPLE_CODE_ID AND T1.CURRICULUM=ST.CURRICULUM AND st.DEGREE='LIC' and st.Servicio_Profesional_Id=@Servicio_profesional_ID )
			BEGIN
			
			INSERT INTO [dbo].[ACC_STDDEGREQ_SS] (
												  [PEOPLE_CODE_ID]
												  ,[MATRIC_YEAR]
												  ,[MATRIC_TERM]
												  ,[RECORD_TYPE]
												  ,[PROGRAM]
												  ,[DEGREE]
												  ,[CURRICULUM]
												  ,[MATRIC_SESSION]
												  ,[PEOPLE_ID]
												  ,[CREDCUBIERTOS]
												  ,[CREDPLAN]
												  ,[PORCENTAJE]
												  ,[PROMEDIO]
												  ,[RVOE]
												  ,[FECHA_RVOE]
												  ,[PRIMER_NOMBRE]
												  ,[SEGUNDO_NOMBRE]
												  ,[APELLIDO_PATERNO]
												  ,[APELLIDO_MATERNO]
												  ,[SEXO]
												  ,[EDAD]
												  ,[ESTADO_CIVIL]
												  ,[CALLE]
												  ,[NUMERO_INTERIOR]
												  ,[NUMERO_EXTERIOR]
												  ,[COLONIA]
												  ,[DELEGACION]
												  ,[CODIGO_POSTAL]
												  ,[TELEFONO_CASA]
												  ,[TELEFONO_CELULAR]
												  ,[TELEFONO_RECADOS]
												  ,[TELEFONO_OFICINA]
												  ,[EXT_OFICINA]
												  ,[CORREO_ELECTRONICO]
												  ,[CHECK_SS]
												  ,[CREATE_DATE]
												  ,[CREATE_TIME]
												  ,[CREATE_OPID]
												  ,[CREATE_TERMINAL]
												  ,[REVISION_DATE]
												  ,[REVISION_TIME]
												  ,[REVISION_OPID]
												  ,[REVISION_TERMINAL]
												  ,[Servicio_Profesional_Id]
												  ,[PLANTEL_IMP]
												  )
												  SELECT t2.PEOPLE_CODE_ID
												  ,t2.MATRIC_YEAR
												  ,t2.MATRIC_TERM
												  ,t2.RECORD_TYPE
												  ,t2.PROGRAM
												  ,t2.DEGREE
												  ,t2.CURRICULUM
												  ,t2.MATRIC_SESSION
												  ,t2.PEOPLE_ID
												  ,t2.CREDCUBIERTOS
												  ,t2.CREDPLAN
												  ,t2.PORCENTAJE
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL												   
												  ,NULL
												  ,@date --CREATE_DATE 
												  ,@time --CREATE_TIME
												  ,'SCTCONV' CREATE_OPID
												  ,'0001' CREATE_TERMINAL
												  ,@date --REVISION_DATE 
												  ,@time --REVISION_TIME
												  ,'SCTCONV' REVISION_OPID
												  ,'0001' REVISION_TERMINAL
												  ,@Servicio_profesional_ID
												  ,(SELECT TOP(1) CODE_ACASESSION.MEDIUM_DESC FROM CODE_ACASESSION WHERE CODE_ACASESSION.CODE_VALUE_KEY=T2.MATRIC_SESSION )
												  FROM @t as t2
												  where t2.DEGREE='LIC'
			END 
	end
	end


if(@Servicio_profesional_ID in (3))
begin 
select t1.* from (
			SELECT td.*,(SELECT TOP(1)  cc.FORMAL_TITLE 
								FROM CODE_CURRICULUM AS CC WITH(NOLOCK) 
								WHERE CC.CODE_VALUE_KEY=td.CURRICULUM ) as carrera_nom 
					,isnull((SELECT TOP(1) TS.CHECK_SS FROM ACC_STDDEGREQ_SS AS TS WITH(NOLOCK) 
								WHERE TS.PEOPLE_CODE_ID=TD.PEOPLE_CODE_ID 
								AND TS.MATRIC_YEAR=TD.MATRIC_YEAR
								AND TS.MATRIC_TERM=TD.MATRIC_TERM
								AND TS.CURRICULUM=TD.CURRICULUM
								AND TS.MATRIC_SESSION=TD.MATRIC_SESSION
								AND TS.DEGREE=TD.DEGREE
								AND TS.PROGRAM=TD.PROGRAM
								and ts.Servicio_Profesional_Id=@Servicio_profesional_ID
					),0) AS check_ss
					
					,isnull((SELECT TOP(1) ss.ESTATUS_ID 
								FROM ACC_STDDEGREQ_SS AS TS WITH(NOLOCK) 
								left join  Acc_Progress_Status_SP as ss on 
								ts.ACC_STDDEGREQ_SS_ID= ss.ACC_STDDEGREQ_SS_ID 
								WHERE TS.PEOPLE_CODE_ID=TD.PEOPLE_CODE_ID 
								AND TS.MATRIC_YEAR=TD.MATRIC_YEAR
								AND TS.MATRIC_TERM=TD.MATRIC_TERM
								AND TS.CURRICULUM=TD.CURRICULUM
								AND TS.MATRIC_SESSION=TD.MATRIC_SESSION
								AND TS.DEGREE=TD.DEGREE
								AND TS.PROGRAM=TD.PROGRAM
								and ts.Servicio_Profesional_Id=@Servicio_profesional_ID
								and ss.ESTATUS_ID=20
					),0) AS check_progres

					,ISNULL((SELECT   ( CASE  WHEN TS.CURRICULUM in ('15','28') AND SUM(sh.Horas)>=1400 
										THEN '1'
										WHEN  TS.CURRICULUM NOT in ('15','28') AND SUM(sh.Horas)>=400
										THEN '1'
										ELSE '0'
										END) AS DTTS
						FROM  ACC_Horas_SP AS sh INNER JOIN
                         ACC_STDDEGREQ_SS AS ts ON sh.ACC_STDDEGREQ_SS_ID = ts.ACC_STDDEGREQ_SS_ID
						 WHERE TS.PEOPLE_CODE_ID=TD.PEOPLE_CODE_ID 
								AND TS.MATRIC_YEAR=TD.MATRIC_YEAR
								AND TS.MATRIC_TERM=TD.MATRIC_TERM
								AND TS.CURRICULUM=TD.CURRICULUM
								AND TS.MATRIC_SESSION=TD.MATRIC_SESSION
								AND TS.DEGREE=TD.DEGREE
								AND TS.PROGRAM=TD.PROGRAM
								and ts.Servicio_Profesional_Id=@Servicio_profesional_ID
								GROUP BY TS.CURRICULUM
							

								),'0') AS Horas
								
				FROM @t as td 
				
				) as t1 --where t1.check_ss<>1
					where (t1.check_progres<>20 AND @Servicio_profesional_ID IN ('1','3')) OR ( Horas='0' AND @Servicio_profesional_ID='2')


/*UNION ALL
SELECT TD.PEOPLE_CODE_ID,TD.PEOPLE_ID,'24' AS CURRICULUM, TD.MATRIC_YEAR,TD.MATRIC_TERM,TD.MATRIC_SESSION,TD.DEGREE,TD.PROGRAM,TD.RECORD_TYPE,TD.CREDCUBIERTOS,TD.CREDPLAN,TD.porcentaje,(SELECT TOP(1)  cc.FORMAL_TITLE FROM CODE_CURRICULUM AS CC WITH(NOLOCK) WHERE CC.CODE_VALUE_KEY='24') as carrera_nom FROM @t as td
*/
if exists (SELECT * FROM @t)
	begin	

			IF NOT EXISTS (SELECT * FROM ACC_STDDEGREQ_SS AS ST INNER JOIN  @t AS T1 ON T1.PEOPLE_CODE_ID=ST.PEOPLE_CODE_ID AND T1.CURRICULUM=ST.CURRICULUM  and st.Servicio_Profesional_Id=@Servicio_profesional_ID )
			BEGIN
			
			INSERT INTO [dbo].[ACC_STDDEGREQ_SS] (
												  [PEOPLE_CODE_ID]
												  ,[MATRIC_YEAR]
												  ,[MATRIC_TERM]
												  ,[RECORD_TYPE]
												  ,[PROGRAM]
												  ,[DEGREE]
												  ,[CURRICULUM]
												  ,[MATRIC_SESSION]
												  ,[PEOPLE_ID]
												  ,[CREDCUBIERTOS]
												  ,[CREDPLAN]
												  ,[PORCENTAJE]
												  ,[PROMEDIO]
												  ,[RVOE]
												  ,[FECHA_RVOE]
												  ,[PRIMER_NOMBRE]
												  ,[SEGUNDO_NOMBRE]
												  ,[APELLIDO_PATERNO]
												  ,[APELLIDO_MATERNO]
												  ,[SEXO]
												  ,[EDAD]
												  ,[ESTADO_CIVIL]
												  ,[CALLE]
												  ,[NUMERO_INTERIOR]
												  ,[NUMERO_EXTERIOR]
												  ,[COLONIA]
												  ,[DELEGACION]
												  ,[CODIGO_POSTAL]
												  ,[TELEFONO_CASA]
												  ,[TELEFONO_CELULAR]
												  ,[TELEFONO_RECADOS]
												  ,[TELEFONO_OFICINA]
												  ,[EXT_OFICINA]
												  ,[CORREO_ELECTRONICO]
												  ,[CHECK_SS]
												  ,[CREATE_DATE]
												  ,[CREATE_TIME]
												  ,[CREATE_OPID]
												  ,[CREATE_TERMINAL]
												  ,[REVISION_DATE]
												  ,[REVISION_TIME]
												  ,[REVISION_OPID]
												  ,[REVISION_TERMINAL]
												  ,[Servicio_Profesional_Id]
												   ,[PLANTEL_IMP]
												  )
												  SELECT t2.PEOPLE_CODE_ID
												  ,t2.MATRIC_YEAR
												  ,t2.MATRIC_TERM
												  ,t2.RECORD_TYPE
												  ,t2.PROGRAM
												  ,t2.DEGREE
												  ,t2.CURRICULUM
												  ,t2.MATRIC_SESSION
												  ,t2.PEOPLE_ID
												  ,t2.CREDCUBIERTOS
												  ,t2.CREDPLAN
												  ,t2.PORCENTAJE
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL
												  ,NULL												   
												  ,NULL
												  ,@date --CREATE_DATE 
												  ,@time --CREATE_TIME
												  ,'SCTCONV' CREATE_OPID
												  ,'0001' CREATE_TERMINAL
												  ,@date --REVISION_DATE 
												  ,@time --REVISION_TIME
												  ,'SCTCONV' REVISION_OPID
												  ,'0001' REVISION_TERMINAL
												  ,@Servicio_profesional_ID
												  ,(SELECT TOP(1) CODE_ACASESSION.MEDIUM_DESC FROM CODE_ACASESSION WHERE CODE_ACASESSION.CODE_VALUE_KEY=T2.MATRIC_SESSION )
												  FROM @t as t2
												  
			END 
	end
	end











GO
/****** Object:  StoredProcedure [dbo].[ACC_serviciosocialdetalle_SS]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ACC_serviciosocialdetalle_SS]  @people_code_id nvarchar(10)
,@MATRIC_YEAR nvarchar(4),@MATRIC_TERM nvarchar(10),@RECORD_TYPE nvarchar(1),@PROGRAM nvarchar(10)
,@DEGREE nvarchar(6),@CURRICULUM nvarchar(6),@MatricSession nvarchar(15), @TIPO INT , @Servicio_profesional_ID int 

     
AS  

DECLARE @date datetime
, @time datetime
select  @date = dbo.fnMakeDate( getdate() )
	, @time = dbo.fnMakeTime( getdate() )

if(@people_code_id is not null and @curriculum is not null)
begin 
	if exists(select * FROM STDDEGREQ AS std 						 
						where std.PEOPLE_CODE_ID=@people_code_id
						and std.MATRIC_YEAR=@MATRIC_YEAR
						and std.MATRIC_TERM=@MATRIC_TERM
						and std.RECORD_TYPE=@RECORD_TYPE
						and std.PROGRAM=@PROGRAM
						and std.DEGREE=@DEGREE
						and std.CURRICULUM=@CURRICULUM
						and std.MatricSession=@MatricSession)
	begin 
	
	declare 
	@creditocubiertos numeric(8,4)
	,@credplan numeric(8,4)
	,@porcentaje numeric(8,4)
	,@promedio numeric(8,2)
	,@rvoe nvarchar(35)
	,@fecha_rvoe datetime
	,@primer_nombre nvarchar(60)
	,@segundo_nombre nvarchar(60)
	,@apellido_paterno nvarchar(60)
	,@apellido_materno nvarchar(60)
	,@ciclo_ingreso nvarchar(15)
	,@turno nvarchar(15)
	,@sexo nvarchar(10)
	,@edad int
	,@estado_civil nvarchar(10)
	,@calle nvarchar(75)
	,@numero_interior nvarchar(75)
	,@numero_exterior nvarchar(75)
	,@colonia nvarchar(75)
	,@delegacion nvarchar(75)
	,@codigo_postal  nvarchar(15)
	,@telefono_casa  nvarchar(30)
	,@telefono_celular  nvarchar(30)
	,@telefono_recados  nvarchar(30)
	,@telefono_oficina  nvarchar(30)
	,@ext_oficina  nvarchar(30)
	,@correo_electronico nvarchar(255)
	,@acc_stddegreq_ss_id int
	,@matric_term_progress nvarchar(10)
	,@cv_carrera nvarchar(15)
	,@plantel_imp nvarchar(20)
	if(@TIPO=1)
	begin
	SELECT    

			@promedio=		isnull((SELECT   SUM(CONVERT(NUMERIC(8,4),TD.CAL))AS Expr1
						FROM            STDDEGREQEVENT AS STDE LEFT OUTER JOIN
												 (SELECT *,CASE WHEN ISNUMERIC(FINAL_GRADE)=1 THEN CONVERT(NUMERIC(8,4),FINAL_GRADE)
														ELSE 0 END AS CAL 
												FROM TRANSCRIPTDETAIL 
												)TD
												 ON STDE.PEOPLE_CODE_ID = TD.PEOPLE_CODE_ID AND (STDE.EVENT_ID = TD.EVENT_ID OR STDE.TAKEN_EVENT_ID=TD.EVENT_ID)
						WHERE  
						(STDE.PEOPLE_CODE_ID = STD.PEOPLE_CODE_ID) 
						AND (STDE.PROGRAM = STD.PROGRAM) 
						AND (STDE.DEGREE = STD.DEGREE) 
						AND (STDE.CURRICULUM = STD.CURRICULUM) 
						AND (STDE.MATRIC_YEAR = STD.MATRIC_YEAR) 
						AND (STDE.MATRIC_TERM = STD.MATRIC_TERM)	
						AND ((
							 STDE.TAKEN_EVENT_ID = TD.EVENT_ID 
							AND STDE.TAKEN_YEAR = TD.ACADEMIC_YEAR
							AND STDE.TAKEN_TERM = TD.ACADEMIC_TERM
							AND (STDE.TAKEN_SUB_TYPE = TD.EVENT_SUB_TYPE )
							AND STDE.TAKEN_SECTION = TD.SECTION	
							and TD.CAL>=6.0 
							--and ISNUMERIC(td.FINAL_GRADE)=1
							AND( STDE.STATUS='C' OR STDE.STATUS='X') )
							OR
							(
							(STDE.EVENT_ID IN ( SELECT TD.EVENT_ID FROM (SELECT *,CASE WHEN ISNUMERIC(FINAL_GRADE)=1 THEN CONVERT(NUMERIC(8,4),FINAL_GRADE)
											ELSE 0 END AS CAL 
									FROM TRANSCRIPTDETAIL 
									)TD 
																	WHERE
							TD.PEOPLE_CODE_ID=STD.PEOPLE_CODE_ID
							AND TD.EVENT_SUB_TYPE IN ('LAB','TALL','COC') 
							--and ISNUMERIC(td.FINAL_GRADE)=1
							and TD.CAL>=6.0 
							AND TD.ADD_DROP_WAIT='A' 
							AND (TD.HONORS IS NULL OR TD.HONORS=' '))
							)))

						),0)/isnull((SELECT        COUNT(STDE.EVENT_ID) AS Expr1
						FROM            STDDEGREQEVENT AS STDE LEFT OUTER JOIN
												 (SELECT *,CASE WHEN ISNUMERIC(FINAL_GRADE)=1 THEN CONVERT(NUMERIC(8,4),FINAL_GRADE)
											ELSE 0 END AS CAL 
									FROM TRANSCRIPTDETAIL 
									)TD
												 ON STDE.PEOPLE_CODE_ID = TD.PEOPLE_CODE_ID AND (STDE.EVENT_ID = TD.EVENT_ID OR STDE.TAKEN_EVENT_ID=TD.EVENT_ID)
						WHERE  
						(STDE.PEOPLE_CODE_ID = STD.PEOPLE_CODE_ID) 
						AND (STDE.PROGRAM = STD.PROGRAM) 
						AND (STDE.DEGREE = STD.DEGREE) 
						AND (STDE.CURRICULUM = STD.CURRICULUM) 
						AND (STDE.MATRIC_YEAR = STD.MATRIC_YEAR) 
						AND (STDE.MATRIC_TERM = STD.MATRIC_TERM)
	
						AND ((
							 STDE.TAKEN_EVENT_ID = TD.EVENT_ID 
							AND STDE.TAKEN_YEAR = TD.ACADEMIC_YEAR
							AND STDE.TAKEN_TERM = TD.ACADEMIC_TERM
							AND (STDE.TAKEN_SUB_TYPE = TD.EVENT_SUB_TYPE )
							AND STDE.TAKEN_SECTION = TD.SECTION	
							and TD.CAL>=6 
	
							AND( STDE.STATUS='C' OR STDE.STATUS='X') )
							OR
							(
							(STDE.EVENT_ID IN ( SELECT TD.EVENT_ID FROM (SELECT *,CASE WHEN ISNUMERIC(FINAL_GRADE)=1 THEN CONVERT(NUMERIC(8,4),FINAL_GRADE)
											ELSE 0 END AS CAL 
									FROM TRANSCRIPTDETAIL 
									) TD WHERE
							TD.PEOPLE_CODE_ID=STD.PEOPLE_CODE_ID
							AND TD.EVENT_SUB_TYPE IN ('LAB','TALL','COC') 
								and TD.CAL>=6 
							--AND TD.ADD_DROP_WAIT='A' 
							AND (TD.HONORS IS NULL OR TD.HONORS=' '))
							)))
						),1)
			,@rvoe =(Case  
			   when CHARINDEX (RVOE.AgreementNumber, RVOE.AgreementNumber)=0 then RVOE.AgreementNumber
			   Else LEFT (RVOE.AgreementNumber, CHARINDEX ('|', RVOE.AgreementNumber)-1) 
			  END)
			  ,@fecha_rvoe =Rvoe.AgreementDate 
			  ,@primer_nombre=PE.FIRST_NAME
			,@segundo_nombre=PE.MIDDLE_NAME 
			,@apellido_paterno=PE.LAST_NAME
			,@apellido_materno=PE.Last_Name_Prefix 
			,@ciclo_ingreso=(SELECT TOP(1) ac.ACADEMIC_YEAR+'-'+ac.ACADEMIC_TERM 
							FROM ACADEMIC AS AC inner join CODE_ACATERM as cd on ac.ACADEMIC_TERM=cd.CODE_VALUE_KEY  
							WHERE AC.PEOPLE_CODE_ID=PE.PEOPLE_CODE_ID AND AC.CURRICULUM=STD.CURRICULUM AND AC.FULL_PART<>''  
							AND CD.STATUS='A'
							AND ISNUMERIC(CD.CODE_XVAL)=1 
							ORDER BY AC.ACADEMIC_YEAR ASC, CD.SORT_ORDER ASC)
			,@matric_term_progress=(SELECT COUNT(*) AS FFG 
									FROM ACADEMIC AS AC 
									WHERE AC.PEOPLE_CODE_ID=std.PEOPLE_CODE_ID 
									AND AC.CURRICULUM=std.CURRICULUM 
									AND AC.POPULATION IN ('REINGR','NVOING') 
									AND AC.ACADEMIC_SESSION NOT IN ('','SELVAL') 
									AND AC.ACADEMIC_TERM IN ('CUATRI1','CUATRI2','CUATRI3','SEMES1','SEMES2','ANUAL')
									AND AC.PRIMARY_FLAG='Y' )
			,@turno=(SELECT TOP(1) CF.LONG_DESC FROM ACADEMIC AS AC INNER JOIN CODE_FULLPARTFLAG AS CF ON AC.FULL_PART=CF.CODE_VALUE_KEY  WHERE AC.PEOPLE_CODE_ID=PE.PEOPLE_CODE_ID AND AC.CURRICULUM=STD.CURRICULUM AND AC.FULL_PART<>''  ORDER BY AC.ACADEMIC_YEAR DESC, AC.ACADEMIC_TERM DESC)
			,@sexo=(select top(1) dm.GENDER 
					from DEMOGRAPHICS dm 
					where dm.PEOPLE_CODE_ID=PE.PEOPLE_CODE_ID 
					and dm.ACADEMIC_SESSION=''
					and dm.ACADEMIC_YEAR='' 
					and dm.ACADEMIC_TERM='')

			,@edad=(((365* year(getdate()))-(365*(year(PE.BIRTH_DATE))))+ (month(getdate())-month(PE.BIRTH_DATE))*30+(day(getdate()) - day(PE.BIRTH_DATE)))/365
			,@estado_civil=( select top(1) dm.MARITAL_STATUS 
					from DEMOGRAPHICS dm 
					where dm.PEOPLE_CODE_ID=PE.PEOPLE_CODE_ID 
					and dm.ACADEMIC_SESSION=''
					and dm.ACADEMIC_YEAR='' 
					and dm.ACADEMIC_TERM='')

			,@calle=(select top(1) ISNULL(ADDRESSSCHEDULE.ADDRESS_LINE_1,' ') as ddr
			FROM            ADDRESSSCHEDULE
									WHERE        (ADDRESSSCHEDULE.PEOPLE_ORG_CODE_ID = pe.PEOPLE_CODE_ID) 
									AND (ADDRESSSCHEDULE.ADDRESS_TYPE = 'CASA')
									)
			,@numero_interior=(select top(1) ISNULL(ADDRESSSCHEDULE.ADDRESS_LINE_2,' ') as ddr
			FROM            ADDRESSSCHEDULE
									WHERE        (ADDRESSSCHEDULE.PEOPLE_ORG_CODE_ID = pe.PEOPLE_CODE_ID) 
									AND (ADDRESSSCHEDULE.ADDRESS_TYPE = 'CASA')
									)
			,@numero_exterior=(select top(1) ISNULL(ADDRESSSCHEDULE.HOUSE_NUMBER,' ') as ddr
			FROM            ADDRESSSCHEDULE
									WHERE        (ADDRESSSCHEDULE.PEOPLE_ORG_CODE_ID = pe.PEOPLE_CODE_ID) 
									AND (ADDRESSSCHEDULE.ADDRESS_TYPE = 'CASA')
									)
			,@colonia=(select top(1) ISNULL(ADDRESSSCHEDULE.CITY,' ') as ddr
			FROM            ADDRESSSCHEDULE
									WHERE        (ADDRESSSCHEDULE.PEOPLE_ORG_CODE_ID = pe.PEOPLE_CODE_ID) 
									AND (ADDRESSSCHEDULE.ADDRESS_TYPE = 'CASA')
									)
			,@delegacion=(select top(1) ISNULL(ADDRESSSCHEDULE.ADDRESS_LINE_3,' ') as ddr
			FROM            ADDRESSSCHEDULE
									WHERE        (ADDRESSSCHEDULE.PEOPLE_ORG_CODE_ID = pe.PEOPLE_CODE_ID) 
									AND (ADDRESSSCHEDULE.ADDRESS_TYPE = 'CASA')
									) 
			,@codigo_postal=(select top(1) ISNULL(ADDRESSSCHEDULE.ZIP_CODE,' ') as ddr
			FROM            ADDRESSSCHEDULE
									WHERE        (ADDRESSSCHEDULE.PEOPLE_ORG_CODE_ID = pe.PEOPLE_CODE_ID) 
									AND (ADDRESSSCHEDULE.ADDRESS_TYPE = 'CASA')
									)
			 ,@telefono_casa=(SELECT        TOP (1) PhoneNumber
										   FROM            PersonPhone AS perp                              					  
										   WHERE        (perp.PersonId = pe.PersonId) AND (perp.PhoneType = 'CASA')) 
			 ,@telefono_celular=(SELECT        TOP (1) PhoneNumber
										   FROM            PersonPhone AS perp                              					  
										   WHERE        (perp.PersonId = pe.PersonId) AND (perp.PhoneType = 'CELULAR')) 
			 ,@telefono_recados=(SELECT        TOP (1) PhoneNumber
										   FROM            PersonPhone AS perp                              					  
										   WHERE        (perp.PersonId = pe.PersonId) AND (perp.PhoneType = 'RECADOS'))
			 ,@telefono_oficina=(SELECT        TOP (1) PhoneNumber
										   FROM            PersonPhone AS perp                              					  
										   WHERE        (perp.PersonId = pe.PersonId) AND (perp.PhoneType = 'OFICINA'))
			,@ext_oficina='' 
			,@cv_carrera= (select top(1) cc.CLAVE_CARRERA from CARRERA_CERTIFICADO as cc where cc.CURRICULUM=std.CURRICULUM) 
			,@correo_electronico=(select top(1) ISNULL(ADDRESSSCHEDULE.EMAIL_ADDRESS,' ') as ddr
			FROM            ADDRESSSCHEDULE
									WHERE        (ADDRESSSCHEDULE.PEOPLE_ORG_CODE_ID = pe.PEOPLE_CODE_ID) 
									AND (ADDRESSSCHEDULE.ADDRESS_TYPE = 'CASA')
									) 
			FROM            STDDEGREQ AS std INNER JOIN
									 DEGREQ ON std.MATRIC_YEAR = DEGREQ.MATRIC_YEAR AND std.MATRIC_TERM = DEGREQ.MATRIC_TERM AND std.PROGRAM = DEGREQ.PROGRAM AND std.DEGREE = DEGREQ.DEGREE AND 
									 std.CURRICULUM = DEGREQ.CURRICULUM INNER JOIN
									 Rvoe ON DEGREQ.DegreeRequirementId = Rvoe.DegreeRequirementId INNER JOIN
									 ORGANIZATION ON Rvoe.OrgCodeId = ORGANIZATION.ORG_CODE_ID INNER JOIN
									 CODE_ACASESSION ON ORGANIZATION.ORG_IDENTIFIER = CODE_ACASESSION.CODE_XVAL AND std.MatricSession = CODE_ACASESSION.CODE_VALUE_KEY
									 INNER JOIN PEOPLE AS PE ON STD.PEOPLE_CODE_ID=PE.PEOPLE_CODE_ID
						 
			where std.PEOPLE_CODE_ID=@people_code_id
			and std.MATRIC_YEAR=@MATRIC_YEAR
			and std.MATRIC_TERM=@MATRIC_TERM
			and std.RECORD_TYPE=@RECORD_TYPE
			and std.PROGRAM=@PROGRAM
			and std.DEGREE=@DEGREE
			and std.CURRICULUM=@CURRICULUM
			and std.MatricSession=@MatricSession	
	

	select top(1) @acc_stddegreq_ss_id=dt.ACC_STDDEGREQ_SS_ID from ACC_STDDEGREQ_SS as dt  
									where dt.PEOPLE_CODE_ID=@people_code_id
									and dt.MATRIC_YEAR=@MATRIC_YEAR
									and dt.MATRIC_TERM=@MATRIC_TERM
									and dt.RECORD_TYPE=@RECORD_TYPE
									and dt.PROGRAM=@PROGRAM
									and dt.DEGREE=@DEGREE
									and dt.CURRICULUM=@CURRICULUM
									and dt.MATRIC_SESSION=@MatricSession
									and dt.Servicio_Profesional_Id=@Servicio_profesional_ID	
select top(1) @plantel_imp=CODE_ACASESSION.MEDIUM_DESC from CODE_ACASESSION where CODE_ACASESSION.CODE_VALUE_KEY=@MatricSession
							if(@acc_stddegreq_ss_id is not null)
							begin
							update ACC_STDDEGREQ_SS
							set [PROMEDIO]=convert(numeric(8,1),round(@promedio,1))
							  ,[RVOE]=@rvoe
							  ,[FECHA_RVOE]=@fecha_rvoe
							  ,[PRIMER_NOMBRE]=@primer_nombre
							  ,[SEGUNDO_NOMBRE]=@segundo_nombre
							  ,[APELLIDO_PATERNO]=@apellido_paterno
							  ,[APELLIDO_MATERNO]=@apellido_materno
							  ,[CICLO_INGRESO]=@ciclo_ingreso
							  ,[TURNO]=@turno
							  ,[SEXO]=@sexo
							  ,[EDAD]=@edad
							  ,[ESTADO_CIVIL]=@estado_civil
							  ,[CALLE]=@calle
							  ,[NUMERO_INTERIOR]=@numero_interior
							  ,[NUMERO_EXTERIOR]=@numero_exterior
							  ,[COLONIA]=@colonia
							  ,[DELEGACION]=@delegacion
							  ,[CODIGO_POSTAL]=@codigo_postal
							  ,[TELEFONO_CASA]=@telefono_casa
							  ,[TELEFONO_CELULAR]=@telefono_celular
							  ,[TELEFONO_RECADOS]=@telefono_recados
							  ,[TELEFONO_OFICINA]=@telefono_oficina
							  ,[EXT_OFICINA]=@ext_oficina
							  ,[CORREO_ELECTRONICO]=@correo_electronico      
							  ,[REVISION_DATE]=@date
							  ,[REVISION_TIME]=@time
							  ,[CV_CARRERA]=@cv_carrera
							  ,[PLANTEL_IMP]=@plantel_imp
							  where 
							  ACC_STDDEGREQ_SS.ACC_STDDEGREQ_SS_ID=@acc_stddegreq_ss_id
							  end  	
	
	  end

	  if(@TIPO=2)
	begin
	SELECT    
				   @creditocubiertos= isnull((SELECT SUM(EV.CREDITS) 
					FROM STDDEGREQEVENT STDE WITH (NOLOCK)
					, EVENT EV WITH (NOLOCK)
					WHERE STDE.EVENT_ID = EV.EVENT_ID
					AND STDE.EVENT_SUB_TYPE = EV.EVENT_TYPE
					AND STDE.PEOPLE_CODE_ID = std.PEOPLE_CODE_ID
					AND STDE.PROGRAM = std.PROGRAM
					AND STDE.DEGREE = std.DEGREE
					AND STDE.CURRICULUM = std.CURRICULUM
					AND STDE.MATRIC_YEAR = std.MATRIC_YEAR
					AND STDE.MATRIC_TERM = std.MATRIC_TERM
					AND ((STDE.STATUS = 'C') 
						OR(STDE.EVENT_ID=(SELECT TD.EVENT_ID 
																FROM (SELECT *,CASE WHEN ISNUMERIC(FINAL_GRADE)=1 THEN CONVERT(NUMERIC(10,2),FINAL_GRADE)
																			ELSE 0 END AS CAL 
																FROM TRANSCRIPTDETAIL WITH (NOLOCK)
																)TD
																WHERE TD.EVENT_ID=STDE.EVENT_ID 
																AND TD.EVENT_SUB_TYPE IN ('LAB','TALL','COC') 
																AND TD.PEOPLE_CODE_ID=std.PEOPLE_CODE_ID                                               
																and TD.CAL>=6.0  
																and TD.ADD_DROP_WAIT='A'
																AND( TD.HONORS=' ' OR  TD.HONORS IS NULL))))
       

										),0)

										, @credplan= ISNULL((SELECT TOP(1) IIF(ST.CREDIT_MIN=0,NULL,ST.CREDIT_MIN) CREDITOS FROM STDDEGREQ ST WITH (NOLOCK) WHERE ST.MATRIC_YEAR=std.MATRIC_YEAR 
                                           AND ST.MATRIC_TERM=std.MATRIC_TERM AND ST.CURRICULUM=std.CURRICULUM 
                                           AND ST.DEGREE=std.DEGREE AND ST.PROGRAM=std.PROGRAM
                                           AND ST.PEOPLE_CODE_ID= std.PEOPLE_CODE_ID
                                           ),
                                           (SELECT TOP(1) ST.CREDIT_MIN FROM DEGREQ ST WITH (NOLOCK) WHERE ST.MATRIC_YEAR=std.MATRIC_YEAR 
                                           AND ST.MATRIC_TERM=std.MATRIC_TERM AND ST.CURRICULUM=std.CURRICULUM 
                                           AND ST.DEGREE=std.DEGREE AND ST.PROGRAM=std.PROGRAM)                            
                                           )  




			,@promedio=		isnull((SELECT   SUM(CONVERT(NUMERIC(8,4),TD.CAL))AS Expr1
						FROM            STDDEGREQEVENT AS STDE LEFT OUTER JOIN
												 (SELECT *,CASE WHEN ISNUMERIC(FINAL_GRADE)=1 THEN CONVERT(NUMERIC(8,4),FINAL_GRADE)
														ELSE 0 END AS CAL 
												FROM TRANSCRIPTDETAIL 
												)TD
												 ON STDE.PEOPLE_CODE_ID = TD.PEOPLE_CODE_ID AND (STDE.EVENT_ID = TD.EVENT_ID OR STDE.TAKEN_EVENT_ID=TD.EVENT_ID)
						WHERE  
						(STDE.PEOPLE_CODE_ID = STD.PEOPLE_CODE_ID) 
						AND (STDE.PROGRAM = STD.PROGRAM) 
						AND (STDE.DEGREE = STD.DEGREE) 
						AND (STDE.CURRICULUM = STD.CURRICULUM) 
						AND (STDE.MATRIC_YEAR = STD.MATRIC_YEAR) 
						AND (STDE.MATRIC_TERM = STD.MATRIC_TERM)	
						AND ((
							 STDE.TAKEN_EVENT_ID = TD.EVENT_ID 
							AND STDE.TAKEN_YEAR = TD.ACADEMIC_YEAR
							AND STDE.TAKEN_TERM = TD.ACADEMIC_TERM
							AND (STDE.TAKEN_SUB_TYPE = TD.EVENT_SUB_TYPE )
							AND STDE.TAKEN_SECTION = TD.SECTION	
							and TD.CAL>=6.0 
							--and ISNUMERIC(td.FINAL_GRADE)=1
							AND( STDE.STATUS='C' OR STDE.STATUS='X') )
							OR
							(
							(STDE.EVENT_ID IN ( SELECT TD.EVENT_ID FROM (SELECT *,CASE WHEN ISNUMERIC(FINAL_GRADE)=1 THEN CONVERT(NUMERIC(8,4),FINAL_GRADE)
											ELSE 0 END AS CAL 
									FROM TRANSCRIPTDETAIL 
									)TD 
																	WHERE
							TD.PEOPLE_CODE_ID=STD.PEOPLE_CODE_ID
							AND TD.EVENT_SUB_TYPE IN ('LAB','TALL','COC') 
							--and ISNUMERIC(td.FINAL_GRADE)=1
							and TD.CAL>=6.0 
							AND TD.ADD_DROP_WAIT='A' 
							AND (TD.HONORS IS NULL OR TD.HONORS=' '))
							)))

						),0)/isnull((SELECT        COUNT(STDE.EVENT_ID) AS Expr1
						FROM            STDDEGREQEVENT AS STDE LEFT OUTER JOIN
												 (SELECT *,CASE WHEN ISNUMERIC(FINAL_GRADE)=1 THEN CONVERT(NUMERIC(8,4),FINAL_GRADE)
											ELSE 0 END AS CAL 
									FROM TRANSCRIPTDETAIL 
									)TD
												 ON STDE.PEOPLE_CODE_ID = TD.PEOPLE_CODE_ID AND (STDE.EVENT_ID = TD.EVENT_ID OR STDE.TAKEN_EVENT_ID=TD.EVENT_ID)
						WHERE  
						(STDE.PEOPLE_CODE_ID = STD.PEOPLE_CODE_ID) 
						AND (STDE.PROGRAM = STD.PROGRAM) 
						AND (STDE.DEGREE = STD.DEGREE) 
						AND (STDE.CURRICULUM = STD.CURRICULUM) 
						AND (STDE.MATRIC_YEAR = STD.MATRIC_YEAR) 
						AND (STDE.MATRIC_TERM = STD.MATRIC_TERM)
	
						AND ((
							 STDE.TAKEN_EVENT_ID = TD.EVENT_ID 
							AND STDE.TAKEN_YEAR = TD.ACADEMIC_YEAR
							AND STDE.TAKEN_TERM = TD.ACADEMIC_TERM
							AND (STDE.TAKEN_SUB_TYPE = TD.EVENT_SUB_TYPE )
							AND STDE.TAKEN_SECTION = TD.SECTION	
							and TD.CAL>=6 
	
							AND( STDE.STATUS='C' OR STDE.STATUS='X') )
							OR
							(
							(STDE.EVENT_ID IN ( SELECT TD.EVENT_ID FROM (SELECT *,CASE WHEN ISNUMERIC(FINAL_GRADE)=1 THEN CONVERT(NUMERIC(8,4),FINAL_GRADE)
											ELSE 0 END AS CAL 
									FROM TRANSCRIPTDETAIL 
									) TD WHERE
							TD.PEOPLE_CODE_ID=STD.PEOPLE_CODE_ID
							AND TD.EVENT_SUB_TYPE IN ('LAB','TALL','COC') 
								and TD.CAL>=6 
							--AND TD.ADD_DROP_WAIT='A' 
							AND (TD.HONORS IS NULL OR TD.HONORS=' '))
							)))
						),1)
			,@rvoe =(Case  
			   when CHARINDEX (RVOE.AgreementNumber, RVOE.AgreementNumber)=0 then RVOE.AgreementNumber
			   Else LEFT (RVOE.AgreementNumber, CHARINDEX ('|', RVOE.AgreementNumber)-1) 
			  END)
			  ,@fecha_rvoe =Rvoe.AgreementDate 
			  ,@primer_nombre=PE.FIRST_NAME
			,@segundo_nombre=PE.MIDDLE_NAME 
			,@apellido_paterno=PE.LAST_NAME
			,@apellido_materno=PE.Last_Name_Prefix 
			,@ciclo_ingreso=(SELECT TOP(1) ac.ACADEMIC_YEAR+'-'+ac.ACADEMIC_TERM 
							FROM ACADEMIC AS AC inner join CODE_ACATERM as cd on ac.ACADEMIC_TERM=cd.CODE_VALUE_KEY  
							WHERE AC.PEOPLE_CODE_ID=PE.PEOPLE_CODE_ID AND AC.CURRICULUM=STD.CURRICULUM AND AC.FULL_PART<>''  
							AND CD.STATUS='A'
							AND ISNUMERIC(CD.CODE_XVAL)=1 
							ORDER BY AC.ACADEMIC_YEAR ASC, CD.SORT_ORDER ASC)
			,@turno=(SELECT TOP(1) CF.LONG_DESC FROM ACADEMIC AS AC INNER JOIN CODE_FULLPARTFLAG AS CF ON AC.FULL_PART=CF.CODE_VALUE_KEY  WHERE AC.PEOPLE_CODE_ID=PE.PEOPLE_CODE_ID AND AC.CURRICULUM=STD.CURRICULUM AND AC.FULL_PART<>''  ORDER BY AC.ACADEMIC_YEAR DESC, AC.ACADEMIC_TERM DESC)
			,@sexo=(select top(1) dm.GENDER 
					from DEMOGRAPHICS dm 
					where dm.PEOPLE_CODE_ID=PE.PEOPLE_CODE_ID 
					and dm.ACADEMIC_SESSION=''
					and dm.ACADEMIC_YEAR='' 
					and dm.ACADEMIC_TERM='')

			,@edad=(((365* year(getdate()))-(365*(year(PE.BIRTH_DATE))))+ (month(getdate())-month(PE.BIRTH_DATE))*30+(day(getdate()) - day(PE.BIRTH_DATE)))/365
			,@estado_civil=( select top(1) dm.MARITAL_STATUS 
					from DEMOGRAPHICS dm 
					where dm.PEOPLE_CODE_ID=PE.PEOPLE_CODE_ID 
					and dm.ACADEMIC_SESSION=''
					and dm.ACADEMIC_YEAR='' 
					and dm.ACADEMIC_TERM='')

			,@calle=(select top(1) ISNULL(ADDRESSSCHEDULE.ADDRESS_LINE_1,' ') as ddr
			FROM            ADDRESSSCHEDULE
									WHERE        (ADDRESSSCHEDULE.PEOPLE_ORG_CODE_ID = pe.PEOPLE_CODE_ID) 
									AND (ADDRESSSCHEDULE.ADDRESS_TYPE = 'CASA')
									)
			,@numero_interior=(select top(1) ISNULL(ADDRESSSCHEDULE.ADDRESS_LINE_2,' ') as ddr
			FROM            ADDRESSSCHEDULE
									WHERE        (ADDRESSSCHEDULE.PEOPLE_ORG_CODE_ID = pe.PEOPLE_CODE_ID) 
									AND (ADDRESSSCHEDULE.ADDRESS_TYPE = 'CASA')
									)
			,@numero_exterior=(select top(1) ISNULL(ADDRESSSCHEDULE.HOUSE_NUMBER,' ') as ddr
			FROM            ADDRESSSCHEDULE
									WHERE        (ADDRESSSCHEDULE.PEOPLE_ORG_CODE_ID = pe.PEOPLE_CODE_ID) 
									AND (ADDRESSSCHEDULE.ADDRESS_TYPE = 'CASA')
									)
			,@colonia=(select top(1) ISNULL(ADDRESSSCHEDULE.CITY,' ') as ddr
			FROM            ADDRESSSCHEDULE
									WHERE        (ADDRESSSCHEDULE.PEOPLE_ORG_CODE_ID = pe.PEOPLE_CODE_ID) 
									AND (ADDRESSSCHEDULE.ADDRESS_TYPE = 'CASA')
									)
			,@delegacion=(select top(1) ISNULL(ADDRESSSCHEDULE.ADDRESS_LINE_3,' ') as ddr
			FROM            ADDRESSSCHEDULE
									WHERE        (ADDRESSSCHEDULE.PEOPLE_ORG_CODE_ID = pe.PEOPLE_CODE_ID) 
									AND (ADDRESSSCHEDULE.ADDRESS_TYPE = 'CASA')
									) 
			,@codigo_postal=(select top(1) ISNULL(ADDRESSSCHEDULE.ZIP_CODE,' ') as ddr
			FROM            ADDRESSSCHEDULE
									WHERE        (ADDRESSSCHEDULE.PEOPLE_ORG_CODE_ID = pe.PEOPLE_CODE_ID) 
									AND (ADDRESSSCHEDULE.ADDRESS_TYPE = 'CASA')
									)
			 ,@telefono_casa=(SELECT        TOP (1) PhoneNumber
										   FROM            PersonPhone AS perp                              					  
										   WHERE        (perp.PersonId = pe.PersonId) AND (perp.PhoneType = 'CASA')) 
			 ,@telefono_celular=(SELECT        TOP (1) PhoneNumber
										   FROM            PersonPhone AS perp                              					  
										   WHERE        (perp.PersonId = pe.PersonId) AND (perp.PhoneType = 'CELULAR')) 
			 ,@telefono_recados=(SELECT        TOP (1) PhoneNumber
										   FROM            PersonPhone AS perp                              					  
										   WHERE        (perp.PersonId = pe.PersonId) AND (perp.PhoneType = 'RECADOS'))
			 ,@telefono_oficina=(SELECT        TOP (1) PhoneNumber
										   FROM            PersonPhone AS perp                              					  
										   WHERE        (perp.PersonId = pe.PersonId) AND (perp.PhoneType = 'OFICINA'))
			,@ext_oficina='' 
			,@cv_carrera= (select top(1) cc.CLAVE_CARRERA from CARRERA_CERTIFICADO as cc where cc.CURRICULUM=std.CURRICULUM) 
			,@correo_electronico=(select top(1) ISNULL(ADDRESSSCHEDULE.EMAIL_ADDRESS,' ') as ddr
			FROM            ADDRESSSCHEDULE
									WHERE        (ADDRESSSCHEDULE.PEOPLE_ORG_CODE_ID = pe.PEOPLE_CODE_ID) 
									AND (ADDRESSSCHEDULE.ADDRESS_TYPE = 'CASA')
									) 
			FROM            STDDEGREQ AS std INNER JOIN
									 DEGREQ ON std.MATRIC_YEAR = DEGREQ.MATRIC_YEAR AND std.MATRIC_TERM = DEGREQ.MATRIC_TERM AND std.PROGRAM = DEGREQ.PROGRAM AND std.DEGREE = DEGREQ.DEGREE AND 
									 std.CURRICULUM = DEGREQ.CURRICULUM INNER JOIN
									 Rvoe ON DEGREQ.DegreeRequirementId = Rvoe.DegreeRequirementId INNER JOIN
									 ORGANIZATION ON Rvoe.OrgCodeId = ORGANIZATION.ORG_CODE_ID INNER JOIN
									 CODE_ACASESSION ON ORGANIZATION.ORG_IDENTIFIER = CODE_ACASESSION.CODE_XVAL AND std.MatricSession = CODE_ACASESSION.CODE_VALUE_KEY
									 INNER JOIN PEOPLE AS PE ON STD.PEOPLE_CODE_ID=PE.PEOPLE_CODE_ID
						 
			where std.PEOPLE_CODE_ID=@people_code_id
			and std.MATRIC_YEAR=@MATRIC_YEAR
			and std.MATRIC_TERM=@MATRIC_TERM
			and std.RECORD_TYPE=@RECORD_TYPE
			and std.PROGRAM=@PROGRAM
			and std.DEGREE=@DEGREE
			and std.CURRICULUM=@CURRICULUM
			and std.MatricSession=@MatricSession	
	

	select top(1) @acc_stddegreq_ss_id=dt.ACC_STDDEGREQ_SS_ID from ACC_STDDEGREQ_SS as dt  
									where dt.PEOPLE_CODE_ID=@people_code_id
									and dt.MATRIC_YEAR=@MATRIC_YEAR
									and dt.MATRIC_TERM=@MATRIC_TERM
									and dt.RECORD_TYPE=@RECORD_TYPE
									and dt.PROGRAM=@PROGRAM
									and dt.DEGREE=@DEGREE
									and dt.CURRICULUM=@CURRICULUM
									and dt.MATRIC_SESSION=@MatricSession	
									and dt.Servicio_Profesional_Id=@Servicio_profesional_ID	
select top(1) @plantel_imp=CODE_ACASESSION.MEDIUM_DESC from CODE_ACASESSION where CODE_ACASESSION.CODE_VALUE_KEY=@MatricSession
							if(@acc_stddegreq_ss_id is not null)
							begin
							update ACC_STDDEGREQ_SS
							set [CREDCUBIERTOS]=convert(numeric(8,0),round(@creditocubiertos,0,1))
							  ,[CREDPLAN]=@credplan
							  ,[PORCENTAJE] =@creditocubiertos/@credplan
							  ,[PROMEDIO]=convert(numeric(8,1),round(@promedio,1))
							  ,[RVOE]=@rvoe
							  ,[FECHA_RVOE]=@fecha_rvoe
							  ,[PRIMER_NOMBRE]=@primer_nombre
							  ,[SEGUNDO_NOMBRE]=@segundo_nombre
							  ,[APELLIDO_PATERNO]=@apellido_paterno
							  ,[APELLIDO_MATERNO]=@apellido_materno
							  ,[CICLO_INGRESO]=@ciclo_ingreso
							  ,[TURNO]=@turno
							  ,[SEXO]=@sexo
							  ,[EDAD]=@edad
							  ,[ESTADO_CIVIL]=@estado_civil
							  ,[CALLE]=@calle
							  ,[NUMERO_INTERIOR]=@numero_interior
							  ,[NUMERO_EXTERIOR]=@numero_exterior
							  ,[COLONIA]=@colonia
							  ,[DELEGACION]=@delegacion
							  ,[CODIGO_POSTAL]=@codigo_postal
							  ,[TELEFONO_CASA]=@telefono_casa
							  ,[TELEFONO_CELULAR]=@telefono_celular
							  ,[TELEFONO_RECADOS]=@telefono_recados
							  ,[TELEFONO_OFICINA]=@telefono_oficina
							  ,[EXT_OFICINA]=@ext_oficina
							  ,[CORREO_ELECTRONICO]=@correo_electronico      
							  ,[REVISION_DATE]=@date
							  ,[REVISION_TIME]=@time
							   ,[CV_CARRERA]=@cv_carrera
							   ,[PLANTEL_IMP]=@plantel_imp
							  where 
							  ACC_STDDEGREQ_SS.ACC_STDDEGREQ_SS_ID=@acc_stddegreq_ss_id
							  end  	
	
	  end



	end
end 






GO
/****** Object:  StoredProcedure [dbo].[ACC_spcreditosobtenidos_SS]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
IF OBJECT_ID ( 'sp_prueba', 'P' ) IS NOT NULL   
    DROP PROCEDURE sp_prueba;  
GO  */

/*CREACIÓN DE SP*/

CREATE PROCEDURE [dbo].[ACC_spcreditosobtenidos_SS]  @people nvarchar(10)  
     
AS  

SELECT t1.PEOPLE_CODE_ID,
   t1.PEOPLE_ID
   ,t1.CURRICULUM
   ,t1.MATRIC_YEAR
   ,t1.MATRIC_TERM
   ,t1.MatricSession
   ,t1.DEGREE
   ,t1.PROGRAM  
    ,t1.RECORD_TYPE  
	,t1.CREDCUBIERTOS
	,t1.CREDPLAN
	,max(IIF(T1.CREDPLAN IS NULL OR T1.CREDPLAN =0,0,iif(T1.CREDCUBIERTOS is null or t1.CREDCUBIERTOS=0,1,T1.CREDCUBIERTOS)*100/T1.CREDPLAN)) AS Porcentaje from (
   select 
   std.PEOPLE_CODE_ID,
   std.PEOPLE_ID
   ,std.CURRICULUM
   ,std.MATRIC_YEAR
   ,std.MATRIC_TERM
   ,std.MatricSession
   ,std.DEGREE
   ,std.PROGRAM  
    ,std.RECORD_TYPE 
   ,(SELECT SUM(EV.CREDITS) 
       FROM STDDEGREQEVENT AS STDE WITH (NOLOCK)
       , [EVENT] AS EV WITH (NOLOCK)
       WHERE STDE.EVENT_ID = EV.EVENT_ID
       AND STDE.EVENT_SUB_TYPE = EV.EVENT_TYPE
       AND STDE.PEOPLE_CODE_ID = std.PEOPLE_CODE_ID
       AND STDE.PROGRAM = std.PROGRAM
       AND STDE.DEGREE = std.DEGREE
       AND STDE.CURRICULUM = std.CURRICULUM
       AND STDE.MATRIC_YEAR = std.MATRIC_YEAR
       AND STDE.MATRIC_TERM = std.MATRIC_TERM
       AND ((STDE.[STATUS] = 'C') 
           OR(STDE.EVENT_ID=(SELECT TD.EVENT_ID 
                                                  FROM (SELECT *,CASE WHEN ISNUMERIC(FINAL_GRADE)=1 THEN CONVERT(NUMERIC(10,2),FINAL_GRADE)
                                                                ELSE 0 END AS CAL 
                                                  FROM TRANSCRIPTDETAIL WITH (NOLOCK)
                                                  )TD
                                                  WHERE TD.EVENT_ID=STDE.EVENT_ID 
                                                  AND TD.EVENT_SUB_TYPE IN ('LAB','TALL','COC') 
                                                  AND TD.PEOPLE_CODE_ID=std.PEOPLE_CODE_ID                                               
                                                  and TD.CAL>=6.0  
                                                  and TD.ADD_DROP_WAIT='A'
                                                  AND( TD.HONORS=' ' OR  TD.HONORS IS NULL))))
       

) CREDCUBIERTOS

,ISNULL((SELECT TOP(1) IIF(ST.CREDIT_MIN=0,NULL,ST.CREDIT_MIN) CREDITOS FROM STDDEGREQ ST WITH (NOLOCK) WHERE ST.MATRIC_YEAR=std.MATRIC_YEAR 
                                           AND ST.MATRIC_TERM=std.MATRIC_TERM AND ST.CURRICULUM=std.CURRICULUM 
                                           AND ST.DEGREE=std.DEGREE AND ST.PROGRAM=std.PROGRAM
                                           AND ST.PEOPLE_CODE_ID= std.PEOPLE_CODE_ID
                                           ),
                                           (SELECT TOP(1) ST.CREDIT_MIN FROM DEGREQ ST WITH (NOLOCK) WHERE ST.MATRIC_YEAR=std.MATRIC_YEAR 
                                           AND ST.MATRIC_TERM=std.MATRIC_TERM AND ST.CURRICULUM=std.CURRICULUM 
                                           AND ST.DEGREE=std.DEGREE AND ST.PROGRAM=std.PROGRAM)                            
                                           ) AS CREDPLAN   
   
   from STDDEGREQ AS STD with(nolock)
   
   WHERE PEOPLE_CODE_ID=@people
   AND STD.DEGREE IN ('LIC','ESPEC','DOCT','MAEST') AND STD.MATRIC_TERM IN ('CUATRI1','CUATRI2','CUATRI3') 
   
   ) as t1
   group by  t1.PEOPLE_CODE_ID,
   t1.PEOPLE_ID
   ,t1.CURRICULUM
   ,t1.MATRIC_YEAR
   ,t1.MATRIC_TERM
   ,t1.MatricSession
   ,t1.DEGREE
   ,t1.PROGRAM  
    ,t1.RECORD_TYPE
	,t1.CREDCUBIERTOS
	,t1.CREDPLAN


GO
/****** Object:  Table [dbo].[ACC_ALUMNO_DOCUMENTOS_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACC_ALUMNO_DOCUMENTOS_SP](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Acc_Progress_Status_Id] [int] NOT NULL,
	[PEOPLE_CODE_ID] [nvarchar](10) NOT NULL,
	[ID_DOCUMENTO] [int] NOT NULL,
	[SERVICIO_PROFESIONAL_ID] [int] NOT NULL,
	[NOMBRE_DOCUMENTO] [nvarchar](100) NOT NULL,
	[RUTA_DOCUMENTO] [nvarchar](100) NOT NULL,
	[ESTATUS_ID] [int] NOT NULL,
 CONSTRAINT [PK__ALUMNO_D__3214EC27DFC40679] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ACC_CAMBIO_DATOS_SPR_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACC_CAMBIO_DATOS_SPR_SP](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Acc_Progress_Status_Id] [int] NOT NULL,
	[ACC_STDDEGREQ_SS_ID] [int] NOT NULL,
	[SERVICIO_PROFESIONAL_ID] [int] NULL,
	[TIPO_ALTA] [int] NOT NULL,
	[ESTATUS_ID] [int] NOT NULL,
	[FECHAREGISTRO] [datetime] NOT NULL,
	[NOMBRE_EMPRESA] [nvarchar](50) NULL,
	[DIRECCION] [nvarchar](100) NULL,
	[PERSONA_CONTACTO] [nvarchar](50) NULL,
	[TELEFONO] [nvarchar](25) NULL,
	[COMENTARIOS] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK__CAMBIO_D__3214EC271F24B8C3] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ACC_CAT_CONVENIO_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACC_CAT_CONVENIO_SP](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NOMBRE_EMPRESA] [nvarchar](100) NOT NULL,
	[REPONSABLE] [nvarchar](100) NULL,
	[CARGO_RESPONSABLE] [nvarchar](40) NOT NULL,
	[DIRECCION] [nvarchar](500) NULL,
	[COLONIA] [nvarchar](30) NOT NULL,
	[CODIGO_POSTAL] [nvarchar](10) NOT NULL,
	[MUNICIPIO] [nvarchar](20) NOT NULL,
	[ID_ENTIDAD_FEDERATIVA] [varchar](100) NULL,
	[TELEFONO] [nvarchar](15) NOT NULL,
	[EXTENCION] [nvarchar](10) NOT NULL,
	[HORARIO_SERVICIO] [nvarchar](20) NOT NULL,
	[SECTOR] [nvarchar](10) NOT NULL,
	[CAT_TIPO_PRESTACION] [int] NOT NULL,
	[RESPONSABLE_LEGAL] [nvarchar](100) NULL,
	[RESPONSABLE_SERVICIO_SOCIAL] [nvarchar](100) NULL,
	[ESTATUS_ID] [int] NOT NULL,
 CONSTRAINT [PK__ACC_CAT___3214EC27342B82BF] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ACC_CAT_DOCUMENTOS_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACC_CAT_DOCUMENTOS_SP](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DESCRIPCION_DOCUMENTOS] [varchar](60) NULL,
	[SERVICIO_PROFESIONAL_ID] [int] NULL,
 CONSTRAINT [PK__ACC_CAT___3214EC2714010B70] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ACC_CAT_ESTATUS_SPR_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACC_CAT_ESTATUS_SPR_SP](
	[ESTATUS_ID] [int] IDENTITY(1,1) NOT NULL,
	[DESCRIPCION_ESTATUS] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ESTATUS_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Acc_Cat_Modalidades_Tipo_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Acc_Cat_Modalidades_Tipo_SP](
	[Acc_Modalidad_titulacion_Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_Acc_Cat_Modalidades_Tipo_SP] PRIMARY KEY CLUSTERED 
(
	[Acc_Modalidad_titulacion_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ACC_CAT_SERVICIO_PROFESIONAL_SS]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACC_CAT_SERVICIO_PROFESIONAL_SS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DESCRIPCION_SERVICIO_PROFESIONAL] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ACC_CAT_TIPO_LIBERACION_SS]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACC_CAT_TIPO_LIBERACION_SS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DESCRIPCION_TIPO_LIBERACION] [nvarchar](55) NOT NULL,
 CONSTRAINT [PK__ACC_CAT___3214EC27D402ABE0] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ACC_CAT_TIPO_PRESTACION_SS]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACC_CAT_TIPO_PRESTACION_SS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DESCRIPCION_TIPO_AYUDA] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ACC_CSP_01_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACC_CSP_01_SP](
	[CSP_SS_01_ID] [int] IDENTITY(1,1) NOT NULL,
	[Acc_Progress_Status_Id] [int] NOT NULL,
	[ACC_STDDEGREQ_SS_ID] [int] NOT NULL,
	[FECHA] [datetime] NOT NULL,
	[TRABAJO_GOBIERNO] [bit] NOT NULL,
	[ESCRITO_ART_91] [bit] NOT NULL,
	[NOMBRE_ALUMNO] [nvarchar](60) NOT NULL,
	[APATERNO] [nvarchar](60) NOT NULL,
	[AMATERNO] [nvarchar](60) NOT NULL,
	[EDAD] [int] NOT NULL,
	[CUATRIMESTRE] [nvarchar](20) NOT NULL,
	[TURNO] [nvarchar](15) NULL,
	[LICENCIATURA] [nvarchar](30) NOT NULL,
	[CICLO_INGRESO_LICENCIATURA] [nvarchar](20) NULL,
	[PLATEL] [nvarchar](25) NOT NULL,
	[DOMICILIO_PARTICULAR] [nvarchar](100) NOT NULL,
	[MUNICIPIO] [nvarchar](25) NULL,
	[TELEFONO_CASA] [nvarchar](25) NULL,
	[TELEFONO_OFICINA] [nvarchar](20) NULL,
	[EXTENCION] [nvarchar](20) NULL,
	[CELULAR] [nvarchar](20) NULL,
	[CORREO] [nvarchar](50) NOT NULL,
	[NOMBRE_EMPRESA] [nvarchar](100) NULL,
	[DIRECCION_EMPRESA] [nvarchar](150) NULL,
	[COLONIA_EMPRESA] [nvarchar](50) NULL,
	[DELEGACION_EMPRESA] [nvarchar](50) NULL,
	[CODIGO_POSTAL_EMPRESA] [nvarchar](20) NULL,
	[TELEFONO_EMPRESA] [nvarchar](20) NULL,
	[DIRIGIDO_A] [nvarchar](50) NULL,
	[PUESTO_DESEMPENADO] [nvarchar](50) NULL,
	[SECTOR] [nvarchar](25) NULL,
 CONSTRAINT [PK__ACC_CSP___5653BFFB7D617FF4] PRIMARY KEY CLUSTERED 
(
	[CSP_SS_01_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ACC_Documentos_Generales]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACC_Documentos_Generales](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PEOPLE_CODE_ID] [nvarchar](10) NOT NULL,
	[ID_DOCUMENTO] [int] NOT NULL,
	[NOMBRE_DOCUMENTO] [nvarchar](100) NOT NULL,
	[RUTA_DOCUMENTO] [nvarchar](100) NOT NULL,
	[ESTATUS_ID] [int] NOT NULL,
 CONSTRAINT [PK_D__3214EC27DFC40679] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Acc_Progress_Status_Id] [int] NOT NULL,
	[ACC_STDDEGREQ_SS_ID] [int] NOT NULL,
	[SERVICIO_PROFESIONAL_ID] [int] NOT NULL,
	[REG_TIPO] [int] NOT NULL,
	[TEACHER_ID] [nvarchar](10) NULL,
	[HIST_REL_ID] [int] NULL,
	[ESTATUS_ID] [int] NOT NULL,
	[COMENTARIOS] [varchar](max) NULL,
	[FECHA_HISTORICO] [datetime] NOT NULL,
	[VISTO_ALUMNO] [nvarchar](2) NULL,
 CONSTRAINT [PK__ACC_HIST__3214EC2757689632] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ACC_Horas_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACC_Horas_SP](
	[Acc_Horas_ID] [int] IDENTITY(1,1) NOT NULL,
	[ACC_STDDEGREQ_SS_ID] [int] NOT NULL,
	[Horas] [int] NOT NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_ACC_Horas_SP] PRIMARY KEY CLUSTERED 
(
	[Acc_Horas_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ACC_PRACTICAS_PROFESIONALES_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACC_PRACTICAS_PROFESIONALES_SP](
	[ACC_PRACTICA_PROFESIONAL_ID] [int] IDENTITY(1,1) NOT NULL,
	[Acc_Progress_Status_Id] [int] NOT NULL,
	[ACC_STDDEGREQ_SS_ID] [int] NOT NULL,
	[NOMBRE_ASESOR] [nvarchar](50) NOT NULL,
	[CARGO_ASESOR] [nvarchar](50) NOT NULL,
	[PROFESION_ASESOR] [nvarchar](50) NOT NULL,
	[ACTIVIDADES] [nvarchar](2000) NOT NULL,
	[HORAS] [int] NOT NULL,
	[FECHA_TERMINO_ALUMNO] [datetime] NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_ACC_PRACTICAS_PROFESIONALES_ID] PRIMARY KEY CLUSTERED 
(
	[ACC_PRACTICA_PROFESIONAL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Acc_Progress_Status_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Acc_Progress_Status_SP](
	[Acc_Progress_Status_Id] [int] IDENTITY(1,1) NOT NULL,
	[ACC_STDDEGREQ_SS_ID] [int] NOT NULL,
	[PEOPLE_CODE_ID] [nvarchar](10) NOT NULL,
	[ProcessStartted] [bit] NOT NULL,
	[StartRequest] [bit] NULL,
	[RequestDone] [bit] NULL,
	[Payment] [bit] NULL,
	[Documentation] [bit] NULL,
	[Compliance] [bit] NULL,
	[Release] [bit] NULL,
	[ESTATUS_ID] [int] NULL,
	[Tipo_Liberacion_ID] [int] NULL,
 CONSTRAINT [PK_Acc_Progress_Status_Acc] PRIMARY KEY CLUSTERED 
(
	[Acc_Progress_Status_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ACC_SERVICIOS_PROFESIONALES_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACC_SERVICIOS_PROFESIONALES_SP](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Acc_Progress_Status_Id] [int] NOT NULL,
	[ACC_STDDEGREQ_SS_ID] [int] NOT NULL,
	[CVE_CARRERA] [varchar](20) NOT NULL,
	[SERVICIO_PROFESIONAL_ID] [int] NULL,
	[CONVENIO_ID] [int] NULL,
	[TIPO_LIBERACION_ID] [int] NOT NULL,
	[ESTATUS_ID] [int] NOT NULL,
	[PAGADO] [int] NOT NULL,
	[FECHA_INICIO] [datetime] NULL,
	[FECHA_TERMINO] [datetime] NULL,
	[FECHA_TERMINO_ALUMNO] [datetime] NULL,
	[FECHAREGISTRO] [datetime] NOT NULL,
 CONSTRAINT [PK__ACC_SERV__3214EC273914A826] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Acc_Solicitudes_Titulacion_SP]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Acc_Solicitudes_Titulacion_SP](
	[Acc_Solicitud_titulacion_Id] [int] IDENTITY(1,1) NOT NULL,
	[Acc_Progress_Status_Id] [int] NOT NULL,
	[Nombre_Asesor] [nvarchar](50) NULL,
	[Nombre_Informe] [nvarchar](50) NULL,
	[Fecha_Registro] [datetime] NULL,
	[Fecha_Inicio_Tramite] [datetime] NULL,
	[Fecha_Ingreso] [datetime] NULL,
	[Cuatrimestre_Actual] [nvarchar](50) NULL,
	[Nombre_institucion] [nvarchar](50) NULL,
	[Nombre_Programa] [nvarchar](50) NULL,
	[Numero_RVOE] [nvarchar](25) NULL,
	[Fecha_RVOE] [datetime] NULL,
	[Fecha_Inicio] [datetime] NULL,
	[Fecha_Termino] [datetime] NULL,
	[Estudia_Posgrado] [bit] NULL,
	[Donde_Posgrado] [nvarchar](100) NULL,
	[CuandoPago] [nvarchar](5) NULL,
	[Acc_Modalidad_titulacion_Id] [int] NULL,
 CONSTRAINT [PK_Acc_Solicitudes_Titulacion_SP] PRIMARY KEY CLUSTERED 
(
	[Acc_Solicitud_titulacion_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ACC_STDDEGREQ_SS]    Script Date: 18/02/2020 09:04:23 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACC_STDDEGREQ_SS](
	[ACC_STDDEGREQ_SS_ID] [int] IDENTITY(1,1) NOT NULL,
	[PEOPLE_CODE_ID] [nvarchar](11) NOT NULL,
	[MATRIC_YEAR] [nvarchar](4) NOT NULL,
	[MATRIC_TERM] [nvarchar](10) NOT NULL,
	[RECORD_TYPE] [nvarchar](1) NOT NULL,
	[PROGRAM] [nvarchar](6) NOT NULL,
	[DEGREE] [nvarchar](6) NOT NULL,
	[CURRICULUM] [nvarchar](6) NOT NULL,
	[MATRIC_TERM_PROGRESS] [nvarchar](20) NULL,
	[MATRIC_SESSION] [nvarchar](10) NULL,
	[PEOPLE_ID] [nvarchar](10) NOT NULL,
	[CREDCUBIERTOS] [numeric](8, 4) NULL,
	[CREDPLAN] [numeric](8, 4) NULL,
	[PORCENTAJE] [numeric](8, 2) NULL,
	[PROMEDIO] [numeric](8, 0) NULL,
	[RVOE] [nvarchar](35) NULL,
	[FECHA_RVOE] [datetime] NULL,
	[PRIMER_NOMBRE] [nvarchar](60) NULL,
	[SEGUNDO_NOMBRE] [nvarchar](60) NULL,
	[APELLIDO_PATERNO] [nvarchar](60) NULL,
	[APELLIDO_MATERNO] [nvarchar](60) NULL,
	[CICLO_INGRESO] [nvarchar](25) NULL,
	[TURNO] [nvarchar](25) NULL,
	[SEXO] [nvarchar](10) NULL,
	[EDAD] [int] NULL,
	[ESTADO_CIVIL] [nvarchar](20) NULL,
	[CALLE] [nvarchar](75) NULL,
	[NUMERO_INTERIOR] [nvarchar](75) NULL,
	[NUMERO_EXTERIOR] [nvarchar](75) NULL,
	[COLONIA] [nvarchar](75) NULL,
	[DELEGACION] [nvarchar](75) NULL,
	[CODIGO_POSTAL] [nvarchar](15) NULL,
	[TELEFONO_CASA] [nvarchar](30) NULL,
	[TELEFONO_CELULAR] [nvarchar](30) NULL,
	[TELEFONO_RECADOS] [nvarchar](30) NULL,
	[TELEFONO_OFICINA] [nvarchar](30) NULL,
	[EXT_OFICINA] [nvarchar](30) NULL,
	[CORREO_ELECTRONICO] [nvarchar](255) NULL,
	[CHECK_SS] [bit] NULL,
	[CREATE_DATE] [datetime] NULL,
	[CREATE_TIME] [datetime] NULL,
	[CREATE_OPID] [nvarchar](8) NULL,
	[CREATE_TERMINAL] [nvarchar](4) NULL,
	[REVISION_DATE] [datetime] NULL,
	[REVISION_TIME] [datetime] NULL,
	[REVISION_OPID] [nvarchar](8) NULL,
	[REVISION_TERMINAL] [nvarchar](4) NULL,
	[CHARGECREDITNUMBER] [int] NULL,
	[Servicio_Profesional_Id] [int] NOT NULL,
	[CV_CARRERA] [nvarchar](15) NULL,
	[PLANTEL_IMP] [nvarchar](20) NULL,
 CONSTRAINT [PK_STDDEGREQ_SS] PRIMARY KEY CLUSTERED 
(
	[PEOPLE_CODE_ID] ASC,
	[MATRIC_YEAR] ASC,
	[MATRIC_TERM] ASC,
	[RECORD_TYPE] ASC,
	[PROGRAM] ASC,
	[DEGREE] ASC,
	[CURRICULUM] ASC,
	[Servicio_Profesional_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[ACC_ALUMNO_DOCUMENTOS_SP] ON 

INSERT [dbo].[ACC_ALUMNO_DOCUMENTOS_SP] ([ID], [Acc_Progress_Status_Id], [PEOPLE_CODE_ID], [ID_DOCUMENTO], [SERVICIO_PROFESIONAL_ID], [NOMBRE_DOCUMENTO], [RUTA_DOCUMENTO], [ESTATUS_ID]) VALUES (1, 1, N'P000092542', 5, 1, N'ConstanciaCreditos_servicio-social_1.pdf', N'servicios-profesionales/docs/P000092542', 17)
SET IDENTITY_INSERT [dbo].[ACC_ALUMNO_DOCUMENTOS_SP] OFF
SET IDENTITY_INSERT [dbo].[ACC_CAMBIO_DATOS_SPR_SP] ON 

INSERT [dbo].[ACC_CAMBIO_DATOS_SPR_SP] ([ID], [Acc_Progress_Status_Id], [ACC_STDDEGREQ_SS_ID], [SERVICIO_PROFESIONAL_ID], [TIPO_ALTA], [ESTATUS_ID], [FECHAREGISTRO], [NOMBRE_EMPRESA], [DIRECCION], [PERSONA_CONTACTO], [TELEFONO], [COMENTARIOS]) VALUES (3, 1, 1, 1, 1, 11, CAST(0x0000AB6400000000 AS DateTime), N'UIN', NULL, N'Miguel', NULL, N'Necesito cambiar mi informacion')
SET IDENTITY_INSERT [dbo].[ACC_CAMBIO_DATOS_SPR_SP] OFF
SET IDENTITY_INSERT [dbo].[ACC_CAT_CONVENIO_SP] ON 

INSERT [dbo].[ACC_CAT_CONVENIO_SP] ([ID], [NOMBRE_EMPRESA], [REPONSABLE], [CARGO_RESPONSABLE], [DIRECCION], [COLONIA], [CODIGO_POSTAL], [MUNICIPIO], [ID_ENTIDAD_FEDERATIVA], [TELEFONO], [EXTENCION], [HORARIO_SERVICIO], [SECTOR], [CAT_TIPO_PRESTACION], [RESPONSABLE_LEGAL], [RESPONSABLE_SERVICIO_SOCIAL], [ESTATUS_ID]) VALUES (1, N'UIN', N'Miguel', N'Proyectos', N'viaducto', N'Colonoa viaducto', N'54645', N'Cuauhtï¿½moc', N'CDMX', N'5556456', N'5', N'N/A', N'PRIVADO', 2, N'wqdfqwd', N'qwdqwdq', 0)
INSERT [dbo].[ACC_CAT_CONVENIO_SP] ([ID], [NOMBRE_EMPRESA], [REPONSABLE], [CARGO_RESPONSABLE], [DIRECCION], [COLONIA], [CODIGO_POSTAL], [MUNICIPIO], [ID_ENTIDAD_FEDERATIVA], [TELEFONO], [EXTENCION], [HORARIO_SERVICIO], [SECTOR], [CAT_TIPO_PRESTACION], [RESPONSABLE_LEGAL], [RESPONSABLE_SERVICIO_SOCIAL], [ESTATUS_ID]) VALUES (2, N'Access', N'Angelixa', N'Representante', N'Avenida del ImÃ¡n', N'XXX-XX', N'06500', N'CDMX', N'Mexico', N'56168000', N'34', N'Lunes a Viernes', N'Privado', 1, NULL, NULL, 1)
INSERT [dbo].[ACC_CAT_CONVENIO_SP] ([ID], [NOMBRE_EMPRESA], [REPONSABLE], [CARGO_RESPONSABLE], [DIRECCION], [COLONIA], [CODIGO_POSTAL], [MUNICIPIO], [ID_ENTIDAD_FEDERATIVA], [TELEFONO], [EXTENCION], [HORARIO_SERVICIO], [SECTOR], [CAT_TIPO_PRESTACION], [RESPONSABLE_LEGAL], [RESPONSABLE_SERVICIO_SOCIAL], [ESTATUS_ID]) VALUES (3, N'IBM', N'Miguel', N'Representante', N'SAnta Fe', N'Santa Fe', N'06500', N'CDMX', N'Mexico', N'56168000', N'123', N'Lunes a Viernes', N'Privado', 1, NULL, NULL, 0)
INSERT [dbo].[ACC_CAT_CONVENIO_SP] ([ID], [NOMBRE_EMPRESA], [REPONSABLE], [CARGO_RESPONSABLE], [DIRECCION], [COLONIA], [CODIGO_POSTAL], [MUNICIPIO], [ID_ENTIDAD_FEDERATIVA], [TELEFONO], [EXTENCION], [HORARIO_SERVICIO], [SECTOR], [CAT_TIPO_PRESTACION], [RESPONSABLE_LEGAL], [RESPONSABLE_SERVICIO_SOCIAL], [ESTATUS_ID]) VALUES (1002, N'Access Media', N'CÃ©sar Sautto', N'Director', N'Miramontes', N'Tlalpan', N'08000', N'CDMX', N'MÃ©xico', N'46879431321', N'412', N'Lunes a Viernes', N'PUBLICO', 2, N'dwqdq', N'dqwdqw', 1)
INSERT [dbo].[ACC_CAT_CONVENIO_SP] ([ID], [NOMBRE_EMPRESA], [REPONSABLE], [CARGO_RESPONSABLE], [DIRECCION], [COLONIA], [CODIGO_POSTAL], [MUNICIPIO], [ID_ENTIDAD_FEDERATIVA], [TELEFONO], [EXTENCION], [HORARIO_SERVICIO], [SECTOR], [CAT_TIPO_PRESTACION], [RESPONSABLE_LEGAL], [RESPONSABLE_SERVICIO_SOCIAL], [ESTATUS_ID]) VALUES (1003, N'Test', N'David', N'Director', N'Divolandia', N'Polanco', N'06500', N'CDMX', N'DF', N'5556168000', N'101', N'de Lunes a Viernes', N'PUBLICO', 2, N'dwqdqw', N'dwqqwqwd', 0)
INSERT [dbo].[ACC_CAT_CONVENIO_SP] ([ID], [NOMBRE_EMPRESA], [REPONSABLE], [CARGO_RESPONSABLE], [DIRECCION], [COLONIA], [CODIGO_POSTAL], [MUNICIPIO], [ID_ENTIDAD_FEDERATIVA], [TELEFONO], [EXTENCION], [HORARIO_SERVICIO], [SECTOR], [CAT_TIPO_PRESTACION], [RESPONSABLE_LEGAL], [RESPONSABLE_SERVICIO_SOCIAL], [ESTATUS_ID]) VALUES (1004, N'Coca Cola', N'Roberto', N'Gerente', N'Cafetales $55', N'Cafetales', N'0600', N'CDMX', N'DF', N'168474', N'14', N'Lunes aViernes', N'PUBLICO', 1, N'Alejandro', N'Alberto', 0)
SET IDENTITY_INSERT [dbo].[ACC_CAT_CONVENIO_SP] OFF
SET IDENTITY_INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ON 

INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (1, N'INE', 1)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (2, N'Acta de Nacimiento', 1)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (3, N'CURP', 1)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (4, N'Comprobante de Domicilio', 1)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (5, N'Constancia de Créditos', 1)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (6, N'Carta de Presentación', 1)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (7, N'Carta de Aceptación', 1)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (8, N'Reporte de Actividades', 1)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (9, N'Carta de Término', 1)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (10, N'Carta de Liberación', 1)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (11, N'Solicitud de Constacia Laboral', 1)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (12, N'Carta de exención  Articulo 52', 1)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (13, N'Carta de exención  Articulo 91', 1)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (14, N'Carta de Liberación Final', 1)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (15, N'Solicitud de Trámite ', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (16, N'Certificado de Bachillerato', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (17, N'Certificado total de licenciatura ', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (18, N'Equivalencia o revalidación de estudios ', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (19, N'Constancia de liberación del Servicio Social ', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (20, N'Constancia de liberación de prácticas profesionales ', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (21, N'Constancia de acreditación del idioma ', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (22, N'Constancia de no adeudo emitida por caja del plantel ', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (23, N'Constancia de no adeudo emitida por biblioteca del plantel', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (24, N'Protocolo de tésis', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (25, N'Dictamen de tesis de revisores', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (26, N'Visto bueno de revisores de tesis 
', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (27, N'Constancia de presentación de examen profesional', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (28, N'Acta de exención de exámen', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (29, N'Aprobación Examen UIN/CENEVAL', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (30, N'Ceritificado parcial de maestría', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (31, N'Título Electrónico', 3)
INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] ([ID], [DESCRIPCION_DOCUMENTOS], [SERVICIO_PROFESIONAL_ID]) VALUES (32, N'Título Físico', 3)
SET IDENTITY_INSERT [dbo].[ACC_CAT_DOCUMENTOS_SP] OFF
SET IDENTITY_INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ON 

INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (1, N'Cambio de información')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (2, N'Cambio de información Rechazado')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (3, N'Cambio de información Aceptada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (4, N'Nuevo Convenio')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (5, N'Nuevo Convenio Rechazado')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (6, N'Nuevo Convenio Aceptado')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (7, N'Solicitud de [SERVICIO] Iniciada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (8, N'Envío de Datos')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (9, N'En espera de Respuesta por Administración')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (10, N'Por Revisar')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (11, N'En Revisión')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (12, N'Falta de Información')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (13, N'Formulario Vacio')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (14, N'Error en el Envío')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (15, N'Pago Realizado')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (16, N'Acta de Nacimiento Cargada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (17, N'Constancia de Créditos Generada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (18, N'Carta de Presentación Generada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (19, N'[SERVICIO] Iniciado')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (20, N'[SERVICIO] Concluido')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (21, N'[SERVICIO] Cancelado')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (22, N'Carta de aceptación guardada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (23, N'Reporte de actividades')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (24, N'Carta de término de servicio social')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (25, N'Documento Aceptado')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (26, N'Documento Rechazado')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (27, N'Carta de Liberación Generada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (28, N'Solicitud de Constancia Laboral Generada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (29, N'Carta de Exención  Articulo 52 Creada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (30, N'Carta de Exención  Articulo 91 Creada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (31, N'Carta de Liberación Final Generada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (32, N'Solicitud de Trámite Realizada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (33, N'Certificado de Bachillerato Cargado')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (34, N'Certificado Total de Licenciatura Cargada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (35, N'Equivalencia o Revalidación de Estudios Cargada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (36, N'Constancia de Liberación del Servicio Social Cargada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (37, N'Constancia de Liberación de Prácticas Profesionales Cargada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (38, N'Constancia de Acreditación del Idioma ')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (39, N'Constancia de no adeudo emitida por caja del plantel ')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (40, N'Constancia de no adeudo emitida por biblioteca del plantel')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (41, N'CURP Cargada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (42, N'Protocolo de tésis Cargada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (43, N'Dictamen de tesis de revisores Cargada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (44, N'Visto bueno de revisores de tesis 
Cargada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (45, N'Constancia de presentación de examen profesional Cargada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (46, N'Acta de extensión de exámen Cargada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (47, N'Aprobación Examen UIN/CENEVAL Cargada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (48, N'Ceritificado parcial de maestría Cargada')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (49, N'Documentos Validados y Autorizados')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (50, N'Título Electrónico Listo')
INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID], [DESCRIPCION_ESTATUS]) VALUES (51, N'Título Físico Listo')
SET IDENTITY_INSERT [dbo].[ACC_CAT_ESTATUS_SPR_SP] OFF
SET IDENTITY_INSERT [dbo].[Acc_Cat_Modalidades_Tipo_SP] ON 

INSERT [dbo].[Acc_Cat_Modalidades_Tipo_SP] ([Acc_Modalidad_titulacion_Id], [Descripcion], [Status]) VALUES (1, N'Especialidad', 1)
INSERT [dbo].[Acc_Cat_Modalidades_Tipo_SP] ([Acc_Modalidad_titulacion_Id], [Descripcion], [Status]) VALUES (2, N'Maestría', 1)
INSERT [dbo].[Acc_Cat_Modalidades_Tipo_SP] ([Acc_Modalidad_titulacion_Id], [Descripcion], [Status]) VALUES (3, N'Individual', 1)
INSERT [dbo].[Acc_Cat_Modalidades_Tipo_SP] ([Acc_Modalidad_titulacion_Id], [Descripcion], [Status]) VALUES (4, N'Colectivo', 1)
SET IDENTITY_INSERT [dbo].[Acc_Cat_Modalidades_Tipo_SP] OFF
SET IDENTITY_INSERT [dbo].[ACC_CAT_SERVICIO_PROFESIONAL_SS] ON 

INSERT [dbo].[ACC_CAT_SERVICIO_PROFESIONAL_SS] ([ID], [DESCRIPCION_SERVICIO_PROFESIONAL]) VALUES (1, N'Servicio Social')
INSERT [dbo].[ACC_CAT_SERVICIO_PROFESIONAL_SS] ([ID], [DESCRIPCION_SERVICIO_PROFESIONAL]) VALUES (2, N'Practicas Profesionales')
INSERT [dbo].[ACC_CAT_SERVICIO_PROFESIONAL_SS] ([ID], [DESCRIPCION_SERVICIO_PROFESIONAL]) VALUES (3, N'Titulación')
SET IDENTITY_INSERT [dbo].[ACC_CAT_SERVICIO_PROFESIONAL_SS] OFF
SET IDENTITY_INSERT [dbo].[ACC_CAT_TIPO_LIBERACION_SS] ON 

INSERT [dbo].[ACC_CAT_TIPO_LIBERACION_SS] ([ID], [DESCRIPCION_TIPO_LIBERACION]) VALUES (1, N'Excención Servicio Social')
INSERT [dbo].[ACC_CAT_TIPO_LIBERACION_SS] ([ID], [DESCRIPCION_TIPO_LIBERACION]) VALUES (2, N'Externos e internos')
INSERT [dbo].[ACC_CAT_TIPO_LIBERACION_SS] ([ID], [DESCRIPCION_TIPO_LIBERACION]) VALUES (3, N'Exención Artículo 52')
INSERT [dbo].[ACC_CAT_TIPO_LIBERACION_SS] ([ID], [DESCRIPCION_TIPO_LIBERACION]) VALUES (4, N'Exención Artículo 91')
INSERT [dbo].[ACC_CAT_TIPO_LIBERACION_SS] ([ID], [DESCRIPCION_TIPO_LIBERACION]) VALUES (5, N'Tesis')
INSERT [dbo].[ACC_CAT_TIPO_LIBERACION_SS] ([ID], [DESCRIPCION_TIPO_LIBERACION]) VALUES (6, N'Informe sobre el servicio Social prestado')
INSERT [dbo].[ACC_CAT_TIPO_LIBERACION_SS] ([ID], [DESCRIPCION_TIPO_LIBERACION]) VALUES (7, N'Informe sobre demostración de experiencia profesional')
INSERT [dbo].[ACC_CAT_TIPO_LIBERACION_SS] ([ID], [DESCRIPCION_TIPO_LIBERACION]) VALUES (8, N'Examen general de conocimientos')
INSERT [dbo].[ACC_CAT_TIPO_LIBERACION_SS] ([ID], [DESCRIPCION_TIPO_LIBERACION]) VALUES (9, N'Estudio de posgrado
')
INSERT [dbo].[ACC_CAT_TIPO_LIBERACION_SS] ([ID], [DESCRIPCION_TIPO_LIBERACION]) VALUES (10, N'Desempeño académico')
INSERT [dbo].[ACC_CAT_TIPO_LIBERACION_SS] ([ID], [DESCRIPCION_TIPO_LIBERACION]) VALUES (11, N'Titulación Directa')
SET IDENTITY_INSERT [dbo].[ACC_CAT_TIPO_LIBERACION_SS] OFF
SET IDENTITY_INSERT [dbo].[ACC_CAT_TIPO_PRESTACION_SS] ON 

INSERT [dbo].[ACC_CAT_TIPO_PRESTACION_SS] ([ID], [DESCRIPCION_TIPO_AYUDA]) VALUES (1, N'Económica
')
INSERT [dbo].[ACC_CAT_TIPO_PRESTACION_SS] ([ID], [DESCRIPCION_TIPO_AYUDA]) VALUES (2, N'Sin Ayuda')
SET IDENTITY_INSERT [dbo].[ACC_CAT_TIPO_PRESTACION_SS] OFF
SET IDENTITY_INSERT [dbo].[ACC_CSP_01_SP] ON 

INSERT [dbo].[ACC_CSP_01_SP] ([CSP_SS_01_ID], [Acc_Progress_Status_Id], [ACC_STDDEGREQ_SS_ID], [FECHA], [TRABAJO_GOBIERNO], [ESCRITO_ART_91], [NOMBRE_ALUMNO], [APATERNO], [AMATERNO], [EDAD], [CUATRIMESTRE], [TURNO], [LICENCIATURA], [CICLO_INGRESO_LICENCIATURA], [PLATEL], [DOMICILIO_PARTICULAR], [MUNICIPIO], [TELEFONO_CASA], [TELEFONO_OFICINA], [EXTENCION], [CELULAR], [CORREO], [NOMBRE_EMPRESA], [DIRECCION_EMPRESA], [COLONIA_EMPRESA], [DELEGACION_EMPRESA], [CODIGO_POSTAL_EMPRESA], [TELEFONO_EMPRESA], [DIRIGIDO_A], [PUESTO_DESEMPENADO], [SECTOR]) VALUES (1, 1, 1, CAST(0x0000AB6400EE8944 AS DateTime), 0, 0, N'FRIDA IRE', N'ROSALES', N'CEDEÑO', 23, N'CUATRI1', N'MATUTINO', N'33', N'2017-CUATRI1', N'CENTRO', N'CDA DE ALLENDE  23 B-101 centro 06300 delegaacion', N'delegaacion', N'57727974', NULL, NULL, N'55535576501', N'fridaa_irc@hotmail.com', N'Access', N'Avenida del ImÃ¡n', N'XXX-XX', N'CDMX', N'06500', N'56168000', N'Angelixa', N'Representante', N'Privado')
INSERT [dbo].[ACC_CSP_01_SP] ([CSP_SS_01_ID], [Acc_Progress_Status_Id], [ACC_STDDEGREQ_SS_ID], [FECHA], [TRABAJO_GOBIERNO], [ESCRITO_ART_91], [NOMBRE_ALUMNO], [APATERNO], [AMATERNO], [EDAD], [CUATRIMESTRE], [TURNO], [LICENCIATURA], [CICLO_INGRESO_LICENCIATURA], [PLATEL], [DOMICILIO_PARTICULAR], [MUNICIPIO], [TELEFONO_CASA], [TELEFONO_OFICINA], [EXTENCION], [CELULAR], [CORREO], [NOMBRE_EMPRESA], [DIRECCION_EMPRESA], [COLONIA_EMPRESA], [DELEGACION_EMPRESA], [CODIGO_POSTAL_EMPRESA], [TELEFONO_EMPRESA], [DIRIGIDO_A], [PUESTO_DESEMPENADO], [SECTOR]) VALUES (2, 2, 2, CAST(0x0000AB6400F4C437 AS DateTime), 0, 0, N'FRIDA IRE', N'ROSALES', N'CEDEÑO', 23, N'CUATRI1', N'MATUTINO', N'33', N'2017-CUATRI1', N'CENTRO', N'CDA DE ALLENDE  23 B-101 centro 06300 sdfsd', N'sdfsd', N'57727974', NULL, NULL, N'55535576501', N'fridaa_irc@hotmail.com', N'Access', N'Avenida del ImÃ¡n', N'XXX-XX', N'CDMX', N'06500', N'56168000', N'Angelixa', N'Representante', N'Privado')
SET IDENTITY_INSERT [dbo].[ACC_CSP_01_SP] OFF
SET IDENTITY_INSERT [dbo].[ACC_Documentos_Generales] ON 

INSERT [dbo].[ACC_Documentos_Generales] ([ID], [PEOPLE_CODE_ID], [ID_DOCUMENTO], [NOMBRE_DOCUMENTO], [RUTA_DOCUMENTO], [ESTATUS_ID]) VALUES (1, N'P000092542', 2, N'21.pdf', N'\DocsAlumno\P000092542', 11)
SET IDENTITY_INSERT [dbo].[ACC_Documentos_Generales] OFF
SET IDENTITY_INSERT [dbo].[ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP] ON 

INSERT [dbo].[ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP] ([ID], [Acc_Progress_Status_Id], [ACC_STDDEGREQ_SS_ID], [SERVICIO_PROFESIONAL_ID], [REG_TIPO], [TEACHER_ID], [HIST_REL_ID], [ESTATUS_ID], [COMENTARIOS], [FECHA_HISTORICO], [VISTO_ALUMNO]) VALUES (1, 1, 1, 1, 0, NULL, NULL, 7, N'', CAST(0x0000AB6400EE895B AS DateTime), NULL)
INSERT [dbo].[ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP] ([ID], [Acc_Progress_Status_Id], [ACC_STDDEGREQ_SS_ID], [SERVICIO_PROFESIONAL_ID], [REG_TIPO], [TEACHER_ID], [HIST_REL_ID], [ESTATUS_ID], [COMENTARIOS], [FECHA_HISTORICO], [VISTO_ALUMNO]) VALUES (2, 1, 1, 1, 0, NULL, NULL, 16, N'', CAST(0x0000AB6400EEA117 AS DateTime), NULL)
INSERT [dbo].[ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP] ([ID], [Acc_Progress_Status_Id], [ACC_STDDEGREQ_SS_ID], [SERVICIO_PROFESIONAL_ID], [REG_TIPO], [TEACHER_ID], [HIST_REL_ID], [ESTATUS_ID], [COMENTARIOS], [FECHA_HISTORICO], [VISTO_ALUMNO]) VALUES (3, 1, 1, 1, 1, N'2', NULL, 17, N'Vista Previa de Constancia de Cr&eacute;ditos<br />Generar Constancia de Cr&eacute;ditos', CAST(0x0000AB6400EB5618 AS DateTime), N'0')
INSERT [dbo].[ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP] ([ID], [Acc_Progress_Status_Id], [ACC_STDDEGREQ_SS_ID], [SERVICIO_PROFESIONAL_ID], [REG_TIPO], [TEACHER_ID], [HIST_REL_ID], [ESTATUS_ID], [COMENTARIOS], [FECHA_HISTORICO], [VISTO_ALUMNO]) VALUES (4, 2, 2, 2, 0, NULL, NULL, 7, N'', CAST(0x0000AB6400F4C43B AS DateTime), NULL)
INSERT [dbo].[ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP] ([ID], [Acc_Progress_Status_Id], [ACC_STDDEGREQ_SS_ID], [SERVICIO_PROFESIONAL_ID], [REG_TIPO], [TEACHER_ID], [HIST_REL_ID], [ESTATUS_ID], [COMENTARIOS], [FECHA_HISTORICO], [VISTO_ALUMNO]) VALUES (5, 1, 1, 1, 1, N'2', NULL, 11, N'Se ha comenzado el Proceso de Revisi&oacute;n', CAST(0x0000AB6400FE4750 AS DateTime), N'0')
INSERT [dbo].[ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP] ([ID], [Acc_Progress_Status_Id], [ACC_STDDEGREQ_SS_ID], [SERVICIO_PROFESIONAL_ID], [REG_TIPO], [TEACHER_ID], [HIST_REL_ID], [ESTATUS_ID], [COMENTARIOS], [FECHA_HISTORICO], [VISTO_ALUMNO]) VALUES (6, 1, 1, 1, 1, N'2', NULL, 11, N'Se ha comenzado el Proceso de Revisi&oacute;n', CAST(0x0000AB6401019FF4 AS DateTime), N'0')
SET IDENTITY_INSERT [dbo].[ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP] OFF
SET IDENTITY_INSERT [dbo].[Acc_Progress_Status_SP] ON 

INSERT [dbo].[Acc_Progress_Status_SP] ([Acc_Progress_Status_Id], [ACC_STDDEGREQ_SS_ID], [PEOPLE_CODE_ID], [ProcessStartted], [StartRequest], [RequestDone], [Payment], [Documentation], [Compliance], [Release], [ESTATUS_ID], [Tipo_Liberacion_ID]) VALUES (1, 1, N'P000092542', 1, 1, 1, 0, 0, 0, 0, 19, 2)
INSERT [dbo].[Acc_Progress_Status_SP] ([Acc_Progress_Status_Id], [ACC_STDDEGREQ_SS_ID], [PEOPLE_CODE_ID], [ProcessStartted], [StartRequest], [RequestDone], [Payment], [Documentation], [Compliance], [Release], [ESTATUS_ID], [Tipo_Liberacion_ID]) VALUES (2, 2, N'P000092542', 1, 1, 1, 0, 0, 0, 0, 19, 0)
SET IDENTITY_INSERT [dbo].[Acc_Progress_Status_SP] OFF
SET IDENTITY_INSERT [dbo].[ACC_SERVICIOS_PROFESIONALES_SP] ON 

INSERT [dbo].[ACC_SERVICIOS_PROFESIONALES_SP] ([ID], [Acc_Progress_Status_Id], [ACC_STDDEGREQ_SS_ID], [CVE_CARRERA], [SERVICIO_PROFESIONAL_ID], [CONVENIO_ID], [TIPO_LIBERACION_ID], [ESTATUS_ID], [PAGADO], [FECHA_INICIO], [FECHA_TERMINO], [FECHA_TERMINO_ALUMNO], [FECHAREGISTRO]) VALUES (1, 1, 1, N'33', 1, 2, 2, 7, 0, NULL, NULL, NULL, CAST(0x0000AB6400EE88BE AS DateTime))
INSERT [dbo].[ACC_SERVICIOS_PROFESIONALES_SP] ([ID], [Acc_Progress_Status_Id], [ACC_STDDEGREQ_SS_ID], [CVE_CARRERA], [SERVICIO_PROFESIONAL_ID], [CONVENIO_ID], [TIPO_LIBERACION_ID], [ESTATUS_ID], [PAGADO], [FECHA_INICIO], [FECHA_TERMINO], [FECHA_TERMINO_ALUMNO], [FECHAREGISTRO]) VALUES (2, 2, 2, N'33', 2, 2, 2, 7, 0, NULL, NULL, NULL, CAST(0x0000AB6400F4C433 AS DateTime))
SET IDENTITY_INSERT [dbo].[ACC_SERVICIOS_PROFESIONALES_SP] OFF
SET IDENTITY_INSERT [dbo].[ACC_STDDEGREQ_SS] ON 

INSERT [dbo].[ACC_STDDEGREQ_SS] ([ACC_STDDEGREQ_SS_ID], [PEOPLE_CODE_ID], [MATRIC_YEAR], [MATRIC_TERM], [RECORD_TYPE], [PROGRAM], [DEGREE], [CURRICULUM], [MATRIC_TERM_PROGRESS], [MATRIC_SESSION], [PEOPLE_ID], [CREDCUBIERTOS], [CREDPLAN], [PORCENTAJE], [PROMEDIO], [RVOE], [FECHA_RVOE], [PRIMER_NOMBRE], [SEGUNDO_NOMBRE], [APELLIDO_PATERNO], [APELLIDO_MATERNO], [CICLO_INGRESO], [TURNO], [SEXO], [EDAD], [ESTADO_CIVIL], [CALLE], [NUMERO_INTERIOR], [NUMERO_EXTERIOR], [COLONIA], [DELEGACION], [CODIGO_POSTAL], [TELEFONO_CASA], [TELEFONO_CELULAR], [TELEFONO_RECADOS], [TELEFONO_OFICINA], [EXT_OFICINA], [CORREO_ELECTRONICO], [CHECK_SS], [CREATE_DATE], [CREATE_TIME], [CREATE_OPID], [CREATE_TERMINAL], [REVISION_DATE], [REVISION_TIME], [REVISION_OPID], [REVISION_TERMINAL], [CHARGECREDITNUMBER], [Servicio_Profesional_Id], [CV_CARRERA], [PLANTEL_IMP]) VALUES (1, N'P000092542', N'2018', N'CUATRI1', N'A', N'MIXTO', N'LIC', N'33', NULL, N'CENTRO', N'000092542', CAST(389.3700 AS Numeric(8, 4)), CAST(403.3700 AS Numeric(8, 4)), CAST(96.53 AS Numeric(8, 2)), CAST(8 AS Numeric(8, 0)), N'20182022', CAST(0x0000A9A800000000 AS DateTime), N'FRIDA', N'IRE', N'ROSALES', N'CEDEÑO', N'2017-CUATRI1', N'MATUTINO', N'F', 23, N'SOLT', N'CDA DE ALLENDE  23 B-101', N'4', N'3', N'centro', N'delegaacion', N'06300', N'57727974', N'55535576501', NULL, NULL, N'', N'fridaa_irc@hotmail.com', NULL, CAST(0x0000AB6400000000 AS DateTime), CAST(0x0000000000EE3831 AS DateTime), N'SCTCONV', N'0001', CAST(0x0000AB6400000000 AS DateTime), CAST(0x0000000000EE3BB6 AS DateTime), N'SCTCONV', N'0001', 4959500, 1, N'231301', N'CENTRO')
INSERT [dbo].[ACC_STDDEGREQ_SS] ([ACC_STDDEGREQ_SS_ID], [PEOPLE_CODE_ID], [MATRIC_YEAR], [MATRIC_TERM], [RECORD_TYPE], [PROGRAM], [DEGREE], [CURRICULUM], [MATRIC_TERM_PROGRESS], [MATRIC_SESSION], [PEOPLE_ID], [CREDCUBIERTOS], [CREDPLAN], [PORCENTAJE], [PROMEDIO], [RVOE], [FECHA_RVOE], [PRIMER_NOMBRE], [SEGUNDO_NOMBRE], [APELLIDO_PATERNO], [APELLIDO_MATERNO], [CICLO_INGRESO], [TURNO], [SEXO], [EDAD], [ESTADO_CIVIL], [CALLE], [NUMERO_INTERIOR], [NUMERO_EXTERIOR], [COLONIA], [DELEGACION], [CODIGO_POSTAL], [TELEFONO_CASA], [TELEFONO_CELULAR], [TELEFONO_RECADOS], [TELEFONO_OFICINA], [EXT_OFICINA], [CORREO_ELECTRONICO], [CHECK_SS], [CREATE_DATE], [CREATE_TIME], [CREATE_OPID], [CREATE_TERMINAL], [REVISION_DATE], [REVISION_TIME], [REVISION_OPID], [REVISION_TERMINAL], [CHARGECREDITNUMBER], [Servicio_Profesional_Id], [CV_CARRERA], [PLANTEL_IMP]) VALUES (2, N'P000092542', N'2018', N'CUATRI1', N'A', N'MIXTO', N'LIC', N'33', NULL, N'CENTRO', N'000092542', CAST(389.3700 AS Numeric(8, 4)), CAST(403.3700 AS Numeric(8, 4)), CAST(96.53 AS Numeric(8, 2)), CAST(8 AS Numeric(8, 0)), N'20182022', CAST(0x0000A9A800000000 AS DateTime), N'FRIDA', N'IRE', N'ROSALES', N'CEDEÑO', N'2017-CUATRI1', N'MATUTINO', N'F', 23, N'SOLT', N'CDA DE ALLENDE  23 B-101', N'4', N'3', N'centro', N'sdfsd', N'06300', N'57727974', N'55535576501', NULL, NULL, N'', N'fridaa_irc@hotmail.com', 1, CAST(0x0000AB6400000000 AS DateTime), CAST(0x0000000000F479EC AS DateTime), N'SCTCONV', N'0001', CAST(0x0000AB6400000000 AS DateTime), CAST(0x0000000000F47E17 AS DateTime), N'SCTCONV', N'0001', 4959501, 2, N'231301', N'CENTRO')
SET IDENTITY_INSERT [dbo].[ACC_STDDEGREQ_SS] OFF
ALTER TABLE [dbo].[Acc_Cat_Modalidades_Tipo_SP] ADD  CONSTRAINT [DF_Acc_Cat_Modalidades_Tipo_SP_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[ACC_HISTORICO_SERVICIOS_PROFESIONALES_SP] ADD  CONSTRAINT [DF_ACC_HISTORICO_SERVICIO_SOCIAL_SS_Visto_Alumno]  DEFAULT ((0)) FOR [VISTO_ALUMNO]
GO
ALTER TABLE [dbo].[ACC_Horas_SP] ADD  CONSTRAINT [DF_ACC_Horas_SP_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[ACC_PRACTICAS_PROFESIONALES_SP] ADD  CONSTRAINT [DF_ACC_PRACTICAS_PROFESIONALES_ID_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[Acc_Progress_Status_SP] ADD  CONSTRAINT [DF_Acc_Progress_Status_SS_ProcessStartted]  DEFAULT ((0)) FOR [ProcessStartted]
GO
ALTER TABLE [dbo].[Acc_Progress_Status_SP] ADD  CONSTRAINT [DF_Acc_Progress_Status_SS_StartRequest]  DEFAULT ((0)) FOR [StartRequest]
GO
ALTER TABLE [dbo].[Acc_Progress_Status_SP] ADD  CONSTRAINT [DF_Acc_Progress_Status_SS_RequestDone]  DEFAULT ((0)) FOR [RequestDone]
GO
ALTER TABLE [dbo].[Acc_Progress_Status_SP] ADD  CONSTRAINT [DF_Acc_Progress_Status_SS_Payment]  DEFAULT ((0)) FOR [Payment]
GO
ALTER TABLE [dbo].[Acc_Progress_Status_SP] ADD  CONSTRAINT [DF_Acc_Progress_Status_SS_Documentation]  DEFAULT ((0)) FOR [Documentation]
GO
ALTER TABLE [dbo].[Acc_Progress_Status_SP] ADD  CONSTRAINT [DF_Acc_Progress_Status_SS_Compliance]  DEFAULT ((0)) FOR [Compliance]
GO
ALTER TABLE [dbo].[Acc_Progress_Status_SP] ADD  CONSTRAINT [DF_Acc_Progress_Status_SS_Release]  DEFAULT ((0)) FOR [Release]
GO
ALTER TABLE [dbo].[ACC_ALUMNO_DOCUMENTOS_SP]  WITH CHECK ADD  CONSTRAINT [FK__ALUMNO_DO__PEOPL__6DEA45F4] FOREIGN KEY([PEOPLE_CODE_ID])
REFERENCES [dbo].[PEOPLE] ([PEOPLE_CODE_ID])
GO
ALTER TABLE [dbo].[ACC_ALUMNO_DOCUMENTOS_SP] CHECK CONSTRAINT [FK__ALUMNO_DO__PEOPL__6DEA45F4]
GO
ALTER TABLE [dbo].[ACC_Documentos_Generales]  WITH CHECK ADD  CONSTRAINT [FK_DO__PEOPL__6DEA45F4] FOREIGN KEY([PEOPLE_CODE_ID])
REFERENCES [dbo].[PEOPLE] ([PEOPLE_CODE_ID])
GO
ALTER TABLE [dbo].[ACC_Documentos_Generales] CHECK CONSTRAINT [FK_DO__PEOPL__6DEA45F4]
GO
ALTER TABLE [dbo].[ACC_SERVICIOS_PROFESIONALES_SP]  WITH CHECK ADD  CONSTRAINT [FK__ACC_SERVI__ESTAT__45A74A70] FOREIGN KEY([ESTATUS_ID])
REFERENCES [dbo].[ACC_CAT_ESTATUS_SPR_SP] ([ESTATUS_ID])
GO
ALTER TABLE [dbo].[ACC_SERVICIOS_PROFESIONALES_SP] CHECK CONSTRAINT [FK__ACC_SERVI__ESTAT__45A74A70]
GO
ALTER TABLE [dbo].[Acc_Solicitudes_Titulacion_SP]  WITH CHECK ADD  CONSTRAINT [FK_Acc_Solicitudes_Titulacion_SP_Acc_Cat_Modalidades_Tipo_SP] FOREIGN KEY([Acc_Modalidad_titulacion_Id])
REFERENCES [dbo].[Acc_Cat_Modalidades_Tipo_SP] ([Acc_Modalidad_titulacion_Id])
GO
ALTER TABLE [dbo].[Acc_Solicitudes_Titulacion_SP] CHECK CONSTRAINT [FK_Acc_Solicitudes_Titulacion_SP_Acc_Cat_Modalidades_Tipo_SP]
GO
