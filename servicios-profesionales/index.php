<?php
    session_start();
    $SecureSection = true;
    $_SESSION["SectionType"] = "Servicios_Profesionales";
    require_once("../util/utilerias.php");
    require_once("secciones.php");
    if(isset($_SESSION['susu'])){
        ?>    
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Universidad Insurgentes</title>
            <link rel="icon" href="../img/uin.ico" />
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
            <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
            <link rel="stylesheet" type="text/css" href="../css/site.css">
            <link rel="stylesheet" type="text/css" href="../css/menu.css">
            <link rel="stylesheet" type="text/css" href="css/style.css">

            <script src="../js/bootstrap.min.js"></script>

            <link rel="stylesheet" href="../js/dist/jquery.fancybox.css" type="text/css">
            
            <script type='text/javascript' src='../js/dist/jquery.fancybox.js'></script>
            <script type='text/javascript' src='../js/tablax2.js'></script>     
            <script type='text/javascript' src='js/funciones.js'></script>
        </head>
        <body>
            <?php
                $obj = new Utilerias;
                $obj -> menu($_SESSION["SectionType"]);
                echo "
                    <div id='container' class='' style='width:100%; height:100%; margin-top:20px; padding: 2% 5%; justify-content:center;'>
                        <div id='x' style='width:100%; text-align:center;'>
                            <div class='row'>";
                                if(isset($_REQUEST["ty"])){
                                    ServProf($_REQUEST["ty"]);
                                }else{
                                    HomeMenu();
                                }
                            echo "</div>  
                        </div>
                        <hr>
                        <footer>
                            <p>© 2019 - Portal</p>
                        </footer>
                    </div>
                ";
            ?>
        </body>
    </html>
<?php
    }else{header("Location: index.php");}
?>