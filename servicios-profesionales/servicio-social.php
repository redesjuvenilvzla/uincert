<?php
    $GLOBALS['thisSPId'] = 1;
    function MenuInterno(){
        echo "
            <a href='?ty=".$_REQUEST["ty"]."&SecId=solicitudes-validacion' class='list-group-item'><i class='fa fa-warning'></i> Solicitudes Pendientes</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=registros-tramite' class='list-group-item'><i class='fa fa-gears'></i> Registros en Tr&aacute;mite</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=servicios-vigentes' class='list-group-item'><i class='fa fa-graduation-cap'></i> Servicios Vigentes</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=servicios-concluidos' class='list-group-item'><i class='fa fa-check'></i> Servicios Concluidos</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=servicios-cancelados' class='list-group-item'><i class='fa fa-times'></i> Servicios Cancelados</a>
        ";
    }
    
    function MuestraContenido(){
        $obj = new ServProf;
        switch ($_REQUEST["SecId"]) {
            case 'solicitudes-validacion':
            case 'registros-tramite':
            case 'servicios-vigentes':
            case 'servicios-concluidos':
            case 'servicios-cancelados':
                Validaciones($_REQUEST["SecId"]);
                break;
            case 'info-alumno':
                InfoAlumno($_POST["thisId"]);
                break;
            case 'actualiza-data':
            case 'nuevo-convenio':
                $obj->UpdateInfo($_POST["SecId"], $_POST["thisId"]);
                break;
            case 'historico':
                InfoAlumno($_REQUEST["regId"]);
                break;
            case 'solicitud-ss-iniciada':
            case 'carta-presentacion-ss':
            case 'carta-liberacion-ss':
            case 'valida-carta-aceptacion':
                $obj->GeneraDocumentos($_REQUEST["SecId"], $_REQUEST["StudentId"]);
                break;
            case 'validar-documentos':
                ValidaDocumentos($_REQUEST["StudentId"], 0);
                break;
            default:
                break;
        }
    }
    
    function ContenidoPrincipal(){
        Validaciones("solicitudes-validacion");
    }
    
    function Validaciones($thisType){
        $obj = new ServProf;
        switch ($thisType) {
            case 'solicitudes-validacion':
                $DisplayButtons     = 1;
                $thisTitle          = "Solicitudes Pendientes de Revisi&oacute;n";

                $thisTable_1        = "ACC_CAMBIO_DATOS_SPR_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(1,4,7,10,11) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                
                $thisTable_2        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_2    = "WHERE ESTATUS_ID IN(16,17) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                    
                $thisTable_3        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_3    = "WHERE ESTATUS_ID IN(7) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
            case 'registros-tramite':
                $DisplayButtons     = 0;
                $thisTitle          = "Registros en Tr&aacute;mite";
                
                $thisTable_1        = "ACC_CAMBIO_DATOS_SPR_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(9,10,12,13,14) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                
                $thisTable_2        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_2    = "WHERE ESTATUS_ID IN(15,17,18,19,22) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                
                $thisTable_3        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_3    = "WHERE ESTATUS_ID IN(7) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
            case 'servicios-vigentes':
                $DisplayButtons     = 1;
                $DisplayData        = 0;
                $thisTitle          = "Listado de Alumnos con Servicio Social vigente";

                $thisTable_1        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(10,18,19,26,28) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
            case 'servicios-concluidos':
                $DisplayButtons     = 1;
                $DisplayData        = 1;
                $thisTitle          = "Listado de Alumnos con Servicio Social Concluido";

                $thisTable_1        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(20,24) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
            case 'servicios-cancelados':
                $DisplayButtons     = 1;
                $DisplayData        = 0;
                $thisTitle          = "Listado de Alumnos con Servicio Social Cancelado";

                $thisTable_1        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(21) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
        }
        echo "
            <div class='info' style='padding:10px; margin-left:0px; margin-top:0px; text-align:left;'>
                <h3 class='page-header'>".$thisTitle."</h3>
                <div id='content' class='content'>
                    <p>Seleccione desde el listado de Alumnos que han realizado una solicitud de revisi&oacute;n de Informaci&oacute;n el registro de su interes:
                </div>
            </div>
            <table id='TablaSolicitudesPendientes' class='table table-rounded table-striped table-sm' cellspacing='0' width='100%' style='text-align:center;'>
                <thead>";
                switch ($thisType) {
                    case 'solicitudes-validacion':
                    case 'registros-tramite':
                        echo "
                            <tr class='bg-primary rounded-top text-white' style='text-align:center;'>
                                <th class='col-xs-2'>#</th>
                                <th class='col-xs-2'>Tipo</th>
                                <th class='col-xs-2'>Id Alumno</th>
                                <th class='col-xs-2'>Nombre</th>
                                <th class='col-xs-2'>A.Paterno</th>
                                <th class='col-xs-2'>A.Materno</th>
                                <th class='col-xs-2'>Liberaci&oacute;n</th>
                                <th class='col-xs-2'>Estatus</th>
                                <th class='col-xs-2'>Pagado</th>
                                <th class='col-xs-2'>Fecha</th>
                                <th class='col-xs-2'>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>";
                            echo $obj->SolicitudesValidacion($thisTable_3, $ThisSqlFilter_3, $DisplayButtons);
                            echo $obj->SolicitudesValidacion($thisTable_2, $ThisSqlFilter_2, $DisplayButtons);
                            echo $obj->SolicitudesValidacion($thisTable_1, $ThisSqlFilter_1, $DisplayButtons);
                        echo "</tbody>";
                        break;
                    case 'servicios-vigentes':
                    case 'servicios-concluidos':
                    case 'servicios-cancelados':
                        echo "
                            <tr class='bg-primary rounded-top text-white' style='text-align:center;'>
                                <th class='col-xs-2'>Alumno</th>
                                <th class='col-xs-2'>Carrera</th>
                                <th class='col-xs-2'>Convenio</th>
                                <th class='col-xs-2'>Estatus</th>
                                <th class='col-xs-2'>Inicio</th>
                                <th class='col-xs-2'>Termino</th>
                                <th class='col-xs-2'>Pagado</th>
                                <th class='col-xs-2'>Liberaci&oacute;n</th>
                                <th class='col-xs-2'>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            ".$obj->RegistrosServiciosProfesionales($thisTable_1, $ThisSqlFilter_1, $DisplayButtons, $DisplayData)."
                        </tbody>
                        ";
                        break;
                }
            echo "</table>";
    }
?>