<?php
    header('Content-Type: text/html; charset=UTF-8');
    
    session_start();
    
    $SecureSection = false;
    require_once("util/utilerias.php");
    $obj = new Utilerias;
    $obj->CnnBD();

    $CAMPUS     = $_POST['thisCampus'];
    $CARRER     = "";
    $STUDENTID  = $_POST['thisStudentId'];
    //$VALPLANTEL = "LEFT JOIN REG_Validacion RVP ON RVP.VP_ID_UNICO_CERT = T1.PEOPLE_CODE_ID+T1.folioControl+T1.Plantel";
   
    $query = str_replace("VARIABLE=CAMPUS", $CAMPUS, TITULOS);
    //$query = str_replace("VARIABLE=TIPO_CERTIFICADO", "AND (RC.TIPO_CERTIFICADO IN (".$TIPCERT."))", $query);
    //$query = str_replace("VARIABLE=CAMPUS", $ANHO, $query);
    $query = str_replace("VARIABLE=CARRERA", $CARRER, $query);
    $query = str_replace("VARIABLE=PEOPLE_CODE_ID", $STUDENTID, $query);
	  //$query = str_replace("VARIABLE=NUMCONTROL", "", $query);
    //$query = str_replace("VARIABLE=VALPLANTEL", $VALPLANTEL, $query);
	  //echo "Query <br>".$query;	
?>
    <!--<link rel='stylesheet' id='compiled.css-css'  href='./css/compiled-4.5.15.min.css' type='text/css' media='all' />-->
<!--   <script type='text/javascript' src='./js/compiled.0.min.js?ver=4.5.15'></script>-->
<!--   <script type='text/javascript' src='./js/tablax2.js'></script>-->
<!--   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
   
   <script>
      $(document).ready(function(){   
       $("#dtDynamicVerticalScrollExample").dataTable().fnDestroy();
       $.fn.dataTable.ext.errMode = 'none';
         $('#dtDynamicVerticalScrollExample').DataTable({
            "scrollY": "45vh",
            "scrollCollapse": true,
            "paging": false
         });
         $('.dataTables_length').addClass('bs-select');
         $('#example').dataTable({
            paging: false,
            searching: false
         });
         $(".btn1").click(function(){
            //valores obtendra el dato del td por posciones [0]
            $("#ViewInfoAlumno").text("");
            var campusId      = $('#CampusId').val();
            var TotalRcds     = $('#TotalRcds').val();
            var carrerId      = $(this).attr("carrer");
            var vNumControl   = $(this).parents("tr").find("td")[1].innerHTML;
            
            // $("#ViewInfoAlumno").load('infoAlumnoT.php?campusId='+campusId
            //    +'&carrerId='+carrerId
            //    +'&vNumControl='+vNumControl
            // );
             $('#infoModalTitle').html('Titulación');
             $("#infoModalBody").empty();

            $("#infoModalBody").load('infoAlumnoT.php?campusId='+campusId
               +'&carrerId='+carrerId
               +'&vNumControl='+vNumControl
               +'&TotalRcds='+TotalRcds
            );

            // $("html, body").animate({scrollTop: $(document).height()}, 1000);
            $('#infoModal').modal();
         });
      });

      $(".form-check-input").click(function(){
         if($("#txtGenXML").val().indexOf($(this).attr("id")) != -1){
            vlCadXML = $("#txtGenXML").val();
            vlCadXML = vlCadXML.replace(",'"+$(this).attr("id")+"'", "");
         }else{
            vlCadXML = $("#txtGenXML").val()+",'"+$(this).attr("id")+"'";
         }
         $("#txtGenXML").val(vlCadXML);

         var vNumControl = $('#txtGenXML').val(); 
         var campo = '#txt'+vNumControl;
         campo = campo.replace("'", "");
         campo = campo.replace("'", "");
         campo = campo.replace(",", "");

         if($("#txtCur").val().indexOf($(campo).val()) != -1){
            vlCadXML = $("#txtCur").val();
            vlCadXML = vlCadXML.replace(","+$(campo).val()+"", "");                                
         }else{
            vlCadXML = $("#txtCur").val()+","+$(campo).val()+"";    
         }
         $("#txtCur").val(vlCadXML);
         $("#ViewInfoAlumno").html("");
      });

      $("#btnGenXML").click(function(){
         var vNumControl = $('#txtGenXML').val();
         if(vNumControl.length == 0){
            alert('Por favor seleccione al menos un alumno antes de generar un XML');
         }else{
            var campusId      = $('#CampusId').val();
            var IdAlumno      = $('#IdAlumno').val();
            var carrerId      = $('#CarrerId').val();
            var curiculum     = $('#txtCur').val();
            var TotalRcds     = $('#TotalRcds').val();

            var vNumControl   = vNumControl.substring(1, 10000);
            thisdivId         = vNumControl.replace("'", "");
            xtipCert          = $("#cmbTipCertificado option:selected").text();

            if(xtipCert == "Ambos"){
               tipCert = "'T','P'";
            }else{
               if(xtipCert == "Total"){
                  tipCert = "'T'";    
               }else{
                  tipCert = "'P'";
               }
            }   

            $("#ViewInfoAlumno").text("");
            $("html, body").animate({scrollTop: $(document).height()}, 1000);

            $("#ViewInfoAlumno").load('GenXMLT.php?campusId='+campusId
               +'&IdAlumno='+IdAlumno
               +'&vNumControl='+vNumControl
               +'&carrerId='+carrerId
               +'&tipCert='+tipCert
               +'&TotalRcds='+TotalRcds
            );
            vlCadXML.empty();
         }                    
      });

      $(document).ready(function(){
          $(".DisplayType").click(function(){
            var thisValue   = $(this).val();
            if($(this).prop('checked')){
              $(".Alumno_"+thisValue).show();
            }else{
              $(".Alumno_"+thisValue).hide();
            }
          });
      })
    </script>

   <?php $rQuery = $obj->xQuery($query); ?>

   <?php 
   //.$_SESSION['susu'].$_SESSION['nom'].$_SESSION['rol']." "?;
   if (($_SESSION['rol'] == '5') OR ($_SESSION['rol'] == '1') )
   { echo " 
    <div style='width:100%; text-align:right;'>
      <button type='submit' id='btnGenXML' class='btn btn-success' style='width:300px; padding:5px;'>
         <i class='fa fa-file-excel-o'></i>
         <span>Generar XML</span>
      </button>
      <div class='col'><input type='hidden' id='txtCur' name='txtCur'></div>
      <div class='col'><input type='hidden' id='txtGenXML' name='txtGenXML'></div>
   </div>
   ";
   }
   ?>

<!--    <div style='width:100%; text-align:right;'>
      <button type='submit' id='btnGenXML' class='btn btn-success' style='width:300px; padding:5px;'>
         <i class='fa fa-file-excel-o'></i>
         <span>Generar XML</span>
      </button>
      <div class="col"><input type="hidden" id="txtCur" name="txtCur"></div>
      <div class="col"><input type="hidden" id="txtGenXML" name="txtGenXML"></div>
   </div> -->

    <section id="datatable-vertical-dynamic-height">
      <section>
<!--        <div style="position:relative; width:100%; height:70px;">
          <div style="position:absolute; width:100%; text-align:center;">
              <input type="checkbox" name="DisplayType" class="DisplayType" value="Prospecto" style="pointer-events:auto; margin-top:5px;" checked> <span style='margin-left:15px'>Prospecto |</span>
              <input type="checkbox" name="DisplayType" class="DisplayType" value="Rechazado" style="pointer-events:auto; margin-top:5px;" checked> <span style='margin-left:15px'>Rechazado |</span>
              <input type="checkbox" name="DisplayType" class="DisplayType" value="Validado" style="pointer-events:auto; margin-top:5px;" checked> <span style='margin-left:15px'>Validado</span>

          </div>
        </div>-->

          <div class="text-center" style="position:relative; width:100%;">
              <label class="container-checkbox mr-2">Prospecto
                  <input type="checkbox" name="DisplayType"  class="DisplayType" value="Prospecto" checked>
                  <span class="checkmark"></span>
              </label>
              <label class="container-checkbox mr-2">Rechazado
                  <input type="checkbox" name="DisplayType"  class="DisplayType" value="Rechazado" checked>
                  <span class="checkmark"></span>
              </label>
              <label class="container-checkbox ">Validado
                  <input type="checkbox" name="DisplayType"  class="DisplayType" value="Validado" checked>
                  <span class="checkmark"></span>
              </label>
          </div>

          <div class="accordion mb-2" id="accordionExample">
              <div class="card border-0">
                  <div class="card-header p-1 px-2 border-0 bg-white rounded-top text-white bb-style-1" id="headingThree">
                      <h6 class=" float-left  text-primary">
                          Notas:

                      </h6>
                      <a class="btn btn-link float-right border-0 p-0 border-0" data-toggle="collapse"
                         data-target="#collapseThree" aria-expanded="true" aria-controls="collapseOne">
                          <i class="fa fa-angle-down text-primary"></i>
                      </a>
                  </div>
                  <div id="collapseThree" class="collapse show" aria-labelledby="headingThree"
                       data-parent="#accordionExample" style="">
                      <div class="card-body p-0 pt-2">
                          <ol>
                              <li><h6>Haga click sobre el icono: <i class="text-info fa fa-id-card-o"></i> para ver o editar la informaci&oacute;n de cada alumno</h6></li>
                              <li><h6>Haga click sobre el icono: <i class="text-warning fa fa-user-times"></i> para ver la raz&oacute;n del por que se rechaz&oacute; cada alumno</h6></li>
                          </ol>
                      </div>
                  </div>
              </div>
          </div>

<div class="dataTables_scrollHead" style="overflow: hidden; position: relative; border: 0px; width: 100%;"> 

  <div class="dataTables_scrollHeadInner" style="box-sizing: content-box; /*width: 1301px;*/ padding-right: 0px;">

        <table id="dtDynamicVerticalScrollExample" class="table table-rounded table-striped table-sm dataTable"  cellspacing="0" width="100%" style="text-align: center; margin-left: 0px; /*width: 1301px;*/" role="grid">
          <thead>
            <tr class="bg-primary rounded-top text-white" style="text-align:center; background-color:#67a1fc;" role="row">
              <th style="text-align:center;">Obtener XML</th>
              <th style="text-align:center;">Id Alumno</th>
              <th style="text-align:center;">Nombre</th>
              <th style="text-align:center;">A.Paterno</th>
              <th style="text-align:center;">A.Materno</th>
              <th style="text-align:center;">Validar Info</th>
              <th style="text-align:center;">Estatus</th>
              <th style="text-align:center;">Raz&oacute;n</th>        	
            </tr>
          </thead>
          <tbody>
			<?php	
      //$data = sqlsrv_fetch_array($rQuery);
      //echo(count($data));
      //if (count($data) !== 0)
      //{
         $TotalRcds = 0;
          while ($data = sqlsrv_fetch_array($rQuery)){
               $thisIdUnicoCert = "P".utf8_encode($data["folioControl"])."_".utf8_encode($data["cveCarrera"]);
               $StudentValidate = $obj->getDbRowName("vp_estatus", "REG_Validacion", "WHERE vp_id_unico_cert = '".$thisIdUnicoCert."'", 1);

               $TotalRcds = $TotalRcds + 1;
               
               if($StudentValidate === "error"){
                  $thisStatus   = "Prospecto";
                  $ClassStatus  = "Prospecto";
                  $vlicono      = "text-warning fa fa-exclamation";
               }else{
                  $thisStatus = $StudentValidate;
                  switch($thisStatus){
                      case "Validado":
                      case "Gen.XML":
                      case "XML autenticado":
                      case "XML timbrado":
                        $ClassStatus  = "Validado";
                        $vlicono      = "text-success fa fa-check";
                      break;
                      case "Rechazado":
                        $ClassStatus  = "Rechazado";
                        $vlicono      = "text-danger fa fa-times";
                      break;
                      default:
                        $ClassStatus  = "Prospecto";
                        $vlicono      = "text-warning fa fa-exclamation";
                      break;
                  }
               }

               switch($thisStatus){
                  case "Validado":
                     if($_SESSION['rol'] <> 4){
//                        $checkRcd = "<input type='checkbox' class='form-check-input' id='".$data["folioControl"]."_".utf8_encode($data["cveCarrera"])."' style='visibility:hidden' value='0'><label class='form-check-label' id='".$data["folioControl"]."_".utf8_encode($data["cveCarrera"])."' for='".$data["folioControl"]."_".utf8_encode($data["cveCarrera"])."'></label>";
                        $checkRcd = "<label class='container-checkbox form-check form-check-inline'>
                <input type='checkbox' name='DisplayType'  class='DisplayType form-check-input' id='".$data["folioControl"]."_".utf8_encode($data["cveCarrera"])."' style='visibility:hidden' value = '0'>
                <span class='checkmark form-check-label'  id='".$data["folioControl"]."_".utf8_encode($data["cveCarrera"])."' for='".$data["folioControl"]."_".utf8_encode($data["cveCarrera"])."'></span>
            </label>";
                     }else{
                        $checkRcd = "N/A";
                     }
                  break;
                  case "Gen.XML":
                     $checkRcd = "XML Generado"; 
                  break;
                  default:
                     $checkRcd = "N/A";
                  break;
               }

              $DeniedReason = $obj->getDbRowName("vp_razon_rechazo", "Reg_Validacion", "WHERE vp_id_unico_cert = 'P".$data["folioControl"]."_".$data["cveCarrera"]."'", 0);

              if($DeniedReason <> ""){
                $DeniedButton = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-fancybox='VerRazonRechazo' data-src='#VerRazonRechazo".$data["folioControl"]."' href='javascript:;'><i class='text-warning fa fa-user-times'></i></a>";
              }else{
                $DeniedButton = "N/A";
              }

               echo '
                    <tr class="Alumno_'.$ClassStatus.'">
                     <td id="CheckAlumno_P'.$data["folioControl"].'">'.$checkRcd.'</td>
                     <td style="text-align:center;">'.$data["folioControl"].'</td>
                     <td style="text-align:center;">'.utf8_encode($data["nombre"]).'</td>
                     <td style="text-align:center;">'.utf8_encode($data["primerApellido"]).'</td>
                     <td style="text-align:center;">'.utf8_encode($data["segundoApellido"]).'</td>
                     <td style="text-align:center;">
                        <input type="hidden" id="CampusId" name="CampusId" value="">
                        <input type="hidden" id="TotalRcds" name="TotalRcds" value="'.$TotalRcds.'">
                        <input type="hidden" id="CarrerId" name="CarrerId" value="'.utf8_encode($data["cveCarrera"]).'">
                        <input type="hidden" id="IdAlumno" name="IdAlumno" type="text" value="P'.utf8_encode($data["folioControl"]).'_'.utf8_encode($data["cveCarrera"]).'" />
                        <button class="btn btn-outline-info btn1" carrer="'.utf8_encode($data["cveCarrera"]).'" value="'.$data["primerApellido"].'"><i class="fa fa-id-card-o"></i></button>
                        <i class="'.$vlicono.'" id="img'.$data["folioControl"].'"></i><input type="hidden" readonly id="txtP'.$data["folioControl"].'" value="'.$data["primerApellido"].'">                         
                     </td>
                     <td style="text-align:center;">
                        <input type="text" readonly class="form-control-plaintext" id="est'.$data["folioControl"].'" value="'.$thisStatus.'" style="text-align:center;">
                     </td>
                     <td id="raz_P'.$data["folioControl"].'_'.utf8_encode($data["cveCarrera"]).'" style="text-align:center;">'.$DeniedButton.'</td>
                  </tr>
               ';
                if(strlen($DeniedReason) <> 0){
                  echo '<span id="VerRazonRechazo'.$data["folioControl"].'" style="width:600px;" class="fancy_info">
                      <b>Raz&oacute;n de Rechazo del Alumno:</b><br />'.utf8_encode($data["nombre"]).' '.utf8_encode($data["primerApellido"]).' '.utf8_encode($data["segundoApellido"]).'
                      <hr style="color: #0056b2;" />
                      <p>'.$DeniedReason.'</p>
                  </span>';
                }
            }
      //}
      //else
      //{
         //echo("No hay alumnos disponibles para esta búsqueda");
      //}

         ?>
         </tbody>
         <tfoot>
            <!--<tr style="background-color:#67a1fc;">
               <th style="text-align:center;">Id Alumno</th>
               <th style="text-align:center;">Nombre</th>
               <th style="text-align:center;">A.Paterno</th>
               <th style="text-align:center;">A.Materno</th>
               <th style="text-align:center;">Validar Info</th>
               <th style="text-align:center;">Estatus</th>
            </tr>-->
         </tfoot>
        </table>
  </div>
</div>

     </section>
    </section>
