<?php
    $SecureSection = false;
	require_once("util/utilerias.php");
	require_once("./config/xData.php");
	
	$thisType = $_POST['SecType'];

	switch($thisType){
		case "CERT_TIM":
			$thisPath = './XMLsCertificados/Timbrados/zip';
		break;
		case "CERT_SEP":
			$thisPath = './XMLsCertificados/Autenticados/zip';
		break;
		case "TIT_TIM":
			$thisPath = './XMLsTitulos/Timbrados/zip';
		break;
		case "TIT_SEP":
			$thisPath = './XMLsTitulos/Autenticados/zip';
		break;
	}
	
	if(!file_exists($thisPath)){
		mkdir($thisPath, 0777, true);
	}

	$thisFileTemp	= $_FILES['file']['tmp_name']; 
	$thisFileSize 	= $_FILES["file"]["size"];
	$thisFileType 	= $_FILES["file"]["type"];
	$thisFileName	= $thisPath."/".$_FILES['file']['name'];
	
	if(is_uploaded_file($thisFileTemp)){ 
		move_uploaded_file($thisFileTemp,$thisFileName);
		$zipFormat	= new ZipArchive;
		$thisFile 	= $zipFormat->open($thisFileName);
		if($thisFile === TRUE){
			switch($thisType){
				case "CERT_TIM":
				case "CERT_SEP":
					$unzipedDir = str_replace('zip', '', $thisFileName);
				break;
				case "TIT_TIM":
				case "TIT_SEP":
					$unzipedDir = str_replace($thisFileName, $thisPath, $thisFileName);
					$unzipedDir = str_replace('zip', 'xml', $unzipedDir);
				break;
			}
			if(!file_exists($unzipedDir)){
				mkdir($unzipedDir, 0777, true);
				$zipFormat -> extractTo($unzipedDir);
				xmlSearch($unzipedDir, $thisType);
			}else{
				$CountFiles = @scandir($unzipedDir);
				if (count($CountFiles) > 2){
					echo "<div style='text-align:center;'><i class='fa fa-times' style='color:#c10409;'></i> <span style='size:30px; color:#c10409;'>Ya se ha cargado un archivo .zip con el mismo nombre con anterioridad, por favor, int&eacute;ntelo con un nombre de archivo distinto</span></div>";
				}else{
					$zipFormat -> extractTo($unzipedDir);
					xmlSearch($unzipedDir, $thisType);
				}
			}
		}else{
        	echo "0"; 
		}
		$zipFormat -> close();
		chown($thisFileName,465);
		unlink($thisFileName);
	}else{ 
        echo "0"; 
	}

	function xmlSearch($thisPath, $thisType){
		$obj = new Utilerias;
		$obj->CnnBD();
		switch($thisType){
			case 'CERT_TIM':
				$FileType		= "CERT";
				$StatusMessage 	= "XML timbrado"; 
			break;
			case 'CERT_SEP': 
				$FileType		= "CERTSEP";
				$StatusMessage 	= "XML autenticado"; 
			break;
			case 'TIT_TIM': 
				$FileType		= "TIT";
				$StatusMessage 	= "XML timbrado"; 
			break;
			case 'TIT_SEP':
				$FileType		= "TIT";
				$StatusMessage 	= "XML autenticado"; 
			break;
		}
		if($handle = opendir($thisPath)){
			while(false !== ($entry = readdir($handle))){
				//echo $handle;
				if($entry != "." && $entry != ".."){
					if(file_exists($thisPath.'/'.$entry)){
						$UpdateGranted = true;
						$xml = simplexml_load_file($thisPath.'/'.$entry) or die("Error: Cannot create object");
						$FolioFileName = "P".$xml[0]['folioControl'];
						switch($thisType){
							case 'TIT_TIM':
								$IdRegValidacion 	= "P".$xml[0]['folioControl'];
								$GetSeal1			= $xml[0]->FirmaResponsables->FirmaResponsable[0]['sello'];
								$GetSeal2			= $xml[0]->FirmaResponsables->FirmaResponsable[1]['sello'];
								if(strlen($GetSeal1) < 300 OR strlen($GetSeal2) < 300){
									$UpdateGranted = false;
									echo "<i class='fa fa-times' style='color:#c10409;'></i> <span style='size:30px; color:#c10409;'>El T&iacute;tulo: <b>".$IdRegValidacion."</b> No fu&eacute; Timbrado, favor de revisarlo.</span><br />";
								}
							break;
							case 'TIT_SEP':
								$IdRegValidacion 	= "P".$xml[0]['folioControl'];
								$GetSeal1			= $xml[0]->Autenticacion['selloTitulo'];
								$GetSeal2			= $xml[0]->Autenticacion['selloAutenticacion'];
								if(strlen($GetSeal1) < 300 OR strlen($GetSeal2) < 300){
									$UpdateGranted = false;
									echo "<i class='fa fa-times' style='color:#c10409;'></i> <span style='size:30px; color:#c10409;'>El T&iacute;tulo de la SEP: <b>".$IdRegValidacion."</b> No se Autentic&oacute;, favor de revisarlo.</span><br />";
								}
							break;
							case 'CERT_TIM': 
								$IdRegValidacion 	= "P".$xml[0]->Alumno['numeroControl'].$xml[0]['folioControl'].$xml[0]->Ipes['campus'];
								$GetSeal1			= $xml['sello'];
								$GetSeal2			= $xml['certificadoResponsable'];
								if(strlen($GetSeal1) < 300 OR strlen($GetSeal2) < 300){
									$UpdateGranted = false;
									echo "<i class='fa fa-times' style='color:#c10409;'></i> <span style='size:30px; color:#c10409;'>El Certificado: <b>".$IdRegValidacion."</b> No fu&eacute; Timbrado, favor de revisarlo.</span><br />";
								}
							break;
							case 'CERT_SEP': 
								$IdRegValidacion 	= "P".$xml[0]->Alumno['numeroControl'].$xml[0]['folioControl'].$xml[0]->Ipes['campus'];
								$GetSeal1			= $xml[0]->Autenticacion['selloTitulo'];
								$GetSeal2			= $xml[0]->Autenticacion['selloAutenticacion'];
								if(strlen($GetSeal1) < 300 OR strlen($GetSeal2) < 300){
									$UpdateGranted = false;
									echo "<i class='fa fa-times' style='color:#c10409;'></i> <span style='size:30px; color:#c10409;'>El Certificado de la SEP: <b>".$IdRegValidacion."</b> No se Autentic&oacute;, favor de revisarlo.</span><br />";
								}
							break;
						}
						if($UpdateGranted){
							$query = "UPDATE REG_Validacion SET vp_estatus = '".$StatusMessage."' WHERE vp_id_unico_cert = '".utf8_decode($IdRegValidacion)."'"; 
							$rQuery = $obj->xQuery($query);
							switch($thisType){
								case 'CERT_TIM':
									echo "<i class='fa fa-check' style='color:#30c005;'></i> <span style='size:30px; color:#30c005;'>El Certificado: <b>".$IdRegValidacion."</b> ha sido Timbrado exitosamente</span><br />";
								break;
								case 'TIT_TIM': 
									echo "<i class='fa fa-check' style='color:#30c005;'></i> <span style='size:30px; color:#30c005;'>El T&iacute;tulo: <b>".$IdRegValidacion."</b> ha sido Timbrado exitosamente</span><br />";
								break;
								case 'CERT_SEP':
									echo "<i class='fa fa-check' style='color:#30c005;'></i> <span style='size:30px; color:#30c005;'>El Certificado de la SEP: <b>".$IdRegValidacion."</b> ha sido Autenticado exitosamente</span><br />";
								break; 
								case 'TIT_SEP':
									echo "<i class='fa fa-check' style='color:#30c005;'></i> <span style='size:30px; color:#30c005;'>El T&iacute;tulo de la SEP: <b>".$IdRegValidacion."</b> ha sido Autenticado exitosamente</span><br />";
								break;
							}
						}else{
							unlink($thisPath.'/'.$entry);
						}
						if(file_exists($thisPath.'/'.$entry)){
							switch($thisType){
								case "TIT_TIM":
								case "TIT_SEP":
									rename($thisPath.'/'.$entry, $thisPath.'/'.$FolioFileName.'.xml');
								break;
							}
						}
					}
				}
			}
			closedir($handle);
		}
	}
?>