    <?php
    header('Content-Type: text/html; charset=UTF-8');

    $SecureSection = false;
    require_once("util/utilerias.php");
    require_once("./config/xData.php");

    $cont = 0;
    $obj = new Utilerias;
    $obj->CnnBD();
    $arrayXML = array();

    echo '<h2 class="text-primary"> Generando XML de alumnos: </h2>';

    $CAMPUS         = $_GET['campus'];
    $ANHO           = $_GET['anho'];
    $FINICIO        = $_GET['finicio'];
    $FFINAL         = $_GET['ffinal'];
    $vNumControl    = $_GET['vNumControl'];
    $TIPCERT        = $_GET['tipCert'];
    $curiculum        = $_GET['curiculum'];
    $campusDescriptionCert        = $_GET['campusDescriptionCert'];
    $NUMCONTROL     = "AND RC.PEOPLE_CODE_ID IN(".$vNumControl.")";
    $VALPLANTEL     = " INNER JOIN REG_Validacion RVP ON RVP.VP_ID_UNICO_CERT = TT.PEOPLE_CODE_ID+TT.folioControl+TT.campus ";
    $CURRICULUM     = 17;

    // $vNumControl;
    //echo $vNumControl;

    $comma_separated = EXPLODE(",", $vNumControl);
    $curriculums = EXPLODE(",", $curiculum);

    //$comma_separated_anho = EXPLODE(",", $ANHO );
    $anhos = EXPLODE(",", $ANHO);

    $ZipFile    = "Cert_".date("Y")."-".date("m")."-".date("d")."-".date("H")."-".date("i")."-".date("s")."_".rand().".zip";

    $FilePath   = $_SERVER["PHP_SELF"];
    $FilePath   = str_replace("GenXML.php", "", $FilePath);
    $testPath   =  $_SERVER['DOCUMENT_ROOT'].''.$FilePath.'XMLsCertificados/General';
    $LinkFile   = 'http://'.$_SERVER["SERVER_NAME"].':'.$_SERVER["SERVER_PORT"].'/'.$FilePath.'/XMLsCertificados/General/'.$ZipFile;
    //$FilePath   = $_SERVER['DOCUMENT_ROOT'].'/'.$FilePath.'/XMLsCertificados/General';

    $thisPath   = 'http://'.$_SERVER["SERVER_NAME"].':'.$_SERVER["SERVER_PORT"].''.$FilePath.'XMLsCertificados/General/';

    //echo '</br>'.'$testPath ' . $testPath;
    //echo '</br>'.'$thisPath ' . $thisPath;
    //echo '</br>'.'$LinkFile ' . $LinkFile;

    //if(file_exists($FilePath)){unlink($FilePath);}

    foreach($comma_separated as $key)
    {
        //echo '</br>'."key ".$key;
        $key2  = str_replace("'", "", $key);
        //echo '</br>'."key2 ".$key2;
        //echo '</br>'."contador ".$cont;

        $curriculumProcesado =  str_replace("'", "", $curriculums[$cont]);
        $anhoProcesado =  str_replace("'", "", $anhos[$cont]);
        //echo '</br>'."curriculumProcesado ".$curriculumProcesado;
        //echo '</br>'."anhoProcesado ".$anhoProcesado;
        $cont++;

        $query      = str_replace("VARIABLE=CAMPUS", $CAMPUS, CERTIFICADOSALTAS);
        $query      = str_replace("VARIABLE=TIPO_CERTIFICADO", "AND (RC.TIPO_CERTIFICADO IN (".$TIPCERT."))", $query);
        $query      = str_replace("VARIABLE=ANHO", $anhoProcesado, $query);
        $query      = str_replace("VARIABLE=FINICIAL", $FINICIO, $query);
        $query      = str_replace("VARIABLE=FFINAL", $FFINAL, $query);

        $NUMCONTROL     = "AND RC.PEOPLE_CODE_ID IN(".$key.")";
        $query      = str_replace("VARIABLE=NUMCONTROL", $NUMCONTROL, $query);
        $query      = str_replace("VARIABLE=VALPLANTEL", $VALPLANTEL, $query);
        $query      = str_replace("VARIABLE=CURRICULUM", $curriculumProcesado, $query);
        $query      = str_replace("VARIABLE=TOP", "", $query);
        $rQuery     = $obj->xQuery($query);

        //echo '</br>'.'</br>'.$query.'</br>'.'</br>';

        $contadorData = 0;
        while($data = sqlsrv_fetch_array($rQuery))
        {
            if ( $contadorData === 0)
            {
                $xquery = "UPDATE REG_Validacion SET vp_estatus = 'Gen.XML' WHERE vp_id_unico_cert = '".$data["ID_UNICO_CERT"]."'";        
                $obj->xQuery($xquery);

                $nombreCampus = (utf8_encode($data["campus"]));

                $sello = "";
                $certificadoResponsable = "";

                $xml                = new DomDocument('1.0', 'UTF-8');
                $xml->formatOutput  = true;
                $el_xml             = $xml->saveXML();

                //echo '</br>'.'</br>'.$thisPath;

                if(!file_exists($testPath)){
                    mkdir($testPath, 0777, true);
                }
                
                //echo 'CAMPUS'.(String)$CAMPUS;

                if(!file_exists($thisPath)){mkdir($thisPath, 0777, true);}
                $filename = date("Y")."-".date("m")."-".date("d")."-".$data["PEOPLE_CODE_ID"]."-Certificado.xml";
                $xml->save(trim($filename));
                $fp = fopen($filename, 'w+');

                $newXml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'."\n";
                $newXml .= '<Dec xmlns="https://www.siged.sep.gob.mx/certificados/" version="2.0"'; 

                $newXml .= " ".'tipoCertificado="'.utf8_encode($data["tipoCertificado"]) .'"';
                $newXml .= " ".'folioControl="'.utf8_encode($data["folioControl"]).'"';
                $newXml .= " ".'sello="'.utf8_encode($sello).'"';
                $newXml .= " ".'certificadoResponsable="'.utf8_encode($certificadoResponsable).'"';
                $newXml .= " ".'noCertificadoResponsable="'.utf8_encode($data["noCertificadoResponsable"]).'"';
                $newXml .= '>'."\n";

                $newXml .= "  ".'<Ipes ';
                $newXml .= 'idNombreInstitucion="'.utf8_encode($data["idNombreInstitucion"]).'"';
                //$newXml .= " ".'nombreInstitucion="'.utf8_encode($data["campus"]).'"';
                $newXml .= " ".'nombreInstitucion="UNIVERSIDAD INSURGENTES"';
                $newXml .= " ".'idCampus="'.utf8_encode($data["idCampus"]).'"';
                $newXml .= " ".'campus="'.$nombreCampus.'"';
                $newXml .= " ".'idEntidadFederativa="'.utf8_encode($data["idEtidadFederativa"]).'"';
                $newXml .= " ".'entidadFederativa="'.utf8_encode($data["entidadFederativa"]).'"';
                $newXml .= '>'."\n";

                $newXml .= "    ".'<Responsable';
                $newXml .= " ".'nombre="'.utf8_encode($data["nombre"]).'"';
                $newXml .= " ".'primerApellido="'.utf8_encode($data["primerApellido"]).'"';
                $newXml .= " ".'segundoApellido="'.utf8_encode($data["segundoApellido"]).'"'; 
                $newXml .= " ".'curp="'.utf8_encode($data["curp"]).'"';
                $newXml .= " ".'idCargo="'.utf8_encode($data["idCargo"]).'"'; 
                $newXml .= " ".'cargo="'.utf8_encode($data["cargo"]).'"';
                $newXml .= '/>'."\n";
                $newXml .= "  ".'</Ipes>'."\n";

                $newXml .= "  ".'<Rvoe';
                $newXml .= " ".'numero="'.utf8_encode($data["numero"]).'"';
                $newXml .= " ".'fechaExpedicion="'.utf8_encode($data["fechaExpedicion"]).'"';
                $newXml .= '/>'."\n";

                $newXml .= "  ".'<Carrera';
                $newXml .= " ".'idCarrera="'.utf8_encode($data["idCarrera"]).'"';
                $newXml .= " ".'claveCarrera="'.utf8_encode($data["claveCarrera"]).'"';
                $newXml .= " ".'nombreCarrera="'.utf8_encode($data["nombreCarrera"]).'"';
                $newXml .= " ".'idTipoPeriodo="'.utf8_encode($data["idTipoPeriodo"]).'"';
                $newXml .= " ".'tipoPeriodo="'.utf8_encode($data["tipoPeriodo"]).'"';
                $newXml .= " ".'clavePlan="'.utf8_encode($data["clavePlan"]).'"';
                $newXml .= " ".'idNivelEstudios="'.utf8_encode($data["ID_NIVEL"]).'"';//idNivelEstudios v 2.0
                $newXml .= " ".'nivelEstudios="'.utf8_encode($data["NOMBRE_ID_ESTUDIOS"]).'"';//nivelEstudios
                $newXml .= " ".'calificacionMinima="'.utf8_encode($data["CALIF_MINIMA"]).'"';//calificacionMinima
                $newXml .= " ".'calificacionMaxima="'.utf8_encode($data["CALIF_MAXIMA"]).'"';//calificacionMaxima
                $newXml .= " ".'calificacionMinimaAprobatoria="'.utf8_encode($data["CALIF_MINIMA_APROBATORIA"]).'"';//calificacionMinimaAprobatoria
                $newXml .= '/>'."\n";

                $newXml .= "  ".'<Alumno';
                $newXml .= " ".'numeroControl="'.utf8_encode($data["numeroControl"]).'"';
                $newXml .= " ".'curp="'.utf8_encode($data["curp2"]).'"';
                $newXml .= " ".'nombre="'.utf8_encode($data["nombre3"]).'"';
                $newXml .= " ".'primerApellido="'.utf8_encode($data["primerApellido4"]).'"';
                $newXml .= " ".'segundoApellido="'.utf8_encode($data["segundoApellido5"]).'"';
                $newXml .= " ".'idGenero="'.utf8_encode($data["idGenero"]).'"';
                $newXml .= " ".'fechaNacimiento="'.utf8_encode($data["fechaNacimiento"]).'"';
                $newXml .= '/>'."\n";

                $newXml .= "  ".'<Expedicion';
                if($data["tipoCertificacion"] === 'TOTAL'){
                    $idTipoCertificacion = 79;
                }else{
                    $idTipoCertificacion = 80;
                }
                $newXml .= " ".'idTipoCertificacion="'.utf8_encode($idTipoCertificacion).'"';
                $newXml .= " ".'tipoCertificacion="'.utf8_encode($data["tipoCertificacion"]).'"';
                $newXml .= " ".'fecha="'.utf8_encode($data["fecha"]).'"';
                $newXml .= " ".'idLugarExpedicion="'.utf8_encode($data["idLugarExpedicion"]).'"';
                $newXml .= " ".'lugarExpedicion="'.utf8_encode($data["lugarExpedicion"]).'"';
                $newXml .= '/>'."\n";

                $newXml .= "  ".'<Asignaturas';
                $newXml .= " ".'total="'.utf8_encode($data["total"]).'"';
                $newXml .= " ".'asignadas="'.utf8_encode($data["asignadas"]).'"';
                $newXml .= " ".'promedio="'.utf8_encode($data["promedio"]).'"';
                $newXml .= " ".'totalCreditos="'.utf8_encode($data["TOTAL_CREDITOS"]).'"'; //totalCreditos
                $newXml .= " ".'creditosObtenidos="'.utf8_encode($data["CREDITOS_OBTENIDOS"]).'"'; //creditosObtenidos
                $newXml .= '>'."\n";

                if($data["tipoCertificacion"] === 'TOTAL'){$TC = 'T';} else { $TC = 'P';}
                $queryA = str_replace("VARIABLE=PEOPLE_CODE_ID", utf8_encode($data["PEOPLE_CODE_ID"]), CERTIFICADOSALTASMATERIAS);
                $queryA = str_replace("VARIABLE=CAMPUS", utf8_encode($data["ACADEMIC_SESSION"]), $queryA);
                $queryA = str_replace("VARIABLE=TIPOCERTIFICADO", $TC, $queryA);                               
                $queryA = str_replace("VARIABLE=ANHO", substr($data["fecha"], 0, 4), $queryA);
                $queryA = str_replace("VARIABLE=CURRICULUM", $curriculumProcesado, $queryA);             
                $queryA = str_replace("VARIABLE=ID_UNICO_CERT", "'T','P'", $queryA);
                $rQueryA = $obj->xQuery($queryA);
                //echo $queryA;
                $TRegA = $obj->xCQuery();
                //echo "<br><br><br><br>CURRICULUM".$CURRICULUM."<br><br><br><br>";
                //echo "<br><br><br><br>".$queryA."<br><br><br><br>";
                if($TRegA != 0){
                    while ($dataA = sqlsrv_fetch_array($rQueryA)){ 
                        $newXml .= "  ".'<Asignatura';
                        $newXml .= " ".'idAsignatura="'.utf8_decode(($dataA["idAsignatura"])).'"';
                        $newXml .= " ".'claveAsignatura="'.utf8_encode($dataA["claveAsignatura"]).'"';
                        $newXml .= " ".'nombre="'.utf8_encode($dataA["nombre6"]).'"';
                        $newXml .= " ".'ciclo="'.utf8_encode($dataA["ciclo"]).'"';
                        $newXml .= " ".'calificacion="'.$dataA["calificacion"].'"';
                        $newXml .= " ".'idObservaciones="'.utf8_encode($dataA["idObservaciones"]).'"';
                        $newXml .= " ".'observaciones="'.utf8_encode($dataA["observaciones"]).'"';
                        $newXml .= " ".'idTipoAsignatura="'.utf8_encode($dataA["ID_TIPO_ASIGNATURA"]).'"';
                        $newXml .= " ".'tipoAsignatura="'.utf8_encode($dataA["TIPO_ASIGNATURA"]).'"';
                        $newXml .= " ".'creditos="'.utf8_encode($dataA["CREDITOS"]).'"';
                        $newXml .= '/>'."\n";
                    }
                }else{
                    echo "Error en consulta asignaturas, alumno ".$data["PEOPLE_CODE_ID"].'</br>';
                //echo "<br>".$queryA."<br>";
                }
                $newXml .= " ".'</Asignaturas>'."\n";
                $newXml .= '</Dec>'."\n";   
                fwrite($fp, $newXml);
                fclose($fp);
            //echo $filename;
                array_push($arrayXML, $filename);
                $contadorData++;

            }
            else
            {
               //Alumno repetido por consulta
            }
        }
    }

    $thisPath   = $_SERVER["PHP_SELF"];
    $thisPath   = str_replace("GenXML.php", "", $thisPath);
    $tmp_file   = 'http://'.$_SERVER["SERVER_NAME"].':'.$_SERVER["SERVER_PORT"].'/'.$thisPath.'/XMLsCertificados/General/'.$ZipFile;
    $file_path  = $_SERVER['DOCUMENT_ROOT'].'/'.$thisPath.'/XMLsCertificados/General/'.$ZipFile;

    if(file_exists($file_path)){
        unlink($file_path);//Destruye el archivo temporal
    } 
    $zip = new ZipArchive;
    foreach($arrayXML as $i =>$key)
    {
        $i >0;
        if ($zip->open($file_path, ZipArchive::CREATE) === TRUE) {
            $zip->addFile($key);
        }
        else {
            //echo 'Failed! temporalXML.zip no existe ';
        }
    }
    $zip->close();
    
    foreach($arrayXML as $i =>$key){unlink($key);}

    if (file_exists($file_path)){
//        echo "Descargar XML de alumnos: <a href=$tmp_file>temporalXML.zip</a>";
        echo "Descargar XML de alumnos: <a class='btn btn-success' href=$tmp_file>   <i class='fa fa-download'></i> DESCARGAR </a>";
        //echo "Archivo .zip generado: <b>".$archive.".zip - <a href='".$tmp_file."'>DESCARGAR</a></b>";
        exit;
    } 
    else {
        echo 'Failed!';
    }
    echo "Archivo .zip generado: <b>".$ZipFile." - <a class='btn btn-success' href='".$LinkFile."'><i class='fa fa-download'></i> DESCARGAR</a></b>";
?>
