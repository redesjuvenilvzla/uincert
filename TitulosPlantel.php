<?php
    session_start();
    $SecureSection = false;
    require_once("util/utilerias.php");
    $obj = new Utilerias;
    $obj->CnnBD();
?>
    <script>
        $("#mainTable").dataTable({
            destroy: true,
            retrieve:true,
            paging: false
        });

        $('#cmbCampusTitulos').change(function(){
        //console.log($('#cmbCampusTitulos').val());
        var id = $('#cmbCampusTitulos').val();
        
        $(this).data('cmbCarreraTitulos').refresh();
        //alert("affds");
        });

        $(document).on('click', '#searchTitulosPlantel', function(e){
            e.preventDefault();
            e.stopImmediatePropagation();
            formdata        = new FormData();
            var campus      = $('#Campus').val();
            var carrer      = $('#Carrer').val();
            var studentid   = $('#StudentId').val();
            var xSearchData = true;

            if(studentid.length < 1)
            {
                xSearchData = false;
            }
            else
            {
                xSearchData = true;
            } 
            
            $('#searchTitulosPlantel').unbind('click.new');
            if(xSearchData){
                formdata.append("thisCampus", campus);
                formdata.append("thisCarrer", carrer);
                formdata.append("thisStudentId", studentid);
                jQuery.ajax({
                    url: 'TitulosPlantelList.php',
                    type: "POST",
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function (result){
                        $("#ListAlumnos").html(result);
                    }
                });
            }else{
                alert("Por favor ingrese un ID Alumno antes de iniciar una búsqueda");
            }
            $("div").find("#ViewInfoAlumno").html("");
            //$("#ListAlumnos").load('CertificadosPlantelList.php?campus='+campus+'&finicial='+finicial+'&ffinal='+ffinal);
        });

        $(document).ready(function(){
            $.ajax({
                type: "POST",
                url: "SeleccionaCampus.php",
                data: {Campus: "CmbTit"} 
            }).done(function(data){
                $("#Campus").html(data);
            });
            $("#Campus").change(function(){
                var thisCampus = $("#Campus option:selected").val();
                $.ajax({
                    type: "POST",
                    url: "SeleccionaCampus.php",
                    data: {Carrer: thisCampus} 
                }).done(function(data){
                    $("#Carrer").html(data);
                });
            });
        });
    </script>

    <!-- <h8 style="color:#0054a4;text-shadow: 5px 5px 5px #aaa; padding:20px 5px;"> Títulos </h8> -->
    <h2 class="text-primary">T&Iacute;TULOS</h2>
    <hr>
    <div class="row">
      <div class="col"><h6>ID Alumno</h6></div>
      <div class="col"><h6>Campus (Opcional)</h6></div>
      <div class="col"><h6>Carrera (Opcional)</h6></div>
      <div class="col"></div>
    </div>
    <div class="row">
      <div class="col"><input type="text" id="StudentId" name="StudentId" class="form-control" placeholder="Ingrese el ID del Alumno"></div>
      <div class="col"><select id='Campus' name='Campus' class='form-control'><option value=''>CAMPUS</option></select></div>
      <div class="col">
        <select id='Carrer' name='Carrer' class='form-control'>
            <option value=''>CARRERRA</option>
            <?php echo $obj->cmbCarreraTitulos($obj->cmbCampusId());?>
        </select>
      </div> 
      <div class="col" align="left"><?php echo $obj->btnBuscar('TitulosPlantel'); ?></div>
    </div>        
    <hr/>    
    <div id='ListAlumnos'></div>
    <br /><br />
    <div id='ViewInfoAlumno'></div>

