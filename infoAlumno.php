﻿<?php
    header('Content-Type: text/html; charset=UTF-8');
    
    $SecureSection = false;
    require_once("util/utilerias.php");
    require_once("./config/xData.php");
?>

<!--<script src="https://code.jquery.com/jquery-1.8.3.min.js"></script>-->
<!--    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>-->
<script>
    function upfle(pname, countFile){
        var countThisFile   = countFile;
        var uploadFile      = '#Upload'+event.target.id;
        var buttonsFile     = '#Buttons'+event.target.id;
        var loadedFiles     = $("div").find('#LoadedFiles').val();

        var formdata        = new FormData();
        var file            = $(pname)[0].files[0];
        var xtitle          = $(pname).attr("title");
        var idAlumno        = $('#FolioCont').val();
        var pathFile        = $(pname).attr("pathfile");

        formdata.append("file", file);
        formdata.append("idAlumno", idAlumno);
        formdata.append("CveCarrera", "NA");
        formdata.append("tipoDoc", xtitle);
        formdata.append("pathFile", pathFile);
        formdata.append("FileNumber", countFile);
        formdata.append("thisType", "XDoc");

        jQuery.ajax({
            url: 'upload.php',
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false
            }).done(function(msg){
                if(msg.split(" | ")[0] == 1){
                    $(uploadFile).fadeOut(500, function(){
                        $(buttonsFile).html(msg.split(" | ")[1]);
                        $(buttonsFile).fadeIn(500);
                        if(countThisFile == 1){
                            loadedFiles = parseInt(loadedFiles) + parseInt(1);
                            $("div").find('#LoadedFiles').attr('value', loadedFiles);
                            if(loadedFiles >= 5){
                                $('#ValidaAlumno').attr('disabled', false);
                            }else{
                                $('#ValidaAlumno').attr('disabled', true);
                            }
                        }
                    });
                }else{
                    $(".main_files").append(msg);
                }
            });
    }

    function mfile(pname){
        var myfiles  = document.getElementById("OtherFiles");
        var files    = myfiles.files;
        var data     = new FormData();
        var pathFile = $(pname).attr("pathfile");

        idAlumno = $('#IdAlumno').val();
        data.append("idAlumno", FolioCont);
        data.append("pathFile", pathFile);
        data.append("tipoDoc", 'OtherFiles');
        data.append("thisType", "XDoc");
        data.append("CveCarrera", "");
        data.append("FileNumber", "");

        for (i = 0; i < files.length; i++){
            data.append('file' + i, files[i]);
        }

        $.ajax({
            url: 'upload.php', 
            type: 'POST',
            data: data,
            contentType: false,
            processData: false,
            cache: false
            }).done(function(msg){
                $(".other_files").append(msg);
            });
    }

    function deleteThisFile(fileId, countFile){
        if(confirm("Estas seguro que deseas eliminar este archivo?")){
            var countThisFile   = countFile;            
            var thisId          = $(fileId).attr('id');
            var thisFile        = $(fileId).attr('file');
            var thisTip         = $(fileId).attr('tip');

            var tipoDoc         = $(fileId).attr("tipoDoc");
            var idAlumno        = $(fileId).attr("fileFolio");
            var ThisCarrer      = $(fileId).attr("fileCarrer");
            
            var data            = $(fileId).serializeArray();
            var loadedFiles     = $("div").find('#LoadedFiles').val();

            data.push({name:'SecType', value:'delete_file'});
            data.push({name:'tipoDoc', value:tipoDoc});
            data.push({name:'idAlumno', value:idAlumno});
            data.push({name:'CveCarrera', value:ThisCarrer});
            data.push({name:'thisFile', value:thisFile});
            
            $.ajax({
                url:        'valplantel.php',
                type:       'post',
                dataType:   'json',
                data: data
            }).always(function(){
                switch(thisTip){
                    case 'newfile': $("div").find('#newfile_'+thisId).fadeOut(500); break;
                    case 'oldfile': $("div").find('#file_'+thisId).fadeOut(500); break;
                    case 'reqfile':
                        $('#Buttonsfile'+thisId).fadeOut(500);
                        $('#Uploadfile'+thisId).delay(500).fadeIn(500);
                    break;
                }
                if(countThisFile == 1){
                    loadedFiles = parseInt(loadedFiles) - parseInt(1);
                    $("div").find('#LoadedFiles').attr('value', loadedFiles);
                    if(loadedFiles < 5){
                        $('#ValidaAlumno').attr('disabled', true);
                    }else{
                        $('#ValidaAlumno').attr('disabled', false);
                    }
                }
            });
            return false;
        }
    }

    $(document).ready(function(){
        $('#ValidaAlumno').click(function(e){
            var opcion = confirm("Esta completamente seguro de querer Validar a este Alumno?");
            if(opcion == true){
                e.preventDefault();
                var data = $(this).serializeArray();
                data.push({name:'SecType', value:'CER'});
                data.push({name:'StudentId', value:$('#IdCertificado').val()});
                data.push({name:'Status', value:'Validado'});
                $.ajax({
                    url: 'valplantel.php',
                    type: 'post',
                    dataType: 'json',
                    data: data
                })
                .always(function(){
                    var thisId  = '#img'+$('#IdAlumno').val();
                    var name    = '#est'+$('#IdAlumno').val();
                    var raz     = '#raz_P'+$('#IdAlumno').val();
                    $(name).val('Validado');
                    $(raz).html("N/A");
                    $("#ValButtons").fadeOut(800);
                    $(thisId).removeClass('text-warning fa fa-exclamation').addClass('text-success fa fa-check');
                    // $('html, body').animate({scrollTop: $("div").find("#dtDynamicVerticalScrollExample").offset().top}, 500);
                    $('#infoModal').modal('hide');
                });
            }
        })
    })

    $(document).ready(function(){
        $('#RechazaAlumno').click(function(e){
            e.preventDefault();
            var data = $(this).serializeArray();
            data.push({name:'SecType', value:'CER'});
            data.push({name:'StudentId', value:$('#IdCertificado').val()});
            data.push({name:'Status', value:'Rechazado por plantel'});
            data.push({name:'RazonRechazo', value:$('#razon_rechazo').val()});

            $(".tab-pane").removeClass("active");
            $("#RAZON").removeClass("fade");
            $("#RAZON").addClass("active");

            if($('#razon_rechazo').val().length == 0){
                alert("Es Necesario que introduzca la razon del rechazo del alumno");
            }else{
                var opcion = confirm("Esta completamente seguro de querer Rechazar a este Alumno?");
                if(opcion == true){
                    $.ajax({
                        url: 'valplantel.php',
                        type: 'post',
                        dataType: 'json',
                        data: data
                    })
                    .always(function(){
                        var thisId  = '#img'+$('#IdAlumno').val();
                        var name    = '#est'+$('#IdAlumno').val();
                        var raz     = '#raz_P'+$('#IdAlumno').val();
                        $(name).val('Rechazado por plantel');
                        $(raz).html("<i class='text-warning fa fa-user-times'></i>");
                        $(thisId).removeClass('text-success fa fa-check').addClass('text-warning fa fa-exclamation');
                        $('#infoModal').modal('hide');

                        // $('html, body').animate({scrollTop: $("div").find("#dtDynamicVerticalScrollExample").offset().top}, 500);
                    });
                }
            }
        })
    })
</script>

<?php
    $obj = new Utilerias;
	$obj->CnnBD();
	
    $CAMPUS = $_GET['campus'];
    $ANHO = $_GET['anho'];
    $FINICIO = $_GET['finicio'];
    $FFINAL = $_GET['ffinal'];
	$vNumControl = $_GET['vNumControl'];
	$TIPCERT = $_GET['tipCert'];
    $CURRICULUM = $_GET['curiculum'];

	$NUMCONTROL = "AND RC.PEOPLE_CODE_ID = '".$vNumControl."'";
    $VALPLANTEL = "";
    $query = str_replace("VARIABLE=CAMPUS", $CAMPUS, CERTIFICADOSALTAS);
    $query = str_replace("VARIABLE=TIPO_CERTIFICADO", "AND (RC.TIPO_CERTIFICADO IN (".$TIPCERT."))", $query);
    $query = str_replace("VARIABLE=ANHO", $ANHO, $query);
    $query = str_replace("VARIABLE=FINICIAL", $FINICIO, $query);
    $query = str_replace("VARIABLE=FFINAL", $FFINAL, $query);
	$query = str_replace("VARIABLE=NUMCONTROL", $NUMCONTROL, $query);
	$query = str_replace("VARIABLE=VALPLANTEL", $VALPLANTEL, $query);
    $query = str_replace("VARIABLE=CURRICULUM", $CURRICULUM, $query);
    $query = str_replace("VARIABLE=TOP", " TOP 1 ", $query); 
//	echo $query . '<br>';
	$rQuery = $obj->xQuery($query);
    $ValidateGranted = true;
    while ($data = sqlsrv_fetch_array($rQuery)){
        $FolioCont      = utf8_encode($data["ID_UNICO_CERT"]);
        $nombreCampus   = utf8_encode($data["campus"]);
        $DeniedReason   = $obj->getDbRowName("vp_razon_rechazo", "Reg_Validacion", "WHERE vp_id_unico_cert = '".$data["ID_UNICO_CERT"]."'", 0);
        $ValidateStatus = $obj->getDbRowName("vp_estatus", "Reg_Validacion", "WHERE vp_id_unico_cert = '".$data["ID_UNICO_CERT"]."'", 0);
          echo "
          <h5>".utf8_encode($data["numeroControl"])."  ".utf8_encode($data["nombre3"])." ".utf8_encode($data["primerApellido4"])." ".utf8_encode($data["segundoApellido5"])." <span class='label label-default'></span></h5>
          <ul class='nav nav-tabs' role='tablist'>
            <li class='nav-item'><a class='nav-link active' data-toggle='tab' href='#GENERAL'>General</a></li>
                <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#IPES'>Ipes</a></li>
                <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#RESPONSABLE'>Responsable</a></li>
                <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#RVOE'>Rvoe</a></li>
                <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#CARRERA'>Carrera</a></li>
                <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#ALUMNO'>Alumno</a></li>
                <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#EXPEDICION'>Expedici&oacute;n</a></li>
                <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#ASIGNATURAS'>Asignaturas</a></li>
                <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#DOCUMENTOS'>Documentos</a></li>
                <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#RAZON'>Raz&oacute;n Rechazo</a></li>
          </ul>
          ";
	echo "
		<div class='tab-content'>
			<div id='GENERAL' class='container tab-pane active'><br>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Tip. Certificado</p></div>
					<div class='col-sm-6'>
						<input id='Tip. Certificado' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["tipoCertificado"])."' disabled/>		
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Fol. Control</p></div>
					<div class='col-sm-6'>
						<input id='Fol. Control' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["folioControl"])."' disabled />		
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Id Unico Certificado</p></div>
					<div class='col-sm-6'>
						<input id='IdCertificado' name='IdCertificado' type='text' class='form-control' size='29' autocomplete='false' value='".utf8_encode($data["ID_UNICO_CERT"])."' disabled/>		
					</div>
				</div>                
			</div>
			<div id='IPES' class='container tab-pane fade'><br>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Nom. Institución</p></div>
					<div class='col-sm-6'>
						<input id='Nom. Institucíon' type='text' size='60' class='form-control' autocomplete='false' value='UNIVERSIDAD INSURGENTES' disabled />		
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Campus</p></div>
					<div class='col-sm-6'>
						<input id='Campus' type='text' size='60' class='form-control' autocomplete='false' value='".$nombreCampus."' disabled />		
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Ent. Federativa</p></div>
					<div class='col-sm-6'>
						<input id='Ent. Federativa' type='text' size='60' class='form-control' autocomplete='false' value='".utf8_encode($data["entidadFederativa"])."' disabled />		
					</div>
				</div>				
			</div>
            <div id='RESPONSABLE' class='container tab-pane fade'><br>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Nombre</p></div>
					<div class='col-sm-6'>
						<input id='Nombre' type='text' autocomplete='false' class='form-control' value='".utf8_encode($data["nombre"])."' disabled />		
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>A. Paterno</p></div>
					<div class='col-sm-6'>
						<input id='A. Paterno' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["primerApellido"])."' disabled />		
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>A. Materno</p></div>
					<div class='col-sm-6'>
						<input id='A. Materno' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["segundoApellido"])."' disabled />		
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Cargo</p></div>
					<div class='col-sm-6'>
						<input id='Cargo' type='text' size='60' class='form-control' autocomplete='false' value='".utf8_encode($data["cargo"])."' disabled />		
					</div>
				</div>			
            </div>
            <div id='RVOE' class='container tab-pane fade'><br>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Número RVOE</p></div>
					<div class='col-sm-6'>
						<input id='Número' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["numero"])."' disabled />
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Fec. Expedición </p></div>
					<div class='col-sm-6'>
						<input id='Fec. Expedición ' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["fechaExpedicion"])."'  disabled />
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Clve. Plan</p></div>
					<div class='col-sm-6'>
						<input id='Clve. Plan' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["clavePlan"])."' disabled />
					</div>
				</div>
            </div>
            <div id='CARRERA' class='container tab-pane fade'><br>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Nom. Carrera</p></div>
					<div class='col-sm-6'>
						<input id='Nom. Carrera' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["nombreCarrera"])."' disabled />
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Tip. Periodo</p></div>
					<div class='col-sm-6'>
						<input id='Tip. Periodo' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["tipoPeriodo"])."' disabled />
					</div>
				</div>
            </div>
            <div id='ALUMNO' class='container tab-pane fade'><br>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>ID</p></div>
					<div class='col-sm-6'>
                        <input type='hidden' id='FolioCont' value='".$FolioCont."'>
						<input id='IdAlumno' name='IdAlumno' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["numeroControl"])."' disabled />
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>CURP</p></div>
					<div class='col-sm-6'>
						<input id='CURP' type='text' class='form-control'  autocomplete='false' value='".utf8_encode($data["curp2"])."' disabled />
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Nombre</p></div>
					<div class='col-sm-6'>
						<input id='Nombre' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["nombre3"])."' disabled />
					</div>
				</div>				
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>A. Paterno</p></div>
					<div class='col-sm-6'>
						<input id='A. Paterno' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["primerApellido4"])."' disabled />
					</div>
				</div>				
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>A. Materno</p></div>
					<div class='col-sm-6'>
						<input id='A. Materno' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["segundoApellido5"])."' disabled />
					</div>
				</div>				
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Género</p></div>
					<div class='col-sm-6'>
						<input id='Id. Genero' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["idGenero"])."' disabled />
					</div>
				</div>				
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Fec. Nacimiento</p></div>
					<div class='col-sm-6'>
						<input id='Fec. Nacimiento' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["fechaNacimiento"])."' disabled />
					</div>
				</div>				
            </div>
            <div id='EXPEDICION' class='container tab-pane fade'><br>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Tip. Certificación</p></div>
					<div class='col-sm-6'>
						<input id='Tip. Certificación' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["tipoCertificacion"])."' disabled />
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Fecha</p></div>
					<div class='col-sm-6'>
						<input id='Fecha' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["fecha"])."' disabled />
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Lugar Expedición</p></div>
					<div class='col-sm-6'>
						<input id='Lugar Espedición' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["lugarExpedicion"])."' disabled />
					</div>
				</div>				
            </div>
            <div id='ASIGNATURAS' class='container tab-pane fade'><br>

               <div class='row'>               
                    <div class='col-sm-3'><p class='navbar-text'>Total de créditos</p></div>
                    <div class='col-sm-6'>
                        <input id='TOTAL_CREDITOS' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["TOTAL_CREDITOS"])."' disabled />
                    </div>
                </div>

                <div class='row'>               
                    <div class='col-sm-3'><p class='navbar-text'>Créditos obtenidos</p></div>
                    <div class='col-sm-6'>
                        <input id='CREDITOS_OBTENIDOS' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["CREDITOS_OBTENIDOS"])."' disabled />
                    </div>
                </div>

                    <div class='tabla table-scroll'>
                        <table id='grid' class='table table-rounded table-striped table-sm dataTable'>
                            <thead>
                                <tr class='bg-primary rounded-top text-white'>
                                    <th>Clave</th>
                                    <th>Nombre</th>
                                    <th>Ciclo</th>
                                    <th>Calificación</th>
                                    <th>Observaciones</th>
                                    <th>id Tipo Asignatura</th>
                                    <th>Tipo Asignatura</th>
                                    <th>Créditos</th>
                                </tr>
                            </thead>
                            <tbody>
	                        ";	
								if($data["tipoCertificacion"] === 'TOTAL'){$TC = 'T';} else { $TC = 'P';}
								$queryA = str_replace("VARIABLE=PEOPLE_CODE_ID", $data["PEOPLE_CODE_ID"], CERTIFICADOSALTASMATERIAS);
								$queryA = str_replace("VARIABLE=CAMPUS", $data["ACADEMIC_SESSION"], $queryA);
								$queryA = str_replace("VARIABLE=TIPOCERTIFICADO", $TC, $queryA);
								$queryA = str_replace("VARIABLE=ANHO", substr($data["fecha"], 0, 4), $queryA);
								$queryA = str_replace("VARIABLE=CURRICULUM", $CURRICULUM , $queryA);		
								$rQueryA = $obj->xQuery($queryA);
								$TRegA = $obj->xCQuery();
	
								while ($datac = sqlsrv_fetch_array($rQueryA)) {
  		                            echo"<tr>";
        	                        echo"    <td>".utf8_encode($datac["claveAsignatura"])."</td>";
            	                    echo"    <td>".utf8_encode($datac["nombre6"])."</td>";
                	                echo"    <td>".utf8_encode($datac["ciclo"])."</td>";
                    	            echo"    <td>".utf8_encode($datac["calificacion"])."</td>";
                        	        echo"    <td>".utf8_encode($datac["observaciones"])."</td>";
                                    echo"    <td>".utf8_encode($datac["ID_TIPO_ASIGNATURA"])."</td>";
                                    echo"    <td>".utf8_encode($datac["TIPO_ASIGNATURA"])."</td>";
                                    echo"    <td>".utf8_encode($datac["CREDITOS"])."</td>";
                            	    echo"</tr>";
								}
								
	                            echo"														
                                <tr></tr>
                            </tbody>
                        </table>
                    </div>
            </div>
            <div id='DOCUMENTOS' class='container tab-pane fade'><br>
                <h4>Documentos Requeridos</h4>
                <table id='dtDynamicVerticalScrollExample' class='main_files table table-rounded table-striped'
                  cellspacing='0' width='100%' style='text-align:center;'>
                  <thead>
                        <tr class='bg-primary rounded-top text-white' style='text-align:center; background-color:#0054a4;'>
                            <th>Tipo de Documento</th>
                            <th>Acci&oacute;n</th>
                        </tr>
                    </thead>
                    <tbody>";
                        $LoadedFiles = count(glob('./XDoc/'.trim($vNumControl).'/{*.pdf}',GLOB_BRACE));
                        echo "<input type='hidden' id='LoadedFiles' value='".$LoadedFiles."'>";
                        for($i=1; $i<=7; $i++){
                            $CountthisFile[1]   = 1;
                            $GrantedFile[1]     = false;
                            $thisFileName[1]    = "<span style='color:#ff0000; float:left; margin-top:-5px;'><h3>*</h3></span> Acta de Nacimiento";
                            $thisDivName[1]     = "Acta";

                            $CountthisFile[2]   = 1;
                            $GrantedFile[2]     = false;
                            $thisFileName[2]    = "<span style='color:#ff0000; float:left; margin-top:-5px;'><h3>*</h3></span> Historial Académico";
                            $thisDivName[2]     = "HAcademico";

                            $CountthisFile[3]   = 1;
                            $GrantedFile[3]     = false;
                            $thisFileName[3]    = "<span style='color:#ff0000; float:left; margin-top:-5px;'><h3>*</h3></span> Certificado Antecedente";
                            $thisDivName[3]     = "CertificadoA";

                            $CountthisFile[4]   = 1;
                            $GrantedFile[4]     = false;
                            $thisFileName[4]    = "<span style='color:#ff0000; float:left; margin-top:-5px;'><h3>*</h3></span> Oficio de Validación Antecedente Académico";
                            $thisDivName[4]     = "OfiValidacion";

                            $CountthisFile[5]   = 1;
                            $GrantedFile[5]     = false;
                            $thisFileName[5]    = "<span style='color:#ff0000; float:left; margin-top:-5px;'><h3>*</h3></span> CURP 200%";
                            $thisDivName[5]     = "CURP";

                            $CountthisFile[6]   = 0;
                            $GrantedFile[6]     = true;
                            $thisFileName[6]    = "Equivalencia o Revalidación Estudios (Opcional)";
                            $thisDivName[6]     = "Equivalencia";

                            $CountthisFile[7]   = 0;
                            $GrantedFile[7]     = true;
                            $thisFileName[7]    = "Carta Compromiso (Opcional)";
                            $thisDivName[7]     = "CartaCompromiso";

                            echo "<script>";
                            echo "    $('#upfile".$i."').click(function(){";
                            echo "        $('#file".$i."').trigger('click');";
                            echo "    });";
                            echo "</script>";

                            $DisplayButtons[$i] = "none";
                            if($CountthisFile[$i] == 1){
                                $thisPath = "./XDoc/".trim($vNumControl);
                            }else{
                                $thisPath = "./XDoc/".trim($vNumControl)."/opcionales/";
                            }

                            if(!file_exists($thisPath)){mkdir($thisPath, 0777, true);}                            
                            if(file_exists($thisPath)){
                                $directorio = opendir($thisPath);
                                $ThisFile = $obj->getDbRowName("NombreDocumento", "Info_Documentos", "WHERE FolioControl = '".$FolioCont."' AND TipoDocumento = '".$thisDivName[$i]."'", 1);
                                if($ThisFile === "error"){
                                    $ThisFile = $thisDivName[$i]."_".$vNumControl.".pdf";
                                    while($archivo = readdir($directorio)){                   
                                        if(!is_dir($archivo)){
                                            $pos = strpos($archivo, '_');
                                            $fcad = substr($archivo, 0, $pos);
                                            if($fcad === $thisDivName[$i]){
                                                $DisplayButtons[$i] = "inline";
                                                $GrantedFile[$i]    = true;
                                            }
                                        }
                                    }
                                }else{
                                    $DisplayButtons[$i] = "inline";
                                    $GrantedFile[$i]    = true;
                                }
                            }
                            
                            if($GrantedFile[$i] == false){$ValidateGranted = false;}
                            if($DisplayButtons[$i] === "inline"){$DisplayUpload[$i] = "none";}else{$DisplayUpload[$i] = "inline";}
                            
                            if($ValidateGranted){
                                echo "<script>";
                                echo "  $('#ValidaAlumno').attr('disabled', false);";
                                echo "</script>";
                            }else{
                                echo "<script>";
                                echo "  $('#ValidaAlumno').attr('disabled', true);";
                                echo "</script>"; 
                            }

                            echo "<tr>";
                            echo "  <td id='FileType' style='text-align:left;'><b>".$thisFileName[$i]."</b></td>";
                            echo "  <td style='text-align:center;'>";
                            echo "      <input type='file' id='file".$i."' name='file".$i."' style='display:none' title='".$thisDivName[$i]."' pathfile='".$thisPath."' onchange='upfle(this, ".$CountthisFile[$i].")' accept='application/pdf'/>";
                            echo "      <div id='Uploadfile".$i."' class='UploadNewFile' style='display:".$DisplayUpload[$i].";'><span id='upfile".$i."' class='btn btn-success' style='padding:5px;'><i class='fa fa-upload'></i> Cargar</span></div>";
                            echo "      <div id='Buttonsfile".$i."' style='display:".$DisplayButtons[$i].";'>";
                            echo "          <a onclick=window.open('".$thisPath."/".$ThisFile."');><span id='viewfile".$i."' class='btn btn-info' style='padding:5px;'><i class='fa fa-eye'></i> Ver Archivo</span></a><a id='".$i."' file='".$thisPath."/".$ThisFile."' tip='reqfile' tipoDoc='".$thisDivName[$i]."' fileFolio='".$FolioCont."' fileCarrer='NA' onclick='deleteThisFile(this, ".$CountthisFile[$i].")'><span id='delete".$i."' class='btn btn-danger UploadNewFile' style='padding:5px;'><i class='fa fa-times'></i> Eliminar</span></a>";
                            echo "      </div>";
                            echo "  </td>";
                            echo "</tr>";
                        }
                    echo "
                    </tbody>
                </table>
                <h4 style='margin-top:20px;'>Otros Documentos</h4>
                <table id='dtDynamicVerticalScrollExample' class='other_files table table-rounded table-striped' cellspacing='0' width='100%' style='text-align:center; background-color:#fff;'>
                    <thead>
                        <tr class='bg-primary rounded-top text-white'style='text-align:center; background-color:#0054a4;'>
                            <th>Tipo de Documento</th>
                            <th>Acci&oacute;n</th>
                        </tr>
                    </thead>
                    <tbody>";
                        $OthersFilesPath = "./XDoc/".trim($vNumControl)."/otros/";
                        if(!file_exists($OthersFilesPath)){mkdir($OthersFilesPath, 0777, true);}
                        echo "<script>";
                        echo "    $('#UploadOthers').click(function(){";
                        echo "        $('#OtherFiles').trigger('click');";
                        echo "    });";
                        echo "</script>"; 
                        echo "<tr>";
                        echo "  <td id='FileType' style='text-align:center;'><b>Otros Documentos</b></td>";
                        echo "  <td style='text-align:center;'>";
                        echo "      <input id='OtherFiles' type='file' name='OtherFiles[]' multiple='multiple' pathfile='".$OthersFilesPath."' onchange='mfile(this)' style='display:none' accept='application/pdf'/>";
                        echo "      <div id='UploadOthers' class='UploadNewFile' style=''><span id='upOtherFile' class='btn btn-success' style='padding:5px;'><i class='fa fa-upload'></i> Cargar Archivos</span></div>";
                        echo "  </td>";
                        echo "</tr>";
                        if(file_exists($OthersFilesPath)){
                            $TotalFiles = 0;
                            $directorio = opendir($OthersFilesPath);
                            while($archivo = readdir($directorio)){
                                if(!is_dir($archivo)){
                                    $TotalFiles = $TotalFiles + 1;
                                    echo "<tr id='file_".$TotalFiles."'><td id='FileType' style='text-align:center;'>Archivo Cargado</td><td style='text-align:center;'><div id='ButtonsFile".$archivo."' style='text-align:center;'><a onclick=window.open('".$OthersFilesPath."/".$archivo."');><span id='ViewFile".$archivo."' class='btn btn-info' style='padding:5px;'><i class='fa fa-eye'></i> Ver Archivo</span></a><a id='".$TotalFiles."' file='".$OthersFilesPath."/".$archivo."' tip='oldfile' onclick='deleteThisFile(this, 0);'><span id='deleteFile' class='btn btn-danger UploadNewFile' style='padding:5px;'><i class='fa fa-times'></i> Eliminar</span></a></div></td></tr>";
                                }
                            }
                        }
                    echo "
                    </tbody>
                </table>
             </div> 
            <div id='RAZON' class='container tab-pane fade'><br>
                <div class='row'>               
                    <div class='col-sm-3'><span style='color:#ff0000; float:left;'><h2>*</h2></span><p class='navbar-text'>Raz&oacute;n de Rechazo</p></div>
                    <div class='col-sm-6'>
                        <textarea name='razon_rechazo' id='razon_rechazo' rows='5' cols='45' class='form-control'>".$DeniedReason."</textarea>
                    </div>
                </div>
            </div>
            <b>Notas:</b>
            <ol>
              <li>Todos los campos marcados con: <span class='text-danger' style='size:15px;'><b>*</b></span> son requeridos</li>
              <li>Para <span class='text-success'><b>VALIDAR</b></span> Un Alumno es necesario llenar todos los campos requeridos y cargar todos los documentos necesarios</li>
              <li>Para <span class='text-danger'><b>RECHAZAR</b></span> Un Alumno es necesario ingresar la raz&oacute;n por la cual ha sido rechazado</li>
            </ol>
            <div id='ValButtons' style='width:100%; text-align:center;'>";
            switch($_SESSION["rol"]){
                case 2:
                    switch($ValidateStatus){
                        case '':
                        case 'Prospecto':
                            echo "<button type='button' id='ValidaAlumno' class='btn btn-success ValidaAlumno' status='validate' style='padding:5px;'>";
                            echo "    <i class='fa fa-check'></i>";
                            echo "    <span>Validar</span>";
                            echo "</button>";
                            echo "<button type='button' id='RechazaAlumno' class='btn btn-danger ValidaAlumno' status='rejected' style='padding:5px;'>";
                            echo "    <i class='fa fa-check'></i>";
                            echo "    <span>Rechazar</span>";
                            echo "</button>";
                        break;
                        case 'Validado':
                            echo "<button type='button' id='RechazaAlumno' class='btn btn-danger ValidaAlumno' status='rejected' style='padding:5px;'>";
                            echo "    <i class='fa fa-check'></i>";
                            echo "    <span>Rechazar</span>";
                            echo "</button>";
                            echo "<script>";
                            echo "  $(document).ready(function(){";
                            echo "      $('.UploadNewFile').hide();";
                            echo "  })";
                            echo "</script>";
                        break;
                        case 'Gen.XML':
                        case 'XML timbrado':
                        case 'XML autenticado':
                        case 'Validado por CSE':
                            echo "<script>";
                            echo "  $(document).ready(function(){";
                            echo "      $('.UploadNewFile').hide();";
                            echo "  })";
                            echo "</script>";
                        break;
                        case 'Rechazado':
                        case 'Rechazado por CSE':
                        case 'Rechazado por plantel':
                            echo "<button type='button' id='ValidaAlumno' class='btn btn-success ValidaAlumno' status='validate' style='padding:5px;'>";
                            echo "    <i class='fa fa-check'></i>";
                            echo "    <span>Validar</span>";
                            echo "</button>";
                        break;
                    }
                break;
                default:
                    switch($ValidateStatus){
                        case '':
                        case 'Prospecto':
                            echo "<button type='button' id='ValidaAlumno' class='btn btn-success ValidaAlumno' status='validate' style='padding:5px;'>";
                            echo "    <i class='fa fa-check'></i>";
                            echo "    <span>Validar</span>";
                            echo "</button>";
                            echo "<button type='button' id='RechazaAlumno' class='btn btn-danger ValidaAlumno' status='rejected' style='padding:5px;'>";
                            echo "    <i class='fa fa-check'></i>";
                            echo "    <span>Rechazar</span>";
                            echo "</button>";
                        break;
                        case 'XML timbrado':
                        case 'XML autenticado':
                            echo "<script>";
                            echo "  $(document).ready(function(){";
                            echo "      $('.UploadNewFile').hide();";
                            echo "  })";
                            echo "</script>";
                        break;
                        case 'Validado':
                            echo "<button type='button' id='RechazaAlumno' class='btn btn-danger ValidaAlumno' status='rejected' style='padding:5px;'>";
                            echo "    <i class='fa fa-check'></i>";
                            echo "    <span>Rechazar</span>";
                            echo "</button>";
                            echo "<script>";
                            echo "  $(document).ready(function(){";
                            echo "      $('.UploadNewFile').hide();";
                            echo "  })";
                            echo "</script>";
                        break;
                        case 'Gen.XML':
                        case 'Validado por CSE':
                            echo "<button type='button' id='RechazaAlumno' class='btn btn-danger ValidaAlumno' status='rejected' style='padding:5px;'>";
                            echo "    <i class='fa fa-check'></i>";
                            echo "    <span>Rechazar</span>";
                            echo "</button>";
                            echo "<script>";
                            echo "  $(document).ready(function(){";
                            echo "      $('.UploadNewFile').hide();";
                            echo "  })";
                            echo "</script>";
                        break;
                        case 'Rechazado':
                        case 'Rechazado por CSE':
                        case 'Rechazado por plantel':
                            echo "<button type='button' id='ValidaAlumno' class='btn btn-success ValidaAlumno' status='validate' style='padding:5px;'>";
                            echo "    <i class='fa fa-check'></i>";
                            echo "    <span>Validar</span>";
                            echo "</button>";
                        break;
                    }
                break;
            }
            echo "</div> ";
	}
?>